package com.mirror.tk.security.api;

import java.io.Serializable;

/**
 * 登陆主体对象
 */
public class SubjectInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 加密后的密码 */
    private String password;

    /** 加密盐值*/
    private String pwdSalt;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPwdSalt() {
        return pwdSalt;
    }

    public void setPwdSalt(String pwdSalt) {
        this.pwdSalt = pwdSalt;
    }
    
}
