package com.mirror.tk.security.exception;

/**
 * 未知账号错误
 */
public class UnknownAccountException extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	public UnknownAccountException(Throwable cause) {
		super(cause);
	}

}
