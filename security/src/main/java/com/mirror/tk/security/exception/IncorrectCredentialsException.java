package com.mirror.tk.security.exception;

/**
 * 密码错误
 */
public class IncorrectCredentialsException extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	public IncorrectCredentialsException(Throwable cause) {
		super(cause);
	}

}
