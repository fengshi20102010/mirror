package com.mirror.tk.security.exception;

/**
 * 验证码错误
 */
public class InvaildCaptchaException extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	public InvaildCaptchaException() {
		super();
	}

	public InvaildCaptchaException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvaildCaptchaException(String message) {
		super(message);
	}

	public InvaildCaptchaException(Throwable cause) {
		super(cause);
	}

}

