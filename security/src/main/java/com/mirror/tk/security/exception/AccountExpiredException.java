package com.mirror.tk.security.exception;

/**
 * 账户过期
 */
public class AccountExpiredException extends AuthenticationException {

	private static final long serialVersionUID = 1L;

}