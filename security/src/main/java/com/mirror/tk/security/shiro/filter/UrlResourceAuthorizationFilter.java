package com.mirror.tk.security.shiro.filter;

import java.io.PrintWriter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;

import com.mirror.tk.framework.common.web.support.JsonResult;
import com.mirror.tk.framework.utils.mapper.JsonMapper;

public class UrlResourceAuthorizationFilter extends AuthorizationFilter {

    private Logger logger = LogManager.getLogger();

    private String ajaxNotAuthCode = "TK_SECURITY_NOT_AUTH";

    private String ajaxNotAuthMsg = "对不起您没有操作权限";


    /**
     * 获取访问的url并进行权限认证
     * @param request
     * @param response
     * @param mappedValue
     * @return
     * @throws Exception
     */
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        Subject subject = getSubject(request, response);
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        // 获取页面url
        String permission = httpRequest.getRequestURI();
        boolean isPermitted = subject.isPermitted(permission);
        logger.trace("url {} is permitted {}", permission, isPermitted);
        return isPermitted;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        //没有权限的处理
        //如果为ajax返回json否则跳转无权限页面
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        if ((httpRequest.getHeader("x-requested-with") != null && httpRequest.getHeader("x-requested-with").equalsIgnoreCase("XMLHttpRequest")) || (httpRequest.getRequestURI()!=null && httpRequest.getRequestURI().indexOf("ajax")!=-1)) {
            PrintWriter printWriter = response.getWriter();
            printWriter.write(JsonMapper.nonDefaultMapper().toJson(new JsonResult(ajaxNotAuthCode, ajaxNotAuthMsg)));
            printWriter.flush();
            printWriter.close();
            return false;
        }
        return super.onAccessDenied(request, response, mappedValue);
    }

    public String getAjaxNotAuthCode() {
        return ajaxNotAuthCode;
    }

    public void setAjaxNotAuthCode(String ajaxNotAuthCode) {
        this.ajaxNotAuthCode = ajaxNotAuthCode;
    }

    public String getAjaxNotAuthMsg() {
        return ajaxNotAuthMsg;
    }

    public void setAjaxNotAuthMsg(String ajaxNotAuthMsg) {
        this.ajaxNotAuthMsg = ajaxNotAuthMsg;
    }
    
}
