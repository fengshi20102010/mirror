package com.mirror.tk.security.shiro.filter;

import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.UserFilter;

import com.mirror.tk.security.listener.LoginLogoutListener;

/**
 * 记住我登陆的回调过滤器
 */
public class RememberMeUserFilter extends UserFilter {

	private Logger logger = LogManager.getLogger();
	
    //记住我登陆回调的session标识， 确保只回调一次
    public final static String TK_SECURITY_REMEMBER_ME_LOGIN="tk_security_remember_me_login";

    private List<LoginLogoutListener> listeners;

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
    	
        Subject subject = getSubject(request, response);

        if(subject.isRemembered() && subject.getSession().getAttribute(TK_SECURITY_REMEMBER_ME_LOGIN) == null) {
            logger.debug("principal {}, call listeners after remember me", subject.getPrincipals());
            //设置自动登陆回调
            if(listeners!=null) {
                for (LoginLogoutListener listener : listeners) {
                    try {
                        listener.afterRememberMe(subject.getPrincipal().toString(), (HttpServletRequest) request, (HttpServletResponse) response);
                    } catch (Exception ex) {
                        logger.error("listener callback after remember me error", ex);
                    }
                }
            }
            subject.getSession().setAttribute(TK_SECURITY_REMEMBER_ME_LOGIN, TK_SECURITY_REMEMBER_ME_LOGIN);
        }
        return super.isAccessAllowed(request, response, mappedValue);
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        return super.onAccessDenied(request, response);
    }

    public List<LoginLogoutListener> getListeners() {
        return listeners;
    }

    public void setListeners(List<LoginLogoutListener> listeners) {
        this.listeners = listeners;
    }
    
}
