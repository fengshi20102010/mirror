package com.mirror.tk.api.common.support;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.mirror.tk.core.module.user.domain.User;

public class JwtUser {

	public static final String JWT_USER_OBJECT_KEY = "jwt_user_obj";

	private JwtUser() {
	}

	public static JwtUser bulider(User user) {
		return new JwtUser().update(user);
	}

	public JwtUser update(User user) {
		this.setId(user.getId());
		this.setUsername(user.getUsername());
		this.setPhone(user.getPhone());
		this.setQq(user.getQq());
		this.setWechat(user.getWechat());
		this.setStatus(user.getStatus());
		this.setAge(user.getAge());
		this.setEmail(user.getEmail());
		this.setScore(user.getScore());
		this.setSex(user.getSex());
		return this;
	}

	/** id */
	private Long id;

	/** 用户名 */
	private String username;

	/** 电话 */
	private String phone;

	/** QQ */
	private String qq;

	/** 微信 */
	private String wechat;

	/** 用户状态 */
	private Integer status;
	
	/** 邮箱 */
	private String email;
	
	/** 性别 */
	private Integer sex;
	
	/** 年龄 */
	private Integer age;
	
	/** 余额 */
	private Long score;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Long getScore() {
		return score;
	}

	public void setScore(Long score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
