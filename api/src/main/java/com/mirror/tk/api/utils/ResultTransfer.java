package com.mirror.tk.api.utils;

import com.mirror.tk.core.biz.common.Result;
import com.mirror.tk.framework.common.web.support.JsonListResult;
import com.mirror.tk.framework.common.web.support.JsonResult;

public class ResultTransfer {

	public  static boolean transfer(JsonResult json ,Result result) {
		json.setCode(result.getCode());
		json.setMessage(result.getDescription());
		json.setSuccess(result.isSuccess());
		return !result.isSuccess();
	}
	
	public static boolean transferList(JsonListResult json,Result result) {
		json.setCode(result.getCode());
		json.setMessage(result.getDescription());
		json.setSuccess(result.isSuccess());
		return !result.isSuccess();
	}
}
