package com.mirror.tk.api.controller.cms;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mirror.tk.api.common.Constant;
import com.mirror.tk.api.controller.CommonController;
import com.mirror.tk.core.log.annotation.Log;
import com.mirror.tk.core.module.cms.domain.AdvertConfig;
import com.mirror.tk.core.module.cms.domain.ComplaintAdvice;
import com.mirror.tk.core.module.cms.domain.Faq;
import com.mirror.tk.core.module.cms.domain.Notice;
import com.mirror.tk.core.module.cms.service.AdvertConfigService;
import com.mirror.tk.core.module.cms.service.ComplaintAdviceService;
import com.mirror.tk.core.module.cms.service.FaqService;
import com.mirror.tk.core.module.cms.service.NoticeService;
import com.mirror.tk.framework.common.exception.BusinessException;
import com.mirror.tk.framework.common.web.support.JsonListResult;
import com.mirror.tk.framework.common.web.support.JsonResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "CMS api", description = "提供CMS相关接口")
@RestController
@RequestMapping("cms")
public class CmsController extends CommonController {

	@Resource
	private AdvertConfigService advertConfigService;
	@Resource
	private NoticeService noticeService;
	@Resource
	private FaqService faqService;
	@Resource
	private ComplaintAdviceService complaintAdviceService;
	
	@ApiOperation(value = "获取广告轮播列表", notes="广告类型：{1：首页，2：热销，3：预告}")
	@Log(module="CMS", moduleName="内容管理", action="getAdvertConfigByType", actionName="获取广告轮播信息", tag="api")
	@RequestMapping(value = "advert", method = RequestMethod.GET)
	public JsonListResult<AdvertConfig> getAdvertConfigByType(HttpServletRequest request, 
			@ApiParam(value = "广告类型：{1：首页，2：热销，3：预告}", required = true)@RequestParam("type")Integer type){
		JsonListResult<AdvertConfig> result = new JsonListResult<AdvertConfig>();
		List<AdvertConfig> list = advertConfigService.findByType(type);
		result.setRows(list);
		result.setTotal(Long.valueOf(list.size()));
		result.setCode(Constant.Global.SUCCESS);
		result.setSuccess(true);
		return result;
	}
	
	@ApiOperation(value = "获取通知公告列表", notes="通知公告类型：{1：普通，2：推荐，3：教程}")
	@Log(module="CMS", moduleName="内容管理", action="getNoticeByType", actionName="获取通知公告列表", tag="api")
	@RequestMapping(value = "notice", method = RequestMethod.GET)
	public JsonListResult<Notice> getNoticeByType(HttpServletRequest request, 
			@ApiParam(value = "通知公告类型：{1：普通，2：推荐，3：教程}", required = true)@RequestParam("type")Integer type){
		JsonListResult<Notice> result = new JsonListResult<Notice>();
		List<Notice> list = noticeService.findByType(type);
		result.setRows(list);
		result.setTotal(Long.valueOf(list.size()));
		result.setCode(Constant.Global.SUCCESS);
		result.setSuccess(true);
		return result;
	}
	
	@ApiOperation(value = "获取faq列表")
	@Log(module="CMS", moduleName="内容管理", action="getFaq", actionName="获取faq列表", tag="api")
	@RequestMapping(value = "faq", method = RequestMethod.GET)
	public JsonListResult<Faq> getFaq(HttpServletRequest request){
		JsonListResult<Faq> result = new JsonListResult<Faq>();
		List<Faq> list = faqService.findFaq();
		result.setRows(list);
		result.setTotal(Long.valueOf(list.size()));
		result.setCode(Constant.Global.SUCCESS);
		result.setSuccess(true);
		return result;
	}
	
	@ApiOperation(value = "新增投诉建议", notes = "类型：{1:投诉,2:建议,3:其他}")
	@Log(module="CMS", moduleName="内容管理", action="addAdvice", actionName="新增投诉建议", tag="api")
	@RequestMapping(value = "advice", method = RequestMethod.POST)
	public JsonResult addAdvice(HttpServletRequest request,
			@ApiParam(value = "姓名", required = true)@RequestParam("name")String name,
			@ApiParam(value = "电话", required = true)@RequestParam("phone")String phone,
			@ApiParam(value = "邮箱", required = true)@RequestParam("email")String email,
			@ApiParam(value = "标题", required = true)@RequestParam("title")String title,
			@ApiParam(value = "内容", required = true)@RequestParam("content")String content,
			@ApiParam(value = "类型：{1:投诉,2:建议,3:其他}", required = true)@RequestParam("type")Integer type){
		JsonResult result = new JsonResult();
		try {
			ComplaintAdvice advice = new ComplaintAdvice();
			advice.setName(name);
			advice.setPhone(phone);
			advice.setEmail(email);
			advice.setTitle(title);
			advice.setContent(content);
			advice.setType(type);
			advice.setCreateTime(new Date());
			advice.setUpdateTime(new Date());
			advice.setStatus(ComplaintAdvice.STATUS_CHECK_WAIT);
			complaintAdviceService.save(advice);
		} catch (BusinessException e) {
			result.setCode(Constant.Global.FAIL);
			result.setSuccess(false);
			return result;
		}
		result.setCode(Constant.Global.SUCCESS);
		result.setSuccess(true);
		return result;
	}
	
}
