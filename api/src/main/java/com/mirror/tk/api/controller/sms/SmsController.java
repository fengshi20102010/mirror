package com.mirror.tk.api.controller.sms;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mirror.tk.api.common.Constant;
import com.mirror.tk.api.controller.CommonController;
import com.mirror.tk.core.log.annotation.Log;
import com.mirror.tk.core.module.sms.service.ValidateCodeService;
import com.mirror.tk.core.sms.service.SendTemplateSmsService.TEMPLATE_CODE;
import com.mirror.tk.framework.common.web.support.JsonEntityResult;
import com.mirror.tk.framework.common.web.support.JsonResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "短信验证码接口", description = "提供短信验证码相关操作")
@RestController
public class SmsController extends CommonController {

	@Resource
	private ValidateCodeService validateCodeService;
	
	private int expSeconds = 60 * 5;
	
	@ApiOperation(value = "发送短信", notes = "发送短信")
	@Log(action = "send", actionName = "发送短信", module = "Sms", moduleName = "短信", tag = "api")
	@RequestMapping(value = "send", method = RequestMethod.POST)
	public JsonResult send(HttpServletRequest request, 
			@ApiParam(value = "验证码类型：1：注册，2：找回密码", required = true)@RequestParam(value = "type", required = true)Integer type,
			@ApiParam(value = "电话号码（不会验证电话号码）", required = true)@RequestParam(value = "phone", required = true)String phone){
		JsonResult result = new JsonResult();
		try {
			validateCodeService.builderCode(phone, TEMPLATE_CODE.getCode(type), expSeconds).sendSms(phone, TEMPLATE_CODE.getCode(type));;
			result.setSuccess(true);
			result.setCode(Constant.Global.SUCCESS);
			result.setMessage(Constant.SmsMassage.SMS_SEND_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			result.setCode(Constant.Global.FAIL);
			result.setMessage(Constant.SmsMassage.SMS_SEND_FAILE);
		}
		return result;
	}
	
	@ApiOperation(value = "验证验证码", notes = "验证验证码，类型{1：注册，2：找回密码}")
	@Log(action = "validate", actionName = "验证验证码", module = "Sms", moduleName = "短信", tag = "api")
	@RequestMapping(value = "validate", method = RequestMethod.POST)
	public JsonEntityResult<Boolean> validate(HttpServletRequest request, 
			@ApiParam(value = "验证码", required = true)@RequestParam(value = "code", required = true)String code,
			@ApiParam(value = "短信类型：1：注册，2：找回密码", required = true)@RequestParam(value = "type", required = true)Integer type,
			@ApiParam(value = "电话号码", required = true)@RequestParam(value = "phone", required = true)String phone){
		JsonEntityResult<Boolean> result = new JsonEntityResult<Boolean>();
		result.setSuccess(true);
		result.setEntity(validateCodeService.vaildateCode(phone, TEMPLATE_CODE.getCode(type), code, false));
		result.setCode(Constant.Global.SUCCESS);
		return result;
	}
	
}
