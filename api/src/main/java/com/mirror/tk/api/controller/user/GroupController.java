package com.mirror.tk.api.controller.user;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mirror.tk.api.common.Constant;
import com.mirror.tk.api.common.support.JwtFace;
import com.mirror.tk.api.controller.CommonController;
import com.mirror.tk.core.log.annotation.Log;
import com.mirror.tk.core.module.user.domain.UserGroup;
import com.mirror.tk.core.module.user.domain.UserGroupDetail;
import com.mirror.tk.core.module.user.service.UserGroupDetailService;
import com.mirror.tk.core.module.user.service.UserGroupService;
import com.mirror.tk.framework.common.exception.BusinessException;
import com.mirror.tk.framework.common.web.support.JsonEntityResult;
import com.mirror.tk.framework.common.web.support.JsonListResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "QQ/微信群设置api", description = "提供QQ/微信群设置相关接口")
@RestController
@RequestMapping("my")
public class GroupController extends CommonController {

	@Resource
	UserGroupService userGroupService;
	@Resource
	UserGroupDetailService userGroupDetailService;
	
	@ApiOperation(value = "获取QQ/微信群列表", notes = "获取用户QQ/微信群列表")
	@Log(module="Group", moduleName="QQ/微信群设置", action="getGroups", actionName="获取QQ/微信群列表", tag="api")
	@RequestMapping(value = "group", method = RequestMethod.GET)
	public JsonListResult<UserGroup> getGroups(HttpServletRequest request, 
			@ApiParam(value = "类型：{1：QQ，2：微信}", required = true)@RequestParam(value = "type") Integer type){
		JsonListResult<UserGroup> result = new JsonListResult<UserGroup>();
		result.setSuccess(true);
		result.setCode(Constant.Global.SUCCESS);
		List<UserGroup> list = userGroupService.getByUserIdAndType(JwtFace.getJwtUser(request).getId(), type);
		result.setRows(list);
		result.setTotal(null != list ? list.size() : 0l);
		return result;
	}
	
	@ApiOperation(value = "新增QQ/微信群", notes = "新增用户QQ/微信群")
	@Log(module="Group", moduleName="QQ/微信群设置", action="saveGroups", actionName="新增QQ/微信群", tag="api")
	@RequestMapping(value = "group", method = RequestMethod.POST)
	public JsonEntityResult<UserGroup> saveGroups(HttpServletRequest request, 
			@ApiParam(value = "类型：{1：QQ，2：微信}", required = true)@RequestParam(value = "type") Integer type,
			@ApiParam(value = "群名称", required = true)@RequestParam(value = "name") String name){
		JsonEntityResult<UserGroup> result = new JsonEntityResult<UserGroup>();
		UserGroup group = new UserGroup();
		group.setName(name);
		group.setType(type);
		group.setCreateTime(new Date());
		group.setUserId(JwtFace.getJwtUser(request).getId());
		try {
			userGroupService.save(group);
			result.setEntity(group);
			result.setSuccess(true);
			result.setCode(Constant.Global.SUCCESS);
		} catch (BusinessException e) {
			logger.error("save group fail! err:{}", e.getMessage());
			result.setCode(Constant.Global.FAIL);
		}
		return result;
	}
	
	@ApiOperation(value = "新增QQ/微信群详情", notes = "修改用户QQ/微信群详情")
	@Log(module="Group", moduleName="QQ/微信群设置", action="addDetail", actionName="新增QQ/微信群", tag="api")
	@RequestMapping(value = "group/{id}/detail", method = RequestMethod.POST)
	public JsonEntityResult<UserGroup> addDetail(HttpServletRequest request, 
			@ApiParam(value = "群组id", required = true)@PathVariable Long id, 
			@ApiParam(value = "群组详情json对象", required = true)@RequestBody List<UserGroupDetail> list){
		JsonEntityResult<UserGroup> result = new JsonEntityResult<UserGroup>();
		userGroupDetailService.saveByGroupId(id, list);
		result.setEntity(userGroupService.get(id));
		result.setSuccess(true);
		result.setCode(Constant.Global.SUCCESS);
		return result;
	}
	
	@ApiOperation(value = "删除QQ/微信群详情", notes = "删除QQ/微信群详情")
	@Log(module="Group", moduleName="QQ/微信群设置", action="deleteDetail", actionName="删除QQ/微信群详情", tag="api")
	@RequestMapping(value = "group/{id}/detail", method = RequestMethod.DELETE)
	public JsonEntityResult<Boolean> deleteDetail(HttpServletRequest request, 
			@ApiParam(value = "群组id", required = true)@PathVariable Long id,
			@ApiParam(value = "群组详情id", required = true)@RequestBody List<Long> list){
		JsonEntityResult<Boolean> result = new JsonEntityResult<Boolean>(userGroupDetailService.deleteByGroupIdAndIds(id, list));
		result.setCode(Constant.Global.SUCCESS);
		result.setSuccess(true);
		return result;
	}
	
	@ApiOperation(value = "删除QQ/微信群", notes = "删除用户QQ/微信群")
	@Log(module="Group", moduleName="QQ/微信群设置", action="deleteGroups", actionName="删除QQ/微信群", tag="api")
	@RequestMapping(value = "group/{id}", method = RequestMethod.DELETE)
	public JsonEntityResult<Boolean> deleteGroups(HttpServletRequest request, 
			@ApiParam(value = "群组id", required = true)@PathVariable Long id){
		JsonEntityResult<Boolean> result = new JsonEntityResult<Boolean>();
		result.setEntity(userGroupService.deleteByIdAndUserId(id, JwtFace.getJwtUser(request).getId()));
		result.setCode(Constant.Global.SUCCESS);
		result.setSuccess(true);
		return result;
	}
	
}
