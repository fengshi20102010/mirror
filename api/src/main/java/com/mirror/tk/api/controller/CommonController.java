package com.mirror.tk.api.controller;

import java.beans.PropertyEditorSupport;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import com.mirror.tk.api.common.Constant;
import com.mirror.tk.framework.common.dao.support.PageInfo;
import com.mirror.tk.framework.common.web.support.JsonListResult;

public class CommonController {

	protected Logger logger = LogManager.getLogger(this.getClass());

	/**
	 * 自定义string类型转换，防止XSS攻击
	 * 
	 * @param request
	 * @param binder
	 */
//	@InitBinder
//	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
//		// String类型转换，将所有传递进来的String进行HTML编码，防止XSS攻击
//		binder.registerCustomEditor(String.class, new PropertyEditorSupport() {
//			@Override
//			public void setAsText(String text) {
//				setValue(text == null ? null : StringEscapeUtils.escapeHtml4(text.trim()));
//			}
//
//			@Override
//			public String getAsText() {
//				Object value = getValue();
//				return value != null ? value.toString() : "";
//			}
//		});
//	}

	/**
	 * 分页
	 * 
	 * @param request
	 * @param pageSize
	 * @return
	 */
	protected <T> PageInfo<T> getPageInfo(HttpServletRequest request, int pageSize) {
		int currentPage = 1;
		if (StringUtils.isNotBlank(request.getParameter("pageNo"))) {
			try {
				currentPage = Integer.parseInt(request.getParameter("pageNo"));
			} catch (Exception ex) {
				
			}
		}
		PageInfo<T> pageInfo = new PageInfo<T>(pageSize, currentPage);
		return pageInfo;
	}
	
	/**
	 * 分页
	 * 
	 * @param request
	 * @return
	 */
	protected <T> PageInfo<T> getPageInfo(HttpServletRequest request) {
		return getPageInfo(request, 10);
	}
	
	/**
	 * bootstrap-table分页转换
	 * 
	 * @param request
	 * @return
	 */
	protected <T> PageInfo<T> fromJsonListResult(HttpServletRequest request){
		int currentPage = 1;
		int pageSize = 10;
		if (StringUtils.isNotBlank(request.getParameter("pageNumber"))) {
			try {
				currentPage = Integer.parseInt(request.getParameter("pageNumber"));
			} catch (Exception ex) {
				
			}
		}
		if (StringUtils.isNotBlank(request.getParameter("pageSize"))) {
			try {
				pageSize = Integer.parseInt(request.getParameter("pageSize"));
			} catch (Exception ex) {
				
			}
		}
		PageInfo<T> pageInfo = new PageInfo<T>(pageSize, currentPage);
		return pageInfo;
	}

	/**
	 * 将pageInfo转换为JsonListResult
	 * 
	 * @param pageInfo
	 * @return
	 */
	protected <T> JsonListResult<T> toJsonListResult(PageInfo<T> pageInfo) {
		JsonListResult<T> result = new JsonListResult<T>();
		if(pageInfo.getTotalCount() > 0){
			result.setCode(Constant.Global.SUCCESS);
		}
		result.setTotal(pageInfo.getTotalCount());
		result.setRows(pageInfo.getPageResults());
		return result;
	}
	
}
