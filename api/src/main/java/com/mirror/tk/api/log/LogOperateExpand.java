package com.mirror.tk.api.log;

import javax.servlet.http.HttpServletRequest;

import com.mirror.tk.api.common.support.JwtFace;
import com.mirror.tk.api.common.support.JwtUser;
import com.mirror.tk.core.log.aop.LogAdviceExpand;
import com.mirror.tk.core.module.sys.domain.SysOlog;

public class LogOperateExpand implements LogAdviceExpand {

    @Override
    public void expand(HttpServletRequest request, SysOlog olog) {
    	// api用户
        if(JwtFace.isUserLogin(request)){
        	JwtUser su = JwtFace.getJwtUser(request);
        	olog.setOperateUserId(su.getId());
        	olog.setOperateUser(su.getUsername() != null ?  su.getUsername() : su.getPhone());
        }
    }
    
}
