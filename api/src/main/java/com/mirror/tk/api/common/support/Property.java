package com.mirror.tk.api.common.support;

import org.springframework.beans.factory.annotation.Value;

//@Component
public class Property {

	@Value("${file.display.url}")
	private String httpUrl;

	public String getHttpUrl() {
		return httpUrl;
	}

	public void setHttpUrl(String httpUrl) {
		this.httpUrl = httpUrl;
	}

}
