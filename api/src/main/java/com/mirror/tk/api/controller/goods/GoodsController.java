package com.mirror.tk.api.controller.goods;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;
import com.mirror.tk.api.common.Constant;
import com.mirror.tk.api.common.support.JwtFace;
import com.mirror.tk.api.controller.CommonController;
import com.mirror.tk.api.utils.ResultTransfer;
import com.mirror.tk.core.biz.common.Result;
import com.mirror.tk.core.biz.dto.GoodsTypeDto;
import com.mirror.tk.core.biz.dto.ResGoodsDetail;
import com.mirror.tk.core.biz.dto.ResGoodsInfo;
import com.mirror.tk.core.biz.dto.ResGoodsSendLog;
import com.mirror.tk.core.biz.dto.ResSendTpl;
import com.mirror.tk.core.biz.dto.ResTaobaoGoods;
import com.mirror.tk.core.biz.dto.ResUserSendContent;
import com.mirror.tk.core.biz.req.ReqAddGoods;
import com.mirror.tk.core.biz.req.ReqGenUserSend;
import com.mirror.tk.core.biz.req.ReqGetSendLog;
import com.mirror.tk.core.biz.req.ReqGoodsInfo;
import com.mirror.tk.core.biz.req.ReqSaveUserTpl;
import com.mirror.tk.core.biz.req.ReqSendLog;
import com.mirror.tk.core.biz.req.ReqTransformUrl;
import com.mirror.tk.core.biz.service.GoodsBiz;
import com.mirror.tk.core.biz.service.TaobaoBiz;
import com.mirror.tk.core.log.annotation.Log;
import com.mirror.tk.core.module.goods.dto.GoodsNumDto;
import com.mirror.tk.core.module.goods.service.GoodsService;
import com.mirror.tk.framework.common.dao.support.PageInfo;
import com.mirror.tk.framework.common.web.support.JsonEntityResult;
import com.mirror.tk.framework.common.web.support.JsonListResult;
import com.mirror.tk.framework.common.web.support.JsonResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "网站商品API", description = "提供商品相关操作（该接口需要授权）")
@RestController
public class GoodsController extends CommonController {
	
	@Autowired
	GoodsBiz goodsBiz;
	
	@Autowired
	TaobaoBiz taobaoBiz;
	
	@Resource
	private GoodsService goodsService;
	
	@ApiOperation(value = "获取自己的商品类型数据", notes = "获取自己的商品类型数据")
	@Log(module="Goods", moduleName="商品", action="getMyType", actionName="获取自己的商品类型数据", tag="api")
	@RequestMapping(value = "my/type", method = RequestMethod.GET)
	public JsonListResult<GoodsNumDto> getMyType(HttpServletRequest request) {
		JsonListResult<GoodsNumDto> json = new JsonListResult<GoodsNumDto>();
		List<GoodsNumDto> list = goodsService.findByMyNum(JwtFace.getJwtUser(request).getId());
		json.setRows(list);
		json.setSuccess(true);
		json.setTotal(Long.valueOf(list.size()));
		return json;
	}
	
	@ApiOperation(value = "获取自己的商品列表", notes = "获取自己的商品列表")
	@Log(module="Goods", moduleName="商品", action="getMyGoods", actionName="获取自己的商品列表", tag="api")
	@RequestMapping(value = "my/goods", method = RequestMethod.GET)
	public JsonListResult<ResGoodsInfo> getMyGoods(HttpServletRequest request, 
			@ApiParam(value = "当前页", required = true)@RequestParam("currentPage")Integer currentPage,
			@ApiParam(value = "页面大小", required = true)@RequestParam("pageSize")Integer pageSize,
			@ApiParam(value = "状态{1:待完善,2:待审核,3:发布中,4:待结算,5:已下架}", required = true)@RequestParam("status")Integer status) {
		JsonListResult<ResGoodsInfo> json = new JsonListResult<ResGoodsInfo>();
		Result<PageInfo<ResGoodsInfo>> result = goodsBiz.getMyGoodsInfo(currentPage, pageSize, JwtFace.getJwtUser(request).getId(), status);
		if(ResultTransfer.transferList(json, result)) {
			return json;
		}
		PageInfo<ResGoodsInfo> page = result.getData();
		json.setTotal(page.getTotalCount());
		json.setRows(page.getPageResults());
		json.setSuccess(true);
		return json;
	}
	
	@ApiOperation(value = "添加商品", notes = "添加商品")
	@Log(module="Goods", moduleName="商品", action="addGoods", actionName="添加商品", tag="api")
	@RequestMapping(value = "my/goods", method = RequestMethod.POST)
	public JsonEntityResult<Long> addGoods(HttpServletRequest request,ReqAddGoods req) throws Exception{
		JsonEntityResult<Long> json = new JsonEntityResult<Long>();
		req.setUserId(JwtFace.getJwtUser(request).getId());
		Result<Long> result = goodsBiz.addGoods(req);
		if(ResultTransfer.transfer(json, result)) {
			return json;
		}
		json.setEntity(result.getData());
		return json;
	}
	
	@ApiOperation(value = "修改商品", notes = "修改商品")
	@Log(module="Goods", moduleName="商品", action="updateGoods", actionName="修改商品", tag="api")
	@RequestMapping(value = "my/goods", method = RequestMethod.PUT)
	public JsonResult updateGoods(HttpServletRequest request,ReqAddGoods req) throws Exception{
		JsonEntityResult<Long> json = new JsonEntityResult<Long>();
		req.setUserId(JwtFace.getJwtUser(request).getId());
		Result<Long> result = goodsBiz.updasteGoods(req);
		if(ResultTransfer.transfer(json, result)) {
			return json;
		}
		json.setEntity(result.getData());
		return json;
	}
	
	@ApiOperation(value = "删除自己的商品", notes = "传入需要删除的商品id列表，删除未发布和冻结的商品")
	@Log(module="Goods", moduleName="商品", action="deleteGoods", actionName="删除自己的商品", tag="api")
	@RequestMapping(value = "my/goods", method = RequestMethod.DELETE)
	public JsonResult deleteGoods(HttpServletRequest request, 
			@ApiParam(value = "商品id列表（淘宝的）", required = true)@RequestParam String ids){
		JsonResult result = new JsonResult(false);
		// 将智福川转化为list
		List<Long> idList = Lists.newArrayList();
		if(ids.indexOf(",") > 0){
			for (String id : ids.split(",")) {
				idList.add(Long.valueOf(id));
			}
		} else {
			idList.add(Long.valueOf(ids));
		}
		boolean flag = goodsService.deleteMyGoods(JwtFace.getJwtUser(request).getId(), idList);
		if(flag){
			result.setSuccess(true);
			result.setCode(Constant.Global.SUCCESS);
			return result;
		}
		result.setSuccess(false);
		result.setCode(Constant.Global.FAIL);
		return result;
	}
	
	@ApiOperation(value = "通过淘宝链接获取商品", notes = "通过淘宝链接获取商品")
	@Log(module="Goods", moduleName="商品", action="getFromUrl", actionName="通过淘宝链接获取商品", tag="api")
	@RequestMapping(value = "getgtb", method = RequestMethod.GET)
	public JsonEntityResult<ResTaobaoGoods> getFromUrl(HttpServletRequest request,@RequestParam("url") String url){
		JsonEntityResult<ResTaobaoGoods> json = new JsonEntityResult<ResTaobaoGoods>();
		Result<ResTaobaoGoods> result = taobaoBiz.fetchGoods(url);
		if(ResultTransfer.transfer(json, result)) {
			return json;
		}
		json.setEntity(result.getData());
		return json;
	}
	
	@ApiOperation(value = "获取所有类型商品数量", notes = "获取所有类型商品数量")
	@Log(module="Goods", moduleName="商品", action="getGoodsType", actionName="获取所有类型商品数量", tag="api")
	@RequestMapping(value = "getgt", method = RequestMethod.GET)
	public JsonListResult<GoodsTypeDto> getGoodsType(HttpServletRequest request) {
		JsonListResult<GoodsTypeDto> json = new JsonListResult<GoodsTypeDto>();
		Result<List<GoodsTypeDto>> result = goodsBiz.getAllGoodsType();
		if(ResultTransfer.transferList(json, result)) {
			return json;
		}
		json.setRows(result.getData());
		return json;
	}
	
	@ApiOperation(value = "获取分页商品信息", notes = "获取分页商品信息")
	@Log(module="Goods", moduleName="商品", action="getPagedGoods", actionName="获取分页商品信息", tag="api")
	@RequestMapping(value = "getpg", method = RequestMethod.GET)
	public JsonListResult<ResGoodsInfo> getPagedGoods(HttpServletRequest request,ReqGoodsInfo req) {
		JsonListResult<ResGoodsInfo> json = new JsonListResult<ResGoodsInfo>();
		Result<PageInfo<ResGoodsInfo>> result = goodsBiz.getGoodsInfo(req);
		if(ResultTransfer.transferList(json, result)) {
			return json;
		}
		PageInfo<ResGoodsInfo> page = result.getData();
		json.setRows(page.getPageResults());
		json.setTotal(page.getTotalCount());
		return json;
	}
	
	@ApiOperation(value = "获取商品详情", notes = "获取商品详情")
	@Log(module="Goods", moduleName="商品", action="getDetail", actionName="获取商品详情", tag="api")
	@RequestMapping(value = "getDetail", method = RequestMethod.POST)
	public JsonEntityResult<ResGoodsDetail> getDetail(HttpServletRequest request,
			@ApiParam(value="商品ID(淘宝ID，非本系统ID)",required=true) @RequestParam("id") Long id){
		JsonEntityResult<ResGoodsDetail> json = new JsonEntityResult<ResGoodsDetail>();
		Result<ResGoodsDetail> result = goodsBiz.getDetail(id);
		if(ResultTransfer.transfer(json, result)) {
			return json;
		}
		json.setEntity(result.getData());
		return json;
	}
	
	@ApiOperation(value = "添加一条商品发送记录", notes = "添加一条商品发送记录")
	@Log(module="Goods", moduleName="商品", action="addSendLog", actionName="添加一条商品发送记录", tag="api")
	@RequestMapping(value = "my/addsl", method = RequestMethod.POST)
	public JsonResult addSendLog(HttpServletRequest request,ReqSendLog req) {
		Long userId = JwtFace.getJwtUser(request).getId();
		req.setUserId(userId);
		JsonResult json = new JsonResult();
		Result<Void> result = goodsBiz.addSendLog(req);
		if(ResultTransfer.transfer(json, result)) {
			return json;
		}
		return json;
	}
	
	
	@ApiOperation(value = "获取发送记录", notes = "获取发送记录")
	@Log(module="Goods", moduleName="商品", action="addSendLog", actionName="获取发送记录", tag="api")
	@RequestMapping(value = "my/getps", method = RequestMethod.POST)
	public JsonListResult<ResGoodsSendLog> getPagedSend(HttpServletRequest request,ReqGetSendLog req){
		Long userId = JwtFace.getJwtUser(request).getId();
		req.setUserId(userId);
		JsonListResult<ResGoodsSendLog> json = new JsonListResult<ResGoodsSendLog>();
		Result<PageInfo<ResGoodsSendLog>> result = goodsBiz.getPagedSendLog(req);
		if(ResultTransfer.transferList(json, result)) {
			return json;
		}
		PageInfo<ResGoodsSendLog> page = result.getData();
		json.setRows(page.getPageResults());
		json.setTotal(page.getTotalCount());
		return json;
	}
	
	@ApiOperation(value = "链接转换", notes = "链接转换")
	@Log(module="Goods", moduleName="商品", action="transformUrl", actionName="链接转换", tag="api")
	@RequestMapping(value = "my/turl", method = RequestMethod.GET)
	public JsonEntityResult<String> transformUrl(HttpServletRequest request,ReqTransformUrl req) {
		req.setUserId(JwtFace.getJwtUser(request).getId());
		JsonEntityResult<String> json = new JsonEntityResult<String>();
		Result<String> result = goodsBiz.transformUrl(req);
		if(ResultTransfer.transfer(json, result)) {
			return json;
		}
		json.setEntity(result.getData());
		return json;
	}
	
	@ApiOperation(value = "获取发送模板可选参数", notes = "获取发送模板可选参数")
	@Log(module="Goods", moduleName="商品", action="getTpl", actionName="获取发送模板可选参数", tag="api")
	@RequestMapping(value = "gettpl", method = RequestMethod.GET)
	public JsonEntityResult<List<ResSendTpl>> getTpl(HttpServletRequest request) {
		JsonEntityResult<List<ResSendTpl>> json = new JsonEntityResult<List<ResSendTpl>>();
		Result<List<ResSendTpl>> result = goodsBiz.getListTpl();
		if(ResultTransfer.transfer(json, result)) {
			return json;
		}
		json.setEntity(result.getData());
		return json;
	}
	
	@ApiOperation(value = "保存发送模板", notes = "保存发送模板")
	@Log(module="Goods", moduleName="商品", action="saveUserTpl", actionName="保存发送模板", tag="api")
	@RequestMapping(value = "my/saveut", method = RequestMethod.GET)
	public JsonResult saveUserTpl(HttpServletRequest request,ReqSaveUserTpl req) {
		Long userId = JwtFace.getJwtUser(request).getId();
		req.setUserId(userId);
		JsonResult json = new JsonResult();
		Result<Void> result = goodsBiz.saveUserTpl(req);
		if(ResultTransfer.transfer(json, result)) {
			return json;
		}
		return json;
	}
	
	@ApiOperation(value = "生成文案", notes = "生成文案")
	@Log(module="Goods", moduleName="商品", action="generateUserSend", actionName="生成文案", tag="api")
	@RequestMapping(value = "my/genus", method = RequestMethod.GET)
	public JsonEntityResult<ResUserSendContent> generateUserSend(HttpServletRequest request,ReqGenUserSend req) {
		Long userId = JwtFace.getJwtUser(request).getId();
		req.setUserId(userId);
		JsonEntityResult<ResUserSendContent> json = new JsonEntityResult<ResUserSendContent>();
		Result<ResUserSendContent> result = goodsBiz.generateUserSend(req);
		if(ResultTransfer.transfer(json, result)) {
			return json;
		}
		json.setEntity(result.getData());
		return json;
	}
	
}
