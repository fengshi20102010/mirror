package com.mirror.tk.api.common;

public final class Constant {

	/**
	 * 全局定义
	 */
	public static final class Global {

		public static final String SUCCESS = "0";
		public static final String FAIL = "-1";
		public static final String UNAUTHORIZED = "401";
		public static final String UNAUTHORIZED_MSG = "TOKEN错误或TOKEN已过期，请重新登录后访问";
		public static final String ILLEGAL_ACCESS = "非法访问";
		public static final String UPLOAD_NO_DATA_FOUND = "没有找到上传对像";

		/** 状态 */
		public static enum State {
			Enable(1, "启用"), Disable(0, "禁用");

			private int value;
			private String title;

			private State(int value, String title) {
				this.value = value;
				this.title = title;
			}

			public int getValue() {
				return value;
			}

			public String getTitle() {
				return title;
			}
		}

	}

	/**
	 * 短信相关提醒
	 */
	public static class SmsMassage{
		public static final String SMS_SEND_SUCCESS = "发送成功，请注意查收！";
		public static final String SMS_SEND_FAILE = "发送短信失败，请稍后重试！";
	}
	
    /**
     * 用户相关提醒
     */
    public static class UserMassage{
    	public static final String USER_IS_EXISTED = "用户名已经存在";
    	public static final String USER_IS_NOT_EXISTED = "用户不存在";
    	public static final String USER_REGIEST_FAILE = "注册失败！请稍后再试";
    	public static final String USER_REGIEST_SUCCESS = "注册成功！";
    	public static final String USER_LOGIN_SUCCESS = "登陆成功！";
    	public static final String USER_LOGIN_FAIL = "用户名或密码错误，请重试";
    	public static final String USER_FIND_PWD_SUCCESS = "找回密码成功";
    	public static final String USER_FIND_PWD_FAIL = "重置密码失败！，请稍后再试";
    	public static final String USER_UPDATE_PWD_SUCCESS = "修改密码成功";
    	public static final String USER_PWD_ERROR = "原始密码错误，请重试";
    	public static final String USER_PID_NOT_EXISTED = "PID不存在";
    	public static final String USER_VERIFICATION_CODE_ERROR = "验证码错误";
    	public static final String USER_UPDATE_SUCCESS = "更新用户信息成功";
    	public static final String USER_UPDATE_FAIL = "更新用户信息失败，请稍后重试！";
    	public static final String USER_SETTING_MODIFY_SUCCESS = "用户设置修改成功";
    	public static final String USER_SETTING_MODIFY_FAIL = "用户设置修改失败，请稍后重试！";
    }
	
}
