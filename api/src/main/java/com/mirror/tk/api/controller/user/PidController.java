package com.mirror.tk.api.controller.user;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mirror.tk.api.common.Constant;
import com.mirror.tk.api.common.support.JwtFace;
import com.mirror.tk.api.controller.CommonController;
import com.mirror.tk.core.log.annotation.Log;
import com.mirror.tk.core.module.user.domain.UserPid;
import com.mirror.tk.core.module.user.service.UserPidService;
import com.mirror.tk.framework.common.web.support.JsonListResult;
import com.mirror.tk.framework.common.web.support.JsonResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "PID接口", description = "提供PID相关操作接口（该接口需要授权）")
@RestController
@RequestMapping("my")
public class PidController extends CommonController {
	
	@Resource
	private UserPidService userPidService;

	@ApiOperation(value = "获取pid列表", notes = "获取pid列表")
	@Log(module="User", moduleName="用户", action="pid", actionName="获取pid列表", tag="api")
	@RequestMapping(value = "pid", method = RequestMethod.GET)
	public JsonListResult<UserPid> pid(HttpServletRequest request){
		JsonListResult<UserPid> result = new JsonListResult<UserPid>();
		List<UserPid> list = userPidService.getByUserId(JwtFace.getJwtUser(request).getId());
		result.setTotal(Long.valueOf(list.size()));
		result.setSuccess(true);
		result.setRows(list);
		return result;
	}
	
	@ApiOperation(value = "增加pid", notes = "增加pid")
	@Log(module="User", moduleName="用户", action="updatePid", actionName="增加pid", tag="api")
	@RequestMapping(value = "pid", method = RequestMethod.POST)
	public JsonResult addPid(HttpServletRequest request, 
			@ApiParam(value = "pid对象", required = true)@Valid UserPid pid){
		JsonResult result = new JsonResult(false);
		pid.setUserId(JwtFace.getJwtUser(request).getId());
		pid.setCreateTime(new Date());
		pid.setUpdateTime(new Date());
		userPidService.save(pid);
		result.setSuccess(true);
		result.setCode(Constant.Global.SUCCESS);
		return result;
	}
	
	@ApiOperation(value = "修改pid", notes = "修改pid")
	@Log(module="User", moduleName="用户", action="updatePid", actionName="修改pid", tag="api")
	@RequestMapping(value = "pid/{id}", method = RequestMethod.PUT)
	public JsonResult updatePid(HttpServletRequest request, 
			@ApiParam(value = "pid id", required = true)@PathVariable Long id, 
			@ApiParam(value = "pid对象", required = true)UserPid pid){
		JsonResult result = new JsonResult();
		UserPid oPid = userPidService.getByUserIdAndId(pid.getId(), JwtFace.getJwtUser(request).getId());
		if(null == oPid){
			result.setCode(Constant.Global.FAIL);
			result.setMessage(Constant.UserMassage.USER_PID_NOT_EXISTED);
			return result;
		}
		oPid.setName(pid.getName());
		oPid.setCommonPid(pid.getCommonPid());
		oPid.setQueqiaoPid(pid.getQueqiaoPid());
		oPid.setUpdateTime(new Date());
		userPidService.update(oPid);
		result.setSuccess(true);
		result.setCode(Constant.Global.SUCCESS);
		return result;
	}
	
	@ApiOperation(value = "删除pid", notes = "删除pid")
	@Log(module="User", moduleName="用户", action="delPid", actionName="删除pid", tag="api")
	@RequestMapping(value = "pid/{id}", method = RequestMethod.DELETE)
	public JsonResult delPid(HttpServletRequest request, 
			@ApiParam(value = "pid id", required = true)@PathVariable Long id){
		JsonResult result = new JsonResult();
		UserPid pid = userPidService.getByUserIdAndId(id, JwtFace.getJwtUser(request).getId());
		if(null == pid){
			result.setCode(Constant.Global.FAIL);
			result.setMessage(Constant.UserMassage.USER_PID_NOT_EXISTED);
			return result;
		}
		userPidService.remove(pid);
		result.setSuccess(true);
		result.setCode(Constant.Global.SUCCESS);
		return result;
	}
	
}
