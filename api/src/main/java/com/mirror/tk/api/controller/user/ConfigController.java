package com.mirror.tk.api.controller.user;

import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mirror.tk.api.common.Constant;
import com.mirror.tk.api.common.support.JwtFace;
import com.mirror.tk.api.controller.CommonController;
import com.mirror.tk.core.log.annotation.Log;
import com.mirror.tk.core.module.user.domain.UserConfig;
import com.mirror.tk.core.module.user.domain.UserGatherConfig;
import com.mirror.tk.core.module.user.service.UserConfigService;
import com.mirror.tk.core.module.user.service.UserGatherConfigService;
import com.mirror.tk.framework.common.exception.BusinessException;
import com.mirror.tk.framework.common.web.support.JsonEntityResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "配置api", description = "提供配置相关接口")
@RestController
@RequestMapping("my")
public class ConfigController extends CommonController {

	@Resource
	UserGatherConfigService userGatherConfigService;
	@Resource
	UserConfigService userConfigService;
	
	@ApiOperation(value = "获取采集配置", notes = "获取采集配置")
	@Log(module="config", moduleName="配置", action="getGacherConfig", actionName="获取采集配置", tag="api")
	@RequestMapping(value = "gacherConfig", method = RequestMethod.GET)
	public JsonEntityResult<UserGatherConfig> getGacherConfig(HttpServletRequest request){
		JsonEntityResult<UserGatherConfig> result = new JsonEntityResult<UserGatherConfig>();
		result.setSuccess(true);
		result.setCode(Constant.Global.SUCCESS);
		result.setEntity(userGatherConfigService.getByUserId(JwtFace.getJwtUser(request).getId()));
		return result;
	}
	
	@ApiOperation(value = "新增/修改采集配置", notes = "新增/修改采集配置")
	@Log(module="config", moduleName="配置", action="gacherConfig", actionName="新增/修改程序设置", tag="api")
	@RequestMapping(value = "gacherConfig", method = RequestMethod.POST)
	public JsonEntityResult<UserGatherConfig> gacherConfig(HttpServletRequest request, 
			@ApiParam(value = "采集设置json对象", required = true)@RequestBody @Valid UserGatherConfig setting){
		JsonEntityResult<UserGatherConfig> result = new JsonEntityResult<UserGatherConfig>();
		UserGatherConfig oSetting = userGatherConfigService.getByUserId(JwtFace.getJwtUser(request).getId());
		if(null == oSetting){
			// 新增
			setting.setId(null);
			setting.setCreateTime(new Date());
		} else {
			setting.setId(oSetting.getId());
		}
		try {
			setting.setUserId(JwtFace.getJwtUser(request).getId());
			userGatherConfigService.save(setting);
			result.setSuccess(true);
			result.setEntity(setting);
			result.setCode(Constant.Global.SUCCESS);
			result.setMessage(Constant.UserMassage.USER_SETTING_MODIFY_SUCCESS);
		} catch (BusinessException e) {
			logger.error("save fail:error:{}", e.getMessage());
			result.setCode(Constant.Global.FAIL);
			result.setMessage(Constant.UserMassage.USER_SETTING_MODIFY_FAIL);
		}
		return result;
	}
	
	@ApiOperation(value = "获取程序配置", notes = "获取程序配置")
	@Log(module="config", moduleName="配置", action="getConfig", actionName="获取程序配置", tag="api")
	@RequestMapping(value = "gacherSetting", method = RequestMethod.GET)
	public JsonEntityResult<UserConfig> getConfig(HttpServletRequest request){
		JsonEntityResult<UserConfig> result = new JsonEntityResult<UserConfig>();
		result.setSuccess(true);
		result.setCode(Constant.Global.SUCCESS);
		result.setEntity(userConfigService.getByUserId(JwtFace.getJwtUser(request).getId()));
		return result;
	}
	
	@ApiOperation(value = "新增/修改采集配置", notes = "新增/修改采集配置")
	@Log(module="config", moduleName="程序设置", action="config", actionName="新增/修改程序设置", tag="api")
	@RequestMapping(value = "gacherSetting", method = RequestMethod.POST)
	public JsonEntityResult<UserConfig> config(HttpServletRequest request, 
			@ApiParam(value = "程序设置json对象", required = true)@RequestBody @Valid UserConfig setting){
		JsonEntityResult<UserConfig> result = new JsonEntityResult<UserConfig>();
		UserConfig oSetting = userConfigService.getByUserId(JwtFace.getJwtUser(request).getId());
		setting.setId(null == oSetting ? null : oSetting.getId());
		setting.setUserId(JwtFace.getJwtUser(request).getId());
		try {
			userConfigService.save(setting);
			result.setSuccess(true);
			result.setEntity(setting);
			result.setCode(Constant.Global.SUCCESS);
			result.setMessage(Constant.UserMassage.USER_SETTING_MODIFY_SUCCESS);
		} catch (BusinessException e) {
			logger.error("save fail:error:{}", e.getMessage());
			result.setCode(Constant.Global.FAIL);
			result.setMessage(Constant.UserMassage.USER_SETTING_MODIFY_FAIL);
		}
		return result;
	}
	
}
