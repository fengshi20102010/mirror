package com.mirror.tk.api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import springfox.documentation.annotations.ApiIgnore;

/**
 * 首页
 */
@ApiIgnore
@Controller
public class IndexController extends CommonController {

	private static final String INDEX = "index";
	
	/**
	 * 接口页面
	 * @return
	 */
	@RequestMapping(value = "index", method = RequestMethod.GET)
	public String index() {
		return INDEX;
	}

}
