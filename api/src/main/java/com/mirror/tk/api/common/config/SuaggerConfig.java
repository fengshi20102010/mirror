package com.mirror.tk.api.common.config;

import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * restful API 接口文档设置
 */
@EnableSwagger2
public class SuaggerConfig {

	private String groupName;
	private String title;
	private String description;
	private String version;
	private boolean enabled;
	private String name = "XXX科技有限公司";
	private String url = "hht://www.mirror.com";
	private String email = "fengshi20102010@qq.com";

	@Bean
	public Docket createRestApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName(groupName).enable(enabled).apiInfo(getApiInfo()).forCodeGeneration(true);
	}

	private ApiInfo getApiInfo() {
		return new ApiInfoBuilder().title(title).description(description).version(version).contact(new Contact(name, url, email)).build();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
