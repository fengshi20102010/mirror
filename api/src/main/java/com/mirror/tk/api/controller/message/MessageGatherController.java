//package com.mirror.tk.api.controller.message;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.google.common.collect.Maps;
//import com.mirror.tk.api.common.support.JwtFace;
//import com.mirror.tk.api.controller.CommonController;
//import com.mirror.tk.api.utils.ResultTransfer;
//import com.mirror.tk.core.biz.common.Result;
//import com.mirror.tk.core.biz.req.ReqMessageGather;
//import com.mirror.tk.core.biz.req.ReqMessageGatherQueryInfo;
//import com.mirror.tk.core.log.annotation.Log;
//import com.mirror.tk.core.module.gather.dto.MessageGatherDto;
//import com.mirror.tk.core.module.gather.service.MessageGatherService;
//import com.mirror.tk.framework.common.dao.support.PageInfo;
//import com.mirror.tk.framework.common.web.support.JsonEntityResult;
//import com.mirror.tk.framework.common.web.support.JsonListResult;
//import com.mirror.tk.framework.common.web.support.JsonResult;
//
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;
//
//@Api(value = "网站消息API", description = "提供消息相关操作（该接口需要授权）")
//@RestController
//public class MessageGatherController extends CommonController {
//	
//
//	
//	@Resource
//	private MessageGatherService messageGatherService;
//	
//	@ApiOperation(value = "新增採集消息", notes = "新增採集消息")
//	@Log(module="MessageGather", moduleName="採集", action="save", actionName="新增採集消息", tag="api")
//	@RequestMapping(value = "gather/save", method = RequestMethod.POST)
//	public JsonResult save(HttpServletRequest request, @RequestBody ReqMessageGather req){
//		
//		JsonEntityResult<Integer> json = new JsonEntityResult<Integer>();
//		
//		req.setUserId(JwtFace.getJwtUser(request).getId());
//		Result<Integer> result = new Result<Integer>().success(messageGatherService.save(req));
//		if(ResultTransfer.transfer(json, result)) {
//			return json;
//		}
//		json.setEntity(result.getData());
//		return json;
//	}
//	
//	@ApiOperation(value = "批量新增", notes = "批量新增")
//	@Log(module="MessageGather", moduleName="采集", action="saveBatch", actionName="批量新增", tag="api")
//	@RequestMapping(value = "gather/saveBatch", method = RequestMethod.POST)
//	public JsonResult saveBatch(HttpServletRequest request, 
//			@ApiParam(value="发送列表")@RequestBody List<ReqMessageGather> reqList){
//		
//		JsonEntityResult<Integer> json = new JsonEntityResult<Integer>();
//		List<ReqMessageGather> list = new ArrayList<>();
//		for(ReqMessageGather req : reqList){
//			req.setUserId(JwtFace.getJwtUser(request).getId());		
//			list.add(req);
//		}
//		
//		Result<Integer> result = new Result<Integer>().success(messageGatherService.save(list));
//		if(ResultTransfer.transfer(json, result)) {
//			return json;
//		}
//		json.setEntity(result.getData());
//		return json;
//	}
//	
//	@ApiOperation(value="修改採集消息",notes="修改採集消息")
//	@Log(module="MessageGather", moduleName="修改", action="update", actionName="修改採集消息", tag="api")
//	@RequestMapping(value="gather/update", method = RequestMethod.PUT)
//	public JsonResult update(HttpServletRequest request,@RequestBody ReqMessageGather req){
//		JsonEntityResult<Integer> json = new JsonEntityResult<Integer>();
//		
//		//req.setUserId(JwtFace.getJwtUser(request).getId());
//		Result<Integer> result = new Result<Integer>().success(messageGatherService.update(req));
//		if(ResultTransfer.transfer(json, result)) {
//			return json;
//		}
//		json.setEntity(result.getData());
//		return json;
//	}
//	@ApiOperation(value="修改採集消息",notes="修改採集消息")
//	@Log(module="MessageGather", moduleName="修改", action="update", actionName="修改採集消息", tag="api")
//	@RequestMapping(value="gather/updateBatch", method = RequestMethod.PUT)
//	public JsonResult updateBatch(HttpServletRequest request,@RequestBody List<ReqMessageGather> reqList){
//		JsonEntityResult<Integer> json = new JsonEntityResult<Integer>();
//		
//		//req.setUserId(JwtFace.getJwtUser(request).getId());
//		Result<Integer> result = new Result<Integer>().success(messageGatherService.update(reqList));
//		if(ResultTransfer.transfer(json, result)) {
//			return json;
//		}
//		json.setEntity(result.getData());
//		return json;
//	}
//	
//	@ApiOperation(value="刪除商採集消息",notes="刪除採集消息")
//	@Log(module="MessageGather", moduleName="刪除", action="delete", actionName="刪除採集消息", tag="api")
//	@RequestMapping(value="gather/delete", method = RequestMethod.DELETE)
//	public JsonResult delete(HttpServletRequest request, 
//			@ApiParam(value = "消息id", required = true)@RequestParam String id){
//			
//		JsonEntityResult<Integer> json = new JsonEntityResult<Integer>();
//	
//		Result<Integer> result = new Result<Integer>().success(messageGatherService.delete(Integer.parseInt(id)));
//		if(ResultTransfer.transfer(json, result)) {
//			return json;
//		}
//		json.setEntity(result.getData());
//		return json;
//
//	}
//	@ApiOperation(value="查詢（待）發送記錄",notes="查詢（待）發送記錄")
//	@Log(module="MessageGather", moduleName="查询待发送记录", action="query", actionName="查询待发送记录", tag="api")
//	@RequestMapping(value="gather/getList", method = RequestMethod.GET)
//	public JsonListResult<MessageGatherDto> getList(HttpServletRequest request, ReqMessageGatherQueryInfo req) {
//		JsonListResult<MessageGatherDto> json = new JsonListResult<MessageGatherDto>();
//		
//		Map<String,Object> searchMap = Maps.newHashMap();
//		PageInfo<MessageGatherDto> pageInfo = new PageInfo<MessageGatherDto>(req.getPageSize(),req.getCurrentPage());
//		
//		pageInfo = 	messageGatherService.queryPage(pageInfo, req);
//
//		Result<PageInfo<MessageGatherDto>> result = new Result<PageInfo<MessageGatherDto>>().success(pageInfo);
//		
//		if(ResultTransfer.transferList(json, result)) {
//			return json;
//		}
//		PageInfo<MessageGatherDto> page = result.getData();
//		json.setRows(page.getPageResults());
//		json.setTotal(page.getTotalCount());
//		return json;
//	}
//	
//	
//}
