package com.mirror.tk.api.controller.user;

import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mirror.tk.api.common.Constant;
import com.mirror.tk.api.common.support.JwtFace;
import com.mirror.tk.api.controller.CommonController;
import com.mirror.tk.core.log.annotation.Log;
import com.mirror.tk.core.module.user.domain.UserSetting;
import com.mirror.tk.core.module.user.service.UserSettingService;
import com.mirror.tk.framework.common.exception.BusinessException;
import com.mirror.tk.framework.common.web.support.JsonEntityResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "程序设置api", description = "提供程序设置相关接口")
@RestController
@RequestMapping("my")
public class SettingController extends CommonController {

	@Resource
	UserSettingService userSettingService;
	
	@ApiOperation(value = "获取程序设置", notes = "获取程序设置")
	@Log(module="Setting", moduleName="程序设置", action="setting", actionName="获取程序设置", tag="api")
	@RequestMapping(value = "setting", method = RequestMethod.GET)
	public JsonEntityResult<UserSetting> getSetting(HttpServletRequest request){
		JsonEntityResult<UserSetting> result = new JsonEntityResult<UserSetting>();
		result.setSuccess(true);
		result.setCode(Constant.Global.SUCCESS);
		result.setEntity(userSettingService.getByUserId(JwtFace.getJwtUser(request).getId()));
		return result;
	}
	
	@ApiOperation(value = "新增/修改程序设置", notes = "新增/修改程序设置")
	@Log(module="Setting", moduleName="程序设置", action="setting", actionName="新增/修改程序设置", tag="api")
	@RequestMapping(value = "setting", method = RequestMethod.POST)
	public JsonEntityResult<UserSetting> setting(HttpServletRequest request, 
			@ApiParam(value = "用户设置对象json", required = true)@RequestBody @Valid UserSetting setting){
		JsonEntityResult<UserSetting> result = new JsonEntityResult<UserSetting>();
		UserSetting oSetting = userSettingService.getByUserId(JwtFace.getJwtUser(request).getId());
		if(null == oSetting){
			// 新增
			setting.setId(null);
			setting.setCreateTime(new Date());
			setting.setUpdateTime(new Date());
			setting.setUserId(JwtFace.getJwtUser(request).getId());
			try {
				userSettingService.save(setting);
				result.setSuccess(true);
				result.setEntity(setting);
				result.setCode(Constant.Global.SUCCESS);
				result.setMessage(Constant.UserMassage.USER_SETTING_MODIFY_SUCCESS);
			} catch (BusinessException e) {
				logger.error("save fail:error:{}", e.getMessage());
				result.setCode(Constant.Global.FAIL);
				result.setMessage(Constant.UserMassage.USER_SETTING_MODIFY_FAIL);
			}
		} else {
			// 修改
			oSetting.setAutoAccount(setting.getAutoAccount());
			oSetting.setAutoPwd(setting.getAutoPwd());
			oSetting.setMamaAccount(setting.getMamaAccount());
			oSetting.setMamaPwd(setting.getMamaPwd());
			oSetting.setWeichatAccount(setting.getWeichatAccount());
			oSetting.setWeichatTip(setting.getWeichatTip());
			oSetting.setUpdateTime(new Date());
			try {
				userSettingService.update(oSetting);
				result.setSuccess(true);
				result.setEntity(oSetting);
				result.setCode(Constant.Global.SUCCESS);
				result.setMessage(Constant.UserMassage.USER_SETTING_MODIFY_SUCCESS);
			} catch (BusinessException e) {
				logger.error("update fail:error:{}", e.getMessage());
				result.setCode(Constant.Global.FAIL);
				result.setMessage(Constant.UserMassage.USER_SETTING_MODIFY_FAIL);
			}
		}
		return result;
	}
	
}
