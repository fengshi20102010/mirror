<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page import="java.io.*,java.util.*"%>  
<%  
	response.setStatus(HttpServletResponse.SC_OK);  
	Exception ex = (Exception) request.getAttribute("javax.servlet.error.exception");
%>
<!DOCTYPE HTML>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <title>500</title>
		<link rel="shortcut icon" href="favicon.ico"> 
	    <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	    <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
	    <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
	</head>
	<body class="gray-bg">
	    <div class="middle-box text-center animated fadeInDown">
	        <h1>500</h1>
	        <h3 class="font-bold">服务器内部错误</h3>
	        <div class="error-desc">
				服务器好像出错了...
				<br/>错误信息：<%=ex.getMessage()%>
	            <br/>您可以返回主页看看
	            <br/><a href="${pageContext.request.contextPath }/index.html" class="btn btn-primary m-t">主页</a>
	        </div>
	    </div>
	</body>
</html>