<#assign entityContextPath="${configuration.pageMapping}/${names.domainClassName?uncap_first}" />
<#assign entityVariable="${names.domainClassName?uncap_first}" />
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title>魔镜助手 - ${table.comment}列表</title>
        <#noparse>
        <!--[if lt IE 9]>
            <meta http-equiv="refresh" content="0;ie.html" />
        <![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
        </#noparse>
    </head>
    
    <body class="gray-bg">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>${table.comment}列表</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <a href="javascript:void(0);">选项1</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">选项2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row row-lg">
                    <!-- 查询条件 -->
                    <div class="well">
                        <h3>搜索</h3>
                        <form id="searchForm" class="form-inline">
                            <div class="form-group">
                            <#assign counter=0 />
                            <#list table.columnMetadatas as entity>
                                <#if configuration.searchColumnsList?size==0 || configuration.searchColumnsList?seq_contains(entity.name?lower_case)>
                                <#assign counter=counter+1 />
                                <div class="input-group">
                                    <span class="input-group-addon">${entity.common}</span>
                                <#if entity.dataType==2>
                                    <#--日期-->
                                    <script type="text/javascript">
                                        require(["jquery", "daterangepicker"], function($) {
                                            var elm = $("#${entity.propertyName}");
                                            $(elm).daterangepicker({
                                                startDate: $(elm).prev().prev().val(),
                                                endDate: $(elm).prev().val(),
                                                format: "YYYY-MM-DD",
                                                showDropdowns: true
                                            }, function(start, end) {
                                                $(elm).find(".date-title").html(start.format("YYYY-MM-DD") + " 至 " + end.format("YYYY-MM-DD"));
                                                $(elm).prev().prev().val(start.format("YYYY-MM-DD"));
                                                $(elm).prev().val(end.format("YYYY-MM-DD"));
                                            });
                                        });
                                    </script>
                                    <input name="begin${entity.propertyName?cap_first}" type="hidden" value="<fmt:formatDate value="${r"${"}begin${entity.propertyName?cap_first}}" pattern="yyyy-MM-dd" />">
                                    <input name="end${entity.propertyName?cap_first}" type="hidden" value="<fmt:formatDate value="${r"${"}end${entity.propertyName?cap_first}}" pattern="yyyy-MM-dd" />">
                                    <button class="btn btn-default daterange" id="${entity.propertyName}" type="button" data-original-title="" title="" style="margin-bottom:0"><span class="date-title"><fmt:formatDate value="${r"${"}begin${entity.propertyName?cap_first}}" pattern="yyyy-MM-dd" /> 至 <fmt:formatDate value="${r"${"}end${entity.propertyName?cap_first}}" pattern="yyyy-MM-dd" /></span> <i class="fa fa-calendar"></i></button>
                                <#elseif entity.dataType==1 || entity.dataType==4>
                                    <#--数字-->
                                    <#if entity.options??>
                                    <select name="${entity.propertyName}" id="${entity.propertyName}" class="form-control">
                                        <option value="">所有</option>
                                        <#list entity.options?keys as key>
                                        <option value="${key}" <c:if test="${r"${"}param.${entity.propertyName} eq '${key}'}"> selected="selected"</c:if>>${entity.options[key]}</option>
                                        </#list>
                                    </select>
                                    <#else>
                                    <input class="form-control" name="${entity.propertyName}" id="${entity.propertyName}" type="text" value="${r"${"}param.${entity.propertyName}}">
                                    </#if>
                                <#else>
                                    <#--字符-->
                                    <input class="form-control" name="${entity.propertyName}" id="${entity.propertyName}" type="text" maxlength="50" value="${r"${"}param.${entity.propertyName}}">
                                </#if>
                                </div>
                                <#if entity_has_next==false || configuration.searchColumnsList?size==counter>
                                <div class="form-group">
                                    <input type="hidden" id="pageSize" name="pageSize">
                                    <input type="hidden" id="pageNumber" name="pageNumber">
                                    <input type="button" class="btn btn-primary" id="search" value="搜索" style="margin-bottom:0">
                                </div>
                                </#if>
                                </#if>
                            </#list>
                            </div>
                        </form>
                        <br>
                    </div>
                    <div class="col-sm-12">
                        <div class="btn-group hidden-xs" id="toolbar" role="group">
                            <button type="button" class="btn btn-outline btn-default" id="add">
                                <i class="glyphicon glyphicon-plus" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="btn btn-outline btn-default">
                                <i class="glyphicon glyphicon-heart" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="btn btn-outline btn-default">
                                <i class="glyphicon glyphicon-trash" aria-hidden="true"></i>
                            </button>
                        </div>
                        <table id="bootstrap-table" data-mobile-responsive="true">
                            <thead>
                                <tr>
                                    <th></th>
                                    <#list table.columnMetadatas as entity>
                                    <#if configuration.listColumnsList?size==0 || configuration.listColumnsList?seq_contains(entity.name?lower_case)>
                                    <th>${entity.common}</th>
                                    </#if>
                                    </#list>
                                    <th>操作</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script>
            require(['bootstrap-table.zh-CN', 'layer', 'moment', 'contabs.min', 'content.min', 'main.min'], function($, layer, moment) {
                layer.config({
                    <#noparse>
                    path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
                    </#noparse>
                });
                
                $('#bootstrap-table').bootstrapTable({
                    contentType: "application/x-www-form-urlencoded",
                    method: 'post',
                    toolbar: '#toolbar',
                    iconSize: "outline",
                    icons: {
                        refresh: "glyphicon-repeat",
                        toggle: "glyphicon-list-alt",
                        columns: "glyphicon-list"
                    },
                    striped: true,
                    cache: false,
                    pagination: true,
                    sortable: false,
                    pageNumber: 1,
                    pageSize: 10,
                    pageList: [10, 25, 50, 100],
                    url: window.location.href,
                    queryParamsType: '',
                    queryParams: function(params){
                        <#if configuration.searchColumnsList?size==0>
                        return params;
                        <#else>
                        $('#pageNumber').val(params.pageNumber);
                        $('#pageSize').val(params.pageSize);
                        return $('#searchForm').serialize();
                        </#if>
                    },
                    sidePagination: "server", 
                    strictSearch: true,
                    showColumns: true, 
                    showRefresh: true, 
                    minimumCountColumns: 2, 
                    searchOnEnterKey: true,
                    pagination: true,
                    columns: [
                        {
                            checkbox: true,
                            align: 'center'
                        }, 
                        <#list table.columnMetadatas as entity>
                            <#if configuration.listColumnsList?size==0 || configuration.listColumnsList?seq_contains(entity.name?lower_case)>
                            <#if entity.dataType == 2>
                            <#--日期-->
                        {
                            field: '${entity.propertyName}',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	return moment(value).format('YYYY-MM-DD');
                            }
                        }, 
                            <#elseif entity.dataType == 1 || entity.dataType == 4>
                            <#--数字-->
                                <#if entity.options??>
                        {
                            field: '${entity.propertyName}',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	var content = '-';
                            	<#list entity.options?keys as key>
                            	if(value == ${key}){
                                    content = '<span class="label label-info">${entity.options[key]}</span>';
                            	}
                                </#list>
                            	return content;
                            }
                        }, 
                                <#else>
                        {
                            field: '${entity.propertyName}',
                            align: 'center'
                        }, 
                                </#if>
                            <#else>
                        {
                            field: '${entity.propertyName}',
                            align: 'center'
                        }, 
                            </#if>
                            </#if>
                        </#list>
                        {
                            field: 'id',
                            align: 'center',
                            formatter: function(value, row, index){
                                var content = '无权限';
                                <security:hasPermission name="${configuration.permissionsName}:view">
                                    content += '<a title="详情" href="javascript:;" class="ml-5 view" style="text-decoration:none"><i class="fa fa-eye"></i></a>';
                                </security:hasPermission>
                                <security:hasPermission name="${configuration.permissionsName}:update">
                                    content += '<a title="编辑" href="javascript:;" class="ml-5 update" style="text-decoration:none" ><i class="fa fa-edit"></i></a>';
                                </security:hasPermission>
                                <security:hasPermission name="${configuration.permissionsName}:del">
                                    content += '<a title="删除" href="javascript:;" class="ml-5 del" style="text-decoration:none"><i class="fa fa-close"></i></a>';
                                </security:hasPermission>
                                return content;
                            }
                        }
                    ]
                });

                // 搜索
                $('#search').on('click', function() {
                    $('#bootstrap-table').bootstrapTable('refresh');
                });
                
                // 新增
                $('#add').on('click', function() {
                    var title = '新增${table.comment}';
                    var url = 'add.html';
                    layerShow(title, url, "800", "500");
                });

                // 编辑
                $('#bootstrap-table').delegate('.update', 'click', function() {
                    var index = $(this).parents('tr').data('index');
                    var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
                    var title = '编辑${table.comment}';
                    var url = 'update.html?id=' + id;
                    layerShow(title, url, 800, 500);
                });

                // 查看
                $('#bootstrap-table').delegate('.view', 'click', function() {
                    var index = $(this).parents('tr').data('index');
                    var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
                    var title = '查看${table.comment}';
                    var url = 'view.html?id=' + id;
                    layerShow(title, url);
                });

                // 删除
                $('#bootstrap-table').delegate('.del', 'click', function() {
                    $this = $(this);
                    var index = $(this).parents('tr').data('index');
                    var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
                    layer.confirm('确认删除吗?\n 删除后的数据将不可恢复！', function(i) {
                        $.post('del.html', {"id": id}, function(d) {
                            if(d.success) {
                                layer.msg('删除成功！', {icon: 1, time: 1000});
                                setTimeout($('#bootstrap-table').bootstrapTable('refresh'), 1000);
                                return;
                            }
                            layer.msg(d.message, {icon: 5, time: 1000});
                        }, "json");
                    });
                });
            });
        </script>
    </body>
</html>