package ${names.serviceImplPackage};

import ${names.servicePackage}.${names.serviceClassName};
import ${names.daoPackage}.${names.daoClassName};
import ${names.domainPackage}.${names.domainClassName};
import com.mirror.tk.framework.common.service.EntityServiceImpl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("${names.serviceClassName?uncap_first}")
public class ${names.serviceImplClassName} extends EntityServiceImpl<${names.domainClassName}, ${names.daoClassName}> implements ${names.serviceClassName} {

}
