package ${names.servicePackage};

import com.mirror.tk.framework.common.service.EntityService;
import ${names.domainPackage}.${names.domainClassName};


/**
 * ${table.comment} Service
 *
 * Date: ${datetime("yyyy-MM-dd HH:mm:ss")}
 *
 * @author Code Generator
 *
 */
public interface ${names.serviceClassName} extends EntityService<${names.domainClassName}> {

}
