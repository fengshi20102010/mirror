<#assign entityContextPath="${configuration.pageMapping}/${names.domainClassName?uncap_first}" />
<#assign entityVariable="${names.domainClassName?uncap_first}" />
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title>魔镜助手 - ${table.comment}新增/修改</title>
        <#noparse>
        <!--[if lt IE 9]>
            <meta http-equiv="refresh" content="0;ie.html" />
        <![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
        </#noparse>
    </head>
    <body>
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="form-horizontal m-t" id="form1">
                    <c:if test="${r"${"}not empty item}">
                        <input type="hidden" name="id" value="${r"${"}item.id}" />
                    </c:if>
                    <#list table.columnMetadatas as entity>
                    <#if entity.name?lower_case != 'id' && (configuration.detailColumnsList?size==0 || configuration.detailColumnsList?seq_contains(entity.name?lower_case))>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">${entity.common}：</label>
                        <#if entity.dataType == 2>
                        <#--日期-->
                            <div class="col-sm-4">
                                <input class="form-control" type="text" name="${entity.propertyName}" id="${entity.propertyName}" readonly="readonly" value="<fmt:formatDate value="${r"${"}item.${entity.propertyName}}" pattern="yyyy-MM-dd" />">
                            </div>
                            <script type="text/javascript">
                                require(['datetimepicker.zh-CN'], function($){
                                    var elm = $("#${entity.propertyName}");
                                    $(elm).datetimepicker({
                                        language: 'zh-CN',
                                        format: "yyyy-mm-dd",
                                        minView: "month",
                                        autoclose: true,
                                        showDropdowns: true,
                                        singleDatePicker:true
                                    });
                                });
                            </script>
                        <#elseif entity.dataType == 1 || entity.dataType == 4>
                        <#--数字-->
                            <#if entity.options??>
                            <#--下拉框 -->
                            <#--<div class="col-sm-4">
                                <select class="form-control m-b" name="${entity.propertyName}" id="${entity.propertyName}" size="1">
                                    <#list entity.options?keys as key>
                                        <option value="${key}"<c:if test="${r"${"}item.${entity.propertyName} eq ${key}}"> selected="selected"</c:if>>${entity.options[key]}</option>
                                    </#list>
                                </select>
                            </div>-->
                            <#--单选框 -->
                            <div class="radio i-checks">
                                <#list entity.options?keys as key>
                                <label>
                                    <input type="radio" name="${entity.propertyName}" id="${entity.propertyName}${key}" value="${key}"> <i></i> ${entity.options[key]}
                                </label>
                                </#list>
                                <script type="text/javascript">
                                    document.getElementById("${entity.propertyName}${r"${"}empty item ? ${entity.options?keys[0]} : item.${entity.propertyName}}").checked=true;
                                </script>
                            </div>
                            <#else>
                            <div class="col-sm-8">
                                <input class="form-control" name="${entity.propertyName}" id="${entity.propertyName}" type="text" value="${r"${"}item.${entity.propertyName}}">
                            </div>
                            </#if>
                        <#else>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="${entity.propertyName}" name="${entity.propertyName}" placeholder="请输入${entity.common}"  value="${r"${"}item.${entity.propertyName}}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                            </div>
                        </#if>
                    </div>
                    </#if>
                    </#list>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-3">
                            <button class="btn btn-primary" type="submit">提交</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        <script>
            require(['jquery', 'layer', 'jquery.validate.methods', 'jquery.icheck', 'contabs.min', 'content.min', 'main.min'], function($, layer) {
                //初始化layer
                layer.config({
                    <#noparse>
                    path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
                    </#noparse>
                });
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                    increaseArea: '20%'
                });
                var err = "<i class='fa fa-times-circle'></i> ";
                $("#form1").validate({
                    rules: {
                        <#assign counter=0 />
                        <#list table.columnMetadatas as entity>
                            <#if entity.name?lower_case != 'id' && (configuration.detailColumnsList?size==0 || configuration.detailColumnsList?seq_contains(entity.name?lower_case))>
                                <#assign counter=counter+1 />
                            ${entity.propertyName} : {
                                required: true<#if entity.dataType == 2>,
                                date:true<#elseif entity.dataType == 1 || entity.dataType == 4>,
                                number:true</#if>
                            }<#if entity_has_next && (configuration.detailColumnsList?size==0 || configuration.detailColumnsList?size>counter)>,</#if>
                            </#if>
                        </#list>
                    },
                    messages: {
                    <#assign counter=0 />
                    <#list table.columnMetadatas as entity>
                        <#if entity.name?lower_case != 'id' && (configuration.detailColumnsList?size==0 || configuration.detailColumnsList?seq_contains(entity.name?lower_case))>
                            <#assign counter=counter+1 />
                        ${entity.propertyName} : {
                            required: err + ' '<#if entity.dataType == 2>,
                            date:err + ' '<#elseif entity.dataType == 1 || entity.dataType == 4>,
                            number:err + ' '</#if>
                        }<#if entity_has_next && (configuration.detailColumnsList?size==0 || configuration.detailColumnsList?size>counter)>,</#if>
                        </#if>
                    </#list>
                    },
                    highlight: function(element) {
                        $(element).closest(".form-group").removeClass("has-success").addClass("has-error")
                    },
                    success: function(element) {
                        element.closest(".form-group").removeClass("has-error").addClass("has-success")
                    },
                    errorElement: "span",
                    errorPlacement: function(element, r) {
                        element.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
                    },
                    errorClass: "help-block m-b-none",
                    validClass: "help-block m-b-none",
                    onkeyup:false,
                    submitHandler:function(form){
                        $.ajax({
                            url:window.location.href,
                            dataType:'json',
                            type:'post',
                            data: $('#form1').serialize(),
                            success:function(data){
                                if(data.success){
                                    var index = parent.layer.getFrameIndex(window.name);
                                    parent.$('#search').click();
                                    parent.layer.close(index);
                                    return;
                                }else{
                                    layer.msg(data.message,{icon:2,time:1000});
                                }
                            }
                        })
                    }
                });
            });
        </script>
    </body>
</html>