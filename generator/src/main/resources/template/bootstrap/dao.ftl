package ${names.daoPackage};

import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;
import ${names.domainPackage}.${names.domainClassName};

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * ${table.comment} JPA Dao
 *
 * Date: ${datetime("yyyy-MM-dd HH:mm:ss")}
 *
 * @author Code Generator
 *
 */
public interface ${names.daoClassName} extends EntityJpaDao<${names.domainClassName}, Long> {

}
