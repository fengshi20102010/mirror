<#assign entityContextPath="${configuration.pageMapping}/${names.domainClassName?uncap_first}" />
<#assign entityVariable="${names.domainClassName?uncap_first}" />
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title>魔镜助手 - ${table.comment}详情</title>
        <#noparse>
        <!--[if lt IE 9]>
            <meta http-equiv="refresh" content="0;ie.html" />
        <![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
        </#noparse>
    </head>
    <body>
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="col-sm-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-info-circle"></i> 详情
                        </div>
                        <div class="panel-body">
                        <#list table.columnMetadatas as entity>
                            <#if entity.name?lower_case != 'id' && (configuration.viewColumnsList?size==0 || configuration.viewColumnsList?seq_contains(entity.name?lower_case))>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">${entity.common}</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                    <#if entity.dataType == 2>
                                    <#--日期-->
                                        <fmt:formatDate value="${r"${"}item.${entity.propertyName}}" pattern="yyyy-MM-dd" />
                                    <#elseif entity.dataType == 1 || entity.dataType == 4>
                                    <#--数字-->
                                        <#if entity.options??>
                                        <c:choose>
                                        <#list entity.options?keys as key>
                                            <c:when test="${r"${"}item.${entity.propertyName} == ${key}}">
                                                <span class="label label-info">${entity.options[key]}</span>
                                            </c:when>
                                        </#list>
                                            <c:otherwise>
                                                <span class="label label-info">无数据</span>
                                            </c:otherwise>
                                        </c:choose>
                                        <#else>
                                            ${r"${"}item.${entity.propertyName}}
                                        </#if>
                                    <#else>
                                        ${r"${"}item.${entity.propertyName}}
                                    </#if>
                                    </p>
                                </div>
                            </div>
                            </#if>
                    </#list>
                    </div>
                </div>
            </div>
        </div>
        <#noparse>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
        </#noparse>
        <script>
            require(['jquery', 'layer', 'contabs.min', 'content.min', 'main.min'], function($, layer) {
                //初始化layer
                layer.config({
                    <#noparse>
                    path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
                    </#noparse>
                });
            });
        </script>
    </body>
</html>