package com.mirror.tk.codegenerator.parser;

public interface NamesResolver {

	NamesHold resolve(String tableName);

}
