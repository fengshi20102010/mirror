package com.mirror.tk.codegenerator.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mirror.tk.codegenerator.CodeGeneratorFactory;

public class CodeGeneratorMain {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/applicationContext-main.xml");
		CodeGeneratorFactory codeGeneratorFactory = (CodeGeneratorFactory) context.getBean("codeGeneratorFactory");
		codeGeneratorFactory.generateTable("MERCHANT");
	}

}
