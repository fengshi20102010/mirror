package com.mirror.tk.codegenerator.module;

import com.mirror.tk.codegenerator.GenerateConfiguration;
import com.mirror.tk.codegenerator.GenerateContext;

/**
 * Domain generator
 */
public class DomainModuleGenerator extends FreeMarkerModuleGenerator {

	@Override
	protected String getOutputPath(GenerateContext generateContext, String template) {
		GenerateConfiguration cfg = getGenerateConfiguration();
		String packagePath = getPackagePath(generateContext.getNames().getDomainPackage());
		return cfg.getWorkspace() + "/" + cfg.getCodePath() + "/" + packagePath;
	}

	@Override
	protected String getOutputFile(GenerateContext generateContext, String template) {
		return generateContext.getNames().getDomainClassName() + ".java";
	}

}
