package com.mirror.tk.codegenerator.module;

import com.mirror.tk.codegenerator.GenerateConfiguration;
import com.mirror.tk.codegenerator.GenerateContext;

/**
 * Domain generator
 */
public class ManagerControllerModuleGenerator extends FreeMarkerModuleGenerator {

	@Override
	protected String getOutputPath(GenerateContext generateContext, String temp) {
		GenerateConfiguration cfg = getGenerateConfiguration();
		String packagePath = getPackagePath(generateContext.getNames().getControllerPackage());
		return cfg.getWebWorkspace() + "/" + cfg.getCodePath() + "/" + packagePath;
	}

	@Override
	protected String getOutputFile(GenerateContext generateContext, String temp) {
		return generateContext.getNames().getControllerClassName() + ".java";
	}

}
