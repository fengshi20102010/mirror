package com.mirror.tk.codegenerator.ui.jdbc.dialect;

public interface MetadataDialect {

	String tablesSql();

	String colSql(String tableName);

	String getDataBase();

}
