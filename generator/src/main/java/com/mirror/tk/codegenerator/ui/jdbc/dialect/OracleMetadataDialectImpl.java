package com.mirror.tk.codegenerator.ui.jdbc.dialect;

public class OracleMetadataDialectImpl implements MetadataDialect {
	
	@Override
	public String tablesSql() {
		return "select table_name, comments as table_comment from user_tab_comments";
	}

	@Override
	public String colSql(String tableName) {
		return "select tc.column_name as name, comments, nullable " + "from user_tab_columns tc, user_col_comments cc " + "where cc.table_name = tc.table_name and tc.column_name = cc.column_name and tc.table_name='"	+ tableName + "'";
	}

	@Override
	public String getDataBase() {
		return null;
	}
	
}
