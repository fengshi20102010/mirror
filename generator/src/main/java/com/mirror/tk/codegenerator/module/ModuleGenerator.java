package com.mirror.tk.codegenerator.module;

import com.mirror.tk.codegenerator.GenerateContext;

public interface ModuleGenerator {

	void generate(GenerateContext generateContext);

}
