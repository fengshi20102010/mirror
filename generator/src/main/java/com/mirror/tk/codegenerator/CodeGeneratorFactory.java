package com.mirror.tk.codegenerator;

public interface CodeGeneratorFactory {

	void generateTable(String tableName);

	void generateTables(String... tableNames);

}
