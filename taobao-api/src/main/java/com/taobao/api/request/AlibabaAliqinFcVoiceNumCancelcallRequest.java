package com.taobao.api.request;

import com.taobao.api.internal.util.RequestCheckUtils;
import java.util.Map;

import com.taobao.api.ApiRuleException;
import com.taobao.api.BaseTaobaoRequest;
import com.taobao.api.internal.util.TaobaoHashMap;

import com.taobao.api.response.AlibabaAliqinFcVoiceNumCancelcallResponse;

/**
 * TOP API: alibaba.aliqin.fc.voice.num.cancelcall request
 * 
 * @author top auto create
 * @since 1.0, 2018.07.25
 */
public class AlibabaAliqinFcVoiceNumCancelcallRequest extends BaseTaobaoRequest<AlibabaAliqinFcVoiceNumCancelcallResponse> {
	
	

	/** 
	* 呼叫唯一id
	 */
	private String callId;

	public void setCallId(String callId) {
		this.callId = callId;
	}

	public String getCallId() {
		return this.callId;
	}

	public String getApiMethodName() {
		return "alibaba.aliqin.fc.voice.num.cancelcall";
	}

	public Map<String, String> getTextParams() {		
		TaobaoHashMap txtParams = new TaobaoHashMap();
		txtParams.put("call_id", this.callId);
		if(this.udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public Class<AlibabaAliqinFcVoiceNumCancelcallResponse> getResponseClass() {
		return AlibabaAliqinFcVoiceNumCancelcallResponse.class;
	}

	public void check() throws ApiRuleException {
		RequestCheckUtils.checkNotEmpty(callId, "callId");
	}
	

}