package com.taobao.api.request;

import com.taobao.api.internal.util.RequestCheckUtils;
import java.util.Map;

import com.taobao.api.ApiRuleException;
import com.taobao.api.BaseTaobaoRequest;
import com.taobao.api.internal.util.TaobaoHashMap;

import com.taobao.api.response.BaichuanItemSubscribeRelationQueryResponse;

/**
 * TOP API: taobao.baichuan.item.subscribe.relation.query request
 * 
 * @author top auto create
 * @since 1.0, 2018.07.25
 */
public class BaichuanItemSubscribeRelationQueryRequest extends BaseTaobaoRequest<BaichuanItemSubscribeRelationQueryResponse> {
	
	

	/** 
	* 商品Id
	 */
	private Long itemId;

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public Long getItemId() {
		return this.itemId;
	}

	public String getApiMethodName() {
		return "taobao.baichuan.item.subscribe.relation.query";
	}

	public Map<String, String> getTextParams() {		
		TaobaoHashMap txtParams = new TaobaoHashMap();
		txtParams.put("item_id", this.itemId);
		if(this.udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public Class<BaichuanItemSubscribeRelationQueryResponse> getResponseClass() {
		return BaichuanItemSubscribeRelationQueryResponse.class;
	}

	public void check() throws ApiRuleException {
		RequestCheckUtils.checkNotEmpty(itemId, "itemId");
	}
	

}