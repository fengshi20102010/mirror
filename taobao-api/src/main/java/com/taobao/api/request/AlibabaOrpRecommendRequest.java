package com.taobao.api.request;

import com.taobao.api.internal.util.json.JSONValidatingReader;
import com.taobao.api.internal.util.RequestCheckUtils;
import java.util.Map;

import com.taobao.api.ApiRuleException;
import com.taobao.api.BaseTaobaoRequest;
import com.taobao.api.internal.util.TaobaoHashMap;

import com.taobao.api.response.AlibabaOrpRecommendResponse;

/**
 * TOP API: alibaba.orp.recommend request
 * 
 * @author top auto create
 * @since 1.0, 2018.07.26
 */
public class AlibabaOrpRecommendRequest extends BaseTaobaoRequest<AlibabaOrpRecommendResponse> {
	
	

	/** 
	* 推荐场景ID
	 */
	private Long appid;

	/** 
	* 调用来源,格式TOP_
	 */
	private String callSource;

	/** 
	* 推荐使用的参数
	 */
	private String params;

	/** 
	* 买家数字ID（如果需要）
	 */
	private String userid;

	public void setAppid(Long appid) {
		this.appid = appid;
	}

	public Long getAppid() {
		return this.appid;
	}

	public void setCallSource(String callSource) {
		this.callSource = callSource;
	}

	public String getCallSource() {
		return this.callSource;
	}

	public void setParams(String params) {
		this.params = params;
	}
	public void setParamsString(String params) {
		this.params = params;
	}

	public String getParams() {
		return this.params;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUserid() {
		return this.userid;
	}

	public String getApiMethodName() {
		return "alibaba.orp.recommend";
	}

	public Map<String, String> getTextParams() {		
		TaobaoHashMap txtParams = new TaobaoHashMap();
		txtParams.put("appid", this.appid);
		txtParams.put("call_source", this.callSource);
		txtParams.put("params", this.params);
		txtParams.put("userid", this.userid);
		if(this.udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public Class<AlibabaOrpRecommendResponse> getResponseClass() {
		return AlibabaOrpRecommendResponse.class;
	}

	public void check() throws ApiRuleException {
		RequestCheckUtils.checkNotEmpty(appid, "appid");
		RequestCheckUtils.checkNotEmpty(callSource, "callSource");
	}
	

}