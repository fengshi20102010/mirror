package com.taobao.api.request;

import com.taobao.api.internal.util.RequestCheckUtils;
import java.util.Map;

import com.taobao.api.ApiRuleException;
import com.taobao.api.BaseTaobaoRequest;
import com.taobao.api.internal.util.TaobaoHashMap;

import com.taobao.api.response.BaichuanItemsSubscribeResponse;

/**
 * TOP API: taobao.baichuan.items.subscribe request
 * 
 * @author top auto create
 * @since 1.0, 2018.07.25
 */
public class BaichuanItemsSubscribeRequest extends BaseTaobaoRequest<BaichuanItemsSubscribeResponse> {
	
	

	/** 
	* 订阅的商品id列表
	 */
	private String itemIds;

	public void setItemIds(String itemIds) {
		this.itemIds = itemIds;
	}

	public String getItemIds() {
		return this.itemIds;
	}

	public String getApiMethodName() {
		return "taobao.baichuan.items.subscribe";
	}

	public Map<String, String> getTextParams() {		
		TaobaoHashMap txtParams = new TaobaoHashMap();
		txtParams.put("item_ids", this.itemIds);
		if(this.udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public Class<BaichuanItemsSubscribeResponse> getResponseClass() {
		return BaichuanItemsSubscribeResponse.class;
	}

	public void check() throws ApiRuleException {
		RequestCheckUtils.checkNotEmpty(itemIds, "itemIds");
		RequestCheckUtils.checkMaxListSize(itemIds, 100, "itemIds");
	}
	

}