package com.taobao.api.request;

import com.taobao.api.internal.util.RequestCheckUtils;
import java.util.Map;

import com.taobao.api.ApiRuleException;
import com.taobao.api.BaseTaobaoRequest;
import com.taobao.api.internal.util.TaobaoHashMap;

import com.taobao.api.response.AlibabaAliqinFcVoiceGetdetailResponse;

/**
 * TOP API: alibaba.aliqin.fc.voice.getdetail request
 * 
 * @author top auto create
 * @since 1.0, 2018.07.25
 */
public class AlibabaAliqinFcVoiceGetdetailRequest extends BaseTaobaoRequest<AlibabaAliqinFcVoiceGetdetailResponse> {
	
	

	/** 
	* 呼叫唯一ID
	 */
	private String callId;

	/** 
	* 语音通知为:11000000300006, 语音验证码为:11010000138001, IVR为:11000000300005, 点击拨号为:11000000300004, SIP为:11000000300009
	 */
	private Long prodId;

	/** 
	* Unix时间戳，会查询这个时间点对应那一天的记录（单位毫秒）
	 */
	private Long queryDate;

	public void setCallId(String callId) {
		this.callId = callId;
	}

	public String getCallId() {
		return this.callId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getProdId() {
		return this.prodId;
	}

	public void setQueryDate(Long queryDate) {
		this.queryDate = queryDate;
	}

	public Long getQueryDate() {
		return this.queryDate;
	}

	public String getApiMethodName() {
		return "alibaba.aliqin.fc.voice.getdetail";
	}

	public Map<String, String> getTextParams() {		
		TaobaoHashMap txtParams = new TaobaoHashMap();
		txtParams.put("call_id", this.callId);
		txtParams.put("prod_id", this.prodId);
		txtParams.put("query_date", this.queryDate);
		if(this.udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public Class<AlibabaAliqinFcVoiceGetdetailResponse> getResponseClass() {
		return AlibabaAliqinFcVoiceGetdetailResponse.class;
	}

	public void check() throws ApiRuleException {
		RequestCheckUtils.checkNotEmpty(callId, "callId");
		RequestCheckUtils.checkNotEmpty(prodId, "prodId");
		RequestCheckUtils.checkNotEmpty(queryDate, "queryDate");
	}
	

}