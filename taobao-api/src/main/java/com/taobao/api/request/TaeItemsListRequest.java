package com.taobao.api.request;

import com.taobao.api.internal.util.RequestCheckUtils;
import java.util.Map;

import com.taobao.api.ApiRuleException;
import com.taobao.api.BaseTaobaoRequest;
import com.taobao.api.internal.util.TaobaoHashMap;

import com.taobao.api.response.TaeItemsListResponse;

/**
 * TOP API: taobao.tae.items.list request
 * 
 * @author top auto create
 * @since 1.0, 2018.07.26
 */
public class TaeItemsListRequest extends BaseTaobaoRequest<TaeItemsListResponse> {
	
	

	/** 
	* 返回值中需要的字段. 可选值 title,nick,pic_url,location,cid,price,post_fee,promoted_service,ju,shop_name字段间用 (,) 逗号分隔
	 */
	private String fields;

	/** 
	* 商品ID，英文逗号(,)分隔，最多50个。优先级低于open_iid，open_iids存在的话，则忽略本参数
	 */
	private String numIids;

	/** 
	* 商品混淆ID，英文逗号(,)分隔，最多50个。优先级高于open_iid，本参数存在的话，则忽略num_iids
	 */
	private String openIids;

	public void setFields(String fields) {
		this.fields = fields;
	}

	public String getFields() {
		return this.fields;
	}

	public void setNumIids(String numIids) {
		this.numIids = numIids;
	}

	public String getNumIids() {
		return this.numIids;
	}

	public void setOpenIids(String openIids) {
		this.openIids = openIids;
	}

	public String getOpenIids() {
		return this.openIids;
	}

	public String getApiMethodName() {
		return "taobao.tae.items.list";
	}

	public Map<String, String> getTextParams() {		
		TaobaoHashMap txtParams = new TaobaoHashMap();
		txtParams.put("fields", this.fields);
		txtParams.put("num_iids", this.numIids);
		txtParams.put("open_iids", this.openIids);
		if(this.udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public Class<TaeItemsListResponse> getResponseClass() {
		return TaeItemsListResponse.class;
	}

	public void check() throws ApiRuleException {
		RequestCheckUtils.checkNotEmpty(fields, "fields");
		RequestCheckUtils.checkMaxLength(openIids, 3000, "openIids");
	}
	

}