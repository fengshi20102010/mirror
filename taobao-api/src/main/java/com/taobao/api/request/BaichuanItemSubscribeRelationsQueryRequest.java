package com.taobao.api.request;

import java.util.Date;
import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.TaobaoObject;
import java.util.Map;

import com.taobao.api.ApiRuleException;
import com.taobao.api.BaseTaobaoRequest;
import com.taobao.api.internal.util.TaobaoHashMap;
import com.taobao.api.internal.util.json.JSONWriter;
import com.taobao.api.response.BaichuanItemSubscribeRelationsQueryResponse;

/**
 * TOP API: taobao.baichuan.item.subscribe.relations.query request
 * 
 * @author top auto create
 * @since 1.0, 2018.07.25
 */
public class BaichuanItemSubscribeRelationsQueryRequest extends BaseTaobaoRequest<BaichuanItemSubscribeRelationsQueryResponse> {
	
	

	/** 
	* 查询条件
	 */
	private String condition;

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public void setCondition(Condition condition) {
		this.condition = new JSONWriter(false,true).write(condition);
	}

	public String getCondition() {
		return this.condition;
	}

	public String getApiMethodName() {
		return "taobao.baichuan.item.subscribe.relations.query";
	}

	public Map<String, String> getTextParams() {		
		TaobaoHashMap txtParams = new TaobaoHashMap();
		txtParams.put("condition", this.condition);
		if(this.udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public Class<BaichuanItemSubscribeRelationsQueryResponse> getResponseClass() {
		return BaichuanItemSubscribeRelationsQueryResponse.class;
	}

	public void check() throws ApiRuleException {
	}
	
	/**
 * 查询条件
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class Condition extends TaobaoObject {

	private static final long serialVersionUID = 8123171393428485171L;

	/**
		 * 结束时间
		 */
		@ApiField("end_time")
		private Date endTime;
		/**
		 * 商品状态
		 */
		@ApiField("item_status")
		private Long itemStatus;
		/**
		 * 查询个数，有数量限制，超过最大值报错
		 */
		@ApiField("limit")
		private Long limit;
		/**
		 * 查询的起始id，如果要连续的分页查询，第n+1页的查询输入是第n页查询结果中最大id+1
		 */
		@ApiField("start_id")
		private Long startId;
		/**
		 * 开始时间
		 */
		@ApiField("start_time")
		private Date startTime;
	

	public Date getEndTime() {
			return this.endTime;
		}
		public void setEndTime(Date endTime) {
			this.endTime = endTime;
		}
		public Long getItemStatus() {
			return this.itemStatus;
		}
		public void setItemStatus(Long itemStatus) {
			this.itemStatus = itemStatus;
		}
		public Long getLimit() {
			return this.limit;
		}
		public void setLimit(Long limit) {
			this.limit = limit;
		}
		public Long getStartId() {
			return this.startId;
		}
		public void setStartId(Long startId) {
			this.startId = startId;
		}
		public Date getStartTime() {
			return this.startTime;
		}
		public void setStartTime(Date startTime) {
			this.startTime = startTime;
		}

}


}