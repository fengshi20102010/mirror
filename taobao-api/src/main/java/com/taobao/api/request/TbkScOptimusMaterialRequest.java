package com.taobao.api.request;

import com.taobao.api.internal.util.RequestCheckUtils;
import java.util.Map;

import com.taobao.api.ApiRuleException;
import com.taobao.api.BaseTaobaoRequest;
import com.taobao.api.internal.util.TaobaoHashMap;

import com.taobao.api.response.TbkScOptimusMaterialResponse;

/**
 * TOP API: taobao.tbk.sc.optimus.material request
 * 
 * @author top auto create
 * @since 1.0, 2018.08.17
 */
public class TbkScOptimusMaterialRequest extends BaseTaobaoRequest<TbkScOptimusMaterialResponse> {
	
	

	/** 
	* mm_xxx_xxx_xxx的第三位
	 */
	private Long adzoneId;

	/** 
	* 设备号加密类型：MD5
	 */
	private String deviceEncrypt;

	/** 
	* 设备号加密后的值
	 */
	private String deviceType;

	/** 
	* 设备号类型：IMEI，或者IDFA，或者UTDID
	 */
	private String deviceValue;

	/** 
	* 官方的物料Id(详细物料id见：https://tbk.bbs.taobao.com/detail.html?appId=45301&postId=8576096)
	 */
	private Long materialId;

	/** 
	* 第几页，默认：1
	 */
	private Long pageNo;

	/** 
	* 页大小，默认20，1~100
	 */
	private Long pageSize;

	/** 
	* mm_xxx_xxx_xxx的第二位
	 */
	private Long siteId;

	public void setAdzoneId(Long adzoneId) {
		this.adzoneId = adzoneId;
	}

	public Long getAdzoneId() {
		return this.adzoneId;
	}

	public void setDeviceEncrypt(String deviceEncrypt) {
		this.deviceEncrypt = deviceEncrypt;
	}

	public String getDeviceEncrypt() {
		return this.deviceEncrypt;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceType() {
		return this.deviceType;
	}

	public void setDeviceValue(String deviceValue) {
		this.deviceValue = deviceValue;
	}

	public String getDeviceValue() {
		return this.deviceValue;
	}

	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}

	public Long getMaterialId() {
		return this.materialId;
	}

	public void setPageNo(Long pageNo) {
		this.pageNo = pageNo;
	}

	public Long getPageNo() {
		return this.pageNo;
	}

	public void setPageSize(Long pageSize) {
		this.pageSize = pageSize;
	}

	public Long getPageSize() {
		return this.pageSize;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public Long getSiteId() {
		return this.siteId;
	}

	public String getApiMethodName() {
		return "taobao.tbk.sc.optimus.material";
	}

	public Map<String, String> getTextParams() {		
		TaobaoHashMap txtParams = new TaobaoHashMap();
		txtParams.put("adzone_id", this.adzoneId);
		txtParams.put("device_encrypt", this.deviceEncrypt);
		txtParams.put("device_type", this.deviceType);
		txtParams.put("device_value", this.deviceValue);
		txtParams.put("material_id", this.materialId);
		txtParams.put("page_no", this.pageNo);
		txtParams.put("page_size", this.pageSize);
		txtParams.put("site_id", this.siteId);
		if(this.udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public Class<TbkScOptimusMaterialResponse> getResponseClass() {
		return TbkScOptimusMaterialResponse.class;
	}

	public void check() throws ApiRuleException {
		RequestCheckUtils.checkNotEmpty(adzoneId, "adzoneId");
		RequestCheckUtils.checkMaxValue(pageSize, 100L, "pageSize");
		RequestCheckUtils.checkMinValue(pageSize, 1L, "pageSize");
		RequestCheckUtils.checkNotEmpty(siteId, "siteId");
	}
	

}