package com.taobao.api.request;

import java.util.Map;

import com.taobao.api.ApiRuleException;
import com.taobao.api.BaseTaobaoRequest;
import com.taobao.api.internal.util.TaobaoHashMap;

import com.taobao.api.response.BaichuanItemSubscribeDailyLeftQueryResponse;

/**
 * TOP API: taobao.baichuan.item.subscribe.daily.left.query request
 * 
 * @author top auto create
 * @since 1.0, 2018.07.25
 */
public class BaichuanItemSubscribeDailyLeftQueryRequest extends BaseTaobaoRequest<BaichuanItemSubscribeDailyLeftQueryResponse> {
	
	

	public String getApiMethodName() {
		return "taobao.baichuan.item.subscribe.daily.left.query";
	}

	public Map<String, String> getTextParams() {		
		TaobaoHashMap txtParams = new TaobaoHashMap();
		if(this.udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public Class<BaichuanItemSubscribeDailyLeftQueryResponse> getResponseClass() {
		return BaichuanItemSubscribeDailyLeftQueryResponse.class;
	}

	public void check() throws ApiRuleException {
	}
	

}