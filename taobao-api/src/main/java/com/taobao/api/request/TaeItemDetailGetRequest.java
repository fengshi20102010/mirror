package com.taobao.api.request;

import com.taobao.api.internal.util.RequestCheckUtils;
import java.util.Map;

import com.taobao.api.ApiRuleException;
import com.taobao.api.BaseTaobaoRequest;
import com.taobao.api.internal.util.TaobaoHashMap;

import com.taobao.api.response.TaeItemDetailGetResponse;

/**
 * TOP API: taobao.tae.item.detail.get request
 * 
 * @author top auto create
 * @since 1.0, 2018.07.26
 */
public class TaeItemDetailGetRequest extends BaseTaobaoRequest<TaeItemDetailGetResponse> {
	
	

	/** 
	* 用户所在位置ip
	 */
	private String buyerIp;

	/** 
	* 区块信息，field 支持 itemInfo,priceInfo,skuInfo,stockInfo,rateInfo,descInfo,sellerInfo,mobileDescInfo,deliveryInfo,storeInfo,itemBuyInfo,couponInfo
	 */
	private String fields;

	/** 
	* 商品open_iid
	 */
	private String id;

	/** 
	* 商品open_iid
	 */
	private String openIid;

	public void setBuyerIp(String buyerIp) {
		this.buyerIp = buyerIp;
	}

	public String getBuyerIp() {
		return this.buyerIp;
	}

	public void setFields(String fields) {
		this.fields = fields;
	}

	public String getFields() {
		return this.fields;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return this.id;
	}

	public void setOpenIid(String openIid) {
		this.openIid = openIid;
	}

	public String getOpenIid() {
		return this.openIid;
	}

	public String getApiMethodName() {
		return "taobao.tae.item.detail.get";
	}

	public Map<String, String> getTextParams() {		
		TaobaoHashMap txtParams = new TaobaoHashMap();
		txtParams.put("buyer_ip", this.buyerIp);
		txtParams.put("fields", this.fields);
		txtParams.put("id", this.id);
		txtParams.put("open_iid", this.openIid);
		if(this.udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public Class<TaeItemDetailGetResponse> getResponseClass() {
		return TaeItemDetailGetResponse.class;
	}

	public void check() throws ApiRuleException {
		RequestCheckUtils.checkNotEmpty(fields, "fields");
	}
	

}