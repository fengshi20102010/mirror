package com.taobao.api.request;

import com.taobao.api.internal.util.RequestCheckUtils;
import java.util.Map;

import com.taobao.api.ApiRuleException;
import com.taobao.api.BaseTaobaoRequest;
import com.taobao.api.internal.util.TaobaoHashMap;

import com.taobao.api.response.TaeExistShopCouponGetResponse;

/**
 * TOP API: taobao.tae.exist.shop.coupon.get request
 * 
 * @author top auto create
 * @since 1.0, 2018.07.26
 */
public class TaeExistShopCouponGetRequest extends BaseTaobaoRequest<TaeExistShopCouponGetResponse> {
	
	

	/** 
	* 卖家昵称
	 */
	private String sellerNick;

	public void setSellerNick(String sellerNick) {
		this.sellerNick = sellerNick;
	}

	public String getSellerNick() {
		return this.sellerNick;
	}

	public String getApiMethodName() {
		return "taobao.tae.exist.shop.coupon.get";
	}

	public Map<String, String> getTextParams() {		
		TaobaoHashMap txtParams = new TaobaoHashMap();
		txtParams.put("seller_nick", this.sellerNick);
		if(this.udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public Class<TaeExistShopCouponGetResponse> getResponseClass() {
		return TaeExistShopCouponGetResponse.class;
	}

	public void check() throws ApiRuleException {
		RequestCheckUtils.checkNotEmpty(sellerNick, "sellerNick");
	}
	

}