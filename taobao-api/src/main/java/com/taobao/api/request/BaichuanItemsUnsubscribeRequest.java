package com.taobao.api.request;

import com.taobao.api.internal.util.RequestCheckUtils;
import java.util.Map;

import com.taobao.api.ApiRuleException;
import com.taobao.api.BaseTaobaoRequest;
import com.taobao.api.internal.util.TaobaoHashMap;

import com.taobao.api.response.BaichuanItemsUnsubscribeResponse;

/**
 * TOP API: taobao.baichuan.items.unsubscribe request
 * 
 * @author top auto create
 * @since 1.0, 2018.07.25
 */
public class BaichuanItemsUnsubscribeRequest extends BaseTaobaoRequest<BaichuanItemsUnsubscribeResponse> {
	
	

	/** 
	* 删除的商品id
	 */
	private String itemIds;

	public void setItemIds(String itemIds) {
		this.itemIds = itemIds;
	}

	public String getItemIds() {
		return this.itemIds;
	}

	public String getApiMethodName() {
		return "taobao.baichuan.items.unsubscribe";
	}

	public Map<String, String> getTextParams() {		
		TaobaoHashMap txtParams = new TaobaoHashMap();
		txtParams.put("item_ids", this.itemIds);
		if(this.udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public Class<BaichuanItemsUnsubscribeResponse> getResponseClass() {
		return BaichuanItemsUnsubscribeResponse.class;
	}

	public void check() throws ApiRuleException {
		RequestCheckUtils.checkNotEmpty(itemIds, "itemIds");
		RequestCheckUtils.checkMaxListSize(itemIds, 100, "itemIds");
	}
	

}