package com.taobao.api.request;

import com.taobao.api.internal.util.RequestCheckUtils;
import java.util.Map;

import com.taobao.api.ApiRuleException;
import com.taobao.api.BaseTaobaoRequest;
import com.taobao.api.internal.util.TaobaoHashMap;

import com.taobao.api.response.AlibabaAliqinFcVoiceRecordGeturlResponse;

/**
 * TOP API: alibaba.aliqin.fc.voice.record.geturl request
 * 
 * @author top auto create
 * @since 1.0, 2018.07.25
 */
public class AlibabaAliqinFcVoiceRecordGeturlRequest extends BaseTaobaoRequest<AlibabaAliqinFcVoiceRecordGeturlResponse> {
	
	

	/** 
	* 一次通话的唯一标识id
	 */
	private String callId;

	public void setCallId(String callId) {
		this.callId = callId;
	}

	public String getCallId() {
		return this.callId;
	}

	public String getApiMethodName() {
		return "alibaba.aliqin.fc.voice.record.geturl";
	}

	public Map<String, String> getTextParams() {		
		TaobaoHashMap txtParams = new TaobaoHashMap();
		txtParams.put("call_id", this.callId);
		if(this.udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public Class<AlibabaAliqinFcVoiceRecordGeturlResponse> getResponseClass() {
		return AlibabaAliqinFcVoiceRecordGeturlResponse.class;
	}

	public void check() throws ApiRuleException {
		RequestCheckUtils.checkNotEmpty(callId, "callId");
	}
	

}