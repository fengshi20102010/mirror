package com.taobao.api.domain;

import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.TaobaoObject;


/**
 * 运费信息
 *
 * @author top auto create
 * @since 1.0, null
 */
public class Carriage extends TaobaoObject {

	private static final long serialVersionUID = 3222774668948571439L;

	/**
	 * 运费
	 */
	@ApiField("name")
	private String name;

	/**
	 * 快递公司
	 */
	@ApiField("price")
	private String price;


	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return this.price;
	}
	public void setPrice(String price) {
		this.price = price;
	}

}
