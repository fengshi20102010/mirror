package com.taobao.api.domain;

import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.TaobaoObject;


/**
 * 商品购买约束
 *
 * @author top auto create
 * @since 1.0, null
 */
public class ItemBuyInfo extends TaobaoObject {

	private static final long serialVersionUID = 2128638545519885722L;

	/**
	 * 是否支持购物车
	 */
	@ApiField("cart_support")
	private String cartSupport;


	public String getCartSupport() {
		return this.cartSupport;
	}
	public void setCartSupport(String cartSupport) {
		this.cartSupport = cartSupport;
	}

}
