package com.taobao.api.domain;

import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.TaobaoObject;


/**
 * 属性sku映射
 *
 * @author top auto create
 * @since 1.0, null
 */
public class PvMapSku extends TaobaoObject {

	private static final long serialVersionUID = 5636374932735695377L;

	/**
	 * 用户选择的属性对
	 */
	@ApiField("pv")
	private String pv;

	/**
	 * sku id
	 */
	@ApiField("sku_id")
	private String skuId;


	public String getPv() {
		return this.pv;
	}
	public void setPv(String pv) {
		this.pv = pv;
	}

	public String getSkuId() {
		return this.skuId;
	}
	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}

}
