package com.taobao.api.domain;

import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.TaobaoObject;


/**
 * 价格单元
 *
 * @author top auto create
 * @since 1.0, null
 */
public class PriceUnit extends TaobaoObject {

	private static final long serialVersionUID = 1122937544351276615L;

	/**
	 * 价格Label
	 */
	@ApiField("name")
	private String name;

	/**
	 * 售卖价格,单位元
	 */
	@ApiField("price")
	private String price;

	/**
	 * price后面的tips
	 */
	@ApiField("tips")
	private String tips;


	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return this.price;
	}
	public void setPrice(String price) {
		this.price = price;
	}

	public String getTips() {
		return this.tips;
	}
	public void setTips(String tips) {
		this.tips = tips;
	}

}
