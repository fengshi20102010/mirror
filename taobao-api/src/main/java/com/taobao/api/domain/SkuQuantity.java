package com.taobao.api.domain;

import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.TaobaoObject;


/**
 * sku库存
 *
 * @author top auto create
 * @since 1.0, null
 */
public class SkuQuantity extends TaobaoObject {

	private static final long serialVersionUID = 2589991586955592954L;

	/**
	 * 库存数
	 */
	@ApiField("quantity")
	private String quantity;

	/**
	 * sku id
	 */
	@ApiField("sku_id")
	private String skuId;


	public String getQuantity() {
		return this.quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getSkuId() {
		return this.skuId;
	}
	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}

}
