package com.taobao.api.domain;

import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.TaobaoObject;


/**
 * 优惠卷信息
 *
 * @author top auto create
 * @since 1.0, null
 */
public class ShopCouponInfo extends TaobaoObject {

	private static final long serialVersionUID = 3614159965473195132L;

	/**
	 * true|false 是否有优惠卷
	 */
	@ApiField("shop_coupon")
	private String shopCoupon;


	public String getShopCoupon() {
		return this.shopCoupon;
	}
	public void setShopCoupon(String shopCoupon) {
		this.shopCoupon = shopCoupon;
	}

}
