package com.taobao.api.domain;

import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.TaobaoObject;


/**
 * 无线描述片段
 *
 * @author top auto create
 * @since 1.0, null
 */
public class DescFragment extends TaobaoObject {

	private static final long serialVersionUID = 5789552873134132582L;

	/**
	 * 内容体目前支持支持图片url
	 */
	@ApiField("content")
	private String content;

	/**
	 * 内容label，目前支持支持图片txt,img
	 */
	@ApiField("label")
	private String label;


	public String getContent() {
		return this.content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	public String getLabel() {
		return this.label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

}
