package com.taobao.api.domain;

import com.taobao.api.internal.util.json.JSONValidatingReader;
import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.TaobaoObject;


/**
 * token info中的扩展字段
 *
 * @author top auto create
 * @since 1.0, null
 */
public class TokenInfoExt extends TaobaoObject {

	private static final long serialVersionUID = 3299897457485738819L;

	/**
	 * 授权登录后返回的信息
	 */
	@ApiField("oauth_other_info")
	private String oauthOtherInfo;

	/**
	 * open account当前token info中open account id对应的open account信息
	 */
	@ApiField("open_account")
	private OpenAccount openAccount;


	public String getOauthOtherInfo() {
		return this.oauthOtherInfo;
	}
	public void setOauthOtherInfo(String oauthOtherInfo) {
		this.oauthOtherInfo = oauthOtherInfo;
	}
	public void setOauthOtherInfoString(String oauthOtherInfo) {
		this.oauthOtherInfo = oauthOtherInfo;
	}
	

	public OpenAccount getOpenAccount() {
		return this.openAccount;
	}
	public void setOpenAccount(OpenAccount openAccount) {
		this.openAccount = openAccount;
	}

}
