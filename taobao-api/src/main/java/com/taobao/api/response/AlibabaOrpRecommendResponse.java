package com.taobao.api.response;

import com.taobao.api.internal.mapping.ApiField;

import com.taobao.api.TaobaoResponse;

/**
 * TOP API: alibaba.orp.recommend response.
 * 
 * @author top auto create
 * @since 1.0, null
 */
public class AlibabaOrpRecommendResponse extends TaobaoResponse {

	private static final long serialVersionUID = 4893823651235929793L;

	/** 
	 * {
itemId: "商品id",
itemName: "商品标题",
price: "商品标价",
promotionPrice: "商品优惠价",
categoryId: "商品叶子类目id",
url: "商品url",
pic: "商品图片地址"
}
	 */
	@ApiField("recommend")
	private String recommend;


	public void setRecommend(String recommend) {
		this.recommend = recommend;
	}
	public String getRecommend( ) {
		return this.recommend;
	}
	


}
