package com.taobao.api.response;

import com.taobao.api.internal.mapping.ApiField;

import com.taobao.api.TaobaoResponse;

/**
 * TOP API: taobao.tae.exist.shop.coupon.get response.
 * 
 * @author top auto create
 * @since 1.0, null
 */
public class TaeExistShopCouponGetResponse extends TaobaoResponse {

	private static final long serialVersionUID = 6228477298841359896L;

	/** 
	 * true:存在店铺优惠券；
false:不存在店铺优惠券；
	 */
	@ApiField("is_exist")
	private Boolean isExist;


	public void setIsExist(Boolean isExist) {
		this.isExist = isExist;
	}
	public Boolean getIsExist( ) {
		return this.isExist;
	}
	


}
