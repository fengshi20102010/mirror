package com.taobao.api.response;

import java.util.List;
import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.TaobaoObject;
import com.taobao.api.internal.mapping.ApiListField;

import com.taobao.api.TaobaoResponse;

/**
 * TOP API: taobao.baichuan.item.subscribe.daily.left.query response.
 * 
 * @author top auto create
 * @since 1.0, null
 */
public class BaichuanItemSubscribeDailyLeftQueryResponse extends TaobaoResponse {

	private static final long serialVersionUID = 3115111222839618956L;

	/** 
	 * 接口返回model
	 */
	@ApiField("result")
	private Result result;


	public void setResult(Result result) {
		this.result = result;
	}
	public Result getResult( ) {
		return this.result;
	}
	
	/**
 * 数据
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class ResultData extends TaobaoObject {

	private static final long serialVersionUID = 1234754652532853258L;

	/**
		 * 返回值个数
		 */
		@ApiField("count")
		private Long count;
		/**
		 * 具体内容
		 */
		@ApiListField("data_list")
		@ApiField("number")
		private List<Long> dataList;
	

	public Long getCount() {
			return this.count;
		}
		public void setCount(Long count) {
			this.count = count;
		}
		public List<Long> getDataList() {
			return this.dataList;
		}
		public void setDataList(List<Long> dataList) {
			this.dataList = dataList;
		}

}

	/**
 * 返回
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class ResultMeta extends TaobaoObject {

	private static final long serialVersionUID = 7424123896381615339L;

	/**
		 * 返回码
		 */
		@ApiField("code")
		private Long code;
		/**
		 * 数据
		 */
		@ApiField("data")
		private ResultData data;
		/**
		 * 返回码对应的文案
		 */
		@ApiField("msg")
		private String msg;
	

	public Long getCode() {
			return this.code;
		}
		public void setCode(Long code) {
			this.code = code;
		}
		public ResultData getData() {
			return this.data;
		}
		public void setData(ResultData data) {
			this.data = data;
		}
		public String getMsg() {
			return this.msg;
		}
		public void setMsg(String msg) {
			this.msg = msg;
		}

}

	/**
 * 接口返回model
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class Result extends TaobaoObject {

	private static final long serialVersionUID = 2673762464378396116L;

	/**
		 * 返回
		 */
		@ApiListField("result_list")
		@ApiField("result_meta")
		private List<ResultMeta> resultList;
	

	public List<ResultMeta> getResultList() {
			return this.resultList;
		}
		public void setResultList(List<ResultMeta> resultList) {
			this.resultList = resultList;
		}

}



}
