package com.taobao.api.response;

import com.taobao.api.domain.ItemDetailData;
import com.taobao.api.internal.mapping.ApiField;

import com.taobao.api.TaobaoResponse;

/**
 * TOP API: taobao.tae.item.detail.get response.
 * 
 * @author top auto create
 * @since 1.0, null
 */
public class TaeItemDetailGetResponse extends TaobaoResponse {

	private static final long serialVersionUID = 7897113116469276144L;

	/** 
	 * 详情业务模块数据
	 */
	@ApiField("data")
	private ItemDetailData data;


	public void setData(ItemDetailData data) {
		this.data = data;
	}
	public ItemDetailData getData( ) {
		return this.data;
	}
	


}
