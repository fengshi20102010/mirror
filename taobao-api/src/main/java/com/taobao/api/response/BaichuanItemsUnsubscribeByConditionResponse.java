package com.taobao.api.response;

import java.util.List;
import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.TaobaoObject;
import com.taobao.api.internal.mapping.ApiListField;

import com.taobao.api.TaobaoResponse;

/**
 * TOP API: taobao.baichuan.items.unsubscribe.by.condition response.
 * 
 * @author top auto create
 * @since 1.0, null
 */
public class BaichuanItemsUnsubscribeByConditionResponse extends TaobaoResponse {

	private static final long serialVersionUID = 2828291845286117281L;

	/** 
	 * 接口返回model
	 */
	@ApiField("result")
	private Result result;


	public void setResult(Result result) {
		this.result = result;
	}
	public Result getResult( ) {
		return this.result;
	}
	
	/**
 * 返回的结果
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class ResultData extends TaobaoObject {

	private static final long serialVersionUID = 3642525724569444635L;

	/**
		 * 删除订阅关系的个数，由于接口有数量限制，故 根据count==0来判断是否全部删除完毕
		 */
		@ApiField("count")
		private Long count;
		/**
		 * 具体的商品id
		 */
		@ApiListField("data_list")
		@ApiField("number")
		private List<Long> dataList;
	

	public Long getCount() {
			return this.count;
		}
		public void setCount(Long count) {
			this.count = count;
		}
		public List<Long> getDataList() {
			return this.dataList;
		}
		public void setDataList(List<Long> dataList) {
			this.dataList = dataList;
		}

}

	/**
 * 分返回码返回结果
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class ResultMeta extends TaobaoObject {

	private static final long serialVersionUID = 6845271881132122475L;

	/**
		 * 返回码
		 */
		@ApiField("code")
		private Long code;
		/**
		 * 返回的结果
		 */
		@ApiField("data")
		private ResultData data;
		/**
		 * 返回码对应文案
		 */
		@ApiField("msg")
		private String msg;
	

	public Long getCode() {
			return this.code;
		}
		public void setCode(Long code) {
			this.code = code;
		}
		public ResultData getData() {
			return this.data;
		}
		public void setData(ResultData data) {
			this.data = data;
		}
		public String getMsg() {
			return this.msg;
		}
		public void setMsg(String msg) {
			this.msg = msg;
		}

}

	/**
 * 接口返回model
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class Result extends TaobaoObject {

	private static final long serialVersionUID = 2742217478359715792L;

	/**
		 * 分返回码返回结果
		 */
		@ApiListField("result_list")
		@ApiField("result_meta")
		private List<ResultMeta> resultList;
	

	public List<ResultMeta> getResultList() {
			return this.resultList;
		}
		public void setResultList(List<ResultMeta> resultList) {
			this.resultList = resultList;
		}

}



}
