package com.taobao.api.response;

import java.util.List;
import java.util.Date;
import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.TaobaoObject;
import com.taobao.api.internal.mapping.ApiListField;

import com.taobao.api.TaobaoResponse;

/**
 * TOP API: taobao.baichuan.item.subscribe.relations.query response.
 * 
 * @author top auto create
 * @since 1.0, null
 */
public class BaichuanItemSubscribeRelationsQueryResponse extends TaobaoResponse {

	private static final long serialVersionUID = 7332354288225595629L;

	/** 
	 * 接口返回model
	 */
	@ApiField("result")
	private Result result;


	public void setResult(Result result) {
		this.result = result;
	}
	public Result getResult( ) {
		return this.result;
	}
	
	/**
 * 详细列表
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class IsvItemSubDo extends TaobaoObject {

	private static final long serialVersionUID = 7515842115685469544L;

	/**
		 * 添加时间
		 */
		@ApiField("add_time")
		private Date addTime;
		/**
		 * 数据库id索引
		 */
		@ApiField("id")
		private Long id;
		/**
		 * 商品id
		 */
		@ApiField("item_id")
		private Long itemId;
		
	

	public Date getAddTime() {
			return this.addTime;
		}
		public void setAddTime(Date addTime) {
			this.addTime = addTime;
		}
		public Long getId() {
			return this.id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public Long getItemId() {
			return this.itemId;
		}
		public void setItemId(Long itemId) {
			this.itemId = itemId;
		}
		

}

	/**
 * 返回详细内容
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class ResultData extends TaobaoObject {

	private static final long serialVersionUID = 7495457248871957779L;

	/**
		 * 数量，由于接口有一次查询的限制，故判断是否全部查询出来 应该看count是否为0
		 */
		@ApiField("count")
		private Long count;
		/**
		 * 详细列表
		 */
		@ApiListField("data_list")
		@ApiField("isv_item_sub_do")
		private List<IsvItemSubDo> dataList;
	

	public Long getCount() {
			return this.count;
		}
		public void setCount(Long count) {
			this.count = count;
		}
		public List<IsvItemSubDo> getDataList() {
			return this.dataList;
		}
		public void setDataList(List<IsvItemSubDo> dataList) {
			this.dataList = dataList;
		}

}

	/**
 * 返回的list
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class ResultMeta extends TaobaoObject {

	private static final long serialVersionUID = 3781543258965614376L;

	/**
		 * 返回码
		 */
		@ApiField("code")
		private Long code;
		/**
		 * 返回详细内容
		 */
		@ApiField("data")
		private ResultData data;
		/**
		 * 返回码文案
		 */
		@ApiField("msg")
		private String msg;
	

	public Long getCode() {
			return this.code;
		}
		public void setCode(Long code) {
			this.code = code;
		}
		public ResultData getData() {
			return this.data;
		}
		public void setData(ResultData data) {
			this.data = data;
		}
		public String getMsg() {
			return this.msg;
		}
		public void setMsg(String msg) {
			this.msg = msg;
		}

}

	/**
 * 接口返回model
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class Result extends TaobaoObject {

	private static final long serialVersionUID = 7592444577312486549L;

	/**
		 * 返回的list
		 */
		@ApiListField("result_list")
		@ApiField("result_meta")
		private List<ResultMeta> resultList;
	

	public List<ResultMeta> getResultList() {
			return this.resultList;
		}
		public void setResultList(List<ResultMeta> resultList) {
			this.resultList = resultList;
		}

}



}
