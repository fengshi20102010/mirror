package com.taobao.api.response;

import com.taobao.api.internal.util.json.JSONValidatingReader;
import java.util.List;
import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.TaobaoObject;
import com.taobao.api.internal.mapping.ApiListField;

import com.taobao.api.TaobaoResponse;

/**
 * TOP API: taobao.tbk.dg.material.optional response.
 * 
 * @author top auto create
 * @since 1.0, null
 */
public class TbkDgMaterialOptionalResponse extends TaobaoResponse {

	private static final long serialVersionUID = 1153188624382328133L;

	/** 
	 * resultList
	 */
	@ApiListField("result_list")
	@ApiField("map_data")
	private List<MapData> resultList;

	/** 
	 * 搜索到符合条件的结果总数
	 */
	@ApiField("total_results")
	private Long totalResults;


	public void setResultList(List<MapData> resultList) {
		this.resultList = resultList;
	}
	public List<MapData> getResultList( ) {
		return this.resultList;
	}

	public void setTotalResults(Long totalResults) {
		this.totalResults = totalResults;
	}
	public Long getTotalResults( ) {
		return this.totalResults;
	}
	
	/**
 * resultList
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class MapData extends TaobaoObject {

	private static final long serialVersionUID = 7816445429821711126L;

	/**
		 * 叶子类目id
		 */
		@ApiField("category_id")
		private Long categoryId;
		/**
		 * 叶子类目名称
		 */
		@ApiField("category_name")
		private String categoryName;
		/**
		 * 佣金比率
		 */
		@ApiField("commission_rate")
		private String commissionRate;
		/**
		 * 佣金类型
		 */
		@ApiField("commission_type")
		private String commissionType;
		/**
		 * 优惠券结束时间
		 */
		@ApiField("coupon_end_time")
		private String couponEndTime;
		/**
		 * 优惠券id
		 */
		@ApiField("coupon_id")
		private String couponId;
		/**
		 * 优惠券面额
		 */
		@ApiField("coupon_info")
		private String couponInfo;
		/**
		 * 优惠券剩余量
		 */
		@ApiField("coupon_remain_count")
		private Long couponRemainCount;
		/**
		 * 券二合一页面链接
		 */
		@ApiField("coupon_share_url")
		private String couponShareUrl;
		/**
		 * 优惠券开始时间
		 */
		@ApiField("coupon_start_time")
		private String couponStartTime;
		/**
		 * 优惠券总量
		 */
		@ApiField("coupon_total_count")
		private Long couponTotalCount;
		/**
		 * 是否包含定向计划
		 */
		@ApiField("include_dxjh")
		private String includeDxjh;
		/**
		 * 是否包含营销计划
		 */
		@ApiField("include_mkt")
		private String includeMkt;
		/**
		 * 定向计划信息
		 */
		@ApiField("info_dxjh")
		private String infoDxjh;
		/**
		 * 商品地址
		 */
		@ApiField("item_url")
		private String itemUrl;
		/**
		 * 一级类目ID
		 */
		@ApiField("level_one_category_id")
		private Long levelOneCategoryId;
		/**
		 * 一级类目名称
		 */
		@ApiField("level_one_category_name")
		private String levelOneCategoryName;
		/**
		 * 宝贝id
		 */
		@ApiField("num_iid")
		private Long numIid;
		/**
		 * 商品主图
		 */
		@ApiField("pict_url")
		private String pictUrl;
		/**
		 * 宝贝所在地
		 */
		@ApiField("provcity")
		private String provcity;
		/**
		 * 商品一口价格
		 */
		@ApiField("reserve_price")
		private String reservePrice;
		/**
		 * 卖家id
		 */
		@ApiField("seller_id")
		private Long sellerId;
		/**
		 * 店铺dsr评分
		 */
		@ApiField("shop_dsr")
		private Long shopDsr;
		/**
		 * 店铺名称
		 */
		@ApiField("shop_title")
		private String shopTitle;
		/**
		 * 商品短标题
		 */
		@ApiField("short_title")
		private String shortTitle;
		/**
		 * 商品小图列表
		 */
		@ApiListField("small_images")
		@ApiField("string")
		private List<String> smallImages;
		/**
		 * 商品标题
		 */
		@ApiField("title")
		private String title;
		/**
		 * 月支出佣金
		 */
		@ApiField("tk_total_commi")
		private String tkTotalCommi;
		/**
		 * 淘客30天月推广量
		 */
		@ApiField("tk_total_sales")
		private String tkTotalSales;
		/**
		 * 商品淘客链接
		 */
		@ApiField("url")
		private String url;
		/**
		 * 卖家类型，0表示集市，1表示商城
		 */
		@ApiField("user_type")
		private Long userType;
		/**
		 * 30天销量
		 */
		@ApiField("volume")
		private Long volume;
		/**
		 * 商品白底图
		 */
		@ApiField("white_image")
		private String whiteImage;
		/**
		 * 商品折扣价格
		 */
		@ApiField("zk_final_price")
		private String zkFinalPrice;
	

	public Long getCategoryId() {
			return this.categoryId;
		}
		public void setCategoryId(Long categoryId) {
			this.categoryId = categoryId;
		}
		public String getCategoryName() {
			return this.categoryName;
		}
		public void setCategoryName(String categoryName) {
			this.categoryName = categoryName;
		}
		public String getCommissionRate() {
			return this.commissionRate;
		}
		public void setCommissionRate(String commissionRate) {
			this.commissionRate = commissionRate;
		}
		public String getCommissionType() {
			return this.commissionType;
		}
		public void setCommissionType(String commissionType) {
			this.commissionType = commissionType;
		}
		public String getCouponEndTime() {
			return this.couponEndTime;
		}
		public void setCouponEndTime(String couponEndTime) {
			this.couponEndTime = couponEndTime;
		}
		public String getCouponId() {
			return this.couponId;
		}
		public void setCouponId(String couponId) {
			this.couponId = couponId;
		}
		public String getCouponInfo() {
			return this.couponInfo;
		}
		public void setCouponInfo(String couponInfo) {
			this.couponInfo = couponInfo;
		}
		public Long getCouponRemainCount() {
			return this.couponRemainCount;
		}
		public void setCouponRemainCount(Long couponRemainCount) {
			this.couponRemainCount = couponRemainCount;
		}
		public String getCouponShareUrl() {
			return this.couponShareUrl;
		}
		public void setCouponShareUrl(String couponShareUrl) {
			this.couponShareUrl = couponShareUrl;
		}
		public String getCouponStartTime() {
			return this.couponStartTime;
		}
		public void setCouponStartTime(String couponStartTime) {
			this.couponStartTime = couponStartTime;
		}
		public Long getCouponTotalCount() {
			return this.couponTotalCount;
		}
		public void setCouponTotalCount(Long couponTotalCount) {
			this.couponTotalCount = couponTotalCount;
		}
		public String getIncludeDxjh() {
			return this.includeDxjh;
		}
		public void setIncludeDxjh(String includeDxjh) {
			this.includeDxjh = includeDxjh;
		}
		public String getIncludeMkt() {
			return this.includeMkt;
		}
		public void setIncludeMkt(String includeMkt) {
			this.includeMkt = includeMkt;
		}
		public String getInfoDxjh() {
			return this.infoDxjh;
		}
		public void setInfoDxjh(String infoDxjh) {
			this.infoDxjh = infoDxjh;
		}
		public void setInfoDxjhString(String infoDxjh) {
			this.infoDxjh = infoDxjh;
		}
		
		public String getItemUrl() {
			return this.itemUrl;
		}
		public void setItemUrl(String itemUrl) {
			this.itemUrl = itemUrl;
		}
		public Long getLevelOneCategoryId() {
			return this.levelOneCategoryId;
		}
		public void setLevelOneCategoryId(Long levelOneCategoryId) {
			this.levelOneCategoryId = levelOneCategoryId;
		}
		public String getLevelOneCategoryName() {
			return this.levelOneCategoryName;
		}
		public void setLevelOneCategoryName(String levelOneCategoryName) {
			this.levelOneCategoryName = levelOneCategoryName;
		}
		public Long getNumIid() {
			return this.numIid;
		}
		public void setNumIid(Long numIid) {
			this.numIid = numIid;
		}
		public String getPictUrl() {
			return this.pictUrl;
		}
		public void setPictUrl(String pictUrl) {
			this.pictUrl = pictUrl;
		}
		public String getProvcity() {
			return this.provcity;
		}
		public void setProvcity(String provcity) {
			this.provcity = provcity;
		}
		public String getReservePrice() {
			return this.reservePrice;
		}
		public void setReservePrice(String reservePrice) {
			this.reservePrice = reservePrice;
		}
		public Long getSellerId() {
			return this.sellerId;
		}
		public void setSellerId(Long sellerId) {
			this.sellerId = sellerId;
		}
		public Long getShopDsr() {
			return this.shopDsr;
		}
		public void setShopDsr(Long shopDsr) {
			this.shopDsr = shopDsr;
		}
		public String getShopTitle() {
			return this.shopTitle;
		}
		public void setShopTitle(String shopTitle) {
			this.shopTitle = shopTitle;
		}
		public String getShortTitle() {
			return this.shortTitle;
		}
		public void setShortTitle(String shortTitle) {
			this.shortTitle = shortTitle;
		}
		public List<String> getSmallImages() {
			return this.smallImages;
		}
		public void setSmallImages(List<String> smallImages) {
			this.smallImages = smallImages;
		}
		public String getTitle() {
			return this.title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getTkTotalCommi() {
			return this.tkTotalCommi;
		}
		public void setTkTotalCommi(String tkTotalCommi) {
			this.tkTotalCommi = tkTotalCommi;
		}
		public String getTkTotalSales() {
			return this.tkTotalSales;
		}
		public void setTkTotalSales(String tkTotalSales) {
			this.tkTotalSales = tkTotalSales;
		}
		public String getUrl() {
			return this.url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		public Long getUserType() {
			return this.userType;
		}
		public void setUserType(Long userType) {
			this.userType = userType;
		}
		public Long getVolume() {
			return this.volume;
		}
		public void setVolume(Long volume) {
			this.volume = volume;
		}
		public String getWhiteImage() {
			return this.whiteImage;
		}
		public void setWhiteImage(String whiteImage) {
			this.whiteImage = whiteImage;
		}
		public String getZkFinalPrice() {
			return this.zkFinalPrice;
		}
		public void setZkFinalPrice(String zkFinalPrice) {
			this.zkFinalPrice = zkFinalPrice;
		}

}



}
