package com.taobao.api.response;

import java.util.List;
import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.TaobaoObject;
import com.taobao.api.internal.mapping.ApiListField;

import com.taobao.api.TaobaoResponse;

/**
 * TOP API: taobao.tbk.sc.optimus.material response.
 * 
 * @author top auto create
 * @since 1.0, null
 */
public class TbkScOptimusMaterialResponse extends TaobaoResponse {

	private static final long serialVersionUID = 7735474599452928742L;

	/** 
	 * resultList
	 */
	@ApiListField("result_list")
	@ApiField("map_data")
	private List<MapData> resultList;


	public void setResultList(List<MapData> resultList) {
		this.resultList = resultList;
	}
	public List<MapData> getResultList( ) {
		return this.resultList;
	}
	
	/**
 * resultList
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class MapData extends TaobaoObject {

	private static final long serialVersionUID = 7125542942351715772L;

	/**
		 * 叶子类目id
		 */
		@ApiField("category_id")
		private Long categoryId;
		/**
		 * 叶子类目名称
		 */
		@ApiField("category_name")
		private String categoryName;
		/**
		 * 单品淘客链接
		 */
		@ApiField("click_url")
		private String clickUrl;
		/**
		 * 佣金比率(%)
		 */
		@ApiField("commission_rate")
		private String commissionRate;
		/**
		 * 券面额
		 */
		@ApiField("coupon_amount")
		private Long couponAmount;
		/**
		 * 优惠券链接
		 */
		@ApiField("coupon_click_url")
		private String couponClickUrl;
		/**
		 * 优惠券结束时间
		 */
		@ApiField("coupon_end_time")
		private String couponEndTime;
		/**
		 * 优惠券剩余量
		 */
		@ApiField("coupon_remain_count")
		private Long couponRemainCount;
		/**
		 * 优惠券起用门槛，满X元可用
		 */
		@ApiField("coupon_start_fee")
		private String couponStartFee;
		/**
		 * 优惠券开始时间
		 */
		@ApiField("coupon_start_time")
		private String couponStartTime;
		/**
		 * 券总量
		 */
		@ApiField("coupon_total_count")
		private Long couponTotalCount;
		/**
		 * 宝贝描述（推荐理由,不一定有）
		 */
		@ApiField("item_description")
		private String itemDescription;
		/**
		 * 宝贝id
		 */
		@ApiField("item_id")
		private Long itemId;
		/**
		 * 聚划算拼团：几人团
		 */
		@ApiField("jdd_num")
		private Long jddNum;
		/**
		 * 聚划算拼团：拼成价，单位元
		 */
		@ApiField("jdd_price")
		private String jddPrice;
		/**
		 * 一级类目ID
		 */
		@ApiField("level_one_category_id")
		private Long levelOneCategoryId;
		/**
		 * 一级类目名称
		 */
		@ApiField("level_one_category_name")
		private String levelOneCategoryName;
		/**
		 * 聚划算拼团：结束时间
		 */
		@ApiField("oetime")
		private String oetime;
		/**
		 * 聚划算拼团：一人价（原价)，单位元
		 */
		@ApiField("orig_price")
		private String origPrice;
		/**
		 * 聚划算拼团：开始时间
		 */
		@ApiField("ostime")
		private String ostime;
		/**
		 * 商品主图
		 */
		@ApiField("pict_url")
		private String pictUrl;
		/**
		 * 聚划算拼团：已售数量
		 */
		@ApiField("sell_num")
		private Long sellNum;
		/**
		 * 卖家id
		 */
		@ApiField("seller_id")
		private Long sellerId;
		/**
		 * 店铺名称
		 */
		@ApiField("shop_title")
		private String shopTitle;
		/**
		 * 商品短标题
		 */
		@ApiField("short_title")
		private String shortTitle;
		/**
		 * 商品小图列表
		 */
		@ApiListField("small_images")
		@ApiField("string")
		private List<String> smallImages;
		/**
		 * 聚划算拼团：剩余库存
		 */
		@ApiField("stock")
		private Long stock;
		/**
		 * 商品标题
		 */
		@ApiField("title")
		private String title;
		/**
		 * 聚划算拼团：库存数量
		 */
		@ApiField("total_stock")
		private Long totalStock;
		/**
		 * 卖家类型，0表示集市，1表示商城
		 */
		@ApiField("user_type")
		private Long userType;
		/**
		 * 30天销量
		 */
		@ApiField("volume")
		private Long volume;
		/**
		 * 商品白底图
		 */
		@ApiField("white_image")
		private String whiteImage;
		/**
		 * 折扣价
		 */
		@ApiField("zk_final_price")
		private String zkFinalPrice;
	

	public Long getCategoryId() {
			return this.categoryId;
		}
		public void setCategoryId(Long categoryId) {
			this.categoryId = categoryId;
		}
		public String getCategoryName() {
			return this.categoryName;
		}
		public void setCategoryName(String categoryName) {
			this.categoryName = categoryName;
		}
		public String getClickUrl() {
			return this.clickUrl;
		}
		public void setClickUrl(String clickUrl) {
			this.clickUrl = clickUrl;
		}
		public String getCommissionRate() {
			return this.commissionRate;
		}
		public void setCommissionRate(String commissionRate) {
			this.commissionRate = commissionRate;
		}
		public Long getCouponAmount() {
			return this.couponAmount;
		}
		public void setCouponAmount(Long couponAmount) {
			this.couponAmount = couponAmount;
		}
		public String getCouponClickUrl() {
			return this.couponClickUrl;
		}
		public void setCouponClickUrl(String couponClickUrl) {
			this.couponClickUrl = couponClickUrl;
		}
		public String getCouponEndTime() {
			return this.couponEndTime;
		}
		public void setCouponEndTime(String couponEndTime) {
			this.couponEndTime = couponEndTime;
		}
		public Long getCouponRemainCount() {
			return this.couponRemainCount;
		}
		public void setCouponRemainCount(Long couponRemainCount) {
			this.couponRemainCount = couponRemainCount;
		}
		public String getCouponStartFee() {
			return this.couponStartFee;
		}
		public void setCouponStartFee(String couponStartFee) {
			this.couponStartFee = couponStartFee;
		}
		public String getCouponStartTime() {
			return this.couponStartTime;
		}
		public void setCouponStartTime(String couponStartTime) {
			this.couponStartTime = couponStartTime;
		}
		public Long getCouponTotalCount() {
			return this.couponTotalCount;
		}
		public void setCouponTotalCount(Long couponTotalCount) {
			this.couponTotalCount = couponTotalCount;
		}
		public String getItemDescription() {
			return this.itemDescription;
		}
		public void setItemDescription(String itemDescription) {
			this.itemDescription = itemDescription;
		}
		public Long getItemId() {
			return this.itemId;
		}
		public void setItemId(Long itemId) {
			this.itemId = itemId;
		}
		public Long getJddNum() {
			return this.jddNum;
		}
		public void setJddNum(Long jddNum) {
			this.jddNum = jddNum;
		}
		public String getJddPrice() {
			return this.jddPrice;
		}
		public void setJddPrice(String jddPrice) {
			this.jddPrice = jddPrice;
		}
		public Long getLevelOneCategoryId() {
			return this.levelOneCategoryId;
		}
		public void setLevelOneCategoryId(Long levelOneCategoryId) {
			this.levelOneCategoryId = levelOneCategoryId;
		}
		public String getLevelOneCategoryName() {
			return this.levelOneCategoryName;
		}
		public void setLevelOneCategoryName(String levelOneCategoryName) {
			this.levelOneCategoryName = levelOneCategoryName;
		}
		public String getOetime() {
			return this.oetime;
		}
		public void setOetime(String oetime) {
			this.oetime = oetime;
		}
		public String getOrigPrice() {
			return this.origPrice;
		}
		public void setOrigPrice(String origPrice) {
			this.origPrice = origPrice;
		}
		public String getOstime() {
			return this.ostime;
		}
		public void setOstime(String ostime) {
			this.ostime = ostime;
		}
		public String getPictUrl() {
			return this.pictUrl;
		}
		public void setPictUrl(String pictUrl) {
			this.pictUrl = pictUrl;
		}
		public Long getSellNum() {
			return this.sellNum;
		}
		public void setSellNum(Long sellNum) {
			this.sellNum = sellNum;
		}
		public Long getSellerId() {
			return this.sellerId;
		}
		public void setSellerId(Long sellerId) {
			this.sellerId = sellerId;
		}
		public String getShopTitle() {
			return this.shopTitle;
		}
		public void setShopTitle(String shopTitle) {
			this.shopTitle = shopTitle;
		}
		public String getShortTitle() {
			return this.shortTitle;
		}
		public void setShortTitle(String shortTitle) {
			this.shortTitle = shortTitle;
		}
		public List<String> getSmallImages() {
			return this.smallImages;
		}
		public void setSmallImages(List<String> smallImages) {
			this.smallImages = smallImages;
		}
		public Long getStock() {
			return this.stock;
		}
		public void setStock(Long stock) {
			this.stock = stock;
		}
		public String getTitle() {
			return this.title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public Long getTotalStock() {
			return this.totalStock;
		}
		public void setTotalStock(Long totalStock) {
			this.totalStock = totalStock;
		}
		public Long getUserType() {
			return this.userType;
		}
		public void setUserType(Long userType) {
			this.userType = userType;
		}
		public Long getVolume() {
			return this.volume;
		}
		public void setVolume(Long volume) {
			this.volume = volume;
		}
		public String getWhiteImage() {
			return this.whiteImage;
		}
		public void setWhiteImage(String whiteImage) {
			this.whiteImage = whiteImage;
		}
		public String getZkFinalPrice() {
			return this.zkFinalPrice;
		}
		public void setZkFinalPrice(String zkFinalPrice) {
			this.zkFinalPrice = zkFinalPrice;
		}

}



}
