package com.taobao.api.response;

import java.util.List;
import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.TaobaoObject;
import com.taobao.api.internal.mapping.ApiListField;

import com.taobao.api.TaobaoResponse;

/**
 * TOP API: taobao.baichuan.items.subscribe response.
 * 
 * @author top auto create
 * @since 1.0, null
 */
public class BaichuanItemsSubscribeResponse extends TaobaoResponse {

	private static final long serialVersionUID = 7124382221224143567L;

	/** 
	 * 接口返回model
	 */
	@ApiField("result")
	private Result result;


	public void setResult(Result result) {
		this.result = result;
	}
	public Result getResult( ) {
		return this.result;
	}
	
	/**
 * 对应code的itemId列表
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class ResultData extends TaobaoObject {

	private static final long serialVersionUID = 4368927849934831295L;

	/**
		 * 个数
		 */
		@ApiField("count")
		private Long count;
		/**
		 * itemId列表
		 */
		@ApiListField("item_list")
		@ApiField("number")
		private List<Long> itemList;
	

	public Long getCount() {
			return this.count;
		}
		public void setCount(Long count) {
			this.count = count;
		}
		public List<Long> getItemList() {
			return this.itemList;
		}
		public void setItemList(List<Long> itemList) {
			this.itemList = itemList;
		}

}

	/**
 * 按不同的返回码将结果分部分返回
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class ResultMeta extends TaobaoObject {

	private static final long serialVersionUID = 7138697452879922724L;

	/**
		 * 返回码
		 */
		@ApiField("code")
		private Long code;
		/**
		 * 对应code的itemId列表
		 */
		@ApiField("data")
		private ResultData data;
		/**
		 * 返回码对应文案
		 */
		@ApiField("msg")
		private String msg;
	

	public Long getCode() {
			return this.code;
		}
		public void setCode(Long code) {
			this.code = code;
		}
		public ResultData getData() {
			return this.data;
		}
		public void setData(ResultData data) {
			this.data = data;
		}
		public String getMsg() {
			return this.msg;
		}
		public void setMsg(String msg) {
			this.msg = msg;
		}

}

	/**
 * 接口返回model
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class Result extends TaobaoObject {

	private static final long serialVersionUID = 8198176214743135788L;

	/**
		 * 按不同的返回码将结果分部分返回
		 */
		@ApiListField("result_list")
		@ApiField("result_meta")
		private List<ResultMeta> resultList;
	

	public List<ResultMeta> getResultList() {
			return this.resultList;
		}
		public void setResultList(List<ResultMeta> resultList) {
			this.resultList = resultList;
		}

}



}
