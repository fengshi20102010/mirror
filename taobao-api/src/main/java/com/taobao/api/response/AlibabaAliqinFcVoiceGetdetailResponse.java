package com.taobao.api.response;

import com.taobao.api.internal.mapping.ApiField;

import com.taobao.api.TaobaoResponse;

/**
 * TOP API: alibaba.aliqin.fc.voice.getdetail response.
 * 
 * @author top auto create
 * @since 1.0, null
 */
public class AlibabaAliqinFcVoiceGetdetailResponse extends TaobaoResponse {

	private static final long serialVersionUID = 2554171451468443343L;

	/** 
	 * 错误码
	 */
	@ApiField("alicom_code")
	private String alicomCode;

	/** 
	 * 错误信息
	 */
	@ApiField("alicom_msg")
	private String alicomMsg;

	/** 
	 * 请求是否成功
	 */
	@ApiField("alicom_success")
	private Boolean alicomSuccess;

	/** 
	 * 返回值，在没有结果时为空。recordFile表示的是录音文件地址
	 */
	@ApiField("model")
	private String model;


	public void setAlicomCode(String alicomCode) {
		this.alicomCode = alicomCode;
	}
	public String getAlicomCode( ) {
		return this.alicomCode;
	}

	public void setAlicomMsg(String alicomMsg) {
		this.alicomMsg = alicomMsg;
	}
	public String getAlicomMsg( ) {
		return this.alicomMsg;
	}

	public void setAlicomSuccess(Boolean alicomSuccess) {
		this.alicomSuccess = alicomSuccess;
	}
	public Boolean getAlicomSuccess( ) {
		return this.alicomSuccess;
	}

	public void setModel(String model) {
		this.model = model;
	}
	public String getModel( ) {
		return this.model;
	}
	


}
