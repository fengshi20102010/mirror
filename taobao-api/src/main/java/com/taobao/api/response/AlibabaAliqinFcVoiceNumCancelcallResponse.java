package com.taobao.api.response;

import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.TaobaoObject;

import com.taobao.api.TaobaoResponse;

/**
 * TOP API: alibaba.aliqin.fc.voice.num.cancelcall response.
 * 
 * @author top auto create
 * @since 1.0, null
 */
public class AlibabaAliqinFcVoiceNumCancelcallResponse extends TaobaoResponse {

	private static final long serialVersionUID = 7639853677579589486L;

	/** 
	 * result
	 */
	@ApiField("result")
	private BizResult result;


	public void setResult(BizResult result) {
		this.result = result;
	}
	public BizResult getResult( ) {
		return this.result;
	}
	
	/**
 * result
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class BizResult extends TaobaoObject {

	private static final long serialVersionUID = 5274582355684761858L;

	/**
		 * code
		 */
		@ApiField("err_code")
		private String errCode;
		/**
		 * model
		 */
		@ApiField("model")
		private String model;
		/**
		 * msg
		 */
		@ApiField("msg")
		private String msg;
		/**
		 * success
		 */
		@ApiField("success")
		private Boolean success;
	

	public String getErrCode() {
			return this.errCode;
		}
		public void setErrCode(String errCode) {
			this.errCode = errCode;
		}
		public String getModel() {
			return this.model;
		}
		public void setModel(String model) {
			this.model = model;
		}
		public String getMsg() {
			return this.msg;
		}
		public void setMsg(String msg) {
			this.msg = msg;
		}
		public Boolean getSuccess() {
			return this.success;
		}
		public void setSuccess(Boolean success) {
			this.success = success;
		}

}



}
