package com.taobao.api.response;

import java.util.List;
import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.TaobaoObject;
import com.taobao.api.internal.mapping.ApiListField;

import com.taobao.api.TaobaoResponse;

/**
 * TOP API: taobao.baichuan.item.unsubscribe response.
 * 
 * @author top auto create
 * @since 1.0, null
 */
public class BaichuanItemUnsubscribeResponse extends TaobaoResponse {

	private static final long serialVersionUID = 3584716836748134656L;

	/** 
	 * 接口返回model
	 */
	@ApiField("result")
	private Result result;


	public void setResult(Result result) {
		this.result = result;
	}
	public Result getResult( ) {
		return this.result;
	}
	
	/**
 * 商品id列表
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class ResultData extends TaobaoObject {

	private static final long serialVersionUID = 7631793645223217856L;

	/**
		 * 商品个数
		 */
		@ApiField("count")
		private Long count;
		/**
		 * 商品列表
		 */
		@ApiListField("item_list")
		@ApiField("number")
		private List<Long> itemList;
	

	public Long getCount() {
			return this.count;
		}
		public void setCount(Long count) {
			this.count = count;
		}
		public List<Long> getItemList() {
			return this.itemList;
		}
		public void setItemList(List<Long> itemList) {
			this.itemList = itemList;
		}

}

	/**
 * 返回按resultCode分为多个返回部分
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class ResultMeta extends TaobaoObject {

	private static final long serialVersionUID = 1629513442388664946L;

	/**
		 * 返回码
		 */
		@ApiField("code")
		private Long code;
		/**
		 * 商品id列表
		 */
		@ApiField("data")
		private ResultData data;
		/**
		 * 返回码对应的文案
		 */
		@ApiField("msg")
		private String msg;
	

	public Long getCode() {
			return this.code;
		}
		public void setCode(Long code) {
			this.code = code;
		}
		public ResultData getData() {
			return this.data;
		}
		public void setData(ResultData data) {
			this.data = data;
		}
		public String getMsg() {
			return this.msg;
		}
		public void setMsg(String msg) {
			this.msg = msg;
		}

}

	/**
 * 接口返回model
 *
 * @author top auto create
 * @since 1.0, null
 */
public static class Result extends TaobaoObject {

	private static final long serialVersionUID = 8447863435227341927L;

	/**
		 * 返回按resultCode分为多个返回部分
		 */
		@ApiListField("result_list")
		@ApiField("result_meta")
		private List<ResultMeta> resultList;
	

	public List<ResultMeta> getResultList() {
			return this.resultList;
		}
		public void setResultList(List<ResultMeta> resultList) {
			this.resultList = resultList;
		}

}



}
