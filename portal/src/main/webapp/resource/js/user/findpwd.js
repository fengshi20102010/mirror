$(document).ready(function() {
	// 验证手机号
	$("#valiphone").validate({
		rules: {
			phone: {
				required: true,
				phone_value: ''
			},
			code: {
				required: true,
				minlength: 6,
				maxlength: 6
			}
		},
		messages: {
			phone: {
				required: "手机号码不能为空"
			},
			code: {
				required: "请输入验证码",
				minlength: "验证码输入错误",
				maxlength: "验证码输入错误"
			}
		},
		focusCleanup: true,
		onfocusin: function(element) {
			$(element).addClass('active');
			$(element).parent().find('strong').hide();
			$(element).parent().find('.tip').css("color", "#666").show();
		},
		onfocusout: function(element) {
			$(element).parent().find('.tip').show();
			$(element).parent().find('strong').show();
			if($(element).valid()) {
				$(element).parent().find('strong').removeClass('error');
				$(element).parent().find('strong').addClass('ok');
				$(element).parent().find('.tip').hide();
			} else {
				$(element).parent().find('strong').removeClass('ok');
				$(element).parent().find('strong').addClass('error');
				$(element).removeClass('active');
				$(element).addClass('error_box');
				$(element).parent().find('.tip').css("color", "#ff464e").show();
			}

		},
		errorPlacement: function(error, element) {
			$(element).parent().find('.tip').html("");
			$(element).parent().find('.tip').append(error).css("color", "#ff464e").show();
			$(element).removeClass('active');
			$(element).addClass('error_box');
			$(element).parent().find('strong').show();
			$(element).parent().find('strong').removeClass('ok');
			$(element).parent().find('strong').addClass('error');
		},
		submitHandler: function() {
			$.ajax({
				url: "checkVC.html",
				type: "post",
				dataType: "json",
				data: {
					phone: $('#phone').val(),
					code: $('#code').val()
				},
				success: function(data) {
					if(data.success){
						$('#valiphone').hide();
						$('#submitNewPwd').show();
						$('.stepBox').find('p').eq(0).removeClass('ac').siblings().addClass('ac')
					} else {
						layer.msg(data.massage);
					}
				}
			});
		}
	});

	//从新写入密码验证
	$("#submitNewPwd").validate({
		rules: {
			password: {
				required: true,
				minlength: 6,
				maxlength: 16
			},
			repassword: {
				required: true,
				minlength: 6,
				maxlength: 16
			}
		},
		messages: {
			password: {
				required: "请输入密码",
				minlength: "6-16个数字、字母或符号，字母区分大小写",
				maxlength: "6-16个数字、字母或符号，字母区分大小写"
			},
			repassword: {
				required: "请重复输入密码",
				minlength: "6-16个数字、字母或符号，字母区分大小写",
				maxlength: "6-16个数字、字母或符号，字母区分大小写"
			}
		},
		focusCleanup: true,
		onfocusin: function(element) {
			$(element).addClass('active');
			$(element).parent().find('strong').hide();
			$(element).parent().find('.tip').css("color", "#666").show();
		},
		onfocusout: function(element) {
			$(element).parent().find('.tip').show();
			$(element).parent().find('strong').show();
			if($(element).valid()) {
				$(element).parent().find('strong').removeClass('error');
				$(element).parent().find('strong').addClass('ok');
				$(element).parent().find('.tip').hide();

				if($(element)[0].id == 'repassword') {
					console.log($(element).val() != $('#password').val())

					if($(element).val() != $('#password').val()) {
						$(element).parent().find('strong').removeClass('ok');
						$(element).parent().find('strong').addClass('error');
						$(element).removeClass('active');
						$(element).addClass('error_box');
						$(element).siblings('#password_warn').find('p').html('两次输入的密码不一致').css("color", "#ff464e").show();
					} else {
						$(element).siblings('#password_warn').find('p').hide();
					}
				}

			} else {
				$(element).parent().find('strong').removeClass('ok');
				$(element).parent().find('strong').addClass('error');
				$(element).removeClass('active');
				$(element).addClass('error_box');
				$(element).parent().find('.tip').css("color", "#ff464e").show();
			}
		},
		errorPlacement: function(error, element) {
			$(element).parent().find('.tip').html("");
			$(element).parent().find('.tip').append(error).css("color", "#ff464e").show();
			$(element).removeClass('active');
			$(element).addClass('error_box');
			$(element).parent().find('strong').show();
			$(element).parent().find('strong').removeClass('ok');
			$(element).parent().find('strong').addClass('error');
		},
		submitHandler: function() {
			$.ajax({
				url: window.location.href,
				type: "post",
				dataType: "json",
				data: {
					newPwd: $('#password').val()
				},
				success: function(data) {
					if(data.success){
						window.location.href = '/login.html';
					} else {
						layer.msg(data.Message);
					}
				}
			});
		}
	});

	$('#sendVerifySmsButton').click(function(e) {
		var myreg = /^1[3|5|7|8][0-9]{1}\d{8}$/;
		if(!myreg.test($("#phone").val())) {
			$('#phone').focus();
			return false;
		}

		waitSend();
		handlerEmbed();
	});
});

var djs = 60;

function waitSend() {
	setTimeout(function() {
		if(djs > 0) {
			$('#sendVerifySmsButton').attr('disabled', 'disabled');
			$('#sendVerifySmsButton').empty().text(djs + '秒后重新获取');
			djs--;
			waitSend();
		} else {
			$('#sendVerifySmsButton').removeProp('disabled');
			$('#sendVerifySmsButton').empty().text('获取验证码');
			djs = 60;
		}
	}, 1000);
}

var handlerEmbed = function(captchaObj) {
	$.ajax({
		url: "/sendSMS.html", // 短信验证码
		type: "post",
		dataType: "json",
		data: {
			phone: $('#phone').val(),
			type: 2
		},
		beforeSend: function() {
			layer.msg('验证码发送中');
		},
		success: function(data) {
			layer.msg(data.message);
			djs = 0;
		}
	});
};