$(document).ready(function() {
	$("#pid").validate({
		rules: {
			name: {
				required: true,
				groupname: ''
			},
			commonPid: {
				required: true,
				validat_pid: ''
			},
			queqiaoPid: {
				required: true,
				validat_pid: ''
			}
		},
		messages: {
			name: {
				required: "请输入分组名称"
			},
			commonPid: {
				required: "请输入正确的通用PID"
			},
			queqiaoPid: {
				required: "请输入正确的鹊桥PID"
			}
		},
		focusCleanup: true,

		onfocusin: function(element) {
			$(element).addClass('active');
			$(element).parent().find('.tip').css("color", "#666").show();
		},
		onfocusout: function(element) {
			$(element).parent().find('.tip').show();
			if($(element).valid()) {
				$(element).parent().find('.tip').hide();
			} else {
				$(element).removeClass('active');
				$(element).addClass('error_box');
				$(element).parent().find('.tip').css("color", "#ff464e").show();
			}
		},
		errorPlacement: function(error, element) {
			$(element).parent().find('.tip').html("");
			$(element).parent().find('.tip').append(error).css("color", "#ff464e").show();
			$(element).removeClass('active');
			$(element).addClass('error_box');
		},
		submitHandler:function(form){
			$.ajax({
				url:window.location.href,
				dataType:'json',
				type:'post',
				data: $('#pid').serialize(),
				success:function(data){
					if(data.success){
						layer.msg(data.message);
						location.reload();
						return;
					}else{
						layer.msg(data.message,{icon:2,time:1000});
					}
				}
			})
		}
	});
});