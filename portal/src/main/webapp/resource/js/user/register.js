$(document).ready(function() {
	// 在键盘按下并释放及提交后验证提交表单
	$("#register").validate({
		rules: {
			phone: {
				required: true,
				phone_value: '',
				remote: {
					url: "isExisted.html", //后台处理程序
					type: "post", //数据发送方式
					data: { //要传递的数据
						mobile: function() {
							return $("#phone").val();
						}
					}
				}
			},
			password: {
				required: true,
				minlength: 6,
				maxlength: 16
			},
			code: {
				required: true,
				minlength: 6,
				maxlength: 6
			},
			agree: "required"
		},
		messages: {
			phone: {
				required: "手机号码不能为空",
				remote: '手机号已被注册，请直接<a href="/login">登录</a>'
			},
			code: {
				required: "请输入验证码",
				minlength: "验证码输入错误",
				maxlength: "验证码输入错误"
			},
			password: {
				required: "请输入密码",
				minlength: "6-16个数字、字母或符号，字母区分大小写",
				maxlength: "6-16个数字、字母或符号，字母区分大小写"
			}
		},
		focusCleanup: true,
		onfocusin: function(element) {
			$(element).addClass('active');
			$(element).parent().find('strong').hide();
			$(element).parent().find('.tip').css("color", "#666").show();
		},
		onfocusout: function(element) {
			$(element).parent().find('.tip').show();
			$(element).parent().find('strong').show();
			if($(element).valid()) {
				$(element).parent().find('strong').removeClass('error');
				$(element).parent().find('strong').addClass('ok');
				$(element).parent().find('.tip').hide();
			} else {
				$(element).parent().find('strong').removeClass('ok');
				$(element).parent().find('strong').addClass('error');
				$(element).removeClass('active');
				$(element).addClass('error_box');
				$(element).parent().find('.tip').css("color", "#ff464e").show();
			}

		},
		errorPlacement: function(error, element) {
			$(element).parent().find('.tip').html("");
			$(element).parent().find('.tip').append(error).css("color", "#ff464e").show();
			$(element).removeClass('active');
			$(element).addClass('error_box');
			$(element).parent().find('strong').show();
			$(element).parent().find('strong').removeClass('ok');
			$(element).parent().find('strong').addClass('error');
		},
		submitHandler: function() {
			$.ajax({
				url: window.location.href, // 进行二次验证
				type: "post",
				dataType: "json",
				data: {
					phone: $('#phone').val(),
					code: $('#code').val(),
					password: $('#password').val()
				},
				success: function(data) {
					if(data.success){
						window.location.href = '/';
					} else {
						layer.msg(data.message);
					}
				}
			});
		}
	});

	$('#sendVerifySmsButton').click(function(e) {
		var myreg = /^1[3|5|7|8][0-9]{1}\d{8}$/;
		if(!myreg.test($("#phone").val())) {
			$('#phone').focus();
			return false;
		}
		waitSend();
		handlerEmbed();
	});
});

var djs = 60;

function waitSend() {
	setTimeout(function() {
		if(djs > 0) {
			$('#sendVerifySmsButton').attr('disabled', 'disabled');
			$('#sendVerifySmsButton').empty().text(djs + '秒后重新获取');
			djs--;
			waitSend();
		} else {
			$('#sendVerifySmsButton').removeProp('disabled');
			$('#sendVerifySmsButton').empty().text('获取验证码');
			djs = 60;
		}
	}, 1000);
}

var handlerEmbed = function(captchaObj) {
	$.ajax({
		url: "/sendSMS.html", // 短信验证码
		type: "post",
		dataType: "json",
		data: {
			phone: $('#phone').val(),
			type: 1
		},
		beforeSend: function() {
			layer.msg('验证码发送中');
		},
		success: function(data) {
			layer.msg(data.message);
			djs = 0;
		}
	});
};