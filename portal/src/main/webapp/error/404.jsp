<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE HTML>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <title>404</title>
		<link rel="shortcut icon" href="favicon.ico"> 
	    <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	    <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
	    <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
	</head>
	<body class="gray-bg">
	    <div class="middle-box text-center animated fadeInDown">
	        <h1>404</h1>
	        <h3 class="font-bold">页面未找到！</h3>
	        <div class="error-desc">
				抱歉，页面好像去火星了~
	            <form class="form-inline m-t" role="form">
	                <div class="form-group">
	                    <input type="email" class="form-control" placeholder="请输入您需要查找的内容 …">
	                </div>
	                <button type="submit" class="btn btn-primary">搜索</button>
	            </form>
	        </div>
	    </div>
	</body>
</html>