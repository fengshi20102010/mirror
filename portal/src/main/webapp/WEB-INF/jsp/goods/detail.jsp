<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>秋冬加厚羊绒衫毛衣 女士套头高领羊毛衫长袖修身短款针织打底衫-墨镜助手，让推广更高效</title>
		<meta name="keywords" content="墨镜助手、鹊桥活动、淘宝客、淘宝客活动、鹊桥查询">
		<meta name="description" content="墨镜助手是一款淘宝客（鹊桥）活动商品查询软件，并集成强大的自定义筛选功能，帮您迅速找到高佣金高转化的推广商品。">
		<link rel="Bookmark" href="${pageContext.request.contextPath }/resource/images/favicon.icon">
		<link rel="Shortcut Icon" href="${pageContext.request.contextPath }/resource/images/favicon.icon">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/common.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/indexcom.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/index4.css">
		<script src="${pageContext.request.contextPath }/resource/js/jquery-1.10.1.min.js"></script>
		<script src="${pageContext.request.contextPath }/resource/js/jquery.lazyload.js"></script>
		<!--[if (gte IE 6)&(lte IE 8)]>
	      <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/selectivizr.js"></script>
		<![endif]-->
		<style>
			body {
				background: #fff;
			}
		</style>
	</head>
	<body>
		<jsp:include page="${pageContext.request.contextPath }/header.html" />
		<jsp:include page="${pageContext.request.contextPath }/type.html">
			<jsp:param name="t" value="-1"/>
		</jsp:include>
		<div class="goods clearfix">
			<div class="detail wth clearfix">
				<div class="dl-title clearfix">
					<p class="fl">我最近一次推广：
						<a href="javascript:;" class="gd_wd">为标记</a>我的累计成交<span>0</span>笔</p>
					<p class="fr">
						<i></i> 放单达人：
						<a href="javascript:;">天神萌-初上 [Ta的主页]</a> 有问题联系我
						<a href="tencent://message/?uin=123456&Site=工具啦&Menu=yes" target="blank" class="qq"></a>
					</p>
				</div>
				<div class="dl-goods clearfix">
					<div class="goods-img fl">
						<a href="${goods.url }" target="_blank"><img src="${goods.picUrl }" class="lazy" width="395" height="395"></a>
					</div>
					<div class="goods-intro fr">
						<div class="intro">
							<div class="title"> <i class="tit0"></i>
								<h3>${goods.name }</h3>
							</div>
							<p>${goods.detail }</p>
						</div>
						<div class="intro1">
							<div class="tp"><span class="a"><span class="b">天猫</span><span class="c">优惠卷</span></span>全天猫实用商品单用</div>
							<ul class="clearfix">
								<li class="tro1 fl">券后价&nbsp;¥&nbsp;<span>${goods.price - goods.money }</span></li>
								<li class="tro2 fl">在售价&nbsp;¥&nbsp;<span>${goods.price }</span></li>
								<li class="tro3 fl">目前销量：<span>${goods.sales }</span></li>
								<li class="tro5 fr">
									<a href="javascript:layer.alert(&#39;此功能需要PC客户端的配合，我们正在紧张开发中，尽请期待&#39;);">加入推广</a>
								</li>
								<li class="tro4 fr">
									<a href="javascript:;" target="_blank">查看详情</a>
								</li>
							</ul>
						</div>
						<div class="intro2 clearfix">
							<p class="int1 fl">优惠券&nbsp;<span>${goods.money }</span>&nbsp;元</p>
							<p class="int2 fl">*单笔满${goods.conditions }元可用，每人限领 差字段 张</p>
						</div>
						<div class="intro3 clearfix">
							<p class="int3 fl">优惠券剩余&nbsp;<span>${goods.remained }</span>&nbsp;张，已领券${goods.applied }张，过期时间${goods.expirationTime }</p>
						</div>
						<div class="intro4">
							<div class="intro4-left fl">
								<ul class="clearfix">
									<li class="intr1">佣金&nbsp;<span>${goods.commission }%</span></li>
									<li class="intr2">通用计划</li>
									<li class="intr3">自动通过</li>
									<li class="intr4">
										<a href="javascript:;" target="_blank">[点击申请计划]</a>
									</li>
								</ul>
								<p>PC端优惠券：
									<a href="${goods.couponUrl }" target="_blank">点击领取</a>&nbsp;&nbsp;手机端优惠券：
									<a href="${goods.couponUrl }" target="_blank">点击领取</a>
								</p>
								<p>商品链接：
									<a href="${goods.url }" target="_blank"> ${goods.url }</a>
								</p>
							</div>
							<div class="intro4-right fr"> <img src="${pageContext.request.contextPath }/resource/images/erweima.png"> </div>
						</div>
					</div>
				</div>
				<div class="examplecase clearfix">
					<div class="casetitle clearfix">
						<p class="fl"><i></i>发群模板示例</p>
						<p class="fl"><i></i>文案介绍</p>
					</div>
					<div class="clearfix">
						<div class="exampleleft fl">
							<div id="wenan">
								<img class="tui_pic" src="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" copy-src="http://acdn.taokezhushou.com/items/0255b32d6ab7e8646fc083a421d28abf.jpg" alt="牛仔裤"><br> 秋冬加厚羊绒衫毛衣 女士套头高领羊毛衫长袖修身短款针织打底衫<br> 领券后59元包邮
								<br> 60.00元内部券:
								<a class="exampleleft-a" href="http://shop.m.taobao.com/shop/coupon.htm?seller_id=656911401&amp;activity_id=15314f45d67245a1b50719f4187dd91f">http://shop.m.taobao.com/shop/coupon.htm?seller_id=656911401&amp;activity_id=15314f45d67245a1b50719f4187dd91f</a><br> 下单链接:
								<a class="exampleleft-a" href="https://item.taobao.com/item.htm?id=539016243357" target="_blank">https://item.taobao.com/item.htm?id=539016243357</a><br> 这款羊绒衫造型衫，修身显瘦，条线感十足，特别有范儿，效果特别好 </div>
							<a href="javascript:void(0);" class="qq-box">
								<button type="button">自定义QQ群模板</button>
							</a>
							<button type="button" class="copy">生成文案并复制</button>
						</div>
						<div class="fr imgtps">
							<ul class="clearfix">
								<li class="fl">
									<img src="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src">
									<p>产品验货图(1)</p>
								</li>
								<li class="fr">
									<img src="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src">
									<p>产品验货图(2)</p>
								</li>
							</ul>
							<div class="a">
								活动产品，慕斯质感80D丝袜，富有超强弹力。活动产品，慕斯质感80D丝袜，富有超强弹力。活动产品，慕斯质感80D丝袜，富有超强弹力。活动产品，慕斯质感80D丝袜，富有超强弹力。活动产品，慕斯质感80D丝袜，富有超强弹力。
							</div>
							<div class="b"><span>放单人：<i>姓名</i></span><span>QQ：<i>123456789</i></span></div>
						</div>
					</div>
				</div>
			</div>
			<div class="goods-hd wth"> <i></i>
				<p>
					<a href="http://www.taokezhushou.com/detail/48170#">查看更多&gt;&gt;</a>
				</p>
			</div>
			<%-- <div class="goods1 wth clearfix">
				<ul class="mg clearfix">
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank"> <img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287">
							</a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia"> <span class="ac1"><i></i>快乐渔家居旗舰店</span> <span class="ac2">通用</span> <span class="ac3">秒过</span> </div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank"> <img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287">
							</a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia"> <span class="ac1"><i></i>快乐渔家居旗舰店</span> <span class="ac2">通用</span> <span class="ac3">秒过</span> </div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank"> <img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287">
							</a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia"> <span class="ac1"><i></i>快乐渔家居旗舰店</span> <span class="ac2">通用</span> <span class="ac3">秒过</span> </div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank"> <img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287">
							</a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia"> <span class="ac1"><i></i>快乐渔家居旗舰店</span> <span class="ac2">通用</span> <span class="ac3">秒过</span> </div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank"> <img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287">
							</a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia"> <span class="ac1"><i></i>快乐渔家居旗舰店</span> <span class="ac2">通用</span> <span class="ac3">秒过</span> </div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank"> <img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287">
							</a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia"> <span class="ac1"><i></i>快乐渔家居旗舰店</span> <span class="ac2">通用</span> <span class="ac3">秒过</span> </div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
				</ul>
			</div> --%>
			<div class="goods-ft">
				<p>
					<a href="${pageContext.request.contextPath }/index.html">点击查看更多领券商品&nbsp;&gt;</a>
				</p>
			</div>
		</div>
		<jsp:include page="${pageContext.request.contextPath }/footer.html" />
		<div id="goTop" class="goTop"></div>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/layer.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/common.js"></script>
		<script>
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': 'IZ7o72rlP6fwkthvYIfVWTYO8pA7V5z0UPe1tfPe'
				}
			});
			var is_login = false;
		</script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/layerbox.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/clipboard.min.js"></script>
		<script>
			$(function() {
				var clipboard = new Clipboard('.copy', {
					/*
					text: function() {
					    return $('.wenan').text().trim().replace(/(^\s*)|(\s*$)/gim, "");;
					}*/
					target: function() {
						if(is_login) {
							var response = {};
							$.ajax({
								url: '/wenan/transform',
								type: 'POST',
								data: {
									id: 48170
								},
								async: false,
								success: function(data) {
									response = data;
								}
							});
							if(response.status == 'ok') {
								$('#wenan').empty().html(response.data);
								$('.tui_pic').attr('src', $('.tui_pic').attr('copy-src'));
								if(response.plan_type == '定向') {
									layer.confirm('该商品最高佣金为定向计划，请在复制后手动申请计划，避免出现推广所得不是最高佣金的情况。', {
										btn: ['好的我知道了']
									});
								}
							} else {
								if(response.msg == '请先设置PID') {
									layer.confirm('您还没有设置PID，推广不能获得佣金，是否马上设置?', function() {
										window.location.href = '/pid/create';
									}, function(index) {
										layer.close(index);
									});
								} else {
									layer.msg(response.msg);
								}
								return false;
							}
						} else {
							layer.confirm('未登录复制推广不能获得佣金哦！', {
								btn: ['好的我知道了']
							});
						}
						return document.querySelector('#wenan');
					}
				});
				clipboard.on('success', function() {
					layer.tips('复制成功', '.copy', {
						tips: [1, '#0FA6D8'] //还可配置颜色
					});
				});
				clipboard.on('error', function() {
					layer.tips('复制失败,请尝试升级您的浏览器', '.copy', {
						tips: [1, '#0FA6D8'] //还可配置颜色
					});
				});
			});
		</script>
		<div style="height: 0px; width: 0px; overflow: hidden;">
			  <object width="0" height="0" tabindex="-1" style="height:0;width:0;overflow:hidden;" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab" id="JSocket">
			    <param name="allowScriptAccess" value="always">
			    <param name="movie" value="http://aeu.alicdn.com/flash/JSocket.swf">
			    <embed src="http://aeu.alicdn.com/flash/JSocket.swf" name="JSocket" allowscriptaccess="always" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer_cn" width="0" height="0">
			  </object>
		</div>
	</body>

</html>