<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<div class="main2 wth">
	<div class="main2_doc clearfix"> <i class="fl"></i>
		<ul>
			<c:if test="${empty types }">
				<li>
					<a href="javascript:;" class="all">暂无分类信息</a>
				</li>
			</c:if>
			<c:if test="${!empty types || t eq -1 }">
				<li>
					<a href="javascript:;" <c:if test="${empty t }">class="all"</c:if>>全部（${all }）</a>
				</li>
			</c:if>
			<c:forEach items="${types }" var="type" varStatus="i">
				<li>
					<a href="javascript:;" <c:if test="${t eq type.id }">class="all"</c:if>>${type.name }(${type.num })</a>
				</li>
			</c:forEach>
		</ul>
		<button type="button" class="searchbtn fr" style="display:none;"></button>
		<input type="text" class="searchword fr" placeholder="搜索商品" style="display:none;">
	</div>
</div>