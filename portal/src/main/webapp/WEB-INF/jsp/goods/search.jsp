<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>魔镜助手-让推广更高效</title>
		<meta name="keywords" content="魔镜助手、鹊桥活动、淘宝客、淘宝客活动、鹊桥查询">
		<meta name="description" content="魔镜助手是一款淘宝客（鹊桥）活动商品查询软件，并集成强大的自定义筛选功能，帮您迅速找到高佣金高转化的推广商品。">
		<link rel="Bookmark" href="${pageContext.request.contextPath }/resource/images/favicon.ico">
		<link rel="Shortcut Icon" href="images/favicon.ico">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/common.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/indexcom.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/index3.css">
		<script src="${pageContext.request.contextPath }/resource/js/jquery-1.10.1.min.js"></script>
		<script src="${pageContext.request.contextPath }/resource/js/jquery.lazyload.js"></script>
		<!--[if (gte IE 6)&(lte IE 8)]>
	      <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/selectivizr.js"></script>
		<![endif]-->
	</head>
	<body>
		<jsp:include page="${pageContext.request.contextPath }/header.html" />
		<jsp:include page="${pageContext.request.contextPath }/type.html">
			<jsp:param name="t" value="1"/>
		</jsp:include>
		<!--未搜索到信息的时候显示此板块-->
		<div class="wth">
			<div class="searchfail"> <i></i>
				<p class="pfail">抱歉！没有找到与<span>"大鱼海棠" </span>相关的宝贝。</p>
				<p>别担心，我们在下面给您推荐了一些优质商品，或者您也可以
					<a href="/">返回首页</a>
				</p>
			</div>
		</div>
		<div class="goods">
			<div class="goods1 wth clearfix">
				<div class="menu">
					<div class="menu1 clearfix">
						<ul>
							<li class="mu1">
								<a href="javascript:;" class="awidth">默认排序</a>
							</li>
							<li class="price ">
								<a href="javascript:;" class="awidth"> 价格 </a>
								<div class="pricetrend" style="display: none;">
									<p>
										<a href="javascript:;" style="border: none;">价格从高到低</a>
									</p>
									<p>
										<a href="javascript:;" class="pricetrend2" style="border: none;">价格从低到高</a>
									</p>
								</div>
							</li>
							<li>
								<a href="javascript:;" class="awidth"> 销量 </a>
							</li>
							<li class="commission ">
								<a href="javascript:;" class="awidth"> 佣金 </a>
								<div class="commission_rate_trend" style="display: none;">
									<p>
										<a href="javascript:;" style="border: none;">佣金从高到低</a>
									</p>
									<p>
										<a href="javascript:;" class="commission2" style="border: none;">佣金从低到高</a>
									</p>
								</div>
							</li>
						</ul>
						<div class="menu-last">
							<a href="javascript:;">&lt;</a>&nbsp;&nbsp;1/100&nbsp;&nbsp;
							<a href="javascript:;">&gt;</a>
						</div>
					</div>
					<div class="menu2 clearfix">
						<ul>
							<li class="input1">
								<input id="is_tmall" type="checkbox" name="is_tmall" onclick="window.location.href=&#39;?is_tmall=1&amp;q=%E7%9A%AE%E5%B8%A6&#39;"> &nbsp;
								<label for="is_tmall">天猫旗舰店</label>
							</li>
							<li class="input2 btn1">月销量&nbsp;
								<input type="text" id="volume_start" value=""> &nbsp;
								<span class="ipt-color">笔及以上</span>
								<button type="button" class="sure1" id="volume_start_btn">确定</button>
							</li>
							<li class="input2 btn2">收入比率&nbsp;
								<input type="text" id="commission_rate_start" value="">
									<span class="ipt-color">&nbsp;%—&nbsp;
							            <input type="text" id="commission_rate_end" value="">
							            &nbsp;%
							        </span>
								<button type="button" class="sure2" id="commission_rate_btn">确定</button>
							</li>
							<li class="input2 btn3">价格&nbsp;
								<input type="text" id="price_start" value="">
									<span class="ipt-color">&nbsp;元—
							            <input type="text" id="price_end" value="">
							            &nbsp;元
							        </span>
								<button type="button" class="sure3" id="price_btn">确定</button>
							</li>
						</ul>
					</div>
				</div>
				<ul class="mg clearfix">
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank"> <img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287"> </a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia"> <span class="ac1"><i></i>快乐渔家居旗舰店</span> <span class="ac2">通用</span> <span class="ac3">秒过</span> </div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank"> <img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287"> </a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia"> <span class="ac1"><i></i>快乐渔家居旗舰店</span> <span class="ac2">通用</span> <span class="ac3">秒过</span> </div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank"> <img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287"> </a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia"> <span class="ac1"><i></i>快乐渔家居旗舰店</span> <span class="ac2">通用</span> <span class="ac3">秒过</span> </div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank"> <img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287"> </a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia"> <span class="ac1"><i></i>快乐渔家居旗舰店</span> <span class="ac2">通用</span> <span class="ac3">秒过</span> </div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank"> <img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287"> </a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia"> <span class="ac1"><i></i>快乐渔家居旗舰店</span> <span class="ac2">通用</span> <span class="ac3">秒过</span> </div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank"> <img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287"> </a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia"> <span class="ac1"><i></i>快乐渔家居旗舰店</span> <span class="ac2">通用</span> <span class="ac3">秒过</span> </div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank"> <img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287"> </a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia"> <span class="ac1"><i></i>快乐渔家居旗舰店</span> <span class="ac2">通用</span> <span class="ac3">秒过</span> </div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank"> <img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287"> </a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia"> <span class="ac1"><i></i>快乐渔家居旗舰店</span> <span class="ac2">通用</span> <span class="ac3">秒过</span> </div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank"> <img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287"> </a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia"> <span class="ac1"><i></i>快乐渔家居旗舰店</span> <span class="ac2">通用</span> <span class="ac3">秒过</span> </div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank"> <img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287"> </a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia"> <span class="ac1"><i></i>快乐渔家居旗舰店</span> <span class="ac2">通用</span> <span class="ac3">秒过</span> </div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="pages wth">
				<ul class="pagination">
					<li class="disabled"><span>«</span></li>
					<li class="active"><span>1</span></li>
					<li>
						<a href="http://www.taokezhushou.com/search?q=%E7%9A%AE%E5%B8%A6&amp;page=2">2</a>
					</li>
					<li>
						<a href="http://www.taokezhushou.com/search?q=%E7%9A%AE%E5%B8%A6&amp;page=3">3</a>
					</li>
					<li>
						<a href="http://www.taokezhushou.com/search?q=%E7%9A%AE%E5%B8%A6&amp;page=2" rel="next">»</a>
					</li>
				</ul>
			</div>
		</div>
		<jsp:include page="${pageContext.request.contextPath }/footer.html" />
		<div id="goTop" class="goTop" style="display: block;"></div>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/layer.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/common.js"></script>
		<script>
			$(".pricetrend").hide();
			$(".price").mouseenter(function() {
				$(".pricetrend").show();
				$(".pricetrend").find("a").css("border", "none");
			});
			$(".price").mouseleave(function() {
				$(".pricetrend").hide();
			});

			$(".commission_rate_trend").hide();
			$(".commission").mouseenter(function() {
				$(".commission_rate_trend").show();
				$(".commission_rate_trend").find("a").css("border", "none");
			});
			$(".commission").mouseleave(function() {
				$(".commission_rate_trend").hide();
			});
			$('#volume_start_btn').click(function() {
				if($('#volume_start').val() > 0) {
					window.location.href = "?timestamp=0.60845700+1476768792&q=%E7%9A%AE%E5%B8%A6&volume_start=" + $('#volume_start').val();
				} else {
					window.location.href = "?q=%E7%9A%AE%E5%B8%A6";
				}
			});
			$('#commission_rate_btn').click(function() {
				if($('#commission_rate_start').val() > 0 || $('#commission_rate_end').val() > 0) {
					window.location.href = "?timestamp=0.60847900+1476768792&q=%E7%9A%AE%E5%B8%A6&commission_rate=" + $('#commission_rate_start').val() + '-' + $('#commission_rate_end').val();
				} else {
					window.location.href = "?q=%E7%9A%AE%E5%B8%A6";
				}
			});
			$('#price_btn').click(function() {
				if($('#price_start').val() > 0 || $('#price_end').val() > 0) {
					window.location.href = "?timestamp=0.60849400+1476768792&q=%E7%9A%AE%E5%B8%A6&price=" + $('#price_start').val() + '-' + $('#price_end').val();
				} else {
					window.location.href = "?q=%E7%9A%AE%E5%B8%A6";
				}
			});
		</script>
		<div style="height: 0px; width: 0px; overflow: hidden;">
			  <object width="0" height="0" tabindex="-1" style="height:0;width:0;overflow:hidden;" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab" id="JSocket">
			    <param name="allowScriptAccess" value="always">
			    <param name="movie" value="http://aeu.alicdn.com/flash/JSocket.swf">
			    <embed src="http://aeu.alicdn.com/flash/JSocket.swf" name="JSocket" allowscriptaccess="always" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer_cn" width="0" height="0">
			  </object>
		</div>
	</body>

</html>