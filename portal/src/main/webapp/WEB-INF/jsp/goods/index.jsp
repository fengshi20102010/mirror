<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<a name="new" id="new"></a>
<div class="goods2 wth clearfix">
	<div class="goods1">
		<div class="newdays1 newds2"> <i></i>
			<p>每天10点上新</p>
		</div>
		<ul class="mg clearfix">
			<c:forEach items="${pageInfo.pageResults }" var="goods" varStatus="i">
				<li class="good1_one fl">
					<div class="goods-a">
						<a href="javascript:;" target="_blank">
							<img src="${pageContext.request.contextPath }resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287">
							<div class="newdays2 newd2"></div>
						</a>
						<div class="title"> <i class="tit1"></i>
							<p>
								<a href="${pageContext.request.contextPath }/goods/${goods.id}.html" target="_blank">${goods.name }</a>
							</p>
						</div>
						<div class="coupon">
							<ul>
								<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
								<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
							</ul>
						</div>
						<div class="commission">
							<ul>
								<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
								<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
							</ul>
						</div>
						<div class="satia">
							<span class="ac1"><i></i>快乐渔家居旗舰店</span>
							<span class="ac2">通用</span>
							<span class="ac3">秒过</span>
						</div>
						<div class="good_btm">
							<ul>
								<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
								<li class="btm2 fl">¥2.9</li>
								<li class="btm3 fr">
									<a href="${pageContext.request.contextPath }/goods/${goods.id}.html" target="_blank">立即推广</a>
								</li>
							</ul>
						</div>
					</div>
				</li>
			</c:forEach>
		</ul>
	</div>
</div>