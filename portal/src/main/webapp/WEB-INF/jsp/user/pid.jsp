<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!--<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/ab0aed3533ff4d7ef444f610d3ce1d1c.js"></script>-->
		<title>设置PID 魔镜助手-让推广更高效</title>
		<meta name="keywords" content="魔镜助手、鹊桥活动、淘宝客、淘宝客活动、鹊桥查询">
		<meta name="description" content="魔镜助手是一款淘宝客（鹊桥）活动商品查询软件，并集成强大的自定义筛选功能，帮您迅速找到高佣金高转化的推广商品。">
		<link rel="Bookmark" href="${pageContext.request.contextPath }/resource/images/favicon.ico">
		<link rel="Shortcut Icon" href="${pageContext.request.contextPath }/resource/images/favicon.ico">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/common.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/pidcom.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/setpid.css">
		<script src="${pageContext.request.contextPath }/resource/js/jquery-1.10.1.min.js"></script>
		<script src="${pageContext.request.contextPath }/resource/js/jquery.lazyload.js"></script>
		<!--[if (gte IE 6)&(lte IE 8)]>
	      <script type="text/javascript" src="${pageContext.request.contextPath }/resource/images/selectivizr.js"></script>
		<![endif]-->
	</head>
	<body>
		<jsp:include page="${pageContext.request.contextPath }/header.html" />
		<div class="pidmain clearfix">
			<div class="leftsidebar fl">
				<h2>账号设置</h2>
				<ul>
					<li class="pid1">
						<a href="pid.html"> <i class="img1"></i>
							<p class="con1">PID管理</p>
						</a>
					</li>
					<li>
						<a href="updatePwd.html"> <i class="img2"></i>
							<p class="con2">修改密码</p>
						</a>
					</li>
				</ul>
			</div>
			<div class="pid-set fl">
				<p class="title">PID设置</p>
				<div class="set-infro">
					<a href="editPid.html" class="addinfro">添&nbsp;加</a>
					<table>
						<tbody>
							<tr class="td-back">
								<td class="td1">分组名称</td>
								<td class="td2">通用PID</td>
								<td class="td2">鹊桥PID</td>
								<td class="td3">操作</td>
							</tr>
							<c:if test="${empty pids }">
								<tr>
									<td class="td1" colspan="4" style="text-align:center">您还没有设置推广PID哦</td>
								</tr>
							</c:if>
							<c:forEach items="${pids }" var="pid" step="1" varStatus="i">
								<tr>
									<td class="td1">${pid.name }</td>
									<td class="td2">${pid.commonPid }</td>
									<td class="td2">${pid.queqiaoPid }</td>
									<td class="td3">
										<a href="editPid.html?id=${pid.id }">修改&nbsp;</a>|&nbsp;
										<a href="javascript:void(0)" class="del" data-id="${pid.id }">删除</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="the-prompt">
					<div class="prompt-infro fl">
						<p class="infro1">温馨提示</p>
						<p class="infro2">普通PID：平时你在阿里后台转链打开后的三段式PID</p>
						<p class="infro3">鹊桥高佣PID：转链统一从联盟鹊桥高佣获取，和普通PID的获取方式一样。此项要求填写，一方面可以分别统计推广效果，另外可以分散风险！</p>
						<p class="infro4">示例：PID为三段式，如 mm_12311550_2344296_9002527</p>
					</div>
					<div class="fl"> <img src="${pageContext.request.contextPath }/resource/images/girl.png" alt=""> </div>
				</div>
			</div>
		</div>
		<jsp:include page="${pageContext.request.contextPath }/footer.html" />
		<div id="goTop" class="goTop"></div>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/layer.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/common.js"></script>
		<script>
			$(function() {
				$('.del').click(function() {
				
					var id = $(this).data("id");
					var url = "pid/" + id + ".html";
					
					layer.confirm('确定删除？不可恢复哦', {
					  btn: ['确定','取消']
					}, function() {
						$.ajax({
							url:url,
							dataType:'json',
							type:'post',
							success:function(data){
								if(data.success){
									layer.msg(data.message||'删除成功');
									setTimeout(function(){location.reload()},1000)									
									return;
								}else{
									layer.msg(data.message,{icon:2,time:1000});
								}
							}
						})						
					},function(){
						layer.closeAll('dialog');	
					});
					return false;
				});
			});
		</script>
		<div style="height: 0px; width: 0px; overflow: hidden;">
			  <object width="0" height="0" tabindex="-1" style="height:0;width:0;overflow:hidden;" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab" id="JSocket">
			    <param name="allowScriptAccess" value="always">
			    <param name="movie" value="http://aeu.alicdn.com/flash/JSocket.swf">
			    <embed src="http://aeu.alicdn.com/flash/JSocket.swf" name="JSocket" allowscriptaccess="always" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer_cn" width="0" height="0">
			  </object>
		</div>
	</body>
</html>