<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>设置PID 魔镜助手-让推广更高效</title>
		<meta name="keywords" content="魔镜助手、鹊桥活动、淘宝客、淘宝客活动、鹊桥查询">
		<meta name="description" content="魔镜助手是一款淘宝客（鹊桥）活动商品查询软件，并集成强大的自定义筛选功能，帮您迅速找到高佣金高转化的推广商品。">
		<link rel="Bookmark" href="${pageContext.request.contextPath }/resource/images/favicon.ico">
		<link rel="Shortcut Icon" href="${pageContext.request.contextPath }/resource/images/favicon.ico">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/common.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/pidcom.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/addpid.css">		
		<script src="${pageContext.request.contextPath }/resource/js/jquery-1.10.1.min.js"></script>
		<script src="${pageContext.request.contextPath }/resource/js/jquery.lazyload.js"></script>
		<!--[if (gte IE 6)&(lte IE 8)]>
	      <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/selectivizr.js"></script>
		<![endif]-->
		<script src="${pageContext.request.contextPath }/resource/js/jquery.validate.min.js"></script>
		<script src="${pageContext.request.contextPath }/resource/js/additional-methods.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/user/editPid.js"></script>
	</head>
	<body>
		<jsp:include page="${pageContext.request.contextPath }/header.html" />
		<div class="pidmain clearfix">
			<div class="leftsidebar fl">
				<h2>账号设置</h2>
				<ul>
					<li class="pid1">
						<a href="pid.html"> <i class="img1"></i>
							<p class="con1">PID管理</p>
						</a>
					</li>
					<li>
						<a href="updatePwd.html"> <i class="img2"></i>
							<p class="con2">修改密码</p>
						</a>
					</li>
				</ul>
			</div>
			<div class="pid-set fl">
				<p class="title">添加PID</p>
				<div class="pid-infro">
					<form action="" method="post" id="pid" novalidate="novalidate">
						<ul>
							<c:if test="${!empty pid }">
								<input type="hidden" id="id" name="id" value="${pid.id }">
							</c:if>
							<li class="group-name clearfix">
								<label class="normal fl">分组名称：</label>
								<input type="text" class="normal-input fl" id="name" name="name" value="${pid.name }" placeholder="请输入分组名称">
								<div class="error-box fl">
									<p class="tip" style="display:none">请输入分组名称</p>
								</div>
							</li>
							<li class="clearfix">
								<label class="normal fl">通用PID：</label>
								<input type="text" class="normal-input fl" id="commonPid" name="commonPid" value="${pid.commonPid }" placeholder="请输入通用PID">
								<div class="error-box fl">
									<p class="tip" style="display:none">请输入通用PID</p>
								</div>
							</li>
							<li class="clearfix">
								<label class="normal fl">鹊桥PID：</label>
								<input type="text" class="normal-input fl" id="queqiaoPid" name="queqiaoPid" value="${pid.queqiaoPid }"placeholder="请输入鹊桥PID">
								<div class="error-box fl">
									<p class="tip" style="display:none">请输入鹊桥PID</p>
								</div>
							</li>
							<li>
								<div class="botn1 fl">
									<input type="submit" class="sub1" value="保 存">
								</div>
								<div class="botn2 fl">
									<button class="sub2" onclick="window.location.href = 'pid.html';return false;">取 消</button>
								</div>
							</li>
						</ul>
					</form>
				</div>
				<div class="the-prompt">
					<div class="prompt-infro fl">
						<p class="infro1">温馨提示</p>
						<p class="infro2">普通PID：平时你在阿里后台转链打开后的三段式PID</p>
						<p class="infro3">鹊桥高佣PID：转链统一从联盟鹊桥高佣获取，和普通PID的获取方式一样。此项要求填写，一方面可以分别统计推广效果，另外可以分散风险！</p>
						<p class="infro4">示例：PID为三段式，如 mm_12311550_2344296_9002527</p>
					</div>
					<div class="fl"> <img src="${pageContext.request.contextPath }/resource/images/girl.png" alt=""> </div>
				</div>
			</div>
		</div>
		<jsp:include page="${pageContext.request.contextPath }/footer.html" />
		<div id="goTop" class="goTop" style="display: none;"></div>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/layer.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/common.js"></script>
		<div style="height: 0px; width: 0px; overflow: hidden;">
			  <object width="0" height="0" tabindex="-1" style="height:0;width:0;overflow:hidden;" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab" id="JSocket">
			    <param name="allowScriptAccess" value="always">
			    <param name="movie" value="http://aeu.alicdn.com/flash/JSocket.swf">
			    <embed src="http://aeu.alicdn.com/flash/JSocket.swf" name="JSocket" allowscriptaccess="always" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer_cn" width="0" height="0">
			  </object>
		</div>
	</body>
</html>