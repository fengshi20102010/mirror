<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>用户登录 魔镜助手-让推广更高效</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/user-common.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/login.css">
		<script src="${pageContext.request.contextPath }/resource/js/jquery-1.10.1.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/layer.js"></script>
		<script src="${pageContext.request.contextPath }/resource/js/jquery.validate.min.js"></script>
		<script src="${pageContext.request.contextPath }/resource/js/additional-methods.js"></script>
		<script src="${pageContext.request.contextPath }/resource/js/user/login.js"></script>
	</head>
	<body>
		<div class="header">
			<div class="logo wth">
				<div class="logoleft fl">
					<a href="index.html"><img src="${pageContext.request.contextPath }/resource/images/logo.png"></a>
				</div>
				<div class="logoright fr"> <img src="${pageContext.request.contextPath }/resource/images/logoright.png"> </div>
			</div>
		</div>
		<div class="main wth">
			<div class="main-title">
				<p class="title-lf1 fl">老用户登录</p>
				<p class="title-lf2 fl">/</p>
				<p class="title-lf3 fl">尊敬的魔镜助手用户，欢迎您回来</p>
				<p class="title-rt1 fr">还没有账号?<span class="title-rt2"><a href="register.html">立即去注册</a></span></p>
			</div>
			<div class="main-infro">
				<div class="infro-left fl">
					<div class="content-landing">
						<form action="" method="post" id="register" novalidate="novalidate">
							<input type="hidden" id="ref" name="ref" value="${ref }">
							<ul>
								<li id="emailMatch_list">
									<div class="clearfix">
										<label class="normal fl">手机号码：</label>
										<input class="normal-input fl" type="text" id="phone" name="phone">
										<div class="error-box fl" id="email_warn"> <strong class="error" style="display:none"></strong>
											<p class="tip" style="display:none">请输入手机号码</p>
										</div>
									</div>
								</li>
								<li class="set-password clearfix">
									<label class="normal fl">密码：</label>
									<input type="password" class="normal-input fl" id="password" name="password">
									<div class="error-box fl" id="password_warn"> <strong class="error" style="display:none"></strong>
										<p class="tip" style="display:none">请输入密码</p>
									</div>
								</li>
								<li class="logintime clearfix">
									<label>
                						<input type="checkbox" class="ck fl" name="remember" checked="">两周内免登录 <span><a href="findPwd.html">忘记密码？</a></span> 
                					</label>
								</li>
								<li class="login-botn">
									<label class="normal fl"></label>
									<div class="botn">
										<input type="submit" class="sub" value="立即登录">
									</div>
								</li>
							</ul>
						</form>
					</div>
				</div>
				<div class="infro-right fr"> <img src="${pageContext.request.contextPath }/resource/images/mainfr.png" alt="魔镜助手，让推广更高效" style="width:372px;height:207px;"> </div>
			</div>
		</div>
		<div class="foot wth">
			<p>武汉云析网络科技有限公司&nbsp;鄂ICP备10209250号&nbsp;|&nbsp;ICP许可证号：鄂B1-20150109&nbsp;|&nbsp;Copyright ©&nbsp;2010-2016&nbsp;taokezhushou.com All Rights Reserved</p>
		</div>
	</body>

</html>