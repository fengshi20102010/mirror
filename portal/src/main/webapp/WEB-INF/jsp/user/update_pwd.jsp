<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>修改密码 魔镜助手-让推广更高效</title>
		<meta name="keywords" content="魔镜助手、鹊桥活动、淘宝客、淘宝客活动、鹊桥查询">
		<meta name="description" content="魔镜助手是一款淘宝客（鹊桥）活动商品查询软件，并集成强大的自定义筛选功能，帮您迅速找到高佣金高转化的推广商品。">
		<link rel="Bookmark" href="${pageContext.request.contextPath }/resource/images/favicon.ico">
		<link rel="Shortcut Icon" href="images/favicon.ico">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/common.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/pidcom.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/passwordpid.css">
		<script src="${pageContext.request.contextPath }/resource/js/jquery-1.10.1.min.js"></script>
		<script src="${pageContext.request.contextPath }/resource/js/jquery.lazyload.js"></script>
		<!--[if (gte IE 6)&(lte IE 8)]>
	      <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/selectivizr.js"></script>
		<![endif]-->
		<script src="${pageContext.request.contextPath }/resource/js/jquery.validate.min.js"></script>
		<script src="${pageContext.request.contextPath }/resource/js/additional-methods.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/user/updatePwd.js"></script>
	</head>
	<body>
		<jsp:include page="${pageContext.request.contextPath }/header.html" />
		<div class="pidmain clearfix">
			<div class="leftsidebar fl">
				<h2>账号设置</h2>
				<ul>
					<li>
						<a href="pid.html"> <i class="img1"></i>
							<p class="con1">PID管理</p>
						</a>
					</li>
					<li class="pid1">
						<a href="updatePwd.html"> <i class="img2"></i>
							<p class="con2">修改密码</p>
						</a>
					</li>
				</ul>
			</div>
			<div class="change-password fl">
				<p class="title">修改密码</p>
				<form action="" method="post" id="register" novalidate="novalidate">
					<ul>
						<li class="old-password clearfix">
							<label class="normal fl">原始密码：</label>
							<input type="password" class="normal-input fl" id="old_password" name="old_password" placeholder="请输入原始密码">
							<div class="error-box fl">
								<p class="tip" style="display:none">请输入原始密码</p>
							</div>
						</li>
						<li class="new-password clearfix">
							<label class="normal fl">新密码：</label>
							<input type="password" class="normal-input fl" id="password" name="password" placeholder="请输入新密码(6-16位)">
							<div class="error-box fl">
								<p class="tip" style="display:none">请输入新密码</p>
							</div>
						</li>
						<li class="clearfix">
							<label class="normal fl">确认新密码：</label>
							<input type="password" class="normal-input fl" id="confirm_password" name="confirm_password" placeholder="请再次输入新密码">
							<div class="error-box fl">
								<p class="tip" style="display:none">请再次输入新密码</p>
							</div>
						</li>
						<li>
							<div class="botn1 fl">
								<input type="submit" class="sub1" value="保 存">
							</div>
							<div class="botn2 fl">
								<input type="reset" class="sub2" value="取 消">
							</div>
						</li>
					</ul>
				</form>
			</div>
		</div>
		<jsp:include page="${pageContext.request.contextPath }/footer.html" />
		<div id="goTop" class="goTop" style="display: none;"></div>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/layer.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/common.js"></script>
		<div style="height: 0px; width: 0px; overflow: hidden;">
			  <object width="0" height="0" tabindex="-1" style="height:0;width:0;overflow:hidden;" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab" id="JSocket">
			    <param name="allowScriptAccess" value="always">
			    <param name="movie" value="http://aeu.alicdn.com/flash/JSocket.swf">
			    <embed src="http://aeu.alicdn.com/flash/JSocket.swf" name="JSocket" allowscriptaccess="always" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer_cn" width="0" height="0">
			  </object>
		</div>
	</body>
</html>