<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>用户注册 魔镜助手-让推广更高效</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/user-common.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/findpwd.css">
		<script src="${pageContext.request.contextPath }/resource/js/jquery-1.10.1.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/layer.js"></script>
		<script src="${pageContext.request.contextPath }/resource/js/jquery.validate.min.js"></script>
		<script src="${pageContext.request.contextPath }/resource/js/additional-methods.js"></script>
		<script src="${pageContext.request.contextPath }/resource/js/gt.js"></script>
		<link rel="stylesheet" href="${pageContext.request.contextPath }/resource/css/style.3.0.23.css">
	</head>
	<body>
		<div class="header">
			<div class="logo wth">
				<div class="logoleft fl">
					<a href="index.html"><img src="${pageContext.request.contextPath }/resource/images/logo.png"></a>
				</div>
				<div class="logoright fr"> <img src="${pageContext.request.contextPath }/resource/images/logoright.png"> </div>
			</div>
		</div>
		<div class="main wth">
			<div class="main-title">
				<p class="title-lf1 fl">找回密码</p>
				<p class="title-lf2 fl">/</p>
				<p class="title-lf3 fl">请牢记新密码哟</p>
				<p class="title-rt1 fr">已有账号,<span class="title-rt2"><a href="login.html">立即登录</a></span></p>
			</div>
			<div class="stepBox">
				<p class="ac"><i>1</i><span>验证手机号</span></p>
				<p><i>2</i><span>设置新密码</span></p>
			</div>
			<div class="main-infro">
				<div class="content-landing">
					<form id="valiphone" novalidate="novalidate">
						<ul>
							<li id="emailMatch_list">
								<div class="clearfix">
									<label class="normal fl">手机号码：</label>
									<input class="normal-input fl" type="text" id="phone" name="phone" placeholder="请输入注册时用的手机号">
									<div class="error-box fl" id="email_warn"> <strong class="error" style="display:none"></strong>
										<p class="tip" style="display:none">请输入手机号码</p>
									</div>
								</div>
							</li>
							<li id="verify_display">
								<label class="normal fl">验证码：</label>
								<input class="normal-input code-input fl" id="code" name="code" type="text" placeholder="验证码" autocomplete="off">
								<button type="button" id="sendVerifySmsButton">点击获取验证码</button>
								<div class="error-box fl" id="code_warn">
									<p class="tip" style="display:none"></p>
								</div>
							</li>
							<li>
								<label class="normal fl"></label>
								<div class="botn">
									<input type="submit" class="sub" value="验证手机">
								</div>
							</li>
						</ul>
					</form>
					<form id="submitNewPwd" novalidate="novalidate" class="submitNewPwd">
						<ul>
							<li class="set-password clearfix">
								<label class="normal fl">登录密码：</label>
								<input type="password" class="normal-input fl" id="password" name="password">
								<div class="error-box fl" id="password_warn"> <strong class="error" style="display:none"></strong>
									<p class="tip" style="display:none">请输入密码</p>
								</div>
							</li>
							<li class="set-password clearfix">
								<label class="normal fl">重复密码：</label>
								<input type="password" class="normal-input fl" id="repassword" name="repassword">
								<div class="error-box fl" id="password_warn"> <strong class="error" style="display:none"></strong>
									<p class="tip" style="display:none">请重复输入密码</p>
								</div>
							</li>
							<li>
								<label class="normal fl"></label>
								<div class="botn">
									<input type="submit" class="sub" value="提交">
								</div>
							</li>
						</ul>
					</form>
				</div>
			</div>
		</div>
		<div class="foot wth">
			<p>武汉云析网络科技有限公司&nbsp;鄂ICP备10209250号&nbsp;|&nbsp;ICP许可证号：鄂B1-20150109&nbsp;|&nbsp;Copyright ©&nbsp;2010-2016&nbsp;taokezhushou.com All Rights Reserved</p>
		</div>
		<script src="${pageContext.request.contextPath }/resource/js/user/findpwd.js"></script>
	</body>
</html>