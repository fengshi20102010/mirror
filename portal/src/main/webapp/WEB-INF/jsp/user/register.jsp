<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>用户注册 魔镜助手-让推广更高效</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/user-common.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/register.css">
		<script src="${pageContext.request.contextPath }/resource/js/jquery-1.10.1.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/layer.js"></script>
		<script src="${pageContext.request.contextPath }/resource/js/jquery.validate.min.js"></script>
		<script src="${pageContext.request.contextPath }/resource/js/additional-methods.js"></script>
		<script src="${pageContext.request.contextPath }/resource/js/gt.js"></script>
		<link rel="stylesheet" href="${pageContext.request.contextPath }/resource/css/style.3.0.23.css">
	</head>
	<body>
		<div class="header">
			<div class="logo wth">
				<div class="logoleft fl">
					<a href="index.html"><img src="${pageContext.request.contextPath }/resource/images/logo.png"></a>
				</div>
				<div class="logoright fr"> <img src="${pageContext.request.contextPath }/resource/images/logoright.png"> </div>
			</div>
		</div>
		<div class="main wth">
			<div class="main-title">
				<p class="title-lf1 fl">新用户注册</p>
				<p class="title-lf2 fl">/</p>
				<p class="title-lf3 fl">注册魔镜助手，高效推广</p>
				<p class="title-rt1 fr">已有账号,<span class="title-rt2"><a href="login.html">立即登录</a></span></p>
			</div>
			<div class="main-infro clearfix">
				<div class="content-landing fl">
					<form id="register" novalidate="novalidate">
						<ul>
							<li id="emailMatch_list">
								<div class="clearfix">
									<label class="normal fl">手机号码：</label>
									<input class="normal-input fl" type="text" id="phone" name="phone">
									<div class="error-box fl" id="email_warn"> <strong class="error" style="display:none"></strong>
										<p class="tip" style="display:none">请输入手机号码</p>
									</div>
								</div>
							</li>
							<li id="verify_display">
								<label class="normal fl">验证码：</label>
								<input class="normal-input code-input fl" id="code" name="code" type="text" placeholder="验证码" autocomplete="off">
								<button type="button" id="sendVerifySmsButton">点击获取验证码</button>
								<div class="error-box fl" id="code_warn">
									<p class="tip" style="display:none"></p>
								</div>
							</li>
							<li class="set-password clearfix">
								<label class="normal fl">登录密码：</label>
								<input type="password" class="normal-input fl" id="password" name="password">
								<div class="error-box fl" id="password_warn"> <strong class="error" style="display:none"></strong>
									<p class="tip" style="display:none">请输入密码</p>
								</div>
							</li>
							<li>
								<label class="normal fl"></label>
								<div class="botn">
									<input type="submit" class="sub" value="立即注册">
								</div>
							</li>
							<li class="agreement">
								<div class="agree"> 
									<span>
						              <label>
						                <input type="checkbox" class="ck fl" name="agree" checked="checked">同意<a href="javascript:;" target="_blank">《魔镜助手网络服务使用协议》</a> </label>
				              		</span> 
				              	</div>
							</li>
						</ul>
					</form>
				</div>
				<div class="infro-right fr">
					<a href="javascript:;"><img src="${pageContext.request.contextPath }/resource/images/main_fr.png" alt="魔镜助手，让推广更高效"></a>
				</div>
			</div>
		</div>
		<div class="foot wth">
			<p>武汉云析网络科技有限公司&nbsp;鄂ICP备10209250号&nbsp;|&nbsp;ICP许可证号：鄂B1-20150109&nbsp;|&nbsp;Copyright ©&nbsp;2010-2016&nbsp;taokezhushou.com All Rights Reserved</p>
		</div>
		<script src="${pageContext.request.contextPath }/resource/js/user/register.js"></script>
	</body>
</html>