<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:if test="${empty su }">
	<div class="main1_right1">
		<div class="rootload ">
			<ul>
				<li>
					<a href="${pageContext.request.contextPath }/login.html">用户登录</a>
				</li>
				<li class="rootld">
					<a href="${pageContext.request.contextPath }/register.html">用户注册</a>
				</li>
			</ul>
		</div>
		<div class="right2">
			<form method="post" id="login">
				<p>登录名: </p>
				<div class="error"></div>
				<input type="text" name="phone" id="phone" value="">
				<p>登录密码:<span class="ft-pw"><a href="${pageContext.request.contextPath }/findPwd.html">忘记登录密码？</a></span></p>
				<input type="password" name="password" id="password">
				<p class="loading">
					<input type="button" class="login" value="登 录">
				</p>
			</form>
		</div>
	</div>
</c:if>
<c:if test="${!empty su }">
	<div class="main1_right3">
		<div class="rootload3">
			<img src="${pageContext.request.contextPath }/resource/images/user.png">
			<h3>${fn:substring(su.phone,0,3) }****${fn:substring(su.phone,7,-1) }<span><a href="javascript:" class="logout">[退出]</a></span></h3>
			<p>
				<a href="${pageContext.request.contextPath }/pid.html">进入用户中心</a>
			</p>
			<ul>
				<li>
					<a href="${pageContext.request.contextPath }/pid.html">设置PID</a>
				</li>
				<li class="rt-bd">
					<a href="${pageContext.request.contextPath }/updatePwd.html">修改密码</a>
				</li>
				<li class="rt-bd">
					<a href="javascript:;">收藏中心</a>
				</li>
			</ul>
		</div>
	</div>
</c:if>
<script type="application/javascript">
	$(document).ready(function(){
		// 登陆
		$('.login').on('click', function(){
			var phone = $('#phone').val();
			var password = $('#password').val();
			if(!phone){
				layer.msg('请输入电话号码');
				return;
			}
			if(!password){
				layer.msg('请输入密码');
				return;
			}
			$.ajax({
				url:'${pageContext.request.contextPath }/login.html',
				dataType:'json',
				type:'post',
				data: {'phone':phone,'password':password},
				success:function(data){
					if(data.success){
						location.reload();
						return;
					}else{
						layer.msg(data.message,{icon:2,time:1000});
					}
				}
			})
		});
		// 退出登陆
		$('.logout').on('click', function(){
			layer.confirm('确认退出登陆吗？', {icon: 3, title:'退出登陆'}, function(index){
				$.ajax({
					url:'${pageContext.request.contextPath }/logout.html',
					dataType:'json',
					type:'get',
					success:function(data){
						if(data.success){
							location.reload();
							return;
						}else{
							layer.msg(data.message,{icon:2,time:1000});
						}
					}
				})
				layer.close(index);
			});
		});
	})
</script>