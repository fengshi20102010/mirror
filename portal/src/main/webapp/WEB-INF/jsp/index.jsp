<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!--<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/ab0aed3533ff4d7ef444f610d3ce1d1c.js"></script>-->
		<title>魔镜助手-让推广更高效 大淘宝客首选，最强大的淘宝隐藏券搜索引擎|鹊桥搜索|大淘客联盟</title>
		<meta name="keywords" content="淘宝客，魔镜助手，大淘客，轻淘客，查淘客，鹊桥查询，隐藏优惠券，淘宝客软件">
		<meta name="description" content="魔镜助手是10万淘宝客推荐的免费软件!魔镜助手涵盖淘宝全网商品一键查询淘宝客佣金,隐藏优惠券,自动申请高佣金计划等功能。使用魔镜助手工具效率提升10倍以上,因为专业,所以信赖!">
		<link rel="Bookmark" href="${pageContext.request.contextPath }/resource/images/favicon.ico">
		<link rel="Shortcut Icon" href="${pageContext.request.contextPath }/resource/images/favicon.ico">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/common.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/indexcom.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/index1.css">
		<script src="${pageContext.request.contextPath }/resource/js/jquery-1.10.1.min.js"></script>
		<script src="${pageContext.request.contextPath }/resource/js/jquery.lazyload.js"></script>
		<!--[if (gte IE 6)&(lte IE 8)]>
      		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/selectivizr.js"></script>
		<![endif]--> 
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/unslider.min.js"></script>		
	</head>

	<body>
		<jsp:include page="${pageContext.request.contextPath }/header.html" />
		<div class="main">
			<div class="main1 wth">
				<div class="main1_left fl">
					<div class="bg"></div>
					<ul class="clearfix">
						<li class="lis1">
							<div class="ldicon icon1"> <i></i>
								<h2><a href="javascript:;" target="_blank">浏览器插件</a></h2>
								<p>佣金、优惠券快捷查询</p>
							</div>
						</li>
						<li class="lis1">
							<div class="ldicon bd-top icon2"> <i></i>
								<h2><a href="javascript:layer.alert(&#39;紧张开发中，尽情期待&#39;);">PC客户端</a></h2>
								<p>批量生成链接，一键群发</p>
							</div>
						</li>
						<li class="lis1">
							<div class="ldicon bd-top icon3"> <i></i>
								<h2><a href="javascript:layer.alert(&#39;给我们一点点时间，我们能做得更好&#39;);">CMS系统</a></h2>
								<p>0门槛建站，提升收益</p>
							</div>
						</li>
						<li class="lis1">
							<div class="ldicon bd-top icon4"> <i></i>
								<h2><a href="javascript:layer.alert(&#39;给我们一点点时间，我们能做得更好&#39;);">手机APP</a></h2>
								<p>微信推广方便快捷</p>
							</div>
						</li>
					</ul>
				</div>
				<div class="main1_center fl">
					<div class="banner has-dots">
						<ul>
							<li>
								<a href="javascript:;" target="_blank"><img src="${pageContext.request.contextPath }/resource/images/banner/banner_1.jpg" alt="广告3" width="1920"></a>
							</li>
							<li>
								<a href="javascript:;" target="_blank"><img src="${pageContext.request.contextPath }/resource/images/banner/banner_2.jpg" alt="广告2" width="1920"></a>
							</li>
							<li>
								<a href="javascript:;" target="_blank"><img src="${pageContext.request.contextPath }/resource/images/banner/banner_3.jpg" alt="广告1" width="1920"></a>
							</li>
						</ul>
					</div>
				</div>
				<div class="main1_right fr">
					<jsp:include page="${pageContext.request.contextPath }/userInfo.html" />
					<div class="main1_right2">
						<ul class="right2-nav clearfix">
							<li class="right-nav1">
								<a href="javascript:;" style="background-color: rgb(255, 255, 255);">公告</a>
							</li>
							<li class="right-nav2">
								<a href="javascript:;">推荐</a>
							</li>
							<li class="right-nav3">
								<a href="javascript:;">教程</a>
							</li>
						</ul>
						<div class="right2_btm">
							<ul class="message1">
								<li>
									<a href="javascript:;" target="_blank">自定义模板无法保存的Bug已经修复</a>
								</li>
								<li>
									<a href="javascript:;" target="_blank">魔镜助手转链以及优惠券商品二合一页发布公告!</a>
								</li>
							</ul>
							<ul class="message2" style="display: none;">
							</ul>
							<ul class="message3" style="display: none;">
								<li>
									<a href="javascript:;" target="_blank">魔镜助手网站插件注册使用教程</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="main3">
				<div class="wth">
					14点整 直播主题： <span>妈妈的心肝宝贝</span> 直播用户想要的、想了解的、未知的、时刻对群都充满好奇..
					<a href="javascript:;">[进入直播间]</a>
				</div>
			</div>
			<div class="main4 wth clearfix">
				<a href="javascript:;"><img src="${pageContext.request.contextPath }/resource/images/img/201610201.jpg" alt="" /></a>
				<a href="javascript:;"><img src="${pageContext.request.contextPath }/resource/images/img/201610202.jpg" alt="" /></a>
				<a href="javascript:;"><img src="${pageContext.request.contextPath }/resource/images/img/201610203.jpg" alt="" /></a>
				<a href="javascript:;"><img src="${pageContext.request.contextPath }/resource/images/img/201610204.jpg" alt="" /></a>
				<a href="javascript:;" class="end"><img src="${pageContext.request.contextPath }/resource/images/img/201610205.jpg" alt="" /></a>
			</div>
			<jsp:include page="${pageContext.request.contextPath }/type.html">
				<jsp:param name="t" value="1"/>
			</jsp:include>
		</div>
		<div class="goods clearfix">
			<!-- <div class="goods0 wth clearfix">
				<h3 class="block-index-title">
			<span class="title-logo title-logo-tb"></span>淘宝行业市场精选
				</h3>
				<ul class="middle-contents  clearfix">
					<li class="middle-block">
						<a href="javascript:;"><img src="http://img.alicdn.com/tps/TB1JQ9gKFXXXXX.XFXXXXXXXXXX-592-236.jpg" p-id="241"></a>
					</li>
					<li class="middle-block">
						<a href="javascript:;"><img src="http://img.alicdn.com/tps/TB1oyxZNpXXXXbdXVXXXXXXXXXX-594-236.png" p-id="244"></a>
					</li>
					<li class="middle-block">
						<a href="javascript:;"><img src="http://img.alicdn.com/tps/TB1EdnZKpXXXXc9XpXXXXXXXXXX-592-236.png" p-id="247"></a>
					</li>
					<li class="middle-block">
						<a href="javascript:;"><img src="http://img.alicdn.com/tps/TB10R6.KpXXXXceXXXXXXXXXXXX-592-236.png" p-id="250"></a>
					</li>
					<li class="middle-block">
						<a href="javascript:;"><img src="http://img.alicdn.com/tps/TB1sGn9KpXXXXXPXpXXXXXXXXXX-592-236.png" p-id="253"></a>
					</li>
					<li class="middle-block">
						<a href="javascript:;"><img src="http://img.alicdn.com/tps/TB1W.6YKpXXXXbBXFXXXXXXXXXX-592-236.png" p-id="256"></a>
					</li>
					<li class="middle-block">
						<a href="javascript:;"><img src="http://img.alicdn.com/tps/TB1xZCmKFXXXXalXpXXXXXXXXXX-592-236.jpg" p-id="259"></a>
					</li>
					<li class="middle-block">
						<a href="javascript:;"><img src="https://img.alicdn.com/tps/TB1IMTTKpXXXXXIXVXXXXXXXXXX-592-236.png" p-id="262"></a>
					</li>
				</ul>
			</div> -->
			<div class="goods1 wth clearfix">
				<div class="newdays1 newds1 clearfix"> <i></i>
					<p>每天10点上新</p>
				</div>
				<ul class="mg clearfix">
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank">
								<img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287">
							</a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia">
								<span class="ac1"><i></i>快乐渔家居旗舰店</span>
								<span class="ac2">通用</span>
								<span class="ac3">秒过</span>
							</div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank">
								<img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287">
							</a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia">
								<span class="ac1"><i></i>快乐渔家居旗舰店</span>
								<span class="ac2">通用</span>
								<span class="ac3">秒过</span>
							</div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank">
								<img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287">
							</a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia">
								<span class="ac1"><i></i>快乐渔家居旗舰店</span>
								<span class="ac2">通用</span>
								<span class="ac3">秒过</span>
							</div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li class="good1_one fl">
						<div class="goods-a">
							<a href="javascript:;" target="_blank">
								<img src="${pageContext.request.contextPath }/resource/images/waiting.png" class="lazy" data-original="${pageContext.request.contextPath }/resource/images/img/f179659b2752cf22b37469d58a1bc740.jpg@1e_1c_0o_0l_287h_287w_100q.src" width="287" height="287">
							</a>
							<div class="title"> <i class="tit1"></i>
								<p>
									<a href="javascript:;" target="_blank">iPhone7plus钢化膜3D全屏全覆盖 苹</a>
								</p>
							</div>
							<div class="coupon">
								<ul>
									<li class="fl">优惠券<span class="num1 gd_wd">1.00</span>元</li>
									<li class="com1 fr">剩余数量<span class="num2 gd_wd">8926</span>/<span class="num3">10000</span></li>
								</ul>
							</div>
							<div class="commission">
								<ul>
									<li class="com1 gd_wd2 fl">佣金<span>50.3%</span></li>
									<li class="com4 fr">目前销量<span class="com4_num gd_wd2">7181</span></li>
								</ul>
							</div>
							<div class="satia">
								<span class="ac1"><i></i>快乐渔家居旗舰店</span>
								<span class="ac2">通用</span>
								<span class="ac3">秒过</span>
							</div>
							<div class="good_btm">
								<ul>
									<li class="btm1 fl">券后&nbsp;¥&nbsp;<span class="value">1.9</span></li>
									<li class="btm2 fl">¥2.9</li>
									<li class="btm3 fr">
										<a href="javascript:;" target="_blank">立即推广</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<jsp:include page="${pageContext.request.contextPath }/goods/index.html" />
			<div class="pages wth">
				<ul class="pagination">
					<li class="disabled"><span>上一页</span></li>
					<li class="active"><span>1</span></li>
					<li>
						<a href="javascript:;">2</a>
					</li>
					<li>
						<a href="javascript:;">3</a>
					</li>
					<li>
						<a href="javascript:;">4</a>
					</li>
					<li>
						<a href="javascript:;">5</a>
					</li>
					<li>
						<a href="javascript:;">6</a>
					</li>
					<li>
						<a href="javascript:;">7</a>
					</li>
					<li>
						<a href="javascript:;">8</a>
					</li>
					<li class="disabled"><span>...</span></li>
					<li>
						<a href="javascript:;">99</a>
					</li>
					<li>
						<a href="javascript:;">100</a>
					</li>
					<li>
						<a href="javascript:;" rel="next">下一页</a>
					</li>
				</ul>
			</div>
		</div>
		<jsp:include page="${pageContext.request.contextPath }/footer.html" />
		<div id="goTop" class="goTop" style="display: none;"></div>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/layer.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/common.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/index1.js"></script>
		<div style="height: 0px; width: 0px; overflow: hidden;">
		  <object width="0" height="0" tabindex="-1" style="height:0;width:0;overflow:hidden;" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab" id="JSocket">
		    <param name="allowScriptAccess" value="always">
		    <param name="movie" value="http://aeu.alicdn.com/flash/JSocket.swf">
		    <embed src="http://aeu.alicdn.com/flash/JSocket.swf" name="JSocket" allowscriptaccess="always" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer_cn" width="0" height="0">
		  </object>
		</div>
	</body>
</html>