<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<div class="hd">
	<div class="hd1">
		<div class="hd1 wth">
			<div class="hd1_left fl">
				<p>
					欢迎加入大淘客交流群：
					<a href="javascript:;" target="_blank">311109018</a>（验证语：淘宝客） 招商负责人QQ：
					<a href="http://wpa.qq.com/msgrd?v=3&amp;uin=3001112570&amp;site=qq&amp;menu=yes" target="_blank">3001112570</a>（验证语：店铺名）
				</p>
			</div>
			<div class="hd1_right fr">
				<ul>
					<c:if test="${empty su }">
						<li>
							<a href="${pageContext.request.contextPath }/login.html">登录</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath }/register.html">免费注册</a>
						</li>
						<li>
							<a href="javascript:;">收藏本站</a>
						</li>
						<li>
							<a href="javascript:;">设为首页</a>
						</li>
					</c:if>
					<c:if test="${!empty su }">
						<li>当前登录用户：${fn:substring(su.phone,0,3) }****${fn:substring(su.phone,7,-1) }</li>
						<li>
							<a href="${pageContext.request.contextPath }/pid.html">用户中心</a>
						</li>
						<li>
							<a href="javascript:" class="logout">退出登录</a>
						</li>
						<li>
							<a href="javascript:;">收藏本站</a>
						</li>
						<li>
							<a href="javascript:;">设为首页</a>
						</li>
						<script type="application/javascript">
							$(document).ready(function(){
								// 退出登陆
								$('.logout').on('click', function(){
									layer.confirm('确认退出登陆吗？', {icon: 3, title:'退出登陆'}, function(index){
										$.ajax({
											url:'${pageContext.request.contextPath }/logout.html',
											dataType:'json',
											type:'get',
											success:function(data){
												if(data.success){
													location.reload();
													return;
												}else{
													layer.msg(data.message,{icon:2,time:1000});
												}
											}
										})
										layer.close(index);
									});
								});
							})
						</script>
					</c:if>
				</ul>
			</div>
		</div>
	</div>
	<div class="logo">
		<div class="hd2 clearfix wth">
			<h1 class="hd2_left fl">
				<a href="javascript:;"><img src="${pageContext.request.contextPath }/resource/images/logo.png"
					alt="魔镜助手" width="207" height="60"></a>
			</h1>
			<div class="hd2_search fl">
				<span class="search_type search_type_1"><a
					href="javascript:;" class="search">站内搜索</a></span> <span class="search_type search_type_2"><a href="javascript:;"
					target="_blank">鹊桥搜索</a></span>
				<form action="search.html" method="get" class="clearfix">
					<input type="text" class="search_text fl" placeholder="请输入您要搜索的商品（本站）名称或链接" name="q" value=""> <input type="submit" value="搜 索" class="search_btn fl">
				</form>
				<div class="kuaiji">
					<a href="javascript:;">套装</a>
					<a href="javascript:;" class="gd_wd2">针织衣</a>
					<a href="javascript:;">卫衣</a>
					<a href="javascript:;">外套</a>
					<a href="javascript:;" class="gd_wd2">连衣裙</a>
					<a href="javascript:;" class="gd_wd2">短靴</a>
					<a href="javascript:;">睡衣</a>
					<a href="javascript:;">小白鞋</a>
					<a href="javascript:;">运动鞋</a>
					<a href="javascript:;">双肩包</a>
					<a href="javascript:;" class="gd_wd2">风衣</a>
					<a href="javascript:;">大衣</a>
					<a href="javascript:;">高跟鞋</a>
					<a href="javascript:;">口红</a>
				</div>
			</div>
			<div class="hd2_right fr">
				<img src="${pageContext.request.contextPath }/resource/images/logofr.png" alt="让推广更高效" width="122" height="100">
			</div>
		</div>
		<div class="hd_nav wth">
			<ul>
				<li class="nav1">
					<a href="index.html" class="navone">首页</a>
				</li>
				<li>
					<a class="hv" href="javascript:;">实时热推榜<i></i></a>
				</li>
				<li>
					<a class="hv" href="javascript:layer.alert(&#39;相关教程正在录制....&#39;);">预热直播</a>
				</li>
				<li>
					<a class="hv" href="javascript:layer.alert(&#39;API接口暂未开放，测试稳定后我们会及时放出&#39;);">开放API</a>
				</li>
				<li>
					<a class="hv" href="javascript:layer.alert(&#39;圈子功能正在开发，上线还需要一点点时间&#39;);">淘客社区</a>
				</li>
			</ul>
		</div>
	</div>
</div>