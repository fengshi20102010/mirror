<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<div class="foot">
	<div class="foot1 wth">
		<ul class="clearfix">
			<li>帮助中心
				<ul>
					<li>
						<a href="javascript:;">淘宝助手安装指南</a>
					</li>
				</ul>
			</li>
			<li>常见问题
				<ul>
					<li>
						<a href="javascript:;">招商规则</a>
					</li>
					<li>
						<a href="javascript:;">违规商家处罚</a>
					</li>
					<li>
						<a href="javascript:;">商家如何报名</a>
					</li>
				</ul>
			</li>
			<li>投诉意见
				<ul>
					<li>
						<a href="javascript:;">招商违规投诉</a>
					</li>
					<li>
						<a href="javascript:;">应用建议</a>
					</li>
				</ul>
			</li>
			<li>关于我们
				<ul>
					<li>
						<a href="javascript:;">关于我们</a>
					</li>
					<li>
						<a href="javascript:;">联系我们</a>
					</li>
				</ul>
			</li>
			<li class="end">关注我们
				<ul>
					<li><img src="${pageContext.request.contextPath }/resource/images/foot1.png" alt="二维码"></li>
					<li>微博：
						<a href="javascript:;" class="a" title="新浪"></a>
						<a href="javascript:;" class="b" title="微博"></a>
					</li>
				</ul>
			</li>
		</ul>
	</div>
	<div class="foot2">
		<div class="foot2_image wth">
			<ul class="clearfix">
				<li class="imageone">
					<a href="javascript:;">腾讯开发平台</a><i>.</i>
					<a href="javascript:;">QQ物联</a><i>.</i>
					<a href="javascript:;">DNSPod</a><i>.</i>
					<a href="javascript:;">企业QQ</a><i>.</i>
					<a href="javascript:;">腾讯地图</a><i>.</i>
					<a href="javascript:;">腾讯蓝鲸</a><i>.</i>
					<a href="javascript:;">QQ音乐</a><i>.</i>
					<a href="javascript:;">腾讯微云</a><i>.</i>
					<a href="javascript:;">手机QQ空间</a><i>.</i>
					<a href="javascript:;">友情链接</a>
				</li>
				<li class="imagetwo">Copyright © 2013-2016 Qcloud.com.All Rigths Reserverd.腾讯版权所有</li>
				<li class="imagethree">增信电信业务许可证：B1.B2-20130326 <img src="${pageContext.request.contextPath }/resource/images/备案图标.png" /> 京ICP备11018762号 京ICP证150476号
				</li>
			</ul>
		</div>
	</div>
</div>