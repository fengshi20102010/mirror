package com.mirror.tk.web.controller.goods;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.common.collect.Maps;
import com.mirror.tk.core.biz.dto.GoodsTypeDto;
import com.mirror.tk.core.module.goods.service.GoodsCloudService;
import com.mirror.tk.core.module.goods.service.GoodsCouponService;
import com.mirror.tk.core.module.goods.service.GoodsService;
import com.mirror.tk.core.module.goods.service.GoodsTypeService;
import com.mirror.tk.web.controller.CommonController;

/**
 * 商品相关controller
 */
@Controller
public class GoodsController extends CommonController {

	private static final String GOODS_TYPE = "goods/type";
	private static final String GOODS_INDEX = "goods/index";
	private static final String GOODS_TOP100 = "goods/top100";
	private static final String GOODS_LAUNCH = "goods/launch";
	private static final String GOODS_DETAIL = "goods/detail";
	private static final String GOODS_SEARCH = "goods/search";
	private static final String GOODS_SECKILL = "goods/seckill";
	private static final String GOODS_RECOMMEND = "goods/recommend";
	
	@Resource
	private GoodsTypeService goodsTypeService;
	@Resource
	private GoodsService goodsService;
	@Resource
	private GoodsCloudService goodsCloudService;
	@Resource
	private GoodsCouponService goodsCouponService;
	
	/**
	 * 获取类型信息
	 * @param request
	 * @param map
	 * @param t
	 * @return
	 */
	@RequestMapping(value = "type", method = RequestMethod.GET)
	public String typeInfo(HttpServletRequest request, ModelMap map ,Integer t){
		// 获取类型信息
		List<GoodsTypeDto> list = goodsTypeService.getNormalType();
		Integer all = 0;
		for (GoodsTypeDto dto : list) {
			all += dto.getGoodsCount();
		}
		map.put("types", list);
		map.put("all", all);
		map.put("t", t);
		return GOODS_TYPE;
	}
	
	/**
	 * 商品首页信息
	 * @param request
	 * @param map
	 * @param t
	 * @return
	 */
	@RequestMapping(value = "goods/index", method = RequestMethod.GET)
	public String index(HttpServletRequest request, ModelMap map ,Integer t){
		Map<String, Object> searchMap = Maps.newHashMap();
		if(null != t){
			searchMap.put("EQ_typeId", t);
		}
		map.put("pageInfo", goodsService.queryGoodsInfo(getPageInfo(request), searchMap));
		return GOODS_INDEX;
	}
	
	/**
	 * 商品销量100榜
	 * @param reequest
	 * @return
	 */
	@RequestMapping(value = "top100", method = RequestMethod.GET)
	public String top100(HttpServletRequest reequest){
		return GOODS_TOP100;
	}
	
	/**
	 * 商品详情
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "goods/{id}")
	public String detail(HttpServletRequest request, @PathVariable Long id, ModelMap map){
		map.put("goods", goodsService.queryGoodsInfo(id));
		return GOODS_DETAIL;
	}
	
	/**
	 * 搜索商品
	 * @param request
	 * @param q
	 * @return
	 */
	@RequestMapping(value = "search", method = RequestMethod.GET)
	public String search(HttpServletRequest request, 
			@RequestParam(value = "q", required = true)String q){
		return GOODS_SEARCH;
	}
	
	/**
	 * 今日推荐
	 * @param request
	 * @return
	 */
	public String recommend(HttpServletRequest request){
		return GOODS_RECOMMEND;
	}
	
	/**
	 * 今日新品
	 * @param request
	 * @return
	 */
	public String launch(HttpServletRequest request){
		return GOODS_LAUNCH;
	}
	
	/**
	 * 秒杀活动
	 * @param request
	 * @return
	 */
	public String seckill(HttpServletRequest request){
		return GOODS_SECKILL;
	}
	
}
