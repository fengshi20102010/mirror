package com.mirror.tk.web.common.support;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import com.mirror.tk.core.module.user.domain.User;

/**
 * 前端用户session对象
 */
public class SessionUser implements HttpSessionBindingListener {

	public static final String SESSION_USER_OBJECT_KEY = "session_user_obj";

	private SessionUser() {
	}

	public static SessionUser bulider(User user) {
		return new SessionUser().update(user);
	}

	public SessionUser update(User user) {
		this.setId(user.getId());
		this.setUsername(user.getUsername());
		this.setPhone(user.getPhone());
		this.setQq(user.getQq());
		this.setWechat(user.getWechat());
		this.setStatus(user.getStatus());
		return this;
	}

	/** id */
	private Long id;

	/** 用户名 */
	private String username;

	/** 电话 */
	private String phone;

	/** QQ */
	private String qq;

	/** 微信 */
	private String wechat;

	/** 用户状态 */
	private Integer status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		if (SESSION_USER_OBJECT_KEY.equals(event.getName())	&& event.getValue() instanceof SessionUser) {
			SessionUser sessionUser = (SessionUser) event.getValue();
			SessionFace.userMap.put(sessionUser.getUsername(), sessionUser);
		}
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		if (SESSION_USER_OBJECT_KEY.equals(event.getName())	&& event.getValue() instanceof SessionUser) {
			SessionUser sessionUser = (SessionUser) event.getValue();
			SessionFace.userMap.remove(sessionUser.getUsername());
		}
	}

}
