package com.mirror.tk.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mirror.tk.web.common.support.SessionFace;

/**
 * 首页及公用页面
 */
@Controller
public class IndexController extends CommonController {

	private static final String INDEX = "index";
	private static final String HEADER = "common/header";
	private static final String FOOTER = "common/footer";
	
	/**
	 * 首页
	 * @return
	 */
	@RequestMapping(value = {"index","/"}, method = RequestMethod.GET)
	public String index() {
		return INDEX;
	}
	
	/**
	 * 头部信息
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "header", method = RequestMethod.GET)
	public String header(HttpServletRequest request, ModelMap map){
		// 如果用户登陆过，将用户信息放入其中
		if(SessionFace.isUserLogin(request)){
			map.put("su", SessionFace.getSessionUser(request));
		}
		// TODO 热门关键词等数据的初始化也在这里
		return HEADER;
	}
	
	/**
	 * 尾部信息
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "footer", method = RequestMethod.GET)
	public String footer(HttpServletRequest request){
		return FOOTER;
	}

}
