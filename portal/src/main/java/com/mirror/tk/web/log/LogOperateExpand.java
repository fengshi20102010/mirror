package com.mirror.tk.web.log;

import javax.servlet.http.HttpServletRequest;

import com.mirror.tk.core.log.aop.LogAdviceExpand;
import com.mirror.tk.core.module.sys.domain.SysOlog;
import com.mirror.tk.web.common.support.SessionFace;
import com.mirror.tk.web.common.support.SessionUser;

public class LogOperateExpand implements LogAdviceExpand {

    @Override
    public void expand(HttpServletRequest request, SysOlog olog) {
        //管理平台操作用户
        SessionUser sessionUser = SessionFace.getSessionUser(request);
        if(sessionUser!=null){
            //olog.setOperateUserId(sessionUser.getTenantId());
            //olog.setOperateUser(sessionUser.getTenantName());
        }else{
            //店铺用户
            /*SessionBranchUser branchUser = SessionFace.getSessionBranchUser(request);
            if(branchUser!=null){
                olog.setOperateUserId(branchUser.getBranchId());
                olog.setOperateUser(branchUser.getBranchName());
            }*/
        }
    }
    
}
