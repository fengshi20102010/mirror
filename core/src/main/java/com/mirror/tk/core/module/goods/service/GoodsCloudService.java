package com.mirror.tk.core.module.goods.service;

import com.mirror.tk.core.module.goods.domain.GoodsCloud;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 商品推送云库表 Service
 *
 * Date: 2016-11-01 23:10:19
 *
 * @author Code Generator
 *
 */
public interface GoodsCloudService extends EntityService<GoodsCloud> {

}
