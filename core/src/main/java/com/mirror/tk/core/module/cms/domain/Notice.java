package com.mirror.tk.core.module.cms.domain;


import java.util.Date;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.google.common.collect.Maps;
import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 公告/信息/教程相关数据表 Entity
 *
 * Date: 2016-11-01 23:10:03
 *
 * @author Acooly Code Generator
 */
@Entity
@Table(name = "notice")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Notice extends AbstractEntity {
	/** ID */
	private Long id;
	
	/** 公告名称 */
	private String name;
	
	/** 公告内容 */
	private String content;
	
	/** 公告状态 */
	private Integer status;
	
	/** 公告类型 */
	private Integer type;
	
	/** 发布者 */
	private String author;
	
	/** 阅读次数 */
	private Long readNo;
	
	/** 创建时间 */
	private Date createTime;
	
	/** 停用 */
	public static final Integer STATUS_DISABLE = 0;
	/** 启用 */
	public static final Integer STATUS_ENABLE = 1;
		
	/** 状态 */
	public static Map<Integer, String> allStatuss = Maps.newTreeMap();
	static {
		allStatuss.put(STATUS_DISABLE, "停用");
		allStatuss.put(STATUS_ENABLE, "启用");
	}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getName(){
		return this.name;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getContent(){
		return this.content;
	}

	public void setContent(String content){
		this.content = content;
	}

	public Integer getStatus(){
		return this.status;
	}

	public void setStatus(Integer status){
		this.status = status;
	}

	public Integer getType(){
		return this.type;
	}

	public void setType(Integer type){
		this.type = type;
	}

	public String getAuthor(){
		return this.author;
	}

	public void setAuthor(String author){
		this.author = author;
	}

	public Long getReadNo(){
		return this.readNo;
	}

	public void setReadNo(Long readNo){
		this.readNo = readNo;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
