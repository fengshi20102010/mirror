package com.mirror.tk.core.module.user.domain;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 用户QQ/微信群详情表 Entity
 *
 * Date: 2017-01-01 22:30:33
 *
 * @author Code Generator
 */
@Entity
@Table(name = "user_group_detail")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserGroupDetail extends AbstractEntity {
	/** id */
	private Long id;
	
	/** 群组id */
	private Long groupId;
	
	/** 群名称 */
	private String name;
	
	/** 群号 */
	private String number;
	
	/** 创建时间 */
	private Date createTime;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getName(){
		return this.name;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getNumber(){
		return this.number;
	}

	public void setNumber(String number){
		this.number = number;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
