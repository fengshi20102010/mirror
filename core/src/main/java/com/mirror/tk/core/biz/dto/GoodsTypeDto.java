package com.mirror.tk.core.biz.dto;

import java.io.Serializable;

public class GoodsTypeDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String name;

	private Integer goodsCount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getGoodsCount() {
		return goodsCount;
	}

	public void setGoodsCount(Integer goodsCount) {
		this.goodsCount = goodsCount;
	}

}
