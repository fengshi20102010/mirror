package com.mirror.tk.core.module.goods.service.impl;


import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mirror.tk.core.module.goods.dao.GoodsCouponDao;
import com.mirror.tk.core.module.goods.domain.GoodsCoupon;
import com.mirror.tk.core.module.goods.service.GoodsCouponService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("goodsCouponService")
public class GoodsCouponServiceImpl extends EntityServiceImpl<GoodsCoupon, GoodsCouponDao> implements GoodsCouponService {

	@Override
	public List<GoodsCoupon> findByNormal() {
		return this.getEntityDao().findByNormal();
	}

	@Override
	@Transactional
	public Boolean deleteByGoodsIds(List<Long> goodsIds) {
		return this.getEntityDao().deleteByGoodsIds(goodsIds) > 0;
	}

}
