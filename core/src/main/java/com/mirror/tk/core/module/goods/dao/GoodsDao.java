package com.mirror.tk.core.module.goods.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mirror.tk.core.module.goods.domain.Goods;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 商品信息主表 JPA Dao
 *
 * Date: 2016-11-01 23:10:23
 *
 * @author Code Generator
 *
 */
public interface GoodsDao extends EntityJpaDao<Goods, Long> {

	Goods findByNumIid(Long numIid);

	@Modifying
	@Query("update Goods set status = ?2 where id = ?1 ")
	int updateByStatus(Long id, Integer status);

	@Modifying
	@Query("update Goods set sales = ?2 where id = ?1 ")
	int updateById(Long id, Long sales);

	@Modifying
	@Query("delete from Goods where numIid in(?2) and userId = ?1 and status < 3")
	int deleteMyGoods(Long id, List<Long> ids);

	@Query(value = "select count(g.id) from goods g where g.shop_id = ?1 ", nativeQuery = true)
	int countByShopId(Long shopId);
	
}
