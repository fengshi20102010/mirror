package com.mirror.tk.core.biz.req;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class ReqMessageGatherQueryInfo {

   	@ApiModelProperty(value = "ID", required = false)
	private Integer id;
	@ApiModelProperty(value = "用戶ID", required = false)
   	private Long userId;
	@ApiModelProperty(value = "发送内容", required = false)
	private String messageContent;
	@ApiModelProperty(value = "商品类型（天猫/淘宝）", required = false)
	private Integer type;
	@ApiModelProperty(value = "转换状态", required = false)
	private String transferStatus;
	@ApiModelProperty(value = "发送状态", required = false)
	private String sendStatus;
	@ApiModelProperty(value = "发送时间区间", required = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private String sendTimeStart;
	@ApiModelProperty(value = "发送时间区间", required = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private String sendTimeEnd;
	@ApiModelProperty(value = "发送次数", required = false)
	private Integer sendNum;
	
	@ApiModelProperty(value="当前页",required=true)
	private Integer currentPage;
	@ApiModelProperty(value="每页多少条",required=true)
	private Integer pageSize;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getMessageContent() {
		return messageContent;
	}
	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getTransferStatus() {
		return transferStatus;
	}
	public void setTransferStatus(String transferStatus) {
		this.transferStatus = transferStatus;
	}
	public String getSendStatus() {
		return sendStatus;
	}
	public void setSendStatus(String sendStatus) {
		this.sendStatus = sendStatus;
	}
	
	public String getSendTimeStart() {
		return sendTimeStart;
	}
	public void setSendTimeStart(String sendTimeStart) {
		this.sendTimeStart = sendTimeStart;
	}
	public String getSendTimeEnd() {
		return sendTimeEnd;
	}
	public void setSendTimeEnd(String sendTimeEnd) {
		this.sendTimeEnd = sendTimeEnd;
	}
	public Integer getSendNum() {
		return sendNum;
	}
	public void setSendNum(Integer sendNum) {
		this.sendNum = sendNum;
	}
	public Integer getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	
}
