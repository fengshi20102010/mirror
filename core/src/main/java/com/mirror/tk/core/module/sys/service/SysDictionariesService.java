package com.mirror.tk.core.module.sys.service;

import com.mirror.tk.core.module.sys.domain.SysDictionaries;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 数据字典 Service
 *
 * Date: 2016-03-30 10:31:38
 *
 * @author Code Generator
 *
 */
public interface SysDictionariesService extends EntityService<SysDictionaries> {

	/**
	 * 获取字典数据
	 * <p>Description:通过key查询字典数据</p>
	 * @param key
	 * @return
	 * @author fengshi on 2016年12月9日
	 */
	public SysDictionaries findByKey(String key);

}
