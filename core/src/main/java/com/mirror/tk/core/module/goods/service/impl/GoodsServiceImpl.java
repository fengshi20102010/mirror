package com.mirror.tk.core.module.goods.service.impl;


import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mirror.tk.core.module.goods.dao.GoodsCouponDao;
import com.mirror.tk.core.module.goods.dao.GoodsCustomDao;
import com.mirror.tk.core.module.goods.dao.GoodsDao;
import com.mirror.tk.core.module.goods.dao.GoodsImgDao;
import com.mirror.tk.core.module.goods.dao.GoodsShopInfoDao;
import com.mirror.tk.core.module.goods.domain.Goods;
import com.mirror.tk.core.module.goods.domain.GoodsCoupon;
import com.mirror.tk.core.module.goods.domain.GoodsImg;
import com.mirror.tk.core.module.goods.domain.GoodsShopInfo;
import com.mirror.tk.core.module.goods.dto.GoodsInfoDto;
import com.mirror.tk.core.module.goods.dto.GoodsNumDto;
import com.mirror.tk.core.module.goods.service.GoodsCouponService;
import com.mirror.tk.core.module.goods.service.GoodsImgService;
import com.mirror.tk.core.module.goods.service.GoodsService;
import com.mirror.tk.core.module.goods.service.GoodsShopInfoService;
import com.mirror.tk.framework.common.dao.support.PageInfo;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("goodsService")
public class GoodsServiceImpl extends EntityServiceImpl<Goods, GoodsDao> implements GoodsService {

	@Resource
	private GoodsCustomDao goodsCustomDao;
	
	@Resource
	private GoodsShopInfoDao goodsShopInfoDao;
	
	@Resource
	private GoodsCouponDao goodsCouponDao;
	
	@Resource
	private GoodsImgDao goodsImgDao;
	
	@Resource
	private GoodsCouponService goodsCouponService;
	
	@Resource
	private GoodsShopInfoService goodsShopInfoService;
	
	@Resource
	private GoodsImgService goodsImgService;
	
	@Override
	public PageInfo<GoodsInfoDto> queryGoodsInfo(PageInfo<GoodsInfoDto> pageInfo, Map<String, Object> searchMap) {
		return goodsCustomDao.queryGoodsInfo(pageInfo, searchMap);
	}

	@Override
	public GoodsInfoDto queryGoodsInfo(Long id) {
		return goodsCustomDao.queryGoodsInfo(id);
	}

	@Override
	public Goods queryGoods(Long numIid) {
		return this.getEntityDao().findByNumIid(numIid);
	}

	@Override
	@Transactional
	public void addGoods(Goods goods, GoodsCoupon goodsCoupon, List<GoodsImg> goodsImg,GoodsShopInfo goodsShopInfo) {
		List<GoodsShopInfo> listShop = goodsShopInfoDao.findBySid(goodsShopInfo.getSid());
		GoodsShopInfo shopEntity = null;
		if(CollectionUtils.isEmpty(listShop)) {
			goodsShopInfoDao.create(goodsShopInfo);
			shopEntity = goodsShopInfo;
		} else {
			shopEntity = listShop.get(0);
		}
		
		goods.setShopId(shopEntity.getId());
		getEntityDao().create(goods);
		
		if(goodsCoupon != null) {
			goodsCoupon.setGoodsId(goods.getId());
			goodsCoupon.setSellerId(shopEntity.getSellerId());
			goodsCouponDao.create(goodsCoupon);
		}
		
		if(!CollectionUtils.isEmpty(goodsImg)) {
			for(GoodsImg img : goodsImg) {
				img.setNumIid(goods.getNumIid());
				goodsImgDao.create(img);
			}
		}
		
	}

	@Override
	public List<GoodsImg> queryGoodsImgs(List<Long> ids) {
		return goodsImgDao.findByNumIidIn(ids);
	}

	@Override
	@Transactional
	public Boolean updateByStatus(Long id, Integer status) {
		return this.getEntityDao().updateByStatus(id, status) > 0;
	}

	@Override
	@Transactional
	public Boolean updateById(Long id, Long sales) {
		return this.getEntityDao().updateById(id, sales) > 0;
	}

	@Override
	public List<GoodsNumDto> findByMyNum(Long userId) {
		List<GoodsNumDto> temp = goodsCustomDao.queryMyGoodsNum(userId);
		List<GoodsNumDto> list = initGoodsNumDto();
		for (GoodsNumDto goodsNumDto : temp) {
			GoodsNumDto dto = list.get(goodsNumDto.getStatus());
			list.remove(dto);
			dto.setNum(goodsNumDto.getNum());
			list.add(goodsNumDto.getStatus(), dto);
		}
		return list;
	}
	
	private List<GoodsNumDto> initGoodsNumDto(){
		List<GoodsNumDto> list = Lists.newArrayList();
		GoodsNumDto dto0 = new GoodsNumDto();
		dto0.setStatusName("冻结");
		dto0.setStatus(0);
		GoodsNumDto dto1 = new GoodsNumDto();
		dto1.setStatusName("待完善");
		dto1.setStatus(1);
		GoodsNumDto dto2 = new GoodsNumDto();
		dto2.setStatusName("待审核");
		dto2.setStatus(2);
		GoodsNumDto dto3 = new GoodsNumDto();
		dto3.setStatusName("发布中");
		dto3.setStatus(3);
		GoodsNumDto dto4 = new GoodsNumDto();
		dto4.setStatusName("待结算");
		dto4.setStatus(4);
		GoodsNumDto dto5 = new GoodsNumDto();
		dto5.setStatusName("已结束");
		dto5.setStatus(5);
		list.add(dto0);
		list.add(dto1);
		list.add(dto2);
		list.add(dto3);
		list.add(dto4);
		list.add(dto5);
		return list;
	}

	@Override
	@Transactional
	public Boolean deleteMyGoods(Long id, List<Long> ids) {
		// 查询出自己的商品
		Map<String, Object> map = Maps.newHashMap();
		map.put("EQ_userId", id);
		map.put("IN_numIid", ids);
		map.put("LT_status", Goods.STATUS_PUBLISH);
		List<Goods> list = query(map, Maps.newHashMap());
		if(null != list && list.size() >0){
			List<Long> goodsIds = Lists.newArrayList();
			List<Long> shopIds = Lists.newArrayList();
			List<Long> numIids = Lists.newArrayList();
			for (Goods goods : list) {
				goodsIds.add(goods.getId());
				numIids.add(goods.getNumIid());
				// 判断是否需要删除店铺
				if(this.getEntityDao().countByShopId(goods.getShopId()) == 0){
					shopIds.add(goods.getShopId());
				}
			}
			// 删除优惠劵
			if(!goodsIds.isEmpty()){
				goodsCouponService.deleteByGoodsIds(goodsIds);
			}
			// 删除店铺信息
			if(!shopIds.isEmpty()){
				goodsShopInfoService.deleteByShopId(shopIds);
			}
			// 删除商品和商品小图
			if(!numIids.isEmpty()){
				this.getEntityDao().deleteMyGoods(id, ids);
				goodsImgService.deleteMyGoodsImg(ids);
			}
			return true;
		}
		return false;
	}

}
