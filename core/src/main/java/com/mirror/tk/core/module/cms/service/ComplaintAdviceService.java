package com.mirror.tk.core.module.cms.service;

import com.mirror.tk.core.module.cms.domain.ComplaintAdvice;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 投诉建议 Service
 *
 * Date: 2016-11-01 23:10:35
 *
 * @author Code Generator
 *
 */
public interface ComplaintAdviceService extends EntityService<ComplaintAdvice> {

}
