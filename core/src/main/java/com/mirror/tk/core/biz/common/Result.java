package com.mirror.tk.core.biz.common;

import java.io.Serializable;

import com.mirror.tk.core.constant.ErrorCode;

public class Result<T> implements Serializable{

	private static final long serialVersionUID = 6534308137146928525L;

	private String code;
	
	private String description;
	
	private boolean success;
	
	private T data;
	
	public static <T> Result<T> create() {
		Result<T> result = new Result<T>();
		result.setSuccess(false);
		return result;
	}

	public Result<T> success(){
		success(null);
		return this;
	}

	public Result<T> success(T data){
		this.setSuccess(true);
		this.data = data;
		return this;
	}

	public Result<T> fail(String code,String description){
		this.setSuccess(false);
		this.setCode(code);
		this.setDescription(description);
		return this;
	}

	public Result<T> fail(String code){
		fail(code, null);
		return this;
	}

	public Result<T> code(String code){
		this.setCode(code);
		return this;
	}

	public Result<T> description(String description){
		this.setDescription(description);
		return this;
	}

	public Result<T> data(T data){
		this.data = data;
		return this;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public static <T> Result<T> nullError() {
		return new Result<T>().fail(ErrorCode.PARAM_NULL,ErrorCode.PARAM_NULL_DES);
	}
	
	public static <T> Result<T> error(String code,String des) {
		return new Result<T>().fail(code,des);
	}
}
