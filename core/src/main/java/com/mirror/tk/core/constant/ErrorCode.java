package com.mirror.tk.core.constant;

public class ErrorCode {
	
	public static final String GOODS_CAT_ERROR ="100001";
	public static final String GOODS_CAT_ERROR_DES ="商品种类参数不合法";
	
	public static final String URL_ERROR ="100002";
	public static final String URL_ERROR_DES ="商品链接地址错误：无法获取到商品ID";
	
	public static final String TAOBAO_API_ERROR ="100003";
	public static final String TAOBAO_API_ERROR_DES ="访问淘宝API出错";
	
	public static final String TAOBAO_SHOP_ERROR ="100004";
	public static final String TAOBAO_SHOP_ERROR_DES ="查询淘宝店铺出错";
	
	public static final String GOODS_EXIST_ERROR ="100005";
	public static final String GOODS_EXIST_ERROR_DES ="商品已经存在";
	
	public static final String GOODS_NOT_EXIST_ERROR ="100006";
	public static final String GOODS_NOT_EXIST_ERROR_DES ="保存发送记录失败，商品不存在";
	
	public static final String COUPON_URL_ERROR ="100007";
	public static final String COUPON_URL_ERROR_DES ="优惠券链接地址错误：无法获取到activityId";
	
	public static final String GOODS_IN_TAOBAO_NOT_EXIST_ERROR ="100008";
	public static final String GOODS_IN_TAOBAO_NOT_EXIST_ERROR_DES ="淘宝尚不存在该商品";
	
	public static final String USER_SEND_TPL_NOT_SET ="100009";
	public static final String USER_SEND_TPL_NOT_SET_DES ="用户尚未设置发送模板";
	
	public static final String USER_PID_NOT_FOUND ="100010";
	public static final String USER_PID_NOT_FOUND_DES ="未找到对应用户PID";
	
	public static final String GOODS_TYPE_ERROR ="100011";
	public static final String GOODS_TYPE_ERROR_DES ="商品推广计划未设置";
	
	public static final String PARAM_NULL ="000001";
	public static final String PARAM_NULL_DES ="参数不能为空";
	
	
}
