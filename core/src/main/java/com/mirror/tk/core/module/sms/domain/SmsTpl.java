package com.mirror.tk.core.module.sms.domain;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 短信模板记录表 Entity
 *
 * Date: 2016-11-01 23:09:33
 *
 * @author Acooly Code Generator
 */
@Entity
@Table(name = "sms_tpl")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SmsTpl extends AbstractEntity {
	/** ID */
	private Long id;
	
	/** 模板名称 */
	private String name;
	
	/** 模板内容 */
	private String tpl;
	
	/** 模板状态 */
	private Integer status;
	
	/** 模板描述 */
	private String description;
	
	/** 创建时间 */
	private Date createTime;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getName(){
		return this.name;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getTpl(){
		return this.tpl;
	}

	public void setTpl(String tpl){
		this.tpl = tpl;
	}

	public Integer getStatus(){
		return this.status;
	}

	public void setStatus(Integer status){
		this.status = status;
	}

	public String getDescription(){
		return this.description;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
