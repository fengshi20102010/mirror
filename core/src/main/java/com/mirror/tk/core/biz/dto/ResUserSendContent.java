package com.mirror.tk.core.biz.dto;

public class ResUserSendContent {

	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	
}
