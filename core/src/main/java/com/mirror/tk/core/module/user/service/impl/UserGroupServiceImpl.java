package com.mirror.tk.core.module.user.service.impl;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mirror.tk.core.module.user.dao.UserGroupDao;
import com.mirror.tk.core.module.user.domain.UserGroup;
import com.mirror.tk.core.module.user.service.UserGroupDetailService;
import com.mirror.tk.core.module.user.service.UserGroupService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("userGroupService")
public class UserGroupServiceImpl extends EntityServiceImpl<UserGroup, UserGroupDao> implements UserGroupService {

	@Resource
	private UserGroupDetailService userGroupDetailService;
	
	@Override
	public List<UserGroup> getByUserIdAndType(Long userId, Integer type) {
		return this.getEntityDao().getByUserIdAndType(userId, type);
	}

	@Transactional
	@Override
	public boolean deleteByIdAndUserId(Long id, Long userId) {
		int success = this.getEntityDao().deleteByIdAndUserId(id, userId);
		if(0 < success) userGroupDetailService.deleteByGroupId(id);
		return true;
	}

}
