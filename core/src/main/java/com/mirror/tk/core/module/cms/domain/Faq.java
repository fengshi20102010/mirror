package com.mirror.tk.core.module.cms.domain;


import java.util.Date;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.google.common.collect.Maps;
import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 常见问题 Entity
 *
 * Date: 2016-11-01 23:10:31
 *
 * @author Acooly Code Generator
 */
@Entity
@Table(name = "faq")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Faq extends AbstractEntity {
	/** ID */
	private Long id;
	
	/** 标题 */
	private String title;
	
	/** 问题 */
	private String question;
	
	/** 答案 */
	private String answer;
	
	/** 状态 */
	private Integer status;
	
	/** 创建时间 */
	private Date createTime;
	
	/** 停用 */
	public static final Integer STATUS_DISABLE = 0;
	/** 启用 */
	public static final Integer STATUS_ENABLE = 1;
		
	/** 状态 */
	public static Map<Integer, String> allStatuss = Maps.newTreeMap();
	static {
		allStatuss.put(STATUS_DISABLE, "停用");
		allStatuss.put(STATUS_ENABLE, "启用");
	}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getTitle(){
		return this.title;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getQuestion(){
		return this.question;
	}

	public void setQuestion(String question){
		this.question = question;
	}

	public String getAnswer(){
		return this.answer;
	}

	public void setAnswer(String answer){
		this.answer = answer;
	}

	public Integer getStatus(){
		return this.status;
	}

	public void setStatus(Integer status){
		this.status = status;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
