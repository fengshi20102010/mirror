package com.mirror.tk.core.module.goods.service.impl;


import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mirror.tk.core.module.goods.dao.GoodsShopInfoDao;
import com.mirror.tk.core.module.goods.domain.GoodsShopInfo;
import com.mirror.tk.core.module.goods.service.GoodsShopInfoService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("goodsShopInfoService")
public class GoodsShopInfoServiceImpl extends EntityServiceImpl<GoodsShopInfo, GoodsShopInfoDao> implements GoodsShopInfoService {

	@Override
	@Transactional
	public Boolean deleteByShopId(List<Long> shopIds) {
		return this.getEntityDao().deleteByShopId(shopIds) > 0;
	}

}
