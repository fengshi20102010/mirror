package com.mirror.tk.core.sdk.haodanku;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import com.google.common.collect.Maps;
import com.mirror.tk.core.sdk.haodanku.req.HighServantReq;
import com.mirror.tk.core.sdk.haodanku.req.ItemListReq;
import com.mirror.tk.core.sdk.haodanku.res.HaodankuRes;
import com.mirror.tk.core.sdk.haodanku.res.HighServantRes;
import com.mirror.tk.core.sdk.haodanku.res.ItemListRes;
import com.mirror.tk.core.utils.HttpClientUtil;
import com.mirror.tk.framework.utils.mapper.JsonMapper;

/**
 * @title 好单库sdk封装
 */
public class HaodankuSDK {

	@Value("${haodanku.appkey:zhuanbei}")
	private static String APPKEY = "zhuanbei";

	// 商品列表
	@Value("${haodanku.itemlist.url:http://v2.api.haodanku.com/itemlist}")
	private static String ITEM_LIST_URL = "http://v2.api.haodanku.com/itemlist";
	// 商品列表
	@Value("${haodanku.highservant.url:http://v2.api.haodanku.com/ratesurl}")
	private static String HIGH_SERVANT_URL = "http://v2.api.haodanku.com/ratesurl";

	private static JsonMapper jsonMapper = JsonMapper.nonDefaultMapper();

	private static final Logger log = LogManager.getLogger();

	// 商品列表API
	public static HaodankuRes<ItemListRes> itemlist(ItemListReq req) {
		// 请求参数
		Map<String, String> param = Maps.newHashMap();
		param.put("apikey", APPKEY);
		param.put("nav", req.getNav().toString());
		param.put("cid", req.getCid().toString());
		param.put("back", req.getBack().toString());
		param.put("min_id", req.getMinId().toString());
		log.info("请求参数：{}", param);
		String rep = HttpClientUtil.doGet(ITEM_LIST_URL, param);
		log.info("返回值：{}", rep);
		if (StringUtils.isBlank(rep)) {
			log.info("未获取到数据...");
			return null;
		}
		// 反序列化为对象
		@SuppressWarnings("unchecked")
		HaodankuRes<ItemListRes> res = jsonMapper.fromJson(rep, HaodankuRes.class);
		if (null == res) {
			log.info("json转换失败，原始json：{}", rep);
			return null;
		}
		return res;
	}

	public static HaodankuRes<HighServantRes> highServant(HighServantReq req) {
		// 请求参数
		Map<String, String> param = Maps.newHashMap();
		param.put("apikey", APPKEY);
		param.put("itemid", req.getItemid().toString());
		param.put("pid", req.getPid());
		if (StringUtils.isNotBlank(req.getActivityid())) {
			param.put("activityid", req.getActivityid());
		}
		log.info("请求参数：{}", param);
		// 请求接口
		String rep = HttpClientUtil.doPost(HIGH_SERVANT_URL, param);
		log.info("返回值：{}", rep);
		if (StringUtils.isBlank(rep)) {
			log.info("未获取到数据...");
			return null;
		}
		// 反序列化为对象
		@SuppressWarnings("unchecked")
		HaodankuRes<HighServantRes> res = jsonMapper.fromJson(rep, HaodankuRes.class);
		if (null == res) {
			log.info("json转换失败，原始json：{}", rep);
			return null;
		}
		return res;
	}

	public static void main(String[] args) {
		// ItemListReq req = new ItemListReq();
		// HaodankuRes<ItemListRes> res = itemlist(req);
		// System.out.println(res);
		HighServantReq req = new HighServantReq();
		req.setItemid(567576377186l);
		req.setPid("mm_111572980_60150202_11093200214");
		HaodankuRes<HighServantRes> res = highServant(req);
		System.out.println(res);
	}

}
