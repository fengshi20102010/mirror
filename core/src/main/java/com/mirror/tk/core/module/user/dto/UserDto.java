package com.mirror.tk.core.module.user.dto;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 用户对象dto
 */
@ApiModel
public class UserDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "用户名", required = false)
	private String username;

	@ApiModelProperty(value = "QQ", required = false)
	private String qq;

	@ApiModelProperty(value = "微信", required = false)
	private String wechat;

	@ApiModelProperty(value = "邮箱", required = false)
	private String email;

	@ApiModelProperty(value = "性别", required = false)
	private Integer sex;

	@ApiModelProperty(value = "年龄", required = false)
	private Integer age;

	@ApiModelProperty(value = "团队或个人名称", required = false)
	private String teamName;

	@ApiModelProperty(value = "团队类型", required = false)
	private Integer type;

	@ApiModelProperty(value = "收入", required = false)
	private Integer income;

	@ApiModelProperty(value = "渠道", required = false)
	private String channel;

	@ApiModelProperty(value = "申请理由", required = false)
	private String reason;

	@ApiModelProperty(value = "附件", required = false)
	private String attachment;

	@NotBlank(message = "{user.dto.username}")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	@Email(message = "{user.dto.email}")
	@NotBlank(message = "{user.dto.email.empty}")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getIncome() {
		return income;
	}

	public void setIncome(Integer income) {
		this.income = income;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
	
}
