package com.mirror.tk.core.module.sms.dao;

import org.springframework.data.jpa.repository.Query;

import com.mirror.tk.core.module.sms.domain.SmsTpl;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 短信模板记录表 JPA Dao
 *
 * Date: 2016-11-01 23:09:33
 *
 * @author Code Generator
 *
 */
public interface SmsTplDao extends EntityJpaDao<SmsTpl, Long> {

	@Query("from SmsTpl where name = ?1 ")
	public SmsTpl getByName(String code);

}
