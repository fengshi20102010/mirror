package com.mirror.tk.core.module.goods.dao;

import java.util.List;
import java.util.Map;

import com.mirror.tk.core.biz.dto.GoodsTypeDto;
import com.mirror.tk.core.module.goods.dto.GoodsInfoDto;
import com.mirror.tk.core.module.goods.dto.GoodsNumDto;
import com.mirror.tk.framework.common.dao.support.PageInfo;

/**
 * 自定义商品相关dao
 */
public interface GoodsCustomDao {

	/**
	 * 获取显示正常的类型
	 * @return
	 */
	List<GoodsTypeDto> getNormalType();
	
	/**
	 * 前台展示商品信息（分页）
	 * @param pageInfo
	 * @param searchMap
	 * @return
	 */
	PageInfo<GoodsInfoDto> queryGoodsInfo(PageInfo<GoodsInfoDto> pageInfo, Map<String, Object> searchMap);

	/**
	 * 商品信息详情
	 * @param id
	 * @return
	 */
	GoodsInfoDto queryGoodsInfo(Long id);

	/**
	 * 获取用户商品数量信息
	 * @param userId
	 * @return
	 */
	List<GoodsNumDto> queryMyGoodsNum(Long userId);
	
}
