package com.mirror.tk.core.sdk.haodanku.res;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HaodankuRes<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	// 状态码（1成功，0失败或没有数据返回）
	private Integer code;
	// 作为请求地址中获取下一页的参数值
	@JsonProperty("min_id")
	private Integer minId;
	// 返回信息说明，SUCCESS代表成功获取，失败则有具体原因
	private String msg;
	// 返回结果
	private List<T> data;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Integer getMinId() {
		return minId;
	}

	public void setMinId(Integer minId) {
		this.minId = minId;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "HaodankuRes [code=" + code + ", minId=" + minId + ", msg=" + msg + ", data=" + data + "]";
	}

}
