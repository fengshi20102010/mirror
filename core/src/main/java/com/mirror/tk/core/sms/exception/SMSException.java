package com.mirror.tk.core.sms.exception;

public class SMSException extends Exception {

	private static final long serialVersionUID = 1L;

	protected String code;
	protected String detail;

	public SMSException(String code, String detail, String message) {
		super(message);
	}

	public String getCode() {
		return code;
	}

	public String getDetail() {
		return detail;
	}

}
