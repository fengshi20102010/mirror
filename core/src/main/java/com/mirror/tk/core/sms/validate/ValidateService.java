package com.mirror.tk.core.sms.validate;

public interface ValidateService {

	public OutObject validate(InObject inObject);

}
