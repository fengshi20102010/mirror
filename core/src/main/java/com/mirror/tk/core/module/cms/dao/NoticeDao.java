package com.mirror.tk.core.module.cms.dao;

import com.mirror.tk.core.module.cms.domain.Notice;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 公告/信息/教程相关数据表 JPA Dao
 *
 * Date: 2016-11-01 23:10:03
 *
 * @author Code Generator
 *
 */
public interface NoticeDao extends EntityJpaDao<Notice, Long> {

}
