package com.mirror.tk.core.constant;

import java.util.Map;

import com.google.common.collect.Maps;

public class GoodsDictionary {

	protected static final Map<String, Map<String,String>> DICTIONARY;
	
	static {
		DICTIONARY = Maps.newHashMap();
		
		Map<String, String> GOODS_SOURCE = Maps.newHashMap();
		GOODS_SOURCE.put("1", "天猫");
		GOODS_SOURCE.put("2", "淘宝");
		
		DICTIONARY.put("source", GOODS_SOURCE);
	}
	
	public static String get(String colName,String key) {
		if(DICTIONARY.get(colName) != null){
			return DICTIONARY.get(colName).get(key);
		}
		return null;
	}
}
