package com.mirror.tk.core.module.user.service.impl;


import org.springframework.stereotype.Service;

import com.mirror.tk.core.module.user.dao.UserSendTplDao;
import com.mirror.tk.core.module.user.domain.UserSendTpl;
import com.mirror.tk.core.module.user.service.UserSendTplService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("userSendTplService")
public class UserSendTplServiceImpl extends EntityServiceImpl<UserSendTpl, UserSendTplDao> implements UserSendTplService {

	@Override
	public UserSendTpl queryByUserId(Long userId) {
		return getEntityDao().findByUserId(userId);
	}

}
