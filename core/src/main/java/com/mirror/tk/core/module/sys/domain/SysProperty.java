package com.mirror.tk.core.module.sys.domain;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 系统_属性表 Entity
 *
 * Date: 2016-01-08 09:54:41
 *
 * @author Acooly Code Generator
 */
@Entity
@Table(name = "sys_property")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SysProperty extends AbstractEntity {
	/** id */
	private Long id;
	
	/** 变量名 */
	private String proKey;
	
	/** 变量值 */
	private String proValue;
	
	/** user_id */
	private Long userId;
	
	/** 创建时间 */
	private Date createTime;
	
	/** 描述 */
	private String description;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getProKey(){
		return this.proKey;
	}

	public void setProKey(String proKey){
		this.proKey = proKey;
	}

	public String getProValue(){
		return this.proValue;
	}

	public void setProValue(String proValue){
		this.proValue = proValue;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public String getDescription(){
		return this.description;
	}

	public void setDescription(String description){
		this.description = description;
	}

	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
