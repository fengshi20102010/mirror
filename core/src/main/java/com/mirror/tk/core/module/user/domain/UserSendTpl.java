package com.mirror.tk.core.module.user.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 用户信息扩展表,将不常用的信息放入这张表 Entity
 *
 * Date: 2016-11-01 23:08:57
 *
 * @author Acooly Code Generator
 */
@Entity
@Table(name = "user_send_tpl")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserSendTpl extends AbstractEntity {
	/** ID */
	private Long id;
	
	/** 用户ID */
	private Long userId;
	
	private String content;
	
	private Long pid;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
