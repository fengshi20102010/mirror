package com.mirror.tk.core.module.user.service.impl;


import org.springframework.stereotype.Service;

import com.mirror.tk.core.module.user.dao.UserGatherConfigDao;
import com.mirror.tk.core.module.user.domain.UserGatherConfig;
import com.mirror.tk.core.module.user.service.UserGatherConfigService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("userGatherConfigService")
public class UserGatherConfigServiceImpl extends EntityServiceImpl<UserGatherConfig, UserGatherConfigDao> implements UserGatherConfigService {

	@Override
	public UserGatherConfig getByUserId(Long userId) {
		return this.getEntityDao().getByUserId(userId);
	}

}
