package com.mirror.tk.core.module.goods.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 商品优惠劵表 Entity
 *
 * Date: 2016-11-01 23:10:16
 *
 * @author Acooly Code Generator
 */
@Entity
@Table(name = "goods_coupon")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class GoodsCoupon extends AbstractEntity {

	/** ID */
	private Long id;

	/** 活动ID */
	private String activityId;

	/** 淘宝对应的卖家ID */
	private Long sellerId;

	/** 商品ID */
	private Long goodsId;

	/** 优惠劵金额 */
	private String money;

	/** 优惠劵总数 */
	private Integer total;

	/** pc链接地址 */
	private String pcUrl;

	/** 手机链接地址 */
	private String mobileUrl;

	/** 优惠劵领取数 */
	private Integer applied;

	/** 优惠劵剩余数 */
	private Integer remained;

	/** 订单满多少才能用这个优惠券 */
	private String conditions;

	/** 优惠劵开始日期 */
	private Date startTime;

	/** 优惠券结束日期 */
	private Date endTime;

	/** 优惠券限领数 */
	private Integer limited;

	/** 创建时间 */
	private Date createTime;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public Long getSellerId() {
		return sellerId;
	}

	public void setSellerId(Long sellerId) {
		this.sellerId = sellerId;
	}

	public Long getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public String getPcUrl() {
		return pcUrl;
	}

	public void setPcUrl(String pcUrl) {
		this.pcUrl = pcUrl;
	}

	public String getMobileUrl() {
		return mobileUrl;
	}

	public void setMobileUrl(String mobileUrl) {
		this.mobileUrl = mobileUrl;
	}

	public Integer getApplied() {
		return applied;
	}

	public void setApplied(Integer applied) {
		this.applied = applied;
	}

	public Integer getRemained() {
		return remained;
	}

	public void setRemained(Integer remained) {
		this.remained = remained;
	}

	public String getConditions() {
		return conditions;
	}

	public void setConditions(String conditions) {
		this.conditions = conditions;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getLimited() {
		return limited;
	}

	public void setLimited(Integer limited) {
		this.limited = limited;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
