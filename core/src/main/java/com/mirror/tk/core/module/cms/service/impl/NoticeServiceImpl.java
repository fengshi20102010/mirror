package com.mirror.tk.core.module.cms.service.impl;


import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.mirror.tk.core.module.cms.dao.NoticeDao;
import com.mirror.tk.core.module.cms.domain.Notice;
import com.mirror.tk.core.module.cms.service.NoticeService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("noticeService")
public class NoticeServiceImpl extends EntityServiceImpl<Notice, NoticeDao> implements NoticeService {

	@Override
	public List<Notice> findByType(Integer type) {
		Map<String, Object> map = Maps.newHashMap();
		Map<String, Boolean> sortMap = Maps.newHashMap();
		map.put("EQ_type", type);
		map.put("EQ_status", Notice.STATUS_ENABLE);
		sortMap.put("createTime", false);
		return query(map, sortMap);
	}

}
