package com.mirror.tk.core.sdk.haodanku.res;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HighServantRes implements Serializable {

	private static final long serialVersionUID = 1L;

	// 高佣优惠券链接
	@JsonProperty("coupon_click_url")
	private String couponClickUrl;
	// 佣金比例
	@JsonProperty("max_commission_rate")
	private String maxCommissionRate;
	// 优惠券金额，若没有优惠券则返回0
	private String couponmoney;
	// 优惠券开始时间，若没有则返回空
	private Integer couponstarttime;
	// 优惠券结束时间，若没有则返回空
	private Integer couponendtime;
	// 优惠券使用条件，若没有则返回空
	private String couponexplain;
	// 优惠券总量，若没有则返回空
	private Integer couponnum;
	// 优惠券剩余量，若没有则返回空
	private Integer couponsurplus;
	// 优惠券领取量，若没有则返回空
	private Integer couponreceive;

	public String getCouponClickUrl() {
		return couponClickUrl;
	}

	public void setCouponClickUrl(String couponClickUrl) {
		this.couponClickUrl = couponClickUrl;
	}

	public String getMaxCommissionRate() {
		return maxCommissionRate;
	}

	public void setMaxCommissionRate(String maxCommissionRate) {
		this.maxCommissionRate = maxCommissionRate;
	}

	public String getCouponmoney() {
		return couponmoney;
	}

	public void setCouponmoney(String couponmoney) {
		this.couponmoney = couponmoney;
	}

	public Integer getCouponstarttime() {
		return couponstarttime;
	}

	public void setCouponstarttime(Integer couponstarttime) {
		this.couponstarttime = couponstarttime;
	}

	public Integer getCouponendtime() {
		return couponendtime;
	}

	public void setCouponendtime(Integer couponendtime) {
		this.couponendtime = couponendtime;
	}

	public String getCouponexplain() {
		return couponexplain;
	}

	public void setCouponexplain(String couponexplain) {
		this.couponexplain = couponexplain;
	}

	public Integer getCouponnum() {
		return couponnum;
	}

	public void setCouponnum(Integer couponnum) {
		this.couponnum = couponnum;
	}

	public Integer getCouponsurplus() {
		return couponsurplus;
	}

	public void setCouponsurplus(Integer couponsurplus) {
		this.couponsurplus = couponsurplus;
	}

	public Integer getCouponreceive() {
		return couponreceive;
	}

	public void setCouponreceive(Integer couponreceive) {
		this.couponreceive = couponreceive;
	}

	@Override
	public String toString() {
		return "HighServantRes [couponClickUrl=" + couponClickUrl + ", maxCommissionRate=" + maxCommissionRate
				+ ", couponmoney=" + couponmoney + ", couponstarttime=" + couponstarttime + ", couponendtime="
				+ couponendtime + ", couponexplain=" + couponexplain + ", couponnum=" + couponnum + ", couponsurplus="
				+ couponsurplus + ", couponreceive=" + couponreceive + "]";
	}

}
