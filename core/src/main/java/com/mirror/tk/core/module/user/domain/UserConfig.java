package com.mirror.tk.core.module.user.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 用户程序配置表 Entity
 *
 * Date: 2016-12-28 23:56:41
 *
 * @author Code Generator
 */
@Entity
@Table(name = "user_config")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserConfig extends AbstractEntity {
	/** ID */
	private Long id;
	
	/** 用户ID */
	private Long userId;
	
	/** 采集天猫 */
	private Integer gatherTmall;
	
	/** 不采集发送过的单子 */
	private Integer notGatherSend;
	
	/** 检测有无拉人信息 */
	private Integer pullInfo;
	
	/** 检测商品AB单 */
	private Integer checkAb;
	
	/** 最低动态评分 */
	private Long minScore;
	
	/** 最低佣金 */
	private Long minCommission;
	
	/** 最低比率 */
	private Long minRatio;
	
	/** 最低转换佣金 */
	private Long minTransformCommission;
	
	/** 最低转换比率 */
	private Long minTransformRatio;
	
	/** 最低转换销量 */
	private Long minTransformSales;
	
	/** 只发送天猫商品 */
	private Integer sendTmall;
	
	/** 无优惠劵不上传 */
	private Integer noCoupon;
	
	/** 微信优惠劵方式 */
	private Integer wechatCoupon;
	
	/** QQ优惠劵方式 */
	private Integer qqCoupon;
	
	/** 微信转换二合一 */
	private Integer wechatDiad;
	
	/** QQ转换二合一 */
	private Integer qqDiad;
	
	/** 优惠劵检测时间 */
	private Long couponTime;
	
	/** 最低优惠劵数量 */
	private Long minCoupon;
	
	/** 开启QQ发送 */
	private Integer openQqSend;
	
	/** 开启微信发送 */
	private Integer openWechatSend;
	
	/** 监控后台 */
	private Integer monitorBg;
	
	/** 监控QQ */
	private Integer monitorQq;
	
	/** 群发送时间间隔 */
	private Long groupTimeInterval;
	
	/** 商品发送时间间隔 */
	private Long goodsTimeInterval;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Integer getGatherTmall(){
		return this.gatherTmall;
	}

	public void setGatherTmall(Integer gatherTmall){
		this.gatherTmall = gatherTmall;
	}

	public Integer getNotGatherSend(){
		return this.notGatherSend;
	}

	public void setNotGatherSend(Integer notGatherSend){
		this.notGatherSend = notGatherSend;
	}

	public Integer getPullInfo(){
		return this.pullInfo;
	}

	public void setPullInfo(Integer pullInfo){
		this.pullInfo = pullInfo;
	}

	public Integer getCheckAb(){
		return this.checkAb;
	}

	public void setCheckAb(Integer checkAb){
		this.checkAb = checkAb;
	}

	public Long getMinScore(){
		return this.minScore;
	}

	public void setMinScore(Long minScore){
		this.minScore = minScore;
	}

	public Long getMinCommission(){
		return this.minCommission;
	}

	public void setMinCommission(Long minCommission){
		this.minCommission = minCommission;
	}

	public Long getMinRatio(){
		return this.minRatio;
	}

	public void setMinRatio(Long minRatio){
		this.minRatio = minRatio;
	}

	public Long getMinTransformCommission(){
		return this.minTransformCommission;
	}

	public void setMinTransformCommission(Long minTransformCommission){
		this.minTransformCommission = minTransformCommission;
	}

	public Long getMinTransformRatio(){
		return this.minTransformRatio;
	}

	public void setMinTransformRatio(Long minTransformRatio){
		this.minTransformRatio = minTransformRatio;
	}

	public Long getMinTransformSales(){
		return this.minTransformSales;
	}

	public void setMinTransformSales(Long minTransformSales){
		this.minTransformSales = minTransformSales;
	}

	public Integer getSendTmall(){
		return this.sendTmall;
	}

	public void setSendTmall(Integer sendTmall){
		this.sendTmall = sendTmall;
	}

	public Integer getNoCoupon(){
		return this.noCoupon;
	}

	public void setNoCoupon(Integer noCoupon){
		this.noCoupon = noCoupon;
	}

	public Integer getWechatCoupon(){
		return this.wechatCoupon;
	}

	public void setWechatCoupon(Integer wechatCoupon){
		this.wechatCoupon = wechatCoupon;
	}

	public Integer getQqCoupon(){
		return this.qqCoupon;
	}

	public void setQqCoupon(Integer qqCoupon){
		this.qqCoupon = qqCoupon;
	}

	public Integer getWechatDiad(){
		return this.wechatDiad;
	}

	public void setWechatDiad(Integer wechatDiad){
		this.wechatDiad = wechatDiad;
	}

	public Integer getQqDiad(){
		return this.qqDiad;
	}

	public void setQqDiad(Integer qqDiad){
		this.qqDiad = qqDiad;
	}

	public Long getCouponTime(){
		return this.couponTime;
	}

	public void setCouponTime(Long couponTime){
		this.couponTime = couponTime;
	}

	public Long getMinCoupon(){
		return this.minCoupon;
	}

	public void setMinCoupon(Long minCoupon){
		this.minCoupon = minCoupon;
	}

	public Integer getOpenQqSend(){
		return this.openQqSend;
	}

	public void setOpenQqSend(Integer openQqSend){
		this.openQqSend = openQqSend;
	}

	public Integer getOpenWechatSend(){
		return this.openWechatSend;
	}

	public void setOpenWechatSend(Integer openWechatSend){
		this.openWechatSend = openWechatSend;
	}

	public Integer getMonitorBg(){
		return this.monitorBg;
	}

	public void setMonitorBg(Integer monitorBg){
		this.monitorBg = monitorBg;
	}

	public Integer getMonitorQq(){
		return this.monitorQq;
	}

	public void setMonitorQq(Integer monitorQq){
		this.monitorQq = monitorQq;
	}

	public Long getGroupTimeInterval(){
		return this.groupTimeInterval;
	}

	public void setGroupTimeInterval(Long groupTimeInterval){
		this.groupTimeInterval = groupTimeInterval;
	}

	public Long getGoodsTimeInterval(){
		return this.goodsTimeInterval;
	}

	public void setGoodsTimeInterval(Long goodsTimeInterval){
		this.goodsTimeInterval = goodsTimeInterval;
	}

	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
