package com.mirror.tk.core.module.goods.dao.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mirror.tk.core.biz.dto.GoodsTypeDto;
import com.mirror.tk.core.module.goods.dao.GoodsCustomDao;
import com.mirror.tk.core.module.goods.dto.GoodsInfoDto;
import com.mirror.tk.core.module.goods.dto.GoodsNumDto;
import com.mirror.tk.framework.common.dao.jdbc.PagedJdbcTemplate;
import com.mirror.tk.framework.common.dao.support.PageInfo;

@Repository("goodsCustomDao")
public class GoodsCustomDaoImpl implements GoodsCustomDao {

	@Resource
	private PagedJdbcTemplate pagedJdbcTemplate;
	
	@Override
	public List<GoodsNumDto> queryMyGoodsNum(Long userId){
		StringBuffer sql = new StringBuffer("SELECT COUNT(1) num, status FROM goods WHERE user_id = ? GROUP BY status");
		List<Object> params = Lists.newArrayList();
		params.add(userId);
		return pagedJdbcTemplate.query(sql.toString(), params.toArray(), BeanPropertyRowMapper.newInstance(GoodsNumDto.class));
	}
	
	@Override
	public List<GoodsTypeDto> getNormalType() {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT gt.id, gt.name, ");
		sql.append(" (SELECT COUNT(1) FROM goods g WHERE g.type_id = gt.id AND g.status = 3 ) goodsCount ");
		sql.append(" FROM goods_type gt WHERE status = 1 ");
		return pagedJdbcTemplate.queryForList(sql.toString(), GoodsTypeDto.class);
	}

	@Override
	public PageInfo<GoodsInfoDto> queryGoodsInfo(PageInfo<GoodsInfoDto> pageInfo, Map<String, Object> searchMap) {
		List<Object> params = Lists.newArrayList();
		String sql = buildGoodsSQL(searchMap, params);
		return pagedJdbcTemplate.queryMySql(pageInfo, sql, params.toArray(), GoodsInfoDto.class);
	}
	
	@Override
	public GoodsInfoDto queryGoodsInfo(Long id) {
		List<Object> params = Lists.newArrayList();
		Map<String, Object> searchMap = Maps.newHashMap();
		searchMap.put("EQ_numIid", id);
		String sql = buildGoodsSQL(searchMap, params);
		List<GoodsInfoDto> list = pagedJdbcTemplate.query(sql,params.toArray(),BeanPropertyRowMapper.newInstance(GoodsInfoDto.class));
		if(null != list && list.size() > 0 ) return list.get(0);
		return null;
	}
	
	private String buildGoodsSQL(Map<String, Object> searchMap, List<Object> params){
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT g.id,g.num_iid, g.source,g.type, g.name, g.detail, g.url, g.is_extend isExtend, g.is_coupon isCoupon, g.type_id typeId, ");
		sql.append(" g.status, g.commission, g.pic_url picUrl, g.final_price price, g.sales, g.create_time createTime, g.qq,g.sub_title,g.ad_time, g.plan_url planUrl, ");
		sql.append(" gc.id cId, gc.activity_id activityId, gc.id couponId, gc.money, gc.total, gc.applied, gc.remained, gc.limited, ");
		sql.append(" gc.conditions, gc.start_time startTime,gc.end_time endTime, gc.pc_url couponUrl,gc.mobile_url mobileCouponUrl, shop.title title ");
		sql.append(" from goods g ");
		sql.append(" LEFT JOIN goods_coupon gc ON gc.goods_id = g.id ");
		sql.append(" left join goods_shop_info shop on g.shop_id = shop.id");
		sql.append(" WHERE 1 = 1");
		
		if(null != searchMap.get("EQ_numIid")){
			sql.append(" AND g.num_iid = ? ");
			params.add(searchMap.get("EQ_numIid"));
		}
		if(null != searchMap.get("EQ_typeId")){
			sql.append(" AND g.type_id = ? ");
			params.add(searchMap.get("EQ_typeId"));
		}
		if(null != searchMap.get("EQ_userId")){
			sql.append(" AND g.user_id = ? ");
			params.add(searchMap.get("EQ_userId"));
		}
		if(null != searchMap.get("EQ_status")){
			sql.append(" AND g.status = ? ");
			params.add(searchMap.get("EQ_status"));
		}
		if(null != searchMap.get("EQ_recommend")) {
			sql.append(" AND g.recommend = " + searchMap.get("EQ_recommend"));
		}
		if(null != searchMap.get("EQ_attribute")) {
			sql.append(" AND g.attribute = " + searchMap.get("EQ_attribute"));
		}
		if(null != searchMap.get("GT_createTime")){
			sql.append(" AND g.create_time >= ? ");
			params.add(searchMap.get("GT_createTime"));
		}
		if(null != searchMap.get("LT_createTime")){
			sql.append(" AND g.create_time < ? ");
			params.add(searchMap.get("LT_createTime"));
		}
		if(null != searchMap.get("LIKE_name")) {
			sql.append(" AND g.name like '%"+searchMap.get("LIKE_name")+"%'");
		}
		if(null != searchMap.get("EQ_source")) {
			sql.append(" AND g.source =  "+searchMap.get("EQ_source"));
		}
		if(null != searchMap.get("LT_price")) {
			sql.append(" AND g.price <=  "+searchMap.get("LT_price"));
		}
		if(null != searchMap.get("GT_price")) {
			sql.append(" AND g.price >=  "+searchMap.get("GT_price"));
		}
		if(null != searchMap.get("LT_sales")) {
			sql.append(" AND g.sales <=  "+searchMap.get("LT_sales"));
		}
		if(null != searchMap.get("GT_sales")) {
			sql.append(" AND g.sales >=  " + searchMap.get("GT_sales"));
		}
		if(null != searchMap.get("LT_commission")) {
			sql.append(" AND g.commission <=  " + searchMap.get("LT_commission"));
		}
		if(null != searchMap.get("GT_commission")) {
			sql.append(" AND g.commission >=  "+searchMap.get("GT_commission"));
		}
		if(null != searchMap.get("EQ_adTime")) {
			sql.append(" AND TO_DAYS(g.ad_time) = TO_DAYS('" + searchMap.get("EQ_adTime") +"') ");
		}
		
		if(null != searchMap.get("ORDERBY")) {
			sql.append(" order by ").append(searchMap.get("ORDERBY"));
		}else {
			sql.append(" ORDER BY g.create_time desc  ");
		}
		return sql.toString();
	}

}
