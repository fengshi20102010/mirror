package com.mirror.tk.core.module.goods.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mirror.tk.core.module.goods.domain.GoodsShopInfo;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 商品店铺信息表 JPA Dao
 *
 * Date: 2016-11-09 21:28:34
 *
 * @author Code Generator
 *
 */
public interface GoodsShopInfoDao extends EntityJpaDao<GoodsShopInfo, Long> {

	public List<GoodsShopInfo> findBySid(Long sid);

	@Modifying
	@Query("delete from GoodsShopInfo where id in (?1)")
	public int deleteByShopId(List<Long> shopIds);
	
}
