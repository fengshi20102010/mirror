package com.mirror.tk.core.module.cms.service.impl;


import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.mirror.tk.core.module.cms.dao.FaqDao;
import com.mirror.tk.core.module.cms.domain.Faq;
import com.mirror.tk.core.module.cms.domain.Notice;
import com.mirror.tk.core.module.cms.service.FaqService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("faqService")
public class FaqServiceImpl extends EntityServiceImpl<Faq, FaqDao> implements FaqService {

	@Override
	public List<Faq> findFaq() {
		Map<String, Object> map = Maps.newHashMap();
		Map<String, Boolean> sortMap = Maps.newHashMap();
		map.put("EQ_status", Notice.STATUS_ENABLE);
		sortMap.put("createTime", false);
		return query(map, sortMap);
	}

}
