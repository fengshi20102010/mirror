package com.mirror.tk.core.module.sms.service;

import java.io.IOException;

import com.mirror.tk.core.sms.exception.SMSException;
import com.mirror.tk.core.sms.service.SendTemplateSmsService;
import com.mirror.tk.core.sms.service.SendTemplateSmsService.TEMPLATE_CODE;

public class ValidateCodeContext {

	private SendTemplateSmsService sendTemplateSmsService;
	private String vaildateCode;

	public ValidateCodeContext(String vaildateCode, SendTemplateSmsService sendTemplateSmsService) {
		this.vaildateCode = vaildateCode;
		this.sendTemplateSmsService = sendTemplateSmsService;
	}

	public void sendSms(String phone, TEMPLATE_CODE codeType) throws IOException, SMSException {
		sendTemplateSmsService.send(phone, codeType, vaildateCode);
	}

	public String getVaildateCode() {
		return vaildateCode;
	}
	
}
