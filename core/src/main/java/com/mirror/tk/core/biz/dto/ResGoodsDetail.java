package com.mirror.tk.core.biz.dto;

import java.util.List;

public class ResGoodsDetail {

	/**
	 * 商品ID
	 */
	private Long numIid;

	/**
	 * 商品名称
	 */
	private String name;

	/**
	 * 商品简介
	 */
	private String detail;

	/**
	 * 商品价格
	 */
	private String price;

	/**
	 * 商品销量
	 */
	private Integer sales;

	/**
	 * 优惠券金额
	 */
	private String couponPrice;

	/**
	 * 优惠劵限制(分)
	 */
	private Integer conditions;

	/**
	 * 优惠券限领数
	 */
	private Integer limited;

	/**
	 * 优惠券剩余
	 */
	private Integer couponRemained;

	/**
	 * 优惠券领取数
	 */
	private Integer couponApplied;

	/**
	 * 优惠券开始时间
	 */
	private String startTime;

	/**
	 * 优惠券结束时间
	 */
	private String endTime;

	/**
	 * 佣金
	 */
	private Integer commission;

	/**
	 * 活动类型(0:无,1:通用,2:定向,3.鹊桥)
	 */
	private Integer type;

	/**
	 * 优惠券领取地址
	 */
	private String couponGetUrl;

	/**
	 * 优惠券领取地址
	 */
	private String mobileCounponUrl;

	/**
	 * 商品链接地址
	 */
	private String goodsUrl;

	/**
	 * 店铺名
	 */
	private String shopName;

	/**
	 * 商品主图
	 */
	private String picUrl;

	/**
	 * 发单人QQ
	 */
	private String qq;

	/**
	 * 副标题
	 */
	private String subTitle;

	/**
	 * 预告时间
	 */
	private String adTime;

	/**
	 * 来源
	 */
	private Integer source;

	/**
	 * 商品类型ID
	 */
	private Long typeId;

	/**
	 * 推广计划链接
	 */
	private String planUrl;

	/**
	 * 商品小图
	 */
	private List<String> smallImg;

	public Long getNumIid() {
		return numIid;
	}

	public void setNumIid(Long numIid) {
		this.numIid = numIid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public Integer getSales() {
		return sales;
	}

	public void setSales(Integer sales) {
		this.sales = sales;
	}

	public String getCouponPrice() {
		return couponPrice;
	}

	public void setCouponPrice(String couponPrice) {
		this.couponPrice = couponPrice;
	}

	public Integer getConditions() {
		return conditions;
	}

	public void setConditions(Integer conditions) {
		this.conditions = conditions;
	}

	public Integer getLimited() {
		return limited;
	}

	public void setLimited(Integer limited) {
		this.limited = limited;
	}

	public Integer getCouponRemained() {
		return couponRemained;
	}

	public void setCouponRemained(Integer couponRemained) {
		this.couponRemained = couponRemained;
	}

	public Integer getCouponApplied() {
		return couponApplied;
	}

	public void setCouponApplied(Integer couponApplied) {
		this.couponApplied = couponApplied;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Integer getCommission() {
		return commission;
	}

	public void setCommission(Integer commission) {
		this.commission = commission;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getCouponGetUrl() {
		return couponGetUrl;
	}

	public void setCouponGetUrl(String couponGetUrl) {
		this.couponGetUrl = couponGetUrl;
	}

	public String getMobileCounponUrl() {
		return mobileCounponUrl;
	}

	public void setMobileCounponUrl(String mobileCounponUrl) {
		this.mobileCounponUrl = mobileCounponUrl;
	}

	public String getGoodsUrl() {
		return goodsUrl;
	}

	public void setGoodsUrl(String goodsUrl) {
		this.goodsUrl = goodsUrl;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getAdTime() {
		return adTime;
	}

	public void setAdTime(String adTime) {
		this.adTime = adTime;
	}

	public Integer getSource() {
		return source;
	}

	public void setSource(Integer source) {
		this.source = source;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public String getPlanUrl() {
		return planUrl;
	}

	public void setPlanUrl(String planUrl) {
		this.planUrl = planUrl;
	}

	public List<String> getSmallImg() {
		return smallImg;
	}

	public void setSmallImg(List<String> smallImg) {
		this.smallImg = smallImg;
	}

}
