package com.mirror.tk.core.module.goods.service;

import java.util.List;

import com.mirror.tk.core.module.goods.domain.GoodsShopInfo;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 商品店铺信息表 Service
 *
 * Date: 2016-11-09 21:28:34
 *
 * @author Code Generator
 *
 */
public interface GoodsShopInfoService extends EntityService<GoodsShopInfo> {

	/**
	 * 根据店铺id删除店铺
	 * @param shopIds
	 * @return
	 */
	Boolean deleteByShopId(List<Long> shopIds);

}
