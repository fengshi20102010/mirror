package com.mirror.tk.core.module.user.service;

import com.mirror.tk.core.module.user.domain.UserConfig;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 用户程序配置表 Service
 *
 * Date: 2016-12-28 23:56:41
 *
 * @author Code Generator
 *
 */
public interface UserConfigService extends EntityService<UserConfig> {

	/**
	 * 根据用户获取用户程序配置
	 * @param userId
	 * @return
	 */
	public UserConfig getByUserId(Long userId);

}
