package com.mirror.tk.core.module.user.service;

import com.mirror.tk.core.module.user.domain.User;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 用户信息主表 Service
 *
 * Date: 2016-11-01 23:09:01
 *
 * @author Code Generator
 *
 */
public interface UserService extends EntityService<User> {

	/**
	 * 通过电话查找用户
	 * @param username
	 * @return
	 */
	User getUserByPhone(String phone);

	/**
	 * 修改用户状态
	 * @param id
	 * @param status
	 * @return
	 */
	Boolean updateByStatus(Long id, Integer status);
	
}
