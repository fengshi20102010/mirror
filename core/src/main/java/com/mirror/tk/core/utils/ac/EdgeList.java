package com.mirror.tk.core.utils.ac;

interface EdgeList {
	State get(byte ch);

	void put(byte ch, State state);

	byte[] keys();
}
