package com.mirror.tk.core.module.goods.service;

import java.util.List;

import com.mirror.tk.core.biz.dto.GoodsTypeDto;
import com.mirror.tk.core.module.goods.domain.GoodsType;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 商品分类信息表 Service
 *
 * Date: 2016-11-01 23:10:13
 *
 * @author Code Generator
 *
 */
public interface GoodsTypeService extends EntityService<GoodsType> {
	
	/**
	 * 获取所有商品分类
	 * @return
	 */
	public List<GoodsType> getAllGoodsType();
	
	/**
	 * 根据状态获取商品分类
	 * @param status
	 * @return
	 */
	public List<GoodsType> getGoodsTypeByStatus(Integer status);

	/**
	 * 获取商品类型信息
	 * @return
	 */
	public List<GoodsTypeDto> getNormalType();
	
}
