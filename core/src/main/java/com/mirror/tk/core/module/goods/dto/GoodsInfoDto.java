package com.mirror.tk.core.module.goods.dto;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 商品信息DTO
 */
public class GoodsInfoDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/** ID */
	private Long id;

	/** 淘宝对应的商品ID */
	private Long numIid;

	/** 商品类型ID */
	private Long typeId;

	/** 推广计划(0:无,1:通用,2:定向,3.鹊桥) */
	private Integer type;

	/** 商品名称 */
	private String name;

	/** 商品简介 */
	private String detail;

	/** 商品链接地址 */
	private String url;

	/** 是否直推（0：否，1：是） */
	private Integer isExtend;

	/** 是否有优惠劵 */
	private Integer isCoupon;

	/** 商品属性 */
	private Integer attribute;

	/** 商品状态 */
	private Integer status;

	/** 佣金比例,1234代表12.34% */
	private Integer commission;

	/** 商品图片 */
	private String picUrl;

	/** 商品价格 */
	private String price;

	/** 商品销量 */
	private Integer sales;

	/** 创建时间 */
	private Date createTime;

	/** 优惠劵ID */
	private Long cId;

	/** 淘宝对应的活动ID */
	private String activityId;

	/** 淘宝对应的优惠劵ID */
	private Long couponId;

	/** 优惠劵金额 */
	private String money;

	/** 优惠劵总数 */
	private Integer total;

	/** 优惠劵领取数 */
	private Integer applied;

	/** 优惠劵剩余数 */
	private Integer remained;

	/** 优惠券限领数 */
	private Integer limited;

	/** 订单满多少分才能用这个优惠券，501就是满501分能使用。注意：返回的是“分”，不是“元” */
	private Integer conditions;

	/** 优惠劵过期时间 */
	private Date startTime;

	private Date endTime;

	/** 优惠劵领取地址 */
	private String couponUrl;

	/* 手机优惠券地址 **/
	private String mobileCouponUrl;

	/* 店铺名 **/
	private String title;

	/* 天猫，淘宝 **/
	private Integer source;

	/* 发单人QQ **/
	private String qq;

	/* 商品副标题 **/
	private String subTitle;

	/** 推广计划链接 */
	private String planUrl;

	private Date adTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNumIid() {
		return numIid;
	}

	public void setNumIid(Long numIid) {
		this.numIid = numIid;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getIsExtend() {
		return isExtend;
	}

	public void setIsExtend(Integer isExtend) {
		this.isExtend = isExtend;
	}

	public Integer getIsCoupon() {
		return isCoupon;
	}

	public void setIsCoupon(Integer isCoupon) {
		this.isCoupon = isCoupon;
	}

	public Integer getAttribute() {
		return attribute;
	}

	public void setAttribute(Integer attribute) {
		this.attribute = attribute;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getCommission() {
		return commission;
	}

	public void setCommission(Integer commission) {
		this.commission = commission;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public Integer getSales() {
		return sales;
	}

	public void setSales(Integer sales) {
		this.sales = sales;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getcId() {
		return cId;
	}

	public void setcId(Long cId) {
		this.cId = cId;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public Long getCouponId() {
		return couponId;
	}

	public void setCouponId(Long couponId) {
		this.couponId = couponId;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getApplied() {
		return applied;
	}

	public void setApplied(Integer applied) {
		this.applied = applied;
	}

	public Integer getRemained() {
		return remained;
	}

	public void setRemained(Integer remained) {
		this.remained = remained;
	}

	public Integer getLimited() {
		return limited;
	}

	public void setLimited(Integer limited) {
		this.limited = limited;
	}

	public Integer getConditions() {
		return conditions;
	}

	public void setConditions(Integer conditions) {
		this.conditions = conditions;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getCouponUrl() {
		return couponUrl;
	}

	public void setCouponUrl(String couponUrl) {
		this.couponUrl = couponUrl;
	}

	public String getMobileCouponUrl() {
		return mobileCouponUrl;
	}

	public void setMobileCouponUrl(String mobileCouponUrl) {
		this.mobileCouponUrl = mobileCouponUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getSource() {
		return source;
	}

	public void setSource(Integer source) {
		this.source = source;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getPlanUrl() {
		return planUrl;
	}

	public void setPlanUrl(String planUrl) {
		this.planUrl = planUrl;
	}

	public Date getAdTime() {
		return adTime;
	}

	public void setAdTime(Date adTime) {
		this.adTime = adTime;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
