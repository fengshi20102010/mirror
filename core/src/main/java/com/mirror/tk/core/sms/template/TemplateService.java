package com.mirror.tk.core.sms.template;

public interface TemplateService {

	String getTemplate(String code);

	String mergeTemplate(String code, String... params);

}
