package com.mirror.tk.core.biz.dto;

import java.util.List;

/**
 * 淘宝API抓取结果
 * @author Administrator
 *
 */
public class ResTaobaoGoods {

	/** 商品名称*/
	private String title;
	
	/** 商品主图*/
	private String imgUrl;
	
	/** */
	private List<String> smallImg;
	
	/* 商品ID**/
	private Long numIid;
	
	/** 商品价格*/
	private Double price;
	
	/** 商品链接*/
	private String url;
	
	/* 卖家类型：0：集市，1：商城**/
	private Long userType;
	
	/* 销量**/
	private Long volume;
	
	private Double finalPrice;
	
	private Long sellerId;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public List<String> getSmallImg() {
		return smallImg;
	}

	public void setSmallImg(List<String> smallImg) {
		this.smallImg = smallImg;
	}

	public Long getNumIid() {
		return numIid;
	}

	public void setNumIid(Long numIid) {
		this.numIid = numIid;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getUserType() {
		return userType;
	}

	public void setUserType(Long userType) {
		this.userType = userType;
	}

	public Long getVolume() {
		return volume;
	}

	public void setVolume(Long volume) {
		this.volume = volume;
	}

	public Double getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(Double finalPrice) {
		this.finalPrice = finalPrice;
	}

	public Long getSellerId() {
		return sellerId;
	}

	public void setSellerId(Long sellerId) {
		this.sellerId = sellerId;
	}
	
}
