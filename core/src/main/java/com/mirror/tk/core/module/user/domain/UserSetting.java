package com.mirror.tk.core.module.user.domain;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 用户程序设置表 Entity
 *
 * Date: 2016-12-14 22:57:08
 *
 * @author Code Generator
 */
@Entity
@Table(name = "user_setting")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserSetting extends AbstractEntity {
	/** id */
	private Long id;
	
	/** user_id */
	private Long userId;
	
	/** mama_account */
	private String mamaAccount;
	
	/** mama_pwd */
	private String mamaPwd;
	
	/** weichat_account */
	private String weichatAccount;
	
	/** weichat_tip */
	private String weichatTip;
	
	/** auto_account */
	private String autoAccount;
	
	/** auto_pwd */
	private String autoPwd;
	
	/** create_time */
	private Date createTime;
	
	/** updaste_time */
	private Date updateTime;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public String getMamaAccount(){
		return this.mamaAccount;
	}

	public void setMamaAccount(String mamaAccount){
		this.mamaAccount = mamaAccount;
	}

	public String getMamaPwd(){
		return this.mamaPwd;
	}

	public void setMamaPwd(String mamaPwd){
		this.mamaPwd = mamaPwd;
	}

	public String getWeichatAccount(){
		return this.weichatAccount;
	}

	public void setWeichatAccount(String weichatAccount){
		this.weichatAccount = weichatAccount;
	}

	public String getWeichatTip(){
		return this.weichatTip;
	}

	public void setWeichatTip(String weichatTip){
		this.weichatTip = weichatTip;
	}

	public String getAutoAccount(){
		return this.autoAccount;
	}

	public void setAutoAccount(String autoAccount){
		this.autoAccount = autoAccount;
	}

	public String getAutoPwd(){
		return this.autoPwd;
	}

	public void setAutoPwd(String autoPwd){
		this.autoPwd = autoPwd;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
