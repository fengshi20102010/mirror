package com.mirror.tk.core.log.aop;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleScriptContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.web.bind.annotation.PathVariable;

import com.google.common.collect.Maps;
import com.mirror.tk.core.log.annotation.Log;
import com.mirror.tk.core.log.service.LogWork;
import com.mirror.tk.core.module.sys.domain.SysOlog;
import com.mirror.tk.framework.common.web.support.Servlets;
import com.mirror.tk.framework.utils.Reflections;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;

public class LogControllerAdvice {

	private static final Logger logger = LogManager.getLogger();

	private String ignoreParameters;
	private LogWork logWork;
	private LogAdviceExpand logAdviceExpand;
	private String ignoreAttribute;
	private final Pattern pattern = Pattern.compile("\\$\\{(.[^\\}]*)?\\}");
	private ScriptEngine engine = new ScriptEngineManager().getEngineByName("groovy");

	private String[] getArgsNames(Method method) {
		LocalVariableTableParameterNameDiscoverer u = new LocalVariableTableParameterNameDiscoverer();
		String[] params = u.getParameterNames(method);
		return params;
	}

	private String getValue(String value, Method method, Object[] args) {
		Matcher matcher = pattern.matcher(value);
		// 判断是否存在表达式
		if (matcher.find() == false) {
			return value;
		}

		matcher.reset();
		StringBuffer resBuffer = new StringBuffer();
		// 设置groovy脚步
		ScriptContext context = new SimpleScriptContext();
		String[] argsName = getArgsNames(method);
		if (argsName != null) {
			if (args == null || args.length != args.length) {
				return "表达式错误：" + value;
			}
			for (int j = 0; j < args.length; j++) {
				context.setAttribute(argsName[j], args[j], ScriptContext.ENGINE_SCOPE);
			}
		}
		while (matcher.find()) {
			String script = matcher.group(1);
			Object res = null;
			try {
				res = engine.eval(script, context);
			} catch (ScriptException e) {
			}
			matcher.appendReplacement(resBuffer, res != null ? res.toString() : "");
		}
		matcher.appendTail(resBuffer);
		return resBuffer.toString();
	}

	/**
	 * SpringMvc 拦截日志处理
	 * 
	 * @param pjp
	 * @return
	 * @throws Throwable
	 */
	public Object interceptLog(ProceedingJoinPoint pjp) throws Throwable {
		Object result = null;
		// 计算处理时间，注意這個處理時間因為是代理執行數據庫操作原因，可能存在誤差。
		StopWatch clock = new StopWatch();
		clock.start(); // 计时开始
		try {
			result = pjp.proceed();
		} catch (Exception e) {
			result = e;
			throw e;
		} finally {
			clock.stop(); // 计时结束
			long executeMilliseconds = clock.getTime();
			try {
				log(pjp, result, executeMilliseconds);
			} catch (Exception ex) {
				logger.error("iceet	 log error", ex);
			}
		}
		return result;

	}

	public void log(ProceedingJoinPoint pjp, Object result, long executeMilliseconds) {
		Object target = pjp.getTarget();
		String methodName = pjp.getSignature().getName();
		// 获取日志注解
		Method method = Reflections.getAccessibleMethodByName(target, methodName);
		Log annotLog = method.getAnnotation(Log.class);
		// 日志打印
		logger.debug("execTime:{}ms;({}.{})", executeMilliseconds, target.getClass().getName(), methodName);
		if (logger.isTraceEnabled()) {
			// 获取request
			HttpServletRequest request = getRequest(pjp.getArgs());
			if (request != null) {
				// 客户端信息
				String clientInformations = getClientInformations(request);
				// request参数
				Map<String, Object> parmMap = Servlets.getParametersStartingWith(request, null);
				// request属性
				Enumeration<String> attrNames = request.getAttributeNames();
				Map<String, Object> attrMap = Maps.newTreeMap();
				while (attrNames != null && attrNames.hasMoreElements()) {
					String arrtName = (String) attrNames.nextElement();
					if (isIgnoreAttribute(arrtName) == false) {
						attrMap.put(arrtName, request.getAttribute(arrtName));
					}
				}
				// springMap
				Map<?, ?> springMap = getSpringMap(pjp.getArgs());
				logger.trace("execTime:{}ms; url:{}; clientInfo:{}; requestParam:{}; requestAttribute:{}; springMap:{}; ({}.{})", executeMilliseconds, request.getRequestURI(), clientInformations, parmMap.toString(), attrMap.toString(), springMap.toString(), target.getClass().getName(), methodName);
			}

		}
		// 如果没有Log注解，不记录日志
		if (annotLog == null) {
			return;
		}
		// 收集日志信息，并持久化，如果出错忽略。
		SysOlog olog = new SysOlog();
		olog.setExecuteMilliseconds(executeMilliseconds);
		olog.setTag(annotLog.tag());
		olog.setModule(this.getValue(annotLog.module(), method, pjp.getArgs()));
		olog.setModuleName(this.getValue(annotLog.moduleName(), method, pjp.getArgs()));
		olog.setAction(this.getValue(annotLog.action(), method, pjp.getArgs()));
		olog.setActionName(this.getValue(annotLog.actionName(), method, pjp.getArgs()));
		olog.setOperateTime(Calendar.getInstance().getTime());

		if (result instanceof Exception) {
			olog.setOperateResult(2);
			olog.setOperateMessage(result.getClass().getName());
		} else {
			olog.setOperateResult(1);
		}
		try {
			HttpServletRequest request = getRequest(pjp.getArgs());
			olog.setClientInfo(getClientInformations(request));
			handleParameters(request, target, methodName, pjp.getArgs(), olog, annotLog);
			// 备注
			olog.setDescription(request.getRequestURI());
			if (logAdviceExpand != null) {
				logAdviceExpand.expand(request, olog);
			}
		} catch (Exception e) {
			logger.error("olog request error:", e);
		}
		logger.debug(olog.toString());
		// 持久化日志
		logWork.doWork(olog);
	}

	protected void handleParameters(HttpServletRequest request, Object target, String methodName, Object args[], SysOlog olog, Log annotLog) {
		if (!annotLog.needRecordParameter()) {
			return;
		}
		String requestParameters = Arrays.toString(args);

		if (request != null) {
			requestParameters = getRequestParameters(request, target, methodName, annotLog, args);
		}
		requestParameters = StringUtils.substring(requestParameters, 0, 256);
		olog.setRequestParameters(requestParameters);
	}

	private String getRequestParameters(HttpServletRequest request, Object target, String methodName, Log annotLog, Object[] args) {
		String parameters = "";
		if (annotLog.needRecordParameter()) {
			Map<String, Object> requestParameters = Servlets.getParametersStartingWith(request, null);
			// 解析@PathVariable注解的参数
			try {
				Annotation[][] annotations = Reflections.getAccessibleMethodByName(target, methodName)	.getParameterAnnotations();
				for (int j = 0; j < annotations.length; j++) {
					Annotation[] annots = annotations[j];
					for (Annotation annot : annots) {
						if (annot instanceof PathVariable) {
							PathVariable var = (PathVariable) annot;
							requestParameters.put(var.value(), args[j]);
						}
					}
				}
			} catch (Exception ex) {
				logger.error("annotations param error:", ex);
			}
			Map<String, String> paramNameMapping = getParameterMapping(target, methodName, annotLog);

			String ignorParams = "";

			if (annotLog.ignorParam() != null) {
				String ignors[] = annotLog.ignorParam();
				if (ignors != null && ignors.length > 0) {
					ignorParams = "," + StringUtils.join(ignors, ",") + ",";
				}
			}
			Map<String, Object> confirmParameters = new TreeMap<String, Object>();
			for (Map.Entry<String, Object> entry : requestParameters.entrySet()) {
				if (!isIgnoreParameter(entry.getKey(), annotLog) && !StringUtils.contains(ignorParams, "," + entry.getKey() + ",") && entry.getValue() != null && StringUtils.isNotBlank(entry.getValue().toString())) {
					String key = entry.getKey();
					if (StringUtils.isNotBlank(paramNameMapping.get(key))) {
						key = paramNameMapping.get(key);
					}
					confirmParameters.put(key, entry.getValue());
				}
			}
			if (confirmParameters.size() > 0) {
				parameters = confirmParameters.toString();
			}
		}
		return parameters;
	}

	private Map<String, String> getParameterMapping(Object target, String methodName, Log annotLog) {
		Map<String, String> paramMapping = Maps.newHashMap();
		if (annotLog.paramMapping() != null) {
			String[] mapping = annotLog.paramMapping();
			if (mapping != null && mapping.length > 0) {
				for (String e : mapping) {
					String[] entry = StringUtils.split(e, ":");
					if (entry != null && entry.length == 2) {
						paramMapping.put(entry[0], entry[1]);
					}
				}
			}
		}
		return paramMapping;
	}

	/**
	 * 过滤掉request attribute 的参数
	 * 
	 * @param attribureName
	 * @return
	 */
	private boolean isIgnoreAttribute(String attribureName) {
		if (StringUtils.isBlank(ignoreAttribute) || StringUtils.isBlank(attribureName)) {
			return true;
		}
		String[] ignores = StringUtils.split(ignoreAttribute, ",");
		for (String str : ignores) {
			if (attribureName.indexOf(str) != -1) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断是否忽略的参数
	 *
	 * @param parameterName
	 * @return
	 */
	private boolean isIgnoreParameter(String parameterName, Log annotLog) {
		String[] ignores = StringUtils.split(ignoreParameters, ",");
		for (String ignoreStr : ignores) {
			if (isLike(ignoreStr, parameterName)) {
				return true;
			}
		}
		return false;
	}

	private boolean isLike(String srcIncludeStar, String dest) {
		if ("*".equals(srcIncludeStar)) {
			return true;
		} else if (srcIncludeStar.indexOf("*") == 0) {
			if (dest.indexOf(srcIncludeStar.substring(1, srcIncludeStar.length())) == dest.length()	- srcIncludeStar.length() + 1) {
				return true;
			} else {
				return false;
			}
		} else if (srcIncludeStar.indexOf("*") == srcIncludeStar.length() - 1) {
			if (dest.indexOf(srcIncludeStar.substring(0, srcIncludeStar.length() - 1)) == 0) {
				return true;
			} else {
				return false;
			}
		} else if (srcIncludeStar.equalsIgnoreCase(dest)) {
			return true;
		}
		return false;
	}

	/**
	 * 获取请求客户端信息
	 * 
	 * @param request
	 * @return
	 */
	private String getClientInformations(HttpServletRequest request) {
		if (request == null)
			return "";
		String clientIP = request.getRemoteAddr();
		String requestUserAgent = request.getHeader("User-Agent");
		UserAgent userAgent = UserAgent.parseUserAgentString(requestUserAgent);
		OperatingSystem os = userAgent.getOperatingSystem();
		Browser browser = userAgent.getBrowser();
		String clientInfo = clientIP + " - " + os.getName() + " - "	+ browser.getName() + "/" + browser.getBrowserType().getName();
		return clientInfo;
	}

	/**
	 * 获取Controller 的 map参数
	 * 
	 * @param args
	 * @return
	 */
	private Map<?, ?> getSpringMap(Object[] args) {
		Map<?, ?> map = Maps.newHashMap();
		for (Object arg : args) {
			if (arg instanceof Map) {
				map = (Map<?, ?>) arg;
				break;
			}
		}
		return map;
	}

	/**
	 * 获取Controller 的 HttpServletRequest
	 * 
	 * @param args
	 * @return
	 */
	private HttpServletRequest getRequest(Object[] args) {
		HttpServletRequest request = null;
		for (Object arg : args) {
			if (arg instanceof HttpServletRequest) {
				request = (HttpServletRequest) arg;
				break;
			}
		}
		return request;
	}

	public String getIgnoreAttribute() {
		return ignoreAttribute;
	}

	public void setIgnoreAttribute(String ignoreAttribute) {
		this.ignoreAttribute = ignoreAttribute;
	}

	public String getIgnoreParameters() {
		return ignoreParameters;
	}

	public void setIgnoreParameters(String ignoreParameters) {
		this.ignoreParameters = ignoreParameters;
	}

	public LogWork getLogWork() {
		return logWork;
	}

	public void setLogWork(LogWork logWork) {
		this.logWork = logWork;
	}

	public LogAdviceExpand getLogAdviceExpand() {
		return logAdviceExpand;
	}

	public void setLogAdviceExpand(LogAdviceExpand logAdviceExpand) {
		this.logAdviceExpand = logAdviceExpand;
	}
	
}
