package com.mirror.tk.core.module.goods.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 商品店铺信息表 Entity
 *
 * Date: 2017-03-04 14:13:36
 *
 * @author Code Generator
 */
@Entity
@Table(name = "goods_shop_info")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class GoodsShopInfo extends AbstractEntity {
	
	/** ID */
	private Long id;
	
	/** 淘宝对应的sid */
	private Long sid;
	
	/** 店铺名称 */
	private String title;
	
	/** 店铺图片 */
	private String pictUrl;
	
	/** 店铺连接地址 */
	private String shopUrl;
	
	/** 店铺状态 */
	private Integer status;
	
	/** 店家id */
	private Long sellerId;
	
	/** 店家昵称 */
	private String sellerNick;
	
	/** 备注 */
	private String remark;
	
	/** 创建时间 */
	private Date createTime;
	
	/** x修改时间 */
	private Date updateTime;
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getSid(){
		return this.sid;
	}

	public void setSid(Long sid){
		this.sid = sid;
	}

	public String getTitle(){
		return this.title;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getPictUrl(){
		return this.pictUrl;
	}

	public void setPictUrl(String pictUrl){
		this.pictUrl = pictUrl;
	}

	public String getShopUrl(){
		return this.shopUrl;
	}

	public void setShopUrl(String shopUrl){
		this.shopUrl = shopUrl;
	}

	public Integer getStatus(){
		return this.status;
	}

	public void setStatus(Integer status){
		this.status = status;
	}

	public Long getSellerId(){
		return this.sellerId;
	}

	public void setSellerId(Long sellerId){
		this.sellerId = sellerId;
	}

	public String getSellerNick(){
		return this.sellerNick;
	}

	public void setSellerNick(String sellerNick){
		this.sellerNick = sellerNick;
	}

	public String getRemark(){
		return this.remark;
	}

	public void setRemark(String remark){
		this.remark = remark;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}

	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
