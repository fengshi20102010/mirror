package com.mirror.tk.core.module.sys.service;

import com.mirror.tk.core.module.sys.domain.SysRole;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 系统角色表 Service
 *
 * Date: 2015-05-12 15:40:19
 *
 * @author Code Generator
 *
 */
public interface SysRoleService extends EntityService<SysRole> {


}
