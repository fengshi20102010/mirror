package com.mirror.tk.core.module.user.service;

import com.mirror.tk.core.module.user.domain.UserSendTpl;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 用户信息扩展表,将不常用的信息放入这张表 Service
 *
 * Date: 2016-11-01 23:08:57
 *
 * @author Code Generator
 *
 */
public interface UserSendTplService extends EntityService<UserSendTpl> {

	public UserSendTpl queryByUserId(Long userId);
}
