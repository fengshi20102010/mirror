package com.mirror.tk.core.module.sys.dao;

import org.springframework.data.jpa.repository.Query;

import com.mirror.tk.core.module.sys.domain.SysDictionaries;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 数据字典 JPA Dao
 *
 * Date: 2016-03-30 10:31:38
 *
 * @author Code Generator
 *
 */
public interface SysDictionariesDao extends EntityJpaDao<SysDictionaries, Long> {

	@Query("from SysDictionaries where key = ?1 ")
	public SysDictionaries findByKey(String key);

}
