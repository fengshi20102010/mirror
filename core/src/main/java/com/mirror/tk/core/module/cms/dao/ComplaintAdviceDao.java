package com.mirror.tk.core.module.cms.dao;

import com.mirror.tk.core.module.cms.domain.ComplaintAdvice;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 投诉建议 JPA Dao
 *
 * Date: 2016-11-01 23:10:35
 *
 * @author Code Generator
 *
 */
public interface ComplaintAdviceDao extends EntityJpaDao<ComplaintAdvice, Long> {

}
