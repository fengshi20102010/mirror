package com.mirror.tk.core.utils;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

public class TaobaoUrlUtil {

	/**
	 * 获取淘宝商品ID
	 * @param url
	 * @return
	 */
	public static String fetchItemId(String url) {
		Map<String, String> map = parseUrl(url);
		if(map == null) {
			return null;
		}
		String id = map.get("id");
		if(StringUtils.isEmpty(id)) {
			return null;
		}
		return id;
	}
	
	/**
	 * 通过优惠券URL获取activityId
	 * @param url
	 * @return
	 */
	public static String fetchActivityId(String url) {
		Map<String, String> map = parseUrl(url);
		if(map == null) {
			return null;
		}
		String activityId = map.get("activityid");
		if(activityId == null) {
			activityId = map.get("activity_id");
		}
		return activityId;
	}
	
	/**
	 * 通过优惠券URL获取sellerid
	 * @param url
	 * @return
	 */
	public static String fetchSellerId(String url) {
		Map<String, String> map = parseUrl(url);
		if(map == null) {
			return null;
		}
		String activityId = map.get("sellerid");
		if(activityId == null) {
			activityId = map.get("seller_id");
		}
		return activityId;
	}
	
	public static String transformItemUrl(String url,String src,String activityId,String pid,String numIid, Boolean isDx) {
		url += "activityId="+activityId;
		url += "&pid="+pid;
		url += "&itemId="+numIid;
		url += "&src="+src;
		if(isDx){
			url += "&dx=1";
		}
		return url ;
	}
	
	private static Map<String,String> parseUrl(String url){
		Map<String, String> map = UrlUtil.URLRequest(url);
		return map;
	}
}
