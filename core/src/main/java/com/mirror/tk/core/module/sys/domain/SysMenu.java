package com.mirror.tk.core.module.sys.domain;


import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 系统菜单表 Entity
 *
 * Date: 2015-05-28 09:55:09
 *
 * @author Acooly Code Generator
 */
@Entity
@Table(name = "sys_menu")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SysMenu extends AbstractEntity {

    /** id */
	private Long id;
	
	/** 父id */
	private Long parentId;
	
	/** 菜单名称 */
	private String title;
	
	/** 菜单名称拼音首字母 */
	private String titleFirstSpell;
	
	/** 菜单图标 */
	private String icon;
	
	/** 显示方式 */
	private Integer showMode;
	
	/** 描述 */
	private String description;
	
	/** 排序 */
	private Long sortNo;
	
	/** 状态 */
	private Integer status;
	
	/** 资源id */
	private Long resourceId;
	
	/** 创建时间 */
	private Date createTime;
	
	private Set<SysMenu> subMenus = new LinkedHashSet<SysMenu>();
	
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleFirstSpell() {
		return titleFirstSpell;
	}

	public void setTitleFirstSpell(String titleFirstSpell) {
		this.titleFirstSpell = titleFirstSpell;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Integer getShowMode() {
		return showMode;
	}

	public void setShowMode(Integer showMode) {
		this.showMode = showMode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getSortNo() {
		return sortNo;
	}

	public void setSortNo(Long sortNo) {
		this.sortNo = sortNo;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getResourceId() {
		return resourceId;
	}

	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public void setSubMenus(Set<SysMenu> subMenus) {
        this.subMenus = subMenus;
    }

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "parentId")
    @OrderBy("sortNo ASC")
    //@Where(clause = "status=1")
    public Set<SysMenu> getSubMenus() {
        return subMenus;
    }

    @Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
