package com.mirror.tk.core.module.goods.service;

import java.util.List;

import com.mirror.tk.core.module.goods.domain.GoodsImg;
import com.mirror.tk.framework.common.service.EntityService;

public interface GoodsImgService extends EntityService<GoodsImg> {

	/**
	 * 删除商品小图
	 * @param id
	 * @param ids
	 * @return
	 */
	Boolean deleteMyGoodsImg(List<Long> ids);

}
