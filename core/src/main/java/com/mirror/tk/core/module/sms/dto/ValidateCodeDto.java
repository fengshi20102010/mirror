package com.mirror.tk.core.module.sms.dto;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ValidateCodeDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String code;

	private String tel;

	private String codeType;

	private int expSeconds;
	
	private long now;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getCodeType() {
		return codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	public int getExpSeconds() {
		return expSeconds;
	}

	public void setExpSeconds(int expSeconds) {
		this.expSeconds = expSeconds;
	}

	public long getNow() {
		return now;
	}

	public void setNow(long now) {
		this.now = now;
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
	
}
