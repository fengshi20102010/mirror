package com.mirror.tk.core.log.service;

import com.mirror.tk.core.module.sys.domain.SysOlog;

public interface LogWork {

	public void doWork(SysOlog sysOlog);

}
