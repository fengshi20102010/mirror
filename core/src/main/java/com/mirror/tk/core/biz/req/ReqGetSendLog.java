package com.mirror.tk.core.biz.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class ReqGetSendLog {

	@ApiModelProperty(value="当前用户ID",required=false)
	private Long userId;
	
	@ApiModelProperty(value="开始时间 yyyy-MM-dd",required=false)
	private String startTime;
	
	@ApiModelProperty(value="结束时间 yyyy-MM-dd",required=false)
	private String endTime;
	
	@ApiModelProperty(value="商品名称",required=false)
	private String title;
	
	@ApiModelProperty(value="当前页",required=true)
	private Integer currentPage;
	@ApiModelProperty(value="每页多少条",required=true)
	private Integer pageSize;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	
	
}
