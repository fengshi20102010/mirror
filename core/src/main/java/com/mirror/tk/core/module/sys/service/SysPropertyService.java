package com.mirror.tk.core.module.sys.service;

import java.util.List;

import com.mirror.tk.core.module.sys.domain.SysProperty;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 系统_属性表 Service
 *
 * Date: 2016-01-08 09:54:41
 *
 * @author Code Generator
 *
 */
public interface SysPropertyService extends EntityService<SysProperty> {

	public List<SysProperty> findSysProperty(String pro_key);
	
	/**
	 * 通过key查询属性值
	 * @param pro_key
	 * @return
	 */
	public SysProperty findByProKey(String proKey);
	
}
