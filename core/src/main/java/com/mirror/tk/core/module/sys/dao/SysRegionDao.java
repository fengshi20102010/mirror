package com.mirror.tk.core.module.sys.dao;

import com.mirror.tk.core.module.sys.domain.SysRegion;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 地区 JPA Dao
 *
 * Date: 2016-03-30 10:31:49
 *
 * @author Code Generator
 *
 */
public interface SysRegionDao extends EntityJpaDao<SysRegion, Long> {

}
