package com.mirror.tk.core.module.gather.dao;

import com.mirror.tk.core.biz.req.ReqMessageGatherQueryInfo;
import com.mirror.tk.core.module.gather.domain.MessageGather;
import com.mirror.tk.core.module.gather.dto.MessageGatherDto;
import com.mirror.tk.framework.common.dao.support.PageInfo;

public interface MessageGatherCustomDao {
	 /**
		 * 更新数据
		 * @param entity
		 * @return
	     */

		//void update(MessageGather entity,String id);


		/**
		 * 分页查询数据
		 * @return
	     */
		public PageInfo<MessageGatherDto> queryPage(PageInfo<MessageGatherDto> pageInfo, ReqMessageGatherQueryInfo reqInfo);
}
