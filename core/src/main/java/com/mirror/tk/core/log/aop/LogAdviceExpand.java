package com.mirror.tk.core.log.aop;

import javax.servlet.http.HttpServletRequest;

import com.mirror.tk.core.module.sys.domain.SysOlog;

public interface LogAdviceExpand {

	public void expand(HttpServletRequest request, SysOlog olog);

}
