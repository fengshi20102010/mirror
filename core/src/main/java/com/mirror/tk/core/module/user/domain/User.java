package com.mirror.tk.core.module.user.domain;

import java.util.Date;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.google.common.collect.Maps;
import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 用户信息主表 Entity
 *
 * Date: 2016-11-09 21:28:59
 *
 * @author Code Generator
 */
@Entity
@Table(name = "user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class User extends AbstractEntity {

	/** id */
	private Long id;

	/** 用户名 */
	private String username;

	/** 密码 */
	private String password;

	/** 加密盐 */
	private String salt;

	/** 电话 */
	private String phone;

	/** 邮箱 */
	private String email;

	/** 性别 */
	private Integer sex;

	/** 年龄 */
	private Integer age;

	/** 余额 */
	private Long score;

	/** QQ */
	private String qq;

	/** 微信 */
	private String wechat;

	/** 用户状态 */
	private Integer status;

	/** 团队或个人名称 */
	private String teamName;

	/** 团队类型 */
	private Integer type;

	/** 收入 */
	private Integer income;

	/** 渠道 */
	private String channel;

	/** 申请理由 */
	private String reason;

	/** 附件 */
	private String attachment;

	/** 用户备注 */
	private String remark;

	/** 用户创建时间 */
	private Date createTime;

	/** 用户修改时间 */
	private Date updateTime;

	/** 未知 */
	public static final Integer SEX_UNKNOWN = 0;
	/** 男 */
	public static final Integer SEX_MALE = 1;
	/** 女 */
	public static final Integer SEX_FEMALE = 2;
	/** 性别 */
	public static Map<Integer, String> allSexs = Maps.newTreeMap();

	static {
		allSexs.put(0, "未知");
		allSexs.put(1, "男");
		allSexs.put(2, "女");
	}

	/** 冻结 */
	public static final Integer STATUS_FROZEN = 0;
	/** 待完善 */
	public static final Integer STATUS_CONSUMMATE = 1;
	/** 待审核 */
	public static final Integer STATUS_CHECK_PENDING = 2;
	/** 已审核 */
	public static final Integer STATUS_NORMAL = 3;
	/** 驳回 */
	public static final Integer STATUS_REJECT = 4;

	/** 状态 */
	public static Map<Integer, String> allStatuss = Maps.newTreeMap();

	static {
		allStatuss.put(0, "冻结");
		allStatuss.put(1, "待完善");
		allStatuss.put(2, "待审核");
		allStatuss.put(3, "已审核");
		allStatuss.put(4, "驳回");
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Long getScore() {
		return score;
	}

	public void setScore(Long score) {
		this.score = score;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getIncome() {
		return income;
	}

	public void setIncome(Integer income) {
		this.income = income;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
