package com.mirror.tk.core.module.sys.dto;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ResDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/** id */
	private Long id;
	
	/** 父id */
	private Long parentId;
	
	/** 资源名称 */
	private String title;
	
	/** 资源类型 */
	private String resType;
	
	/** 排序号 */
	private Long sortNo;
	
	/** 是否已选择 */
	private Boolean selected = false;
	
	/** 子资源 */
    private List<ResDto> subRes;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getResType() {
		return resType;
	}

	public void setResType(String resType) {
		this.resType = resType;
	}

	public Long getSortNo() {
		return sortNo;
	}

	public void setSortNo(Long sortNo) {
		this.sortNo = sortNo;
	}

	public List<ResDto> getSubRes() {
		return subRes;
	}

	public void setSubRes(List<ResDto> subRes) {
		this.subRes = subRes;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
