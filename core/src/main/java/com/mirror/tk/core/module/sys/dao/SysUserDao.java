package com.mirror.tk.core.module.sys.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mirror.tk.core.module.sys.domain.SysUser;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 系统用户表 JPA Dao
 *
 * Date: 2015-05-11 16:11:20
 *
 * @author Code Generator
 *
 */
public interface SysUserDao extends EntityJpaDao<SysUser, Long> {

	public SysUser findByUsername(String username);
	
    @Modifying
    @Query(value = "update SysUser set status=?2 where id=?1")
    public int updateStatusById(Long id, Integer status);

    @Modifying
    @Query(value = "update SysUser t set t.password=?2, t.salt=?3 where id=?1")
    public int updateUserpassById(Long id, String userpass, String salt);
    
    @Modifying
    @Query(value = "update SysUser t set t.lastLoginTime=?2, t.lastLoginIp=?3 where id=?1")
    public int updateUserLoginById(Long id, Date lastLoginDate, String lastLoginIp);
    
    public SysUser findByUsernameAndStatus(String username,Integer status);

}
