package com.mirror.tk.core.module.gather.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;


/**
 * Table:MessageGather 云产品库采集产品
 * @author tonysu
 * @date 2017-04-02 14:37:36
 *
 */
@Entity
@Table(name = "message_gather")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MessageGather implements Serializable{

    private static final long serialVersionUID = -7964309821395968168L;

   	//ID
	private Integer id;
	//采集时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private String gatherDate;
	//采集QQ群
	private String gatherQq;
	//发送消息人
	private String senderQq;
	//发送给群组详情
	private String sendTo;
	//群组名称
	private String groupsetName;
	//发送内容
	private String messageContent;
	//是否采集
	private String gatherOrNot;
	//商品类型（天猫/淘宝）
	private Integer type;
	//转换状态
	private String transferStatus;
	//发送状态
	private String sendStatus;
	//发送时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private String sendTime;
	//发送次数
	private Integer sendNum;
	//淘宝日志
	private String taobaolog;
	//微信发送图片等待几秒发送文字
	private Integer weixinWaitTime;
	//检测图片是否发送成功
	private String picCheck;
	//插入时间
	/*@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Timestamp insertTime;*/
	//可用标记
	private Integer valiableTag;
	
	
	//用戶DI
   	private Long userId;
   	
	
   	public Integer getValiableTag() {
   		if(null == valiableTag){
			this.valiableTag = 0;
   		}
		return valiableTag;
	}
	public void setValiableTag(Integer valiableTag) {
	
		this.valiableTag = valiableTag;

	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	//getters and setters
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
   	public Integer getId(){
		return id;
	}
	public void setId(Integer id){
		this.id = id;
	}
	public String getGatherDate(){
		return gatherDate;
	}
	public void setGatherDate(String gatherDate){
		this.gatherDate = gatherDate;
	}
	
	public String getSendTo(){
		return sendTo;
	}
	public void setSendTo(String sendTo){
		this.sendTo = sendTo;
	}
	public String getGroupsetName(){
		return groupsetName;
	}
	public void setGroupsetName(String groupsetName){
		this.groupsetName = groupsetName;
	}
	public String getMessageContent(){
		return messageContent;
	}
	public void setMessageContent(String messageContent){
		this.messageContent = messageContent;
	}
	public String getGatherOrNot(){
		return gatherOrNot;
	}
	public void setGatherOrNot(String gatherOrNot){
		this.gatherOrNot = gatherOrNot;
	}
	public Integer getType(){
		return type;
	}
	public void setType(Integer type){
		this.type = type;
	}
	public String getTransferStatus(){
		return transferStatus;
	}
	public void setTransferStatus(String transferStatus){
		this.transferStatus = transferStatus;
	}
	public String getSendStatus(){
		return sendStatus;
	}
	public void setSendStatus(String sendStatus){
		this.sendStatus = sendStatus;
	}
	public String getSendTime(){
		return sendTime;
	}
	public void setSendTime(String sendTime){
		this.sendTime = sendTime;
	}
	public Integer getSendNum(){
		return sendNum;
	}
	public void setSendNum(Integer sendNum){
		this.sendNum = sendNum;
	}
	public String getTaobaolog(){
		return taobaolog;
	}
	public void setTaobaolog(String taobaolog){
		this.taobaolog = taobaolog;
	}
	public Integer getWeixinWaitTime(){
		return weixinWaitTime;
	}
	public void setWeixinWaitTime(Integer weixinWaitTime){
		this.weixinWaitTime = weixinWaitTime;
	}
	public String getPicCheck(){
		return picCheck;
	}
	public void setPicCheck(String picCheck){
		this.picCheck = picCheck;
	}
	public String getGatherQq() {
		return gatherQq;
	}
	public void setGatherQq(String gatherQq) {
		this.gatherQq = gatherQq;
	}
	public String getSenderQq() {
		return senderQq;
	}
	public void setSenderQq(String senderQq) {
		this.senderQq = senderQq;
	}
	@ApiModelProperty(value = "时间间隔", required = false)
	private String sendCycleTime ;
	public String getSendCycleTime() {
		return sendCycleTime;
	}
	public void setSendCycleTime(String sendCycleTime) {
		this.sendCycleTime = sendCycleTime;
	}

	@ApiModelProperty(value = "采集群号", required = false)
	private String gatherGroup;


	public String getGatherGroup() {
		return gatherGroup;
	}
	public void setGatherGroup(String gatherGroup) {
		this.gatherGroup = gatherGroup;
	}

}