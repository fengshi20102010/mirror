package com.mirror.tk.core.biz.req;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class ReqAddGoods implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "商品ID(修改商品时有效)", required = false)
	private Long id;

	@ApiModelProperty(value = "用户ID(不填，系统自动获取)", required = false)
	private Long userId;

	@ApiModelProperty(value = "商品原始链接", required = true)
	private String goodsUrl;

	@ApiModelProperty(value = "优惠劵链接", required = true)
	private String couponUrl;

	@ApiModelProperty(value = "商品分类ID", required = true)
	private Long typeId;
	
	@ApiModelProperty(value = "佣金比例,1234代表12.34%", required = true)
	private Integer commission;

	@ApiModelProperty(value = "推广计划{1:通用,2:定向,3.鹊桥}", required = true)
	private Integer plan;

	@ApiModelProperty(value = "推广计划链接", required = false)
	private String planUrl;

	@ApiModelProperty(value = "商品导购", required = false)
	private String subTitle;

	@ApiModelProperty(value = "商品文案", required = false)
	private String memo;

	@ApiModelProperty(value = "发单人QQ", required = true)
	private String qq;

	@ApiModelProperty(value = "属性 {0:无属性,1:优质,2:秒杀,3:精选直播,4:客户端首页推广,5：明日预告}", required = true)
	private Integer attr;

	@ApiModelProperty(value = "预告时间（格式：yyyy-MM-dd，当为预告的时候生效）", required = false)
	private String adTime;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGoodsUrl() {
		return goodsUrl;
	}

	public void setGoodsUrl(String goodsUrl) {
		this.goodsUrl = goodsUrl;
	}

	public String getCouponUrl() {
		return couponUrl;
	}

	public void setCouponUrl(String couponUrl) {
		this.couponUrl = couponUrl;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public Integer getCommission() {
		return commission;
	}

	public void setCommission(Integer commission) {
		this.commission = commission;
	}

	public Integer getPlan() {
		return plan;
	}

	public void setPlan(Integer plan) {
		this.plan = plan;
	}

	public String getPlanUrl() {
		return planUrl;
	}

	public void setPlanUrl(String planUrl) {
		this.planUrl = planUrl;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public Integer getAttr() {
		return attr;
	}

	public void setAttr(Integer attr) {
		this.attr = attr;
	}

	public String getAdTime() {
		return adTime;
	}

	public void setAdTime(String adTime) {
		this.adTime = adTime;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
