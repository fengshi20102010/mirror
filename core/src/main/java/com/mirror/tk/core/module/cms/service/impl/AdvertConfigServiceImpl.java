package com.mirror.tk.core.module.cms.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.mirror.tk.core.module.cms.dao.AdvertConfigDao;
import com.mirror.tk.core.module.cms.domain.AdvertConfig;
import com.mirror.tk.core.module.cms.service.AdvertConfigService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;


@Service("advertConfigService")
public class AdvertConfigServiceImpl extends EntityServiceImpl<AdvertConfig, AdvertConfigDao> implements AdvertConfigService {

	@Override
	public List<AdvertConfig> findByType(Integer type) {
		Map<String, Object> map = Maps.newHashMap();
		Map<String, Boolean> sortMap = Maps.newHashMap();
		map.put("EQ_type", type);
		map.put("EQ_status", AdvertConfig.STATUS_ENABLE);
		sortMap.put("sortNo", false);
		return query(map, sortMap);
	}

}
