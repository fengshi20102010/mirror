package com.mirror.tk.core.module.gather.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.mirror.tk.core.biz.req.ReqMessageGather;
import com.mirror.tk.core.biz.req.ReqMessageGatherQueryInfo;
import com.mirror.tk.core.module.gather.dao.MessageGatherCustomDao;
import com.mirror.tk.core.module.gather.dao.MessageGatherDao;
import com.mirror.tk.core.module.gather.domain.MessageGather;
import com.mirror.tk.core.module.gather.dto.MessageGatherDto;
import com.mirror.tk.core.module.gather.service.MessageGatherService;
import com.mirror.tk.framework.common.dao.support.PageInfo;

/**
 *
 * @author sufj
 * @date 2017-04-02 14:37:36
 *
 */
@Service
public class MessageGatherServiceImpl  implements MessageGatherService{

	@Resource
	MessageGatherDao messageGatherDao;
	
	@Resource
	MessageGatherCustomDao messageGatherCustomDao;
	
	@Override
	public int save(ReqMessageGather entity) {
		MessageGather gather = new MessageGather();
		BeanUtils.copyProperties(entity, gather);
		MessageGather message = messageGatherDao.save(gather);	
		if(message != null){
			return 1;
		}
		return 0;
	}

	@Override
	public int update(ReqMessageGather entity) {
		
		MessageGather gather = new MessageGather();
		BeanUtils.copyProperties(entity, gather);
		messageGatherDao.update(gather);
		return 1;
	}

	@Override
	public int delete(int id) {
		return messageGatherDao.delete(id);	
	}

	@Override
	public MessageGather queryById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PageInfo<MessageGatherDto> queryPage(PageInfo<MessageGatherDto> pageInfo, ReqMessageGatherQueryInfo reqInfo){
		return messageGatherCustomDao.queryPage(pageInfo, reqInfo);		
	}

	@Override
	public int save(List<ReqMessageGather> list) {
		List<MessageGather> saveList = new ArrayList<MessageGather>();
		MessageGather mg;
		for(ReqMessageGather entity: list){
			mg = new MessageGather();
			BeanUtils.copyProperties(entity,mg);
			saveList.add(mg);
		}
		List<MessageGather> result = messageGatherDao.save(saveList);		
		
		return result.size();
	}

	@Override
	public Integer update(List<ReqMessageGather> reqList) {

		
		for (ReqMessageGather reqMessageGather : reqList) {
			MessageGather gather = new MessageGather();
			BeanUtils.copyProperties(reqMessageGather, gather);
			messageGatherDao.update(gather);
			
		}
		
		return reqList.size();
	}


}