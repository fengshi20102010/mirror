package com.mirror.tk.core.module.gather.dao.impl;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.mirror.tk.core.biz.req.ReqMessageGatherQueryInfo;
import com.mirror.tk.core.module.gather.dao.MessageGatherCustomDao;
import com.mirror.tk.core.module.gather.dto.MessageGatherDto;
import com.mirror.tk.framework.common.dao.jdbc.PagedJdbcTemplate;
import com.mirror.tk.framework.common.dao.support.PageInfo;

@Repository("messageGatherCustomDao")
public class MessageGatherCustomDaoImpl implements MessageGatherCustomDao {

 @Resource
 private PagedJdbcTemplate pagedJdbcTemplate;
 
 /*public void update(MessageGather entity,String id){
  StringBuffer sql = new StringBuffer();
  sql.append("UPDATE message_gather set ")
  .append("tranfer_status = ?")
  .append("send_status = ?")
  .append("send_time =?")
  .append("send_num=?")
  .append("taobaolog=?")
  .append("weixin_wait_time=?")
  .append("pic_check=?")
  .append("WHERE id = ?");
  
  
 }*/

	 @SuppressWarnings("unchecked")
	 @Override
	 public PageInfo<MessageGatherDto> queryPage(PageInfo<MessageGatherDto> pageInfo, ReqMessageGatherQueryInfo reqInfo) {
	  
		  String sql = buildMessageGatherSQL(reqInfo);
		  return  pagedJdbcTemplate.queryMySql(pageInfo, sql, null, MessageGatherDto.class);
	 }
	
	 private String buildMessageGatherSQL(ReqMessageGatherQueryInfo reqInfo) {
	  StringBuffer sb = new StringBuffer()
	  .append("SELECT")
	  .append(" mg.id,")
	  .append(" mg.user_id,")
	  .append(" mg.gather_date,")
	  .append(" mg.gather_group,")
	  .append(" mg.gather_qq,")
	  .append(" mg.sender_qq,")
	  .append(" mg.send_to,")
	  .append(" mg.groupset_name,")
	  .append(" mg.message_content,")
	  .append(" mg.gather_or_not,")
	  .append(" mg.type,")
	  .append(" mg.transfer_status,")
	  .append(" mg.send_status,")
	  .append(" mg.send_time,")
	  .append(" mg.send_cycle_time,")
	  .append(" mg.send_num,")
	  .append(" mg.taobaolog,")
	  .append(" mg.weixin_wait_time,")
	  .append(" mg.pic_check,")
	  .append(" mg.insert_time,")
	  .append(" mg.valiable_tag")
	  .append(" from message_gather AS mg")
	  .append("  WHERE mg.valiable_tag = 0");
	  if(null != reqInfo.getId()){
		  sb.append(" and mg.id=").append(reqInfo.getId());
	  }
	  if(null != reqInfo.getUserId()){
		  sb.append(" and mg.user_id =").append(reqInfo.getUserId());
	  }
	  if(null != reqInfo.getType()){
		  sb.append(" and mg.type = ").append(reqInfo.getType());
	  }
	  if(null != reqInfo.getTransferStatus()){
		  sb.append(" and mg.transfer_status=").append(reqInfo.getTransferStatus());
	  }
	  if(null != reqInfo.getSendStatus()){
		  sb.append(" and mg.send_status=").append(reqInfo.getSendStatus());
	  }
	  if(StringUtils.isNotBlank(reqInfo.getSendTimeStart())){
		  sb.append(" and mg.send_time > '").append(reqInfo.getSendTimeStart()).append("'");
	  }
	  if(StringUtils.isNotBlank( reqInfo.getSendTimeEnd())){
		  sb.append(" and mg.send_time < '").append(reqInfo.getSendTimeEnd()).append("'");
	  }
	  if(null != reqInfo.getMessageContent()){
		  sb.append(" and mg.message_content like '%").append(reqInfo.getMessageContent()).append("%'");
	  }
	  
	  return sb.toString();
	 }

}
