package com.mirror.tk.core.module.user.service;

import java.util.List;

import com.mirror.tk.core.module.user.domain.UserGroupDetail;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 用户QQ/微信群详情表 Service
 *
 * Date: 2017-01-01 22:30:33
 *
 * @author Code Generator
 *
 */
public interface UserGroupDetailService extends EntityService<UserGroupDetail> {

	/**
	 * 通过groupId删除子对象
	 * @param groupId
	 * @return
	 */
	public boolean deleteByGroupId(Long groupId);

	/**
	 * 保存对象
	 * @param id
	 * @param list
	 */
	public void saveByGroupId(Long id, List<UserGroupDetail> list);

	/**
	 * 根据组id和id删除详情
	 * @param groupId
	 * @param list
	 * @return
	 */
	public boolean deleteByGroupIdAndIds(Long groupId, List<Long> list);

}
