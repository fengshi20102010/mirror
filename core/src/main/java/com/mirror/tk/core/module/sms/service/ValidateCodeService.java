package com.mirror.tk.core.module.sms.service;

import com.mirror.tk.core.sms.service.SendTemplateSmsService.TEMPLATE_CODE;

public interface ValidateCodeService {

	/**
	 * 生成验证码
	 * @param phone 会话ID，确保生成和验证时使用的值一致
	 * @param codeType 验证码类型
	 * @param expSeconds 过期时间，单位秒
	 * @return ValidateCodeContext 验证码上下文，包含生成的验证码和发送服务
	 */
	ValidateCodeContext builderCode(String phone, TEMPLATE_CODE codeType, int expSeconds);

	/**
	 * 验证码是否有效
	 * @param phone 会话ID，确保生成和验证时使用的值一致
	 * @param codeType 验证码类型
	 * @param code 验证码
	 * @param isDelete 是否验证成功后删除
	 * @return boolean true - 有效， false - 无效
	 */
	boolean vaildateCode(String phone, TEMPLATE_CODE codeType, String code, Boolean isDelete);
	
	
}
