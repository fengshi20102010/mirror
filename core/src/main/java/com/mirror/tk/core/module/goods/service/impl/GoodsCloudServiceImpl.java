package com.mirror.tk.core.module.goods.service.impl;


import org.springframework.stereotype.Service;

import com.mirror.tk.core.module.goods.dao.GoodsCloudDao;
import com.mirror.tk.core.module.goods.domain.GoodsCloud;
import com.mirror.tk.core.module.goods.service.GoodsCloudService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("goodsCloudService")
public class GoodsCloudServiceImpl extends EntityServiceImpl<GoodsCloud, GoodsCloudDao> implements GoodsCloudService {

}
