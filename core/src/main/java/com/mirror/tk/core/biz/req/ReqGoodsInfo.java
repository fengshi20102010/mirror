package com.mirror.tk.core.biz.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class ReqGoodsInfo {

	/**
	 * 商品类型 ： 1：今日推荐，2：今日新品
	 */
	@ApiModelProperty(value="商品类型 ： 1：今日推荐（网站首页今日推荐商品），2：今日上新,3：精选直播（客户端精选直播商品数据），4：客户端首页商品数据，5：明日预告",required=false)
	private Integer cat;
	
	@ApiModelProperty(value="当前页",required=true)
	private Integer currentPage;
	@ApiModelProperty(value="每页多少条",required=true)
	private Integer pageSize;
	
	/* 商品名称（搜索)**/
	@ApiModelProperty(value="商品名称")
	private String name;
	
	@ApiModelProperty(value="商品分类",required=false)
	private Long type;
	
	@ApiModelProperty(value="排序方式 0-综合，1-最新，2-销量(从高到低)，-2-销量（从低到高）,3-佣金(从高到低)，-3-佣金(从低到高)，4-价格（从高到低），-4-价格（从低到高），5-剩余时间",required=false)
	private Byte sort;
	
	@ApiModelProperty(value="去重最高佣金 1-是，0:-否",required=false)
	private Byte highestCom;
	
	@ApiModelProperty(value="来源 1-天猫，2-淘宝",required=false)
	private Byte source;
	
	@ApiModelProperty(value="价格区间（高）",required=false)
	private Integer priceHigh;
	
	@ApiModelProperty(value="价格区间（低）",required=false)
	private Integer priceLow;
	
	@ApiModelProperty(value="销量区间（高）",required=false)
	private Integer salesHigh;
	
	@ApiModelProperty(value="销量区间（低）",required=false)
	private Integer salesLow;
	
	@ApiModelProperty(value="佣金区间（高）-4位整数",required=false)
	private Integer comHigh;
	
	@ApiModelProperty(value="佣金区间（低）-4位整数",required=false)
	private Integer comLow;
	
	@ApiModelProperty(value="预告时间",required=false)
	private String adTime;

	public Integer getCat() {
		return cat;
	}

	public void setCat(Integer cat) {
		this.cat = cat;
	}

	public Integer getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public Byte getSort() {
		return sort;
	}

	public void setSort(Byte sort) {
		this.sort = sort;
	}

	public Byte getHighestCom() {
		return highestCom;
	}

	public void setHighestCom(Byte highestCom) {
		this.highestCom = highestCom;
	}

	public Byte getSource() {
		return source;
	}

	public void setSource(Byte source) {
		this.source = source;
	}

	public Integer getPriceHigh() {
		return priceHigh;
	}

	public void setPriceHigh(Integer priceHigh) {
		this.priceHigh = priceHigh;
	}

	public Integer getPriceLow() {
		return priceLow;
	}

	public void setPriceLow(Integer priceLow) {
		this.priceLow = priceLow;
	}

	public Integer getSalesHigh() {
		return salesHigh;
	}

	public void setSalesHigh(Integer salesHigh) {
		this.salesHigh = salesHigh;
	}

	public Integer getSalesLow() {
		return salesLow;
	}

	public void setSalesLow(Integer salesLow) {
		this.salesLow = salesLow;
	}

	public Integer getComHigh() {
		return comHigh;
	}

	public void setComHigh(Integer comHigh) {
		this.comHigh = comHigh;
	}

	public Integer getComLow() {
		return comLow;
	}

	public void setComLow(Integer comLow) {
		this.comLow = comLow;
	}

	public String getAdTime() {
		return adTime;
	}

	public void setAdTime(String adTime) {
		this.adTime = adTime;
	}

	
}
