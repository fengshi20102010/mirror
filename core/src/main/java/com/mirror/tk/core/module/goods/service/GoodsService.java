package com.mirror.tk.core.module.goods.service;

import java.util.List;
import java.util.Map;

import com.mirror.tk.core.module.goods.domain.Goods;
import com.mirror.tk.core.module.goods.domain.GoodsCoupon;
import com.mirror.tk.core.module.goods.domain.GoodsImg;
import com.mirror.tk.core.module.goods.domain.GoodsShopInfo;
import com.mirror.tk.core.module.goods.dto.GoodsInfoDto;
import com.mirror.tk.core.module.goods.dto.GoodsNumDto;
import com.mirror.tk.framework.common.dao.support.PageInfo;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 商品信息主表 Service
 *
 * Date: 2016-11-01 23:10:23
 *
 * @author Code Generator
 *
 */
public interface GoodsService extends EntityService<Goods> {

	/**
	 * 查询商品分页信息
	 * @param pageInfo
	 * @param searchMap
	 * @return
	 */
	PageInfo<GoodsInfoDto> queryGoodsInfo(PageInfo<GoodsInfoDto> pageInfo, Map<String, Object> searchMap);
	
	/**
	 * 查询商品详细信息
	 * @param id
	 * @return
	 */
	GoodsInfoDto queryGoodsInfo(Long id);
	
	/**
	 * 根据淘宝ID查询商品
	 * @param numIid
	 * @return
	 */
	Goods queryGoods(Long numIid);
	
	/**
	 * 添加商品
	 * @param goods
	 * @param goodsCoupon
	 * @param goodsImg
	 */
	void addGoods(Goods goods, GoodsCoupon goodsCoupon, List<GoodsImg> goodsImg,GoodsShopInfo goodsShopInfo);
	
	/**
	 * 查询商品小图
	 * @param ids
	 * @return
	 */
	List<GoodsImg> queryGoodsImgs(List<Long> ids);

	/**
	 * 修改商品状态
	 * @param id
	 * @param status
	 * @return
	 */
	Boolean updateByStatus(Long id, Integer status);

	/**
	 * 根据id更新销量
	 * @param id
	 * @param sales
	 * @return
	 */
	Boolean updateById(Long id, Long sales);
	
	/**
	 * 获取我的商品数据
	 * @param userId
	 * @return
	 */
	List<GoodsNumDto> findByMyNum(Long userId);

	/**
	 * 根据用户id和商品id，删除商品信息
	 * @param id
	 * @param ids
	 * @return
	 */
	Boolean deleteMyGoods(Long id, List<Long> ids);
	
}
