package com.mirror.tk.core.module.sys.dto;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL) 
public class DictionariesDto implements Serializable{

	private static final long serialVersionUID = 1L;

	/** id */
	private Long id;
	
	/** 父id */
	private Long parentId;
	
	/** 键 */
	private String dicKey;
	
	/** 值 */
	private String dicValue;
	
	/** 类型 */
	private Integer type;
	
	/** 描述 */
	private String description;
	
	/** 下级字典 */
	private List<DictionariesDto> subList;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<DictionariesDto> getSubList() {
		return subList;
	}

	public void setSubList(List<DictionariesDto> subList) {
		this.subList = subList;
	}
	
	public String getDicKey() {
		return dicKey;
	}

	public void setDicKey(String dicKey) {
		this.dicKey = dicKey;
	}

	public String getDicValue() {
		return dicValue;
	}

	public void setDicValue(String dicValue) {
		this.dicValue = dicValue;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
	
}
