package com.mirror.tk.core.sms.work;

import java.io.IOException;

import com.mirror.tk.core.sms.exception.SMSException;

public interface SmsSendService {

	/**
	 * 发送短信
	 * @param phone
	 * @param msg
	 * @throws SMSException
	 * @throws IOException
	 */
	void send(String phone, String msg) throws SMSException, IOException;

}
