package com.mirror.tk.core.module.cms.domain;

import java.util.Date;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.google.common.collect.Maps;
import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 广告设置表 Entity
 *
 * Date: 2017-02-25 10:24:51
 *
 * @author Code Generator
 */
@Entity
@Table(name = "advert_config")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class AdvertConfig extends AbstractEntity {
	/** id */
	private Long id;
	
	/** 标题 */
	private String title;
	
	/** 图片地址 */
	private String imageUrl;
	
	/** 跳转连接 */
	private String url;
	
	/** 类型: */
	private Integer type;
	
	/** 状态: */
	private Integer status;
	
	/** 排序 */
	private Integer sortNo;
	
	/** 创建时间 */
	private Date createTime;
	
	/** 停用 */
	public static final Integer STATUS_DISABLE = 0;
	/** 启用 */
	public static final Integer STATUS_ENABLE = 1;
		
	/** 状态 */
	public static Map<Integer, String> allStatuss = Maps.newTreeMap();
	static {
		allStatuss.put(STATUS_DISABLE, "停用");
		allStatuss.put(STATUS_ENABLE, "启用");
	}
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getTitle(){
		return this.title;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getImageUrl(){
		return this.imageUrl;
	}

	public void setImageUrl(String imageUrl){
		this.imageUrl = imageUrl;
	}

	public String getUrl(){
		return this.url;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public Integer getType(){
		return this.type;
	}

	public void setType(Integer type){
		this.type = type;
	}

	public Integer getStatus(){
		return this.status;
	}

	public void setStatus(Integer status){
		this.status = status;
	}

	public Integer getSortNo(){
		return this.sortNo;
	}

	public void setSortNo(Integer sortNo){
		this.sortNo = sortNo;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
