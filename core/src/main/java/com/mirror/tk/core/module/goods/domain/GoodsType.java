package com.mirror.tk.core.module.goods.domain;


import java.util.Date;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.google.common.collect.Maps;
import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 商品分类信息表 Entity
 *
 * Date: 2016-11-01 23:10:13
 *
 * @author Acooly Code Generator
 */
@Entity
@Table(name = "goods_type")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class GoodsType extends AbstractEntity {
	/** ID */
	private Long id;
	
	/** 类型名称 */
	private String name;
	
	/** 类型描述 */
	private String description;
	
	/** 状态 */
	private Integer status;
	
	/** 创建时间 */
	private Date createTime;
	
	/** 修改时间 */
	private Date updateTime;
	
	/** 禁用 */
	public static final Integer STATUS_DISABLE = 0;
	
	/** 启用 */
	public static final Integer STATUS_ENABLE = 1;
		
	/** 状态 */
	public static Map<Integer, String> allStatuss = Maps.newTreeMap();
	static {
		allStatuss.put(0, "禁用");
		allStatuss.put(1, "启用");
	}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getName(){
		return this.name;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getDescription(){
		return this.description;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public Integer getStatus(){
		return this.status;
	}

	public void setStatus(Integer status){
		this.status = status;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
