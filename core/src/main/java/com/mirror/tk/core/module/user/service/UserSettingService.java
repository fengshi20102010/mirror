package com.mirror.tk.core.module.user.service;

import com.mirror.tk.core.module.user.domain.UserSetting;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 用户程序设置表 Service
 *
 * Date: 2016-12-14 22:57:08
 *
 * @author Code Generator
 *
 */
public interface UserSettingService extends EntityService<UserSetting> {

	/**
	 * 通过用户id获取程序设置
	 * @param userId
	 * @return
	 */
	public UserSetting getByUserId(Long userId);

}
