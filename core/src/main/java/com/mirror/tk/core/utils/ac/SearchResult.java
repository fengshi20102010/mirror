package com.mirror.tk.core.utils.ac;

import java.util.Set;

public class SearchResult {

	State lastMatchedState;
	byte[] bytes;
	int lastIndex;

	SearchResult(State s, byte[] bs, int i) {
		this.lastMatchedState = s;
		this.bytes = bs;
		this.lastIndex = i;
	}

	public Set<Object> getOutputs() {
		return lastMatchedState.getOutputs();
	}

	public int getLastIndex() {
		return lastIndex;
	}

}
