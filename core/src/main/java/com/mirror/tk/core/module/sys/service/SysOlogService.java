package com.mirror.tk.core.module.sys.service;

import com.mirror.tk.core.module.sys.domain.SysOlog;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 系统_操作日志表 Service
 *
 * Date: 2016-01-08 10:01:30
 *
 * @author Code Generator
 *
 */
public interface SysOlogService extends EntityService<SysOlog> {

}
