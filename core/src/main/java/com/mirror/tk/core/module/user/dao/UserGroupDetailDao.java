package com.mirror.tk.core.module.user.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mirror.tk.core.module.user.domain.UserGroupDetail;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 用户QQ/微信群详情表 JPA Dao
 *
 * Date: 2017-01-01 22:30:33
 *
 * @author Code Generator
 *
 */
public interface UserGroupDetailDao extends EntityJpaDao<UserGroupDetail, Long> {

	@Modifying
	@Query("delete from UserGroupDetail where groupId = ?1")
	public int deleteByGroupId(Long groupId);

	@Modifying
	@Query("delete from UserGroupDetail where groupId = ?1 and id in(?2)")
	public int deleteByGroupIdAndIds(Long groupId, List<Long> list);

}
