package com.mirror.tk.core.module.sys.domain;


import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.OrderBy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 系统角色表 Entity
 *
 * Date: 2015-05-12 17:30:34
 *
 * @author Acooly Code Generator
 */
@Entity
@Table(name = "sys_role")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SysRole extends AbstractEntity {
	
	/** id */
	private Long id;
	
	/** 角色名称 */
	private String roleName;
	
	/** 角色描述 */
	private String description;
	
	/** 包含的用户 */
	@JsonIgnore
	private Set<SysUser> users;
	
	/** 包含的资源 */
	private Set<SysResource> rescs;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}
	
	public void setId(Long id){
		this.id = id;
	}
	public String getRoleName(){
		return this.roleName;
	}
	
	public void setRoleName(String roleName){
		this.roleName = roleName;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(mappedBy = "role", targetEntity = SysUser.class, cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@LazyCollection(value = LazyCollectionOption.EXTRA)
	public Set<SysUser> getUsers() {
		return users;
	}

	public void setUsers(Set<SysUser> users) {
		this.users = users;
	}

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER, targetEntity = SysResource.class)
	@JoinTable(name = "sys_role_resc", joinColumns = { @JoinColumn(name = "role_id") }, inverseJoinColumns = @JoinColumn(name = "resc_id"))
	@OrderBy(clause = "resc_id")
	@LazyCollection(value = LazyCollectionOption.EXTRA)
	public Set<SysResource> getRescs() {
		return rescs;
	}

	public void setRescs(Set<SysResource> rescs) {
		this.rescs = rescs;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
