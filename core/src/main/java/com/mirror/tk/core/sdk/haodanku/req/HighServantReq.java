package com.mirror.tk.core.sdk.haodanku.req;

import java.io.Serializable;

/**
 * @title 高级佣金
 */
public class HighServantReq implements Serializable {

	private static final long serialVersionUID = 1L;

	// 放单后台获取的Apikey值（*必要）
	private String apikey;
	// 宝贝ID（*必要）
	private Long itemid;
	// 推广位ID（*必要 需是授权淘宝号下的推广位）
	private String pid;
	// 阿里妈妈推广券ID （选填）
	private String activityid;
	// 营销计划（选填）
	private String me;
	// 授权后的淘宝名称（选填，多授权淘宝号的就需加改参数）
	private String tbName;

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public Long getItemid() {
		return itemid;
	}

	public void setItemid(Long itemid) {
		this.itemid = itemid;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getActivityid() {
		return activityid;
	}

	public void setActivityid(String activityid) {
		this.activityid = activityid;
	}

	public String getMe() {
		return me;
	}

	public void setMe(String me) {
		this.me = me;
	}

	public String getTbName() {
		return tbName;
	}

	public void setTbName(String tbName) {
		this.tbName = tbName;
	}

	@Override
	public String toString() {
		return "HighServantReq [apikey=" + apikey + ", itemid=" + itemid + ", pid=" + pid + ", activityid=" + activityid
				+ ", me=" + me + ", tbName=" + tbName + "]";
	}

}
