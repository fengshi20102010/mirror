package com.mirror.tk.core.module.cms.dao;

import com.mirror.tk.core.module.cms.domain.AdvertConfig;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 广告设置表 JPA Dao
 *
 * Date: 2017-02-25 10:24:51
 *
 * @author Code Generator
 *
 */
public interface AdvertConfigDao extends EntityJpaDao<AdvertConfig, Long> {

}
