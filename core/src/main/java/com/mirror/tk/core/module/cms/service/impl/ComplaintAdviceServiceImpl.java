package com.mirror.tk.core.module.cms.service.impl;


import org.springframework.stereotype.Service;

import com.mirror.tk.core.module.cms.dao.ComplaintAdviceDao;
import com.mirror.tk.core.module.cms.domain.ComplaintAdvice;
import com.mirror.tk.core.module.cms.service.ComplaintAdviceService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("complaintAdviceService")
public class ComplaintAdviceServiceImpl extends EntityServiceImpl<ComplaintAdvice, ComplaintAdviceDao> implements ComplaintAdviceService {

}
