package com.mirror.tk.core.module.user.domain;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 用户淘宝信息关联表 Entity
 *
 * Date: 2016-11-01 23:08:37
 *
 * @author Acooly Code Generator
 */
@Entity
@Table(name = "user_pid")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserPid extends AbstractEntity {
	/** ID */
	private Long id;
	
	/** 用户ID */
	private Long userId;
	
	/** 分组名称 */
	private String name;
	
	/** 通用pid */
	private String commonPid;
	
	/** 鹊桥PID */
	private String queqiaoPid;
	
	/** 模板 */
	private String tpl;
	
	/** 创建时间 */
	private Date createTime;
	
	/** 修改时间 */
	private Date updateTime;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public String getName(){
		return this.name;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getCommonPid() {
		return commonPid;
	}

	public void setCommonPid(String commonPid) {
		this.commonPid = commonPid;
	}

	public String getQueqiaoPid(){
		return this.queqiaoPid;
	}

	public void setQueqiaoPid(String queqiaoPid){
		this.queqiaoPid = queqiaoPid;
	}

	public String getTpl(){
		return this.tpl;
	}

	public void setTpl(String tpl){
		this.tpl = tpl;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}

	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
