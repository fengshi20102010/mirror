package com.mirror.tk.core.sms.service;

import java.io.IOException;

import com.mirror.tk.core.sms.exception.SMSException;

public interface SendTemplateSmsService {

	public enum TEMPLATE_CODE {

		// 注册
		register(1, "verify_code_tpl"),
		// 找回密码
		retakePwd(2, "verify_code_tpl");
		
		private Integer code;
		private String key;

		private TEMPLATE_CODE(Integer code, String key) {
			this.code = code;
			this.key = key;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}
		
		public Integer getCode() {
			return code;
		}

		public void setCode(Integer code) {
			this.code = code;
		}

		public static TEMPLATE_CODE getCode(Integer type){
			
			TEMPLATE_CODE code;
			switch (type) {
			case 1:
				code = TEMPLATE_CODE.register;
				break;
			case 2:
				code = TEMPLATE_CODE.retakePwd;
				break;

			default:
				code = TEMPLATE_CODE.register;
				break;
			}
			return code;
		}
		
	}

	/**
	 * 发送模板短信
	 * @param phone
	 * @param templateCode
	 * @param param
	 * @throws IOException
	 * @throws SMSException
	 */
	void send(String phone, TEMPLATE_CODE templateCode, String... param) throws IOException, SMSException;
}
