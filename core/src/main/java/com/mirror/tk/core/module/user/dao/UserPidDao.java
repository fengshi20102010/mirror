package com.mirror.tk.core.module.user.dao;

import org.springframework.data.jpa.repository.Query;

import com.mirror.tk.core.module.user.domain.UserPid;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 用户淘宝信息关联表 JPA Dao
 *
 * Date: 2016-11-01 23:08:37
 *
 * @author Code Generator
 *
 */
public interface UserPidDao extends EntityJpaDao<UserPid, Long> {

	@Query("from UserPid where id = ?1 and userId = ?2")
	public UserPid getByUserIdAndId(Long id, Long userId);

}
