package com.mirror.tk.core.module.goods.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mirror.tk.core.module.goods.domain.GoodsCoupon;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 商品优惠劵表 JPA Dao
 *
 * Date: 2016-11-01 23:10:16
 *
 * @author Code Generator
 *
 */
public interface GoodsCouponDao extends EntityJpaDao<GoodsCoupon, Long> {

	@Query("select a from GoodsCoupon a, Goods b where a.goodsId = b.id and b.status <= 3 and b.status >= 1")
	List<GoodsCoupon> findByNormal();

	@Modifying
	@Query("delete from GoodsCoupon where goodsId in (?1) ")
	int deleteByGoodsIds(List<Long> goodsIds);
	
}
