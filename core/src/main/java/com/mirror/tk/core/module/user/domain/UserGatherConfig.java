package com.mirror.tk.core.module.user.domain;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 用户采集群配置表 Entity
 *
 * Date: 2016-12-28 23:56:35
 *
 * @author Code Generator
 */
@Entity
@Table(name = "user_gather_config")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserGatherConfig extends AbstractEntity {
	/** id */
	private Long id;
	
	/** 用户ID */
	private Long userId;
	
	/** 0:否,1:是 */
	private Integer gather2send;
	
	/** 0:否,1:是 */
	private Integer gather2add;
	
	/** 0:否,1:是 */
	private Integer display;
	
	/** 0:否,1:是 */
	private Integer notPic;
	
	/** send_group */
	private String sendGroup;
	
	/** gather_group */
	private String gatherGroup;
	
	/** live_group */
	private String liveGroup;
	
	/** not_keywords */
	private String notKeywords;
	
	/** 模板ID */
	private Long tplId;
	
	/** 创建时间 */
	private Date createTime;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Integer getGather2send(){
		return this.gather2send;
	}

	public void setGather2send(Integer gather2send){
		this.gather2send = gather2send;
	}

	public Integer getGather2add(){
		return this.gather2add;
	}

	public void setGather2add(Integer gather2add){
		this.gather2add = gather2add;
	}

	public Integer getDisplay(){
		return this.display;
	}

	public void setDisplay(Integer display){
		this.display = display;
	}

	public Integer getNotPic(){
		return this.notPic;
	}

	public void setNotPic(Integer notPic){
		this.notPic = notPic;
	}

	public String getSendGroup(){
		return this.sendGroup;
	}

	public void setSendGroup(String sendGroup){
		this.sendGroup = sendGroup;
	}

	public String getGatherGroup(){
		return this.gatherGroup;
	}

	public void setGatherGroup(String gatherGroup){
		this.gatherGroup = gatherGroup;
	}

	public String getLiveGroup(){
		return this.liveGroup;
	}

	public void setLiveGroup(String liveGroup){
		this.liveGroup = liveGroup;
	}

	public String getNotKeywords(){
		return this.notKeywords;
	}

	public void setNotKeywords(String notKeywords){
		this.notKeywords = notKeywords;
	}

	public Long getTplId(){
		return this.tplId;
	}

	public void setTplId(Long tplId){
		this.tplId = tplId;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
