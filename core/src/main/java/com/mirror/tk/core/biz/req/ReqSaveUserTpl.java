package com.mirror.tk.core.biz.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class ReqSaveUserTpl {

	@ApiModelProperty(value="",required=false)
	private Long userId;
	
	@ApiModelProperty(value="用户PID分组ID",required=true)
	private Long group;
	
	@ApiModelProperty(value="",required=true)
	private String content;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getGroup() {
		return group;
	}

	public void setGroup(Long group) {
		this.group = group;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
