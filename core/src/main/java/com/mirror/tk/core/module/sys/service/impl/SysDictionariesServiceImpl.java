package com.mirror.tk.core.module.sys.service.impl;


import org.springframework.stereotype.Service;

import com.mirror.tk.core.module.sys.dao.SysDictionariesDao;
import com.mirror.tk.core.module.sys.domain.SysDictionaries;
import com.mirror.tk.core.module.sys.service.SysDictionariesService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("sysDictionariesService")
public class SysDictionariesServiceImpl extends EntityServiceImpl<SysDictionaries, SysDictionariesDao> implements SysDictionariesService {

	@Override
	public SysDictionaries findByKey(String key) {
		return this.getEntityDao().findByKey(key);
	}
	
}
