package com.mirror.tk.core.module.cms.domain;


import java.util.Date;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.google.common.collect.Maps;
import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 投诉建议 Entity
 *
 * Date: 2016-11-01 23:10:35
 *
 * @author Acooly Code Generator
 */
@Entity
@Table(name = "complaint_advice")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ComplaintAdvice extends AbstractEntity {
	/** ID */
	private Long id;
	
	/** 投诉/建议标题 */
	private String title;
	
	/** 投诉/建议内容 */
	private String content;
	
	/** 姓名 */
	private String name;
	
	/** 电话 */
	private String phone;
	
	/** 邮箱 */
	private String email;
	
	/** 状态 */
	private Integer status;
	
	/** 类型 */
	private Integer type;
	
	/** 创建时间 */
	private Date createTime;
	
	/** 修改时间 */
	private Date updateTime;
	
	/** 冻结 */
	public static final Integer STATUS_FROZEN = 0;
	/** 待审核 */
	public static final Integer STATUS_CHECK_WAIT = 1;
	/** 正常 */
	public static final Integer STATUS_CHECKED = 2;
		
	/** 状态 */
	public static Map<Integer, String> allStatuss = Maps.newTreeMap();
	static {
		allStatuss.put(STATUS_FROZEN, "冻结");
		allStatuss.put(STATUS_CHECK_WAIT, "待审核");
		allStatuss.put(STATUS_CHECKED, "正常");
	}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getTitle(){
		return this.title;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getContent(){
		return this.content;
	}

	public void setContent(String content){
		this.content = content;
	}

	public String getName(){
		return this.name;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getPhone(){
		return this.phone;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getEmail(){
		return this.email;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public Integer getStatus(){
		return this.status;
	}

	public void setStatus(Integer status){
		this.status = status;
	}

	public Integer getType(){
		return this.type;
	}

	public void setType(Integer type){
		this.type = type;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}

	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
