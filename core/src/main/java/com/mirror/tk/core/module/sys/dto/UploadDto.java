package com.mirror.tk.core.module.sys.dto;

import java.io.Serializable;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.google.common.collect.Maps;

public class UploadDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	// 输出文件地址
	private String url = "";
	// 上传文件名
	private String fileName = "";
	// 状态
	private String state = "";
	// 文件类型
	private String type = "";
	// 原始文件名
	private String originalName = "";
	// 文件大小
	private long size = 0;
	// title
	private String title = "";
	// 保存路径
	private String savePath = "upload";
	// 文件允许格式
	private String[] allowFiles = { ".rar", ".doc", ".docx", ".zip", ".pdf",".txt", ".swf", ".wmv", ".gif", ".png", ".jpg", ".jpeg", ".bmp" };
	// 文件大小限制，单位KB
	private int maxSize = 10000;
	
	public static Map<Integer, String> allState = Maps.newHashMap();
	static {
		allState.put(1, "SUCCESS"); //默认成功
		allState.put(2, "未包含文件上传域");
		allState.put(3, "不允许的文件格式");
		allState.put(4, "文件大小超出限制");
		allState.put(5, "请求类型ENTYPE错误");
		allState.put(6, "上传请求异常");
		allState.put(7, "IO异常");
		allState.put(8, "目录创建失败");
		allState.put(9, "未知错误");
		allState.put(10, "未登录或登陆过期");
	}
	
	public static final int STATE_SUCCESS = 1;
	public static final int STATE_NOFILE = 2;
	public static final int STATE_TYPE = 3;
	public static final int STATE_SIZE = 4;
	public static final int STATE_ENTYPE = 5;
	public static final int STATE_REQUEST = 6;
	public static final int STATE_IO = 7;
	public static final int STATE_DIR = 8;
	public static final int STATE_UNKNOWN = 9;
	public static final int STATE_UNAUTHORIZED = 10;
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOriginalName() {
		return originalName;
	}
	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSavePath() {
		return savePath;
	}
	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}
	public String[] getAllowFiles() {
		return allowFiles;
	}
	public void setAllowFiles(String[] allowFiles) {
		this.allowFiles = allowFiles;
	}
	public int getMaxSize() {
		return maxSize;
	}
	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
	
}
