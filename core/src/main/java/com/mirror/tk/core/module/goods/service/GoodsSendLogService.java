package com.mirror.tk.core.module.goods.service;

import java.util.Date;

import com.mirror.tk.core.module.goods.domain.GoodsSendLog;
import com.mirror.tk.framework.common.dao.support.PageInfo;
import com.mirror.tk.framework.common.service.EntityService;

public interface GoodsSendLogService  extends EntityService<GoodsSendLog>{

	public GoodsSendLog queryLog(Long numIid,Long userId);
	
	public PageInfo<GoodsSendLog> getPagedSendLog(PageInfo<GoodsSendLog> page,Long userId,Date startTime,Date endTime,String title);
}
