package com.mirror.tk.core.module.sys.service.impl;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.mirror.tk.core.module.sys.cache.SecurityCache;
import com.mirror.tk.core.module.sys.dao.SysRoleDao;
import com.mirror.tk.core.module.sys.domain.SysRole;
import com.mirror.tk.core.module.sys.service.SysRoleService;
import com.mirror.tk.framework.common.exception.BusinessException;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("sysRoleService")
public class SysRoleServiceImpl extends EntityServiceImpl<SysRole, SysRoleDao> implements SysRoleService {

    @Resource
    private SecurityCache securityCache;

    @Override
    public void update(SysRole o) throws BusinessException {
        super.update(o);
        securityCache.clearAllAuthorization();
        securityCache.clearMenu(o.getId());
    }
    
}

