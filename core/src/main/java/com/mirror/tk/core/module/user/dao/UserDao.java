package com.mirror.tk.core.module.user.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mirror.tk.core.module.user.domain.User;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 用户信息主表 JPA Dao
 *
 * Date: 2016-11-01 23:09:01
 *
 * @author Code Generator
 *
 */
public interface UserDao extends EntityJpaDao<User, Long> {

	@Query("from User where phone = ?1 and status != 0")
	public User findByPhone(String phone);

	@Modifying
	@Query("update User set status = ?2 where id = ?1 ")
	public int updateByStatus(Long id, Integer status);

}
