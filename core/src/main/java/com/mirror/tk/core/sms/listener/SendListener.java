package com.mirror.tk.core.sms.listener;

public interface SendListener {

	void handler(String phone, String msg, String address, String params, Exception e);

}
