package com.mirror.tk.core.module.sys.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.mirror.tk.core.module.sys.dao.SysPropertyDao;
import com.mirror.tk.core.module.sys.domain.SysProperty;
import com.mirror.tk.core.module.sys.service.SysPropertyService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("sysPropertyService")
public class SysPropertyServiceImpl extends EntityServiceImpl<SysProperty, SysPropertyDao> implements SysPropertyService {

	// 查询数据字典数据 @author lzj 2016年3月1日9:45:04
	public List<SysProperty> findSysProperty(String pro_key) {
		return this.getEntityDao().findSysProperty(pro_key);
	}

	@Override
	public SysProperty findByProKey(String proKey) {
		return this.getEntityDao().findByProKey(proKey);
	}
	
}
