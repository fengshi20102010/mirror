package com.mirror.tk.core.module.user.service;

import java.util.List;

import com.mirror.tk.core.module.user.domain.UserPid;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 用户淘宝信息关联表 Service
 *
 * Date: 2016-11-01 23:08:37
 *
 * @author Code Generator
 *
 */
public interface UserPidService extends EntityService<UserPid> {

	/**
	 * 获取单个pid，加入userId以增加安全性
	 * @param id
	 * @param userId
	 * @return
	 */
	public UserPid getByUserIdAndId(Long id, Long userId);
	
	/**
	 * 获取用户的PID
	 * @param userId
	 * @return
	 */
	public List<UserPid> getByUserId(Long userId);
}
