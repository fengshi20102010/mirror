package com.mirror.tk.core.module.sys.domain;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 系统_附件表 Entity
 *
 * Date: 2016-01-08 09:54:33
 *
 * @author Acooly Code Generator
 */
@Entity
@Table(name = "sys_attachment")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SysAttachment extends AbstractEntity {
	/** id */
	private Long id;
	
	/** 用户id */
	private Long userId;
	
	/** 源文件名 */
	private String sourceName;
	
	/** 文件名 */
	private String fileName;
	
	/** 文件类型 */
	private String fileType;
	
	/** 磁盘路径 */
	private String filePath;
	
	/** 文件大小 */
	private Long fileSize;
	
	/** 附件地址 */
	private String attachUrl;
	
	/** 关联模块 */
	private String relationKey;
	
	/** 创建时间 */
	private Date createTime;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public String getSourceName(){
		return this.sourceName;
	}

	public void setSourceName(String sourceName){
		this.sourceName = sourceName;
	}

	public String getFileName(){
		return this.fileName;
	}

	public void setFileName(String fileName){
		this.fileName = fileName;
	}

	public String getFileType(){
		return this.fileType;
	}

	public void setFileType(String fileType){
		this.fileType = fileType;
	}

	public String getFilePath(){
		return this.filePath;
	}

	public void setFilePath(String filePath){
		this.filePath = filePath;
	}

	public Long getFileSize(){
		return this.fileSize;
	}

	public void setFileSize(Long fileSize){
		this.fileSize = fileSize;
	}

	public String getAttachUrl(){
		return this.attachUrl;
	}

	public void setAttachUrl(String attachUrl){
		this.attachUrl = attachUrl;
	}

	public String getRelationKey(){
		return this.relationKey;
	}

	public void setRelationKey(String relationKey){
		this.relationKey = relationKey;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
