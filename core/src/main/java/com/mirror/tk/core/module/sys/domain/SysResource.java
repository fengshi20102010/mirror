package com.mirror.tk.core.module.sys.domain;


import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 系统资源表 Entity
 *
 * Date: 2015-06-01 13:59:43
 *
 * @author Acooly Code Generator
 */
@Entity
@Table(name = "sys_resource")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SysResource extends AbstractEntity {

    /** id */
	private Long id;
	
	/** 父id */
	private Long parentId;
	
	/** 资源名称 */
	private String title;
	
	/** 资源类型 */
	private String resType;
	
	/** 资源值 */
	private String resKey;
	
	/** 权限值 */
	private String permissionValue;
	
	/** 描述 */
	private String description;
	
	/** 状态 */
	private Integer status;
	
	/** 排序号 */
	private Long sortNo;
	
	/** 创建时间 */
	private Date createTime;
	
	/** 子资源 */
    private Set<SysResource> subResource;
	
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
    	return id;
    }
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getResType() {
		return resType;
	}

	public void setResType(String resType) {
		this.resType = resType;
	}

	public String getResKey() {
		return resKey;
	}

	public void setResKey(String resKey) {
		this.resKey = resKey;
	}

	public String getPermissionValue() {
		return permissionValue;
	}

	public void setPermissionValue(String permissionValue) {
		this.permissionValue = permissionValue;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getSortNo() {
		return sortNo;
	}

	public void setSortNo(Long sortNo) {
		this.sortNo = sortNo;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "parentId")
    @OrderBy("sortNo ASC")
    public Set<SysResource> getSubResource() {
        return subResource;
    }
	
	public void setSubResource(Set<SysResource> subResource) {
        this.subResource = subResource;
    }

    @Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
