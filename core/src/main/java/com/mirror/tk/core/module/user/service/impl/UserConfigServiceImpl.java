package com.mirror.tk.core.module.user.service.impl;


import org.springframework.stereotype.Service;

import com.mirror.tk.core.module.user.dao.UserConfigDao;
import com.mirror.tk.core.module.user.domain.UserConfig;
import com.mirror.tk.core.module.user.service.UserConfigService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("userConfigService")
public class UserConfigServiceImpl extends EntityServiceImpl<UserConfig, UserConfigDao> implements UserConfigService {

	@Override
	public UserConfig getByUserId(Long userId) {
		return this.getEntityDao().getByUserId(userId);
	}

}
