package com.mirror.tk.core.module.goods.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.mirror.tk.core.module.goods.dao.GoodsImgDao;
import com.mirror.tk.core.module.goods.domain.GoodsImg;
import com.mirror.tk.core.module.goods.service.GoodsImgService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("goodsImgService")
public class GoodsImgServiceImpl extends EntityServiceImpl<GoodsImg,GoodsImgDao> implements GoodsImgService{

	@Override
	public Boolean deleteMyGoodsImg(List<Long> ids) {
		return this.getEntityDao().deleteByNumIid(ids) > 0;
	}

}
