package com.mirror.tk.core.biz.dto;

public class ResTaobaoCoupon {

	/**
	 * 券ID
	 */
	private String couponActivityId;
	/**
	 * 优惠券金额
	 */
	private String couponAmount;
	/**
	 * 优惠券结束时间
	 */
	private String couponEndTime;
	/**
	 * 优惠券剩余量
	 */
	private Long couponRemainCount;
	/**
	 * 券类型，1 表示全网公开券，4 表示妈妈渠道券
	 */
	private Long couponSrcScene;
	/**
	 * 优惠券门槛金额
	 */
	private String couponStartFee;
	/**
	 * 优惠券开始时间
	 */
	private String couponStartTime;
	/**
	 * 优惠券总量
	 */
	private Long couponTotalCount;
	/**
	 * 券属性，0表示店铺券，1表示单品券
	 */
	private Long couponType;

	public String getCouponActivityId() {
		return couponActivityId;
	}

	public void setCouponActivityId(String couponActivityId) {
		this.couponActivityId = couponActivityId;
	}

	public String getCouponAmount() {
		return couponAmount;
	}

	public void setCouponAmount(String couponAmount) {
		this.couponAmount = couponAmount;
	}

	public String getCouponEndTime() {
		return couponEndTime;
	}

	public void setCouponEndTime(String couponEndTime) {
		this.couponEndTime = couponEndTime;
	}

	public Long getCouponRemainCount() {
		return couponRemainCount;
	}

	public void setCouponRemainCount(Long couponRemainCount) {
		this.couponRemainCount = couponRemainCount;
	}

	public Long getCouponSrcScene() {
		return couponSrcScene;
	}

	public void setCouponSrcScene(Long couponSrcScene) {
		this.couponSrcScene = couponSrcScene;
	}

	public String getCouponStartFee() {
		return couponStartFee;
	}

	public void setCouponStartFee(String couponStartFee) {
		this.couponStartFee = couponStartFee;
	}

	public String getCouponStartTime() {
		return couponStartTime;
	}

	public void setCouponStartTime(String couponStartTime) {
		this.couponStartTime = couponStartTime;
	}

	public Long getCouponTotalCount() {
		return couponTotalCount;
	}

	public void setCouponTotalCount(Long couponTotalCount) {
		this.couponTotalCount = couponTotalCount;
	}

	public Long getCouponType() {
		return couponType;
	}

	public void setCouponType(Long couponType) {
		this.couponType = couponType;
	}

	@Override
	public String toString() {
		return "ResTaobaoCoupon [couponActivityId=" + couponActivityId + ", couponAmount=" + couponAmount
				+ ", couponEndTime=" + couponEndTime + ", couponRemainCount=" + couponRemainCount + ", couponSrcScene="
				+ couponSrcScene + ", couponStartFee=" + couponStartFee + ", couponStartTime=" + couponStartTime
				+ ", couponTotalCount=" + couponTotalCount + ", couponType=" + couponType + "]";
	}

}
