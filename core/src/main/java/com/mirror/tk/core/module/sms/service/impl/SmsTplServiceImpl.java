package com.mirror.tk.core.module.sms.service.impl;


import org.springframework.stereotype.Service;

import com.mirror.tk.core.module.sms.dao.SmsTplDao;
import com.mirror.tk.core.module.sms.domain.SmsTpl;
import com.mirror.tk.core.module.sms.service.SmsTplService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("smsTplService")
public class SmsTplServiceImpl extends EntityServiceImpl<SmsTpl, SmsTplDao> implements SmsTplService {

	@Override
	public SmsTpl getByName(String code) {
		return this.getEntityDao().getByName(code);
	}

}
