package com.mirror.tk.core.biz.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class ReqTransformUrl {

	@ApiModelProperty(value="用户ID",required=false)
	private Long userId;
	
	@ApiModelProperty(value="用户PID分组ID",required=true)
	private Long gorup;
	
	@ApiModelProperty(value="淘宝商品链接",required=true)
	private String url;
	
	@ApiModelProperty(value="优惠券链接",required=true)
	private String couponUrl;

	public Long getUserId() {
		return userId;
	}

	public String getCouponUrl() {
		return couponUrl;
	}

	public void setCouponUrl(String couponUrl) {
		this.couponUrl = couponUrl;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUrl() {
		return url;
	}

	public Long getGorup() {
		return gorup;
	}

	public void setGorup(Long gorup) {
		this.gorup = gorup;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
