package com.mirror.tk.core.module.user.dao;

import com.mirror.tk.core.module.user.domain.UserSetting;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 用户程序设置表 JPA Dao
 *
 * Date: 2016-12-14 22:57:08
 *
 * @author Code Generator
 *
 */
public interface UserSettingDao extends EntityJpaDao<UserSetting, Long> {

}
