package com.mirror.tk.core.module.goods.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.mirror.tk.core.biz.dto.GoodsTypeDto;
import com.mirror.tk.core.module.goods.dao.GoodsCustomDao;
import com.mirror.tk.core.module.goods.dao.GoodsTypeDao;
import com.mirror.tk.core.module.goods.domain.GoodsType;
import com.mirror.tk.core.module.goods.service.GoodsTypeService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("goodsTypeService")
public class GoodsTypeServiceImpl extends EntityServiceImpl<GoodsType, GoodsTypeDao> implements GoodsTypeService {

	@Resource
	private GoodsCustomDao goodsCustomDao;
	
	@Override
	public List<GoodsType> getAllGoodsType() {
		return this.getEntityDao().findByStatus(GoodsType.STATUS_ENABLE);
	}
	
	@Override
	public List<GoodsType> getGoodsTypeByStatus(Integer status) {
		return this.getEntityDao().findByStatus(status);
	}
	
	@Override
	public List<GoodsTypeDto> getNormalType() {
		return goodsCustomDao.getNormalType();
	}

}
