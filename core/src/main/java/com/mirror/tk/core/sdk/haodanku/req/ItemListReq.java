package com.mirror.tk.core.sdk.haodanku.req;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @title 商品列表
 */
public class ItemListReq implements Serializable {

	private static final long serialVersionUID = 1L;

	// 放单后台获取的Apikey值
	private String apikey;
	// 默认全部商品（1实时跑单商品，2爆单榜商品，3全部商品，4纯视频单，5聚淘专区）
	private Integer nav = 0;
	// 商品类目
	// 0全部，1女装，2男装，3内衣，4美妆，5配饰，6鞋品，7箱包，8儿童，9母婴，10居家，11美食，12数码，13家电，14其他，15车品，16文体（支持多类目筛选，如1,2获取类目为女装、男装的商品，逗号仅限英文逗号）
	private Integer cid = 0;
	// 每页返回条数（请在1,2,10,20,50,100,120,200,500,1000中选择一个数值返回）
	private Integer back = 100;
	// 分页，用于实现类似分页抓取效果，来源于上次获取后的数据的min_id值，默认开始请求值为1（该方案比单纯123分页的优势在于：数据更新的情况下保证不会重复也无需关注和计算页数）
	@JsonProperty("min_id")
	private Integer minId = 1;
	// 0.综合（最新），1.券后价(低到高)，2.券后价（高到低），3.券面额（高到低），4.月销量（高到低），5.佣金比例（高到低），6.券面额（低到高），7.月销量（低到高），8.佣金比例（低到高），9.全天销量（高到低），10全天销量（低到高），11.近2小时销量（高到低），12.近2小时销量（低到高）注意：该排序仅对nav=3，4，5有效，1，2无效
	private Integer sort;
	// 券后价筛选，筛选大于等于所设置的券后价的商品
	@JsonProperty("price_min")
	private Integer priceMin;
	// 券后价筛选，筛选小于等于所设置的券后价的商品
	@JsonProperty("price_max")
	private Integer priceMax;
	// 销量筛选，筛选大于等于所设置的销量的商品
	@JsonProperty("sale_min")
	private Integer saleMin;
	// 销量筛选，筛选小于等于所设置的销量的商品
	@JsonProperty("sale_max")
	private Integer saleMax;
	// 券金额筛选，筛选大于等于所设置的券金额的商品
	@JsonProperty("coupon_min")
	private Integer couponMin;
	// 券金额筛选，筛选小于等于所设置的券金额的商品
	@JsonProperty("coupon_max")
	private Integer couponMax;
	// 佣金比例筛选，筛选大于等于所设置的佣金比例的商品
	@JsonProperty("tkrates_min")
	private Integer tkratesMin;
	// 佣金筛选，筛选大于等于所设置的佣金的商品
	@JsonProperty("tkmoney_min")
	private Integer tkmoneyMin;

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public Integer getNav() {
		return nav;
	}

	public void setNav(Integer nav) {
		this.nav = nav;
	}

	public Integer getCid() {
		return cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	public Integer getBack() {
		return back;
	}

	public void setBack(Integer back) {
		this.back = back;
	}

	public Integer getMinId() {
		return minId;
	}

	public void setMinId(Integer minId) {
		this.minId = minId;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Integer getPriceMin() {
		return priceMin;
	}

	public void setPriceMin(Integer priceMin) {
		this.priceMin = priceMin;
	}

	public Integer getPriceMax() {
		return priceMax;
	}

	public void setPriceMax(Integer priceMax) {
		this.priceMax = priceMax;
	}

	public Integer getSaleMin() {
		return saleMin;
	}

	public void setSaleMin(Integer saleMin) {
		this.saleMin = saleMin;
	}

	public Integer getSaleMax() {
		return saleMax;
	}

	public void setSaleMax(Integer saleMax) {
		this.saleMax = saleMax;
	}

	public Integer getCouponMin() {
		return couponMin;
	}

	public void setCouponMin(Integer couponMin) {
		this.couponMin = couponMin;
	}

	public Integer getCouponMax() {
		return couponMax;
	}

	public void setCouponMax(Integer couponMax) {
		this.couponMax = couponMax;
	}

	public Integer getTkratesMin() {
		return tkratesMin;
	}

	public void setTkratesMin(Integer tkratesMin) {
		this.tkratesMin = tkratesMin;
	}

	public Integer getTkmoneyMin() {
		return tkmoneyMin;
	}

	public void setTkmoneyMin(Integer tkmoneyMin) {
		this.tkmoneyMin = tkmoneyMin;
	}

	@Override
	public String toString() {
		return "ItemListReq [apikey=" + apikey + ", nav=" + nav + ", cid=" + cid + ", back=" + back + ", minId=" + minId
				+ ", sort=" + sort + ", priceMin=" + priceMin + ", priceMax=" + priceMax + ", saleMin=" + saleMin
				+ ", saleMax=" + saleMax + ", couponMin=" + couponMin + ", couponMax=" + couponMax + ", tkratesMin="
				+ tkratesMin + ", tkmoneyMin=" + tkmoneyMin + "]";
	}
}
