package com.mirror.tk.core.module.goods.dao;

import com.mirror.tk.core.module.goods.domain.GoodsCloud;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 商品推送云库表 JPA Dao
 *
 * Date: 2016-11-01 23:10:19
 *
 * @author Code Generator
 *
 */
public interface GoodsCloudDao extends EntityJpaDao<GoodsCloud, Long> {

}
