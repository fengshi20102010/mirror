package com.mirror.tk.core.module.sms.service;

import com.mirror.tk.core.module.sms.domain.SmsTpl;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 短信模板记录表 Service
 *
 * Date: 2016-11-01 23:09:33
 *
 * @author Code Generator
 *
 */
public interface SmsTplService extends EntityService<SmsTpl> {

	public SmsTpl getByName(String code);

}
