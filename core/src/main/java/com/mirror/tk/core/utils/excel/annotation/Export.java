package com.mirror.tk.core.utils.excel.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Description:数据导出注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
@Documented
public @interface Export {

	/**
	 * 导出字段名
	 */
	public String name();

	/**
	 * 格式化字段输出，默认为时间：yyyy-MM-dd HH:mm:ss 
	 * 例如：日期格式化:yyyy-MM-dd、yyyy-MM-dd HH:mm:ss
	 * 数字格式化:#.00
	 */
	public String pattern() default "yyyy-MM-dd HH:mm:ss";

	/**
	 * 状态字段描述 json字符串格式
	 */
	public String json() default "";

	/**
	 * 导出列排序(升序排列)
	 */
	public int order();

}
