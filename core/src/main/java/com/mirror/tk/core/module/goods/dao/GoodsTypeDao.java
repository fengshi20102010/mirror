package com.mirror.tk.core.module.goods.dao;

import java.util.List;

import com.mirror.tk.core.module.goods.domain.GoodsType;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 商品分类信息表 JPA Dao
 *
 * Date: 2016-11-01 23:10:13
 *
 * @author Code Generator
 *
 */
public interface GoodsTypeDao extends EntityJpaDao<GoodsType, Long> {

	public List<GoodsType> findByStatus(Integer status);
}
