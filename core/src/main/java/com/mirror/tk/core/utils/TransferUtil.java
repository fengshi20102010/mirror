package com.mirror.tk.core.utils;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TransferUtil {
	
	private static Logger log = LoggerFactory.getLogger(TransferUtil.class);

	public static Integer longToint(Long l) {
		return l == null ? 0 : l.intValue();
	}
	
	public static Double strToDouble(String s) {
		return s == null ? 0 : Double.parseDouble(s);
	}
	
	public static String dateToStr(Date d) {
		return d == null ? "" : DateFormatUtils.format(d, "yyyy-MM-dd");
	}
	
	public static String dateToStr(Date d,String pattern) {
		return d == null ? "" : DateFormatUtils.format(d, pattern);
	}
	
	public static Date getDay(int offset) {
		Calendar cal = Calendar.getInstance();  
		cal.set(Calendar.HOUR_OF_DAY, 0);  
        cal.set(Calendar.SECOND, 0);  
        cal.set(Calendar.MINUTE, 0);  
        cal.set(Calendar.MILLISECOND, 0);
        if(offset != 0) {
        	cal.setTimeInMillis(cal.getTimeInMillis() + (3600 * 24 * 1000) * offset);
        }
        return cal.getTime();  
	}

	public static Date strToDate(String s) {
		return strToDate(s, "yyyy-MM-dd");
	}
	
	public static Date strToDate(String s,String... pattern) {
		try {
			return s == null ? null : DateUtils.parseDate(s, pattern);
		} catch (ParseException e) {
			log.error("",e);
			return null;
		}
	}
	
	public static Byte intToByte(Integer i) {
		return i == null ? 0 : i.byteValue();
	}
	
	public static Integer byteToInt(Byte b) {
		return b == null ? 0 : b.intValue();
	}
	
	//4位
	public static String intToPercent(Integer i ){
		return i == null ? null : String.valueOf(((double)i / 100)) + "%";
	}
	
}
