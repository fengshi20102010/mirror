package com.mirror.tk.core.sdk.haodanku.res;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @title 商品列表
 */
public class ItemListRes implements Serializable {

	private static final long serialVersionUID = 1L;

	// 自增ID
	@JsonProperty("product_id")
	private Integer productId;
	// 宝贝ID
	private Integer itemid;
	// 宝贝标题
	private String itemtitle;
	// 宝贝短标题
	private String itemshorttitle;
	// 宝贝推荐语
	private String itemdesc;
	// 在售价
	private String itemprice;
	// 宝贝月销量
	private Integer itemsale;
	// 宝贝近2小时跑单
	private Integer itemsale2;
	// 当天销量
	private Integer todaysale;
	// 宝贝主图原始图像（由于图片原图过大影响加载速度，建议加上后缀_310x310.jpg，如https://img.alicdn.com/imgextra/i2/3412518427/TB26gs7bb7U5uJjSZFFXXaYHpXa_!!3412518427.jpg_310x310.jpg）
	private String itempic;
	// 推广长图（带http://img.haodanku.com/0_553757100845_1509175123.jpg-600进行访问）
	@JsonProperty("itempic_copy")
	private String itempicCopy;
	// 商品类目(1女装，2男装，3内衣，4美妆，5配饰，6鞋品，7箱包，8儿童，9母婴，10居家，11美食，12数码，13家电，14其他，15车品，16文体)
	private Integer fqcat;
	// 宝贝券后价
	private String itemendprice;
	// 店铺类型：天猫店（B）淘宝店（C）
	private String shoptype;
	// 优惠券链接
	private String couponurl;
	// 优惠券金额
	private String couponmoney;
	// 是否为品牌产品（1是）
	@JsonProperty("is_brand")
	private Integer isBrand;
	// 是否为直播（1是）
	@JsonProperty("is_live")
	private Integer isLive;
	// 推广导购文案
	@JsonProperty("guide_article")
	private String guideArticle;
	// 商品视频ID（id大于0的为有视频单，视频拼接地址http://cloud.video.taobao.com/play/u/1/p/1/e/6/t/1/+videoid+.mp4）
	private Integer videoid;
	// 活动类型：（普通活动，聚划算, 淘抢购）
	@JsonProperty("activity_type")
	private String activityType;
	// 营销计划链接
	private String planlink;
	// 店主的userid
	private Integer userid;
	// 店铺掌柜名
	private String sellernick;
	// 佣金计划(隐藏,营销)
	private String tktype;
	// 佣金比例
	private String tkrates;
	// 是否村淘（1是）
	private Integer cuntao;
	// 预计可得（宝贝价格 * 佣金比例 / 100）
	private String tkmoney;
	// 定向计划链接
	private String tkurl;
	// 当天优惠券领取量
	private Integer couponreceive2;
	// 优惠券总数量
	private Integer couponnum;
	// 优惠券使用条件
	private Integer couponexplain;
	// 优惠券开始时间
	private Integer couponstarttime;
	// 优惠券结束时间
	private Integer couponendtime;
	// 活动开始时间
	@JsonProperty("start_time")
	private Integer start_time;
	// 活动结束时间
	@JsonProperty("end_time")
	private Integer end_time;
	// 发布时间
	@JsonProperty("starttime")
	private Integer starttime;
	// 是否优选（1是）该字段已停用
	private Integer isquality;
	// 举报处理条件 0未举报 1为待处理 2为忽略 3为下架
	@JsonProperty("report_status")
	private Integer report_status;
	// 好单指数
	@JsonProperty("general_index")
	private Integer general_index;
	// 放单人名号
	@JsonProperty("seller_name")
	private String seller_name;
	// 实拍图
	@JsonProperty("original_img")
	private String original_img;
	// 实拍图文案
	@JsonProperty("original_article")
	private String original_article;
	// 折扣力度
	@JsonProperty("discount")
	private String discount;

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getItemid() {
		return itemid;
	}

	public void setItemid(Integer itemid) {
		this.itemid = itemid;
	}

	public String getItemtitle() {
		return itemtitle;
	}

	public void setItemtitle(String itemtitle) {
		this.itemtitle = itemtitle;
	}

	public String getItemshorttitle() {
		return itemshorttitle;
	}

	public void setItemshorttitle(String itemshorttitle) {
		this.itemshorttitle = itemshorttitle;
	}

	public String getItemdesc() {
		return itemdesc;
	}

	public void setItemdesc(String itemdesc) {
		this.itemdesc = itemdesc;
	}

	public String getItemprice() {
		return itemprice;
	}

	public void setItemprice(String itemprice) {
		this.itemprice = itemprice;
	}

	public Integer getItemsale() {
		return itemsale;
	}

	public void setItemsale(Integer itemsale) {
		this.itemsale = itemsale;
	}

	public Integer getItemsale2() {
		return itemsale2;
	}

	public void setItemsale2(Integer itemsale2) {
		this.itemsale2 = itemsale2;
	}

	public Integer getTodaysale() {
		return todaysale;
	}

	public void setTodaysale(Integer todaysale) {
		this.todaysale = todaysale;
	}

	public String getItempic() {
		return itempic;
	}

	public void setItempic(String itempic) {
		this.itempic = itempic;
	}

	public String getItempicCopy() {
		return itempicCopy;
	}

	public void setItempicCopy(String itempicCopy) {
		this.itempicCopy = itempicCopy;
	}

	public Integer getFqcat() {
		return fqcat;
	}

	public void setFqcat(Integer fqcat) {
		this.fqcat = fqcat;
	}

	public String getItemendprice() {
		return itemendprice;
	}

	public void setItemendprice(String itemendprice) {
		this.itemendprice = itemendprice;
	}

	public String getShoptype() {
		return shoptype;
	}

	public void setShoptype(String shoptype) {
		this.shoptype = shoptype;
	}

	public String getCouponurl() {
		return couponurl;
	}

	public void setCouponurl(String couponurl) {
		this.couponurl = couponurl;
	}

	public String getCouponmoney() {
		return couponmoney;
	}

	public void setCouponmoney(String couponmoney) {
		this.couponmoney = couponmoney;
	}

	public Integer getIsBrand() {
		return isBrand;
	}

	public void setIsBrand(Integer isBrand) {
		this.isBrand = isBrand;
	}

	public Integer getIsLive() {
		return isLive;
	}

	public void setIsLive(Integer isLive) {
		this.isLive = isLive;
	}

	public String getGuideArticle() {
		return guideArticle;
	}

	public void setGuideArticle(String guideArticle) {
		this.guideArticle = guideArticle;
	}

	public Integer getVideoid() {
		return videoid;
	}

	public void setVideoid(Integer videoid) {
		this.videoid = videoid;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public String getPlanlink() {
		return planlink;
	}

	public void setPlanlink(String planlink) {
		this.planlink = planlink;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public String getSellernick() {
		return sellernick;
	}

	public void setSellernick(String sellernick) {
		this.sellernick = sellernick;
	}

	public String getTktype() {
		return tktype;
	}

	public void setTktype(String tktype) {
		this.tktype = tktype;
	}

	public String getTkrates() {
		return tkrates;
	}

	public void setTkrates(String tkrates) {
		this.tkrates = tkrates;
	}

	public Integer getCuntao() {
		return cuntao;
	}

	public void setCuntao(Integer cuntao) {
		this.cuntao = cuntao;
	}

	public String getTkmoney() {
		return tkmoney;
	}

	public void setTkmoney(String tkmoney) {
		this.tkmoney = tkmoney;
	}

	public String getTkurl() {
		return tkurl;
	}

	public void setTkurl(String tkurl) {
		this.tkurl = tkurl;
	}

	public Integer getCouponreceive2() {
		return couponreceive2;
	}

	public void setCouponreceive2(Integer couponreceive2) {
		this.couponreceive2 = couponreceive2;
	}

	public Integer getCouponnum() {
		return couponnum;
	}

	public void setCouponnum(Integer couponnum) {
		this.couponnum = couponnum;
	}

	public Integer getCouponexplain() {
		return couponexplain;
	}

	public void setCouponexplain(Integer couponexplain) {
		this.couponexplain = couponexplain;
	}

	public Integer getCouponstarttime() {
		return couponstarttime;
	}

	public void setCouponstarttime(Integer couponstarttime) {
		this.couponstarttime = couponstarttime;
	}

	public Integer getCouponendtime() {
		return couponendtime;
	}

	public void setCouponendtime(Integer couponendtime) {
		this.couponendtime = couponendtime;
	}

	public Integer getStart_time() {
		return start_time;
	}

	public void setStart_time(Integer start_time) {
		this.start_time = start_time;
	}

	public Integer getEnd_time() {
		return end_time;
	}

	public void setEnd_time(Integer end_time) {
		this.end_time = end_time;
	}

	public Integer getStarttime() {
		return starttime;
	}

	public void setStarttime(Integer starttime) {
		this.starttime = starttime;
	}

	public Integer getIsquality() {
		return isquality;
	}

	public void setIsquality(Integer isquality) {
		this.isquality = isquality;
	}

	public Integer getReport_status() {
		return report_status;
	}

	public void setReport_status(Integer report_status) {
		this.report_status = report_status;
	}

	public Integer getGeneral_index() {
		return general_index;
	}

	public void setGeneral_index(Integer general_index) {
		this.general_index = general_index;
	}

	public String getSeller_name() {
		return seller_name;
	}

	public void setSeller_name(String seller_name) {
		this.seller_name = seller_name;
	}

	public String getOriginal_img() {
		return original_img;
	}

	public void setOriginal_img(String original_img) {
		this.original_img = original_img;
	}

	public String getOriginal_article() {
		return original_article;
	}

	public void setOriginal_article(String original_article) {
		this.original_article = original_article;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	@Override
	public String toString() {
		return "ItemListRes [productId=" + productId + ", itemid=" + itemid + ", itemtitle=" + itemtitle
				+ ", itemshorttitle=" + itemshorttitle + ", itemdesc=" + itemdesc + ", itemprice=" + itemprice
				+ ", itemsale=" + itemsale + ", itemsale2=" + itemsale2 + ", todaysale=" + todaysale + ", itempic="
				+ itempic + ", itempicCopy=" + itempicCopy + ", fqcat=" + fqcat + ", itemendprice=" + itemendprice
				+ ", shoptype=" + shoptype + ", couponurl=" + couponurl + ", couponmoney=" + couponmoney + ", isBrand="
				+ isBrand + ", isLive=" + isLive + ", guideArticle=" + guideArticle + ", videoid=" + videoid
				+ ", activityType=" + activityType + ", planlink=" + planlink + ", userid=" + userid + ", sellernick="
				+ sellernick + ", tktype=" + tktype + ", tkrates=" + tkrates + ", cuntao=" + cuntao + ", tkmoney="
				+ tkmoney + ", tkurl=" + tkurl + ", couponreceive2=" + couponreceive2 + ", couponnum=" + couponnum
				+ ", couponexplain=" + couponexplain + ", couponstarttime=" + couponstarttime + ", couponendtime="
				+ couponendtime + ", start_time=" + start_time + ", end_time=" + end_time + ", starttime=" + starttime
				+ ", isquality=" + isquality + ", report_status=" + report_status + ", general_index=" + general_index
				+ ", seller_name=" + seller_name + ", original_img=" + original_img + ", original_article="
				+ original_article + ", discount=" + discount + "]";
	}

}
