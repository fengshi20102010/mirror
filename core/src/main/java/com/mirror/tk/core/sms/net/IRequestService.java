package com.mirror.tk.core.sms.net;

import java.io.IOException;
import java.util.Map;

/**
 * 短信接口封装
 */
public interface IRequestService {

	public String post(String url, Map<String, String> param) throws IOException;

}

