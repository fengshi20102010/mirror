package com.mirror.tk.core.module.goods.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 商品信息主表 Entity
 *
 * Date: 2016-11-09 21:28:48
 *
 * @author Code Generator
 */
@Entity
@Table(name = "goods")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Goods extends AbstractEntity {
	
	/** ID */
	private Long id;

	/** 对应的店铺信息ID */
	private Long shopId;
	
	/** 上传用户ID */
	private Long userId;

	/** 淘宝对应的商品ID */
	private Long numIid;

	/** 商品类型ID */
	private Long typeId;

	/** 活动类型 */
	private Integer type;

	/** 商品名称 */
	private String name;

	/** 商品简介 */
	private String detail;

	/** 商品链接地址 */
	private String url;

	/** 是否直推（0：否，1：是） */
	private Integer isExtend;

	/** 是否有优惠劵 */
	private Integer isCoupon;

	/* 今日推荐 **/
	private Integer recommend;

	/** 商品属性 */
	private Integer attribute;

	/** 商品状态 */
	private Integer status;

	/** 佣金比例,1234代表12.34% */
	private Integer commission;

	/** 商品图片 */
	private String picUrl;

	/** 商品价格 */
	private String price;

	/** 折扣价 */
	private String finalPrice;

	/** 副标题 */
	private String subTitle;

	/* 预告时间 **/
	private Date adTime;

	/** 商品销量 */
	private Long sales;

	/** 创建时间 */
	private Date createTime;

	/** 修改时间 */
	private Date updateTime;

	/** 推广计划链接 */
	private String planUrl;

	/** 商品来源，1：天猫，2：淘宝 */
	private Integer source;

	private String qq;

	public static final int STATUS_CHECKING = 2;
	public static final int STATUS_PUBLISH = 3;
	public static final int STATUS_PENDING_SETTLEMENT = 4;
	public static final int STATUS_SHELVES = 5;

	public static final int TYPE_TONGYONG = 1;
	public static final int TYPE_DINGXIANG = 2;
	public static final int TYPE_QUEQIAO = 3;

	public static final int Extend_true = 1;
	public static final int Extend_false = 0;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getShopId() {
		return this.shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getNumIid() {
		return this.numIid;
	}

	public void setNumIid(Long numIid) {
		this.numIid = numIid;
	}

	public Long getTypeId() {
		return this.typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetail() {
		return this.detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getIsExtend() {
		return this.isExtend;
	}

	public void setIsExtend(Integer isExtend) {
		this.isExtend = isExtend;
	}

	public Integer getIsCoupon() {
		return this.isCoupon;
	}

	public void setIsCoupon(Integer isCoupon) {
		this.isCoupon = isCoupon;
	}

	public Integer getAttribute() {
		return this.attribute;
	}

	public void setAttribute(Integer attribute) {
		this.attribute = attribute;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getCommission() {
		return this.commission;
	}

	public void setCommission(Integer commission) {
		this.commission = commission;
	}

	public String getPicUrl() {
		return this.picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getPrice() {
		return this.price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(String finalPrice) {
		this.finalPrice = finalPrice;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public Long getSales() {
		return this.sales;
	}

	public void setSales(Long sales) {
		this.sales = sales;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getPlanUrl() {
		return planUrl;
	}

	public void setPlanUrl(String planUrl) {
		this.planUrl = planUrl;
	}

	public Integer getSource() {
		return source;
	}

	public void setSource(Integer source) {
		this.source = source;
	}

	public Integer getRecommend() {
		return recommend;
	}

	public void setRecommend(Integer recommend) {
		this.recommend = recommend;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public Date getAdTime() {
		return adTime;
	}

	public void setAdTime(Date adTime) {
		this.adTime = adTime;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
