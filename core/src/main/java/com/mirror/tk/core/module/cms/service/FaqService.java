package com.mirror.tk.core.module.cms.service;

import java.util.List;

import com.mirror.tk.core.module.cms.domain.Faq;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 常见问题 Service
 *
 * Date: 2016-11-01 23:10:31
 *
 * @author Code Generator
 *
 */
public interface FaqService extends EntityService<Faq> {

	/**
	 * 获取Faq列表
	 * @return
	 */
	List<Faq> findFaq();

}
