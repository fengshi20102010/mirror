package com.mirror.tk.core.biz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mirror.tk.core.biz.common.Result;
import com.mirror.tk.core.module.user.domain.UserPid;
import com.mirror.tk.core.module.user.service.UserPidService;

@Service
public class UserBiz {

	@Autowired
	UserPidService userPidService;

	public Result<List<UserPid>> getListUserPid(Long userId) {
		if(userId == null) {
			return Result.nullError();
		}
		
		List<UserPid> list = userPidService.getByUserId(userId);
		return new Result<List<UserPid>>().success(list);
	}
}
