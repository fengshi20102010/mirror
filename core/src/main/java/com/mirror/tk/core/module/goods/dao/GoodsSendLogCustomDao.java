package com.mirror.tk.core.module.goods.dao;

import java.util.Date;

import com.mirror.tk.core.module.goods.domain.GoodsSendLog;
import com.mirror.tk.framework.common.dao.support.PageInfo;

public interface GoodsSendLogCustomDao {

	public PageInfo<GoodsSendLog> getPagedSendLog(PageInfo<GoodsSendLog> page,Long userId, Date startTime, Date endTime, String title);
}
