package com.mirror.tk.core.module.goods.service.impl;


import org.springframework.stereotype.Service;

import com.mirror.tk.core.module.goods.dao.GoodsTplContrastDao;
import com.mirror.tk.core.module.goods.domain.GoodsTplContrast;
import com.mirror.tk.core.module.goods.service.GoodsTplContrastService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("goodsTplContrastService")
public class GoodsTplContrastServiceImpl extends EntityServiceImpl<GoodsTplContrast, GoodsTplContrastDao> implements GoodsTplContrastService {

}
