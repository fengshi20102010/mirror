package com.mirror.tk.core.module.user.service;

import com.mirror.tk.core.module.user.domain.UserGatherConfig;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 用户采集群配置表 Service
 *
 * Date: 2016-12-28 23:56:35
 *
 * @author Code Generator
 *
 */
public interface UserGatherConfigService extends EntityService<UserGatherConfig> {

	/**
	 * 获取用户采集配置信息
	 * @param userId
	 * @return
	 */
	public UserGatherConfig getByUserId(Long userId);

}
