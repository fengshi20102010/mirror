package com.mirror.tk.core.sms.work;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mirror.tk.core.sms.exception.SMSException;
import com.mirror.tk.core.sms.listener.SendListener;
import com.mirror.tk.core.sms.net.IRequestService;
import com.mirror.tk.core.sms.validate.InObject;
import com.mirror.tk.core.sms.validate.OutObject;
import com.mirror.tk.core.sms.validate.ValidateService;

public abstract class BaseSmsSendServiceImpl implements SmsSendService {

	protected static final Logger logger = LogManager.getLogger();

	protected IRequestService requestService;

	protected SendListener sendListener;

	protected List<ValidateService> validateList = new ArrayList<ValidateService>();

	protected String httpPost(String url, Map<String, String> map) throws IOException {
		return requestService.post(url, map);
	}

	protected void validate(InObject inObject) throws SMSException {
		for (ValidateService validate : validateList) {
			OutObject outObject = validate.validate(inObject);
			if (outObject != null) {
				throw new SMSException(outObject.getCode(), outObject.getDetail(), "Validate Failure, error:[" + outObject.getCode() + "]" + outObject.getDetail() + "; inObject " + inObject.toString());
			}
		}
	}

	@Override
	public void send(String phone, String msg) throws SMSException, IOException {
		// 验证对象
		InObject inObject = new InObject();
		// 验证方法
		this.validate(inObject);
		// 接口地址
		String address = this.getInterfaceAddress();
		// post参数
		Map<String, String> params = this.builderParam(phone, msg);
		// 异常
		Exception e = null;
		try {
			// 发送post请求
			this.responseHandler(httpPost(address, params));
		} catch (IOException ex) {
			e = ex;
			throw ex;
		} catch (SMSException ex) {
			e = ex;
			throw ex;
		} catch (Exception ex) {
			e = ex;
			throw new RuntimeException(ex);
		} finally {
			// 发送监听
			if (sendListener != null) {
				try {
					sendListener.handler(phone, msg, address, params != null ? params.toString() : null, e);
				} catch (Exception ex) {
					logger.error("Send Listener Error", ex);
				}
			}
		}
	}

	protected abstract Map<String, String> builderParam(String phone, String msg);

	protected abstract String getInterfaceAddress();

	protected abstract void responseHandler(String result) throws SMSException;

	public IRequestService getRequestService() {
		return requestService;
	}

	public void setRequestService(IRequestService requestService) {
		this.requestService = requestService;
	}

	public List<ValidateService> getValidateList() {
		return validateList;
	}

	public void setValidateList(List<ValidateService> validateList) {
		this.validateList = validateList;
	}

	public SendListener getSendListener() {
		return sendListener;
	}

	public void setSendListener(SendListener sendListener) {
		this.sendListener = sendListener;
	}
	
}
