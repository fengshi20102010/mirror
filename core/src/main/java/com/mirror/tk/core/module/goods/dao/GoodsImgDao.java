package com.mirror.tk.core.module.goods.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mirror.tk.core.module.goods.domain.GoodsImg;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

public interface GoodsImgDao extends EntityJpaDao<GoodsImg, Long>{

	public List<GoodsImg> findByNumIidIn(List<Long> numIid);

	@Modifying
	@Query("delete from GoodsImg where numIid in (?1) ")
	public int deleteByNumIid(List<Long> ids);
	
}
