package com.mirror.tk.core.module.goods.dao;

import com.mirror.tk.core.module.goods.domain.GoodsSendLog;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

public interface GoodsSendLogDao extends EntityJpaDao<GoodsSendLog,Long>{

	public GoodsSendLog findByUserIdAndNumIid(Long userId,Long goods_id);
}
