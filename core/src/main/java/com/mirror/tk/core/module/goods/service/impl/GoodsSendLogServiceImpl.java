package com.mirror.tk.core.module.goods.service.impl;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.mirror.tk.core.module.goods.dao.GoodsSendLogCustomDao;
import com.mirror.tk.core.module.goods.dao.GoodsSendLogDao;
import com.mirror.tk.core.module.goods.domain.GoodsSendLog;
import com.mirror.tk.core.module.goods.service.GoodsSendLogService;
import com.mirror.tk.framework.common.dao.support.PageInfo;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service
public class GoodsSendLogServiceImpl extends EntityServiceImpl<GoodsSendLog,GoodsSendLogDao> implements GoodsSendLogService{

	@Resource
	GoodsSendLogDao goodsSendLogDao;
	
	@Resource
	GoodsSendLogCustomDao goodsSendLogCustomDao;
	@Override
	public GoodsSendLog queryLog(Long numIid, Long userId) {
		return goodsSendLogDao.findByUserIdAndNumIid(userId, numIid);
	}
	@Override
	public PageInfo<GoodsSendLog> getPagedSendLog(PageInfo<GoodsSendLog> page,Long userId, Date startTime, Date endTime, String title) {
		return goodsSendLogCustomDao.getPagedSendLog(page, userId, startTime, endTime, title);
	}

}
