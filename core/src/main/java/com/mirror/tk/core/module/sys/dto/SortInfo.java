package com.mirror.tk.core.module.sys.dto;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 排序信息
 */
public class SortInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	/** id */
	private Long id;

	/** parentId */
	private Long parentId;

	/** 排序号 */
	private Long sort;

	/** 子排序信息 */
	private List<SortInfo> children;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Long getSort() {
		return sort;
	}

	public void setSort(Long sort) {
		this.sort = sort;
	}

	public List<SortInfo> getChildren() {
		return children;
	}

	public void setChildren(List<SortInfo> children) {
		this.children = children;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
