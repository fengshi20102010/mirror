package com.mirror.tk.core.module.user.service.impl;


import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.mirror.tk.core.module.user.dao.UserSettingDao;
import com.mirror.tk.core.module.user.domain.UserSetting;
import com.mirror.tk.core.module.user.service.UserSettingService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("userSettingService")
public class UserSettingServiceImpl extends EntityServiceImpl<UserSetting, UserSettingDao> implements UserSettingService {

	@Override
	public UserSetting getByUserId(Long userId) {
		Map<String, Object> map = Maps.newHashMap();
		map.put("EQ_userId", userId);
		List<UserSetting> list = query(map, null);
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}

}
