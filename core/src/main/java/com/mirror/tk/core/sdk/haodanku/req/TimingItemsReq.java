package com.mirror.tk.core.sdk.haodanku.req;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @title 
 */
public class TimingItemsReq implements Serializable {

	private static final long serialVersionUID = 1L;

	// 放单后台获取的Apikey值
	private String Apikey;
	// 小时点数，如0点是0、13点是13（最小值是0，最大值是23）
	private Integer start = 0;
	// 小时点数，如0点是0、13点是13（最小值是0，最大值是23）
	private Integer end = 23;
	// 分页，用于实现类似分页抓取效果，来源于上次获取后的数据的min_id值，默认开始请求值为1（该方案比单纯123分页的优势在于：数据更新的情况下保证不会重复也无需关注和计算页数）
	@JsonProperty("min_id")
	private Integer minId = 1;
	// 每页返回条数（请在1,2,10,20,50,100,120,200,500,1000中选择一个数值返回）
	private Integer back = 500;

	public String getApikey() {
		return Apikey;
	}

	public void setApikey(String apikey) {
		Apikey = apikey;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getEnd() {
		return end;
	}

	public void setEnd(Integer end) {
		this.end = end;
	}

	public Integer getMinId() {
		return minId;
	}

	public void setMinId(Integer minId) {
		this.minId = minId;
	}

	public Integer getBack() {
		return back;
	}

	public void setBack(Integer back) {
		this.back = back;
	}

	@Override
	public String toString() {
		return "TimingItemsReq [Apikey=" + Apikey + ", start=" + start + ", end=" + end + ", minId=" + minId + ", back="
				+ back + "]";
	}

}
