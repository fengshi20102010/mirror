package com.mirror.tk.core.module.sys.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 地区 Entity
 *
 * Date: 2016-03-30 10:31:49
 *
 * @author Acooly Code Generator
 */
@Entity
@Table(name = "sys_region")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SysRegion extends AbstractEntity {
	/** id */
	private Long id;
	
	/** 地区代码 */
	private String code;
	
	/** 省 */
	private String province;
	
	/** 地区 */
	private String name;
	
	/** 显示全称 */
	private String fullName;
	
	/** 地区类型代码： */
	private Integer type;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getCode(){
		return this.code;
	}

	public void setCode(String code){
		this.code = code;
	}

	public String getProvince(){
		return this.province;
	}

	public void setProvince(String province){
		this.province = province;
	}

	public String getName(){
		return this.name;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getFullName(){
		return this.fullName;
	}

	public void setFullName(String fullName){
		this.fullName = fullName;
	}

	public Integer getType(){
		return this.type;
	}

	public void setType(Integer type){
		this.type = type;
	}

	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
