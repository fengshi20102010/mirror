package com.mirror.tk.core.module.sys.service;

import com.mirror.tk.core.module.sys.dto.DictionariesDto;

/**
 * 公用基础接口 初始化所需数据（地区，字典等数据）
 */
public interface BasicService {
	
	/**
	 * 初始化方法
	 */
	public void init();
	
	/**
	 * 销毁方法
	 */
	public void destroy();
	
	/**
	 * 重新读取配置
	 */
	public void reloadsetting();
	
	/**
	 * 根据key获取value
	 * @param key
	 * @return
	 */
	public String getProValue(String key);
	
	/**
	 * 根据模板名称，获取短信模板
	 * @param code
	 * @return
	 */
	public String getSmsTpl(String code);
	
	/**
	 * 获取数据字典对像
	 * @return
	 */
	public DictionariesDto getDictionariesDto(String key);
	
}
