package com.mirror.tk.core.sms.validate;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OutObject {

	// 错误代码
	private String code;
	// 错误描述
	private String detail;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

}
