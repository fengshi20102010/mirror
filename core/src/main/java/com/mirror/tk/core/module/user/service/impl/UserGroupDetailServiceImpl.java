package com.mirror.tk.core.module.user.service.impl;


import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mirror.tk.core.module.user.dao.UserGroupDetailDao;
import com.mirror.tk.core.module.user.domain.UserGroupDetail;
import com.mirror.tk.core.module.user.service.UserGroupDetailService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("userGroupDetailService")
public class UserGroupDetailServiceImpl extends EntityServiceImpl<UserGroupDetail, UserGroupDetailDao> implements UserGroupDetailService {

	@Transactional
	@Override
	public boolean deleteByGroupId(Long groupId) {
		return this.getEntityDao().deleteByGroupId(groupId) > 0;
	}

	@Transactional
	@Override
	public void saveByGroupId(Long id, List<UserGroupDetail> list) {
		if(null != list && list.size() > 0){
			List<UserGroupDetail> deiailList = Lists.newArrayList();
			for (UserGroupDetail detail : list) {
				detail.setGroupId(id);
				detail.setCreateTime(new Date());
				deiailList.add(detail);
			}
			this.saves(deiailList);
		}
	}

	@Transactional
	@Override
	public boolean deleteByGroupIdAndIds(Long groupId, List<Long> list) {
		return this.getEntityDao().deleteByGroupIdAndIds(groupId, list) >= 0;
	}

}
