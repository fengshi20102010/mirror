package com.mirror.tk.core.sms.service.impl;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mirror.tk.core.sms.exception.SMSException;
import com.mirror.tk.core.sms.service.SendTemplateSmsService;
import com.mirror.tk.core.sms.template.TemplateService;
import com.mirror.tk.core.sms.work.SmsSendService;

public class SendTemplateSmsServiceImpl implements SendTemplateSmsService {

	private static final Logger logger = LogManager.getLogger();

	private TemplateService templateService;
	private SmsSendService ihuyiSmsSendService;

	@Override
	public void send(String phone, TEMPLATE_CODE templateCode, String... param)	throws IOException, SMSException {
		// 获取模板短信内容
		String smsContent = templateService.mergeTemplate(templateCode.getKey(), param);
		logger.debug("Send SMS Content:{}", smsContent);
		// 发送短信
		ihuyiSmsSendService.send(phone, smsContent);
	}

	public TemplateService getTemplateService() {
		return templateService;
	}

	public void setTemplateService(TemplateService templateService) {
		this.templateService = templateService;
	}

	public SmsSendService getIhuyiSmsSendService() {
		return ihuyiSmsSendService;
	}

	public void setIhuyiSmsSendService(SmsSendService ihuyiSmsSendService) {
		this.ihuyiSmsSendService = ihuyiSmsSendService;
	}

}
