package com.mirror.tk.core.module.user.domain;


import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 用户QQ/微信群组设置表 Entity
 *
 * Date: 2017-01-01 22:30:28
 *
 * @author Code Generator
 */
@Entity
@Table(name = "user_group")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserGroup extends AbstractEntity {
	/** ID */
	private Long id;
	
	/** 用户ID */
	private Long userId;
	
	/** 类型 */
	private Integer type;
	
	/** 群名称 */
	private String name;
	
	/** 备注 */
	private String remark;
	
	/** 创建时间 */
	private Date createTime;
	
	/** 修改时间 */
	private Date updateTime;
	
	/** 群详情列表 */
	private List<UserGroupDetail> detail;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Integer getType(){
		return this.type;
	}

	public void setType(Integer type){
		this.type = type;
	}

	public String getName(){
		return this.name;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getRemark(){
		return this.remark;
	}

	public void setRemark(String remark){
		this.remark = remark;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}
	
    @OneToMany(cascade=CascadeType.ALL)
    @JoinColumn(name="groupId")
    @OrderBy(value = "id desc")
	public List<UserGroupDetail> getDetail() {
		return detail;
	}

	public void setDetail(List<UserGroupDetail> detail) {
		this.detail = detail;
	}

	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
