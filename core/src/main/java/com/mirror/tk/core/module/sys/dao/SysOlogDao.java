package com.mirror.tk.core.module.sys.dao;

import com.mirror.tk.core.module.sys.domain.SysOlog;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 系统日志 JPA Dao
 *
 * Date: 2015-05-14 16:04:44
 *
 * @author Code Generator
 *
 */
public interface SysOlogDao extends EntityJpaDao<SysOlog, Long> {


}
