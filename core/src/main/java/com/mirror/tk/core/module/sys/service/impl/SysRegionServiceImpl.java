package com.mirror.tk.core.module.sys.service.impl;


import org.springframework.stereotype.Service;

import com.mirror.tk.core.module.sys.dao.SysRegionDao;
import com.mirror.tk.core.module.sys.domain.SysRegion;
import com.mirror.tk.core.module.sys.service.SysRegionService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("sysRegionService")
public class SysRegionServiceImpl extends EntityServiceImpl<SysRegion, SysRegionDao> implements SysRegionService {

}
