package com.mirror.tk.core.module.user.dao;

import com.mirror.tk.core.module.user.domain.UserConfig;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 用户程序配置表 JPA Dao
 *
 * Date: 2016-12-28 23:56:41
 *
 * @author Code Generator
 *
 */
public interface UserConfigDao extends EntityJpaDao<UserConfig, Long> {

	public UserConfig getByUserId(Long userId);

}
