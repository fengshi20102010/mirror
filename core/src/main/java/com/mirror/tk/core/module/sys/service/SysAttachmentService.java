package com.mirror.tk.core.module.sys.service;

import org.springframework.web.multipart.MultipartFile;

import com.mirror.tk.core.module.sys.domain.SysAttachment;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 系统_附件表 Service
 *
 * Date: 2016-01-08 09:54:33
 *
 * @author Code Generator
 *
 */
public interface SysAttachmentService extends EntityService<SysAttachment> {

	/**
	 * 删除附件 会同时删除文件
	 * 
	 * @param oldAttachUrl
	 * @return
	 */
	public SysAttachment delete(String oldAttachUrl);
	
	/**
	 * 删除附件 只能删除自己的文件
	 * 
	 * @param userId
	 * @param oldAttachUrl
	 * @return
	 */
	public SysAttachment delete(Long userId, String oldAttachUrl);

	/**
	 * 上传附件
	 * 
	 * @param userId
	 * @param sysKey
	 * @param relationKey
	 * @param file
	 * @return
	 */
	public SysAttachment upload(Long userId, String sysKey, String relationKey, MultipartFile file);

	/**
	 * 上传附件 用于替换时使用
	 * 
	 * @param userId
	 * @param sysKey
	 * @param relationKey
	 * @param file
	 * @param oldAttachUrl
	 * @return
	 */
	public SysAttachment upload(Long userId, String sysKey, String relationKey, MultipartFile file, String oldAttachUrl);
	
}

