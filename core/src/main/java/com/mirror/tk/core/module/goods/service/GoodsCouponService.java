package com.mirror.tk.core.module.goods.service;

import java.util.List;

import com.mirror.tk.core.module.goods.domain.GoodsCoupon;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 商品优惠劵表 Service
 *
 * Date: 2016-11-01 23:10:16
 *
 * @author Code Generator
 *
 */
public interface GoodsCouponService extends EntityService<GoodsCoupon> {

	/**
	 * 查询状态为发布中的优惠劵信息
	 * @return
	 */
	List<GoodsCoupon> findByNormal();

	/**
	 * 根据商品id删除商品优惠劵
	 * @param goodsIds
	 * @return
	 */
	Boolean deleteByGoodsIds(List<Long> goodsIds);
	
}
