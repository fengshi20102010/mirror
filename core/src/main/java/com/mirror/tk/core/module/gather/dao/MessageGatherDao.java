package com.mirror.tk.core.module.gather.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.mirror.tk.core.module.gather.domain.MessageGather;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 *
 * @author sufj
 * @date 2017-04-02 14:37:36
 *
 *
 */

public interface MessageGatherDao extends EntityJpaDao<MessageGather,Long>{



		/**
		 * 删除数据
		 * @param id
		 * @return
	     */
		@Transactional
		@Modifying
		@Query("update MessageGather SET valiableTag = 1 where id = (?1) ")
		int delete(int id);
}