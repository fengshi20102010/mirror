package com.mirror.tk.core.module.user.service;

import java.util.List;

import com.mirror.tk.core.module.user.domain.UserGroup;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 用户QQ/微信群组设置表 Service
 *
 * Date: 2017-01-01 22:30:28
 *
 * @author Code Generator
 *
 */
public interface UserGroupService extends EntityService<UserGroup> {

	/**
	 * 根据用户Id和类型查询群信息列表
	 * @param userId
	 * @param type
	 * @return
	 */
	public List<UserGroup> getByUserIdAndType(Long userId, Integer type);

	/**
	 * 根据id和用户id删除群组信息
	 * @param id
	 * @param userId
	 * @return
	 */
	public boolean deleteByIdAndUserId(Long id, Long userId);

}
