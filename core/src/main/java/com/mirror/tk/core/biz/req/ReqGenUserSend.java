package com.mirror.tk.core.biz.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class ReqGenUserSend {

	@ApiModelProperty(value="",required=false)
	private Long userId;
	
	@ApiModelProperty(value="",required=true)
	private Long numIid;
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getNumIid() {
		return numIid;
	}

	public void setNumIid(Long numIid) {
		this.numIid = numIid;
	}
}
