package com.mirror.tk.core.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

public class RequestUtil {

	/**
	 * 获取最终的ip地址
	 * 
	 * @param request
	 * @return
	 */
	public static String getRequestIpAddr(HttpServletRequest request) {

		String loginIp = request.getHeader("X-Forwarded-For");
		if (null == loginIp || loginIp.length() == 0 || "unknown".equalsIgnoreCase(loginIp))
			loginIp = request.getHeader("Proxy-Client-lastLoginIp");
		if (loginIp == null || loginIp.length() == 0 || "unknown".equalsIgnoreCase(loginIp)) {
			loginIp = request.getHeader("Proxy-Client-IP");
		}
		if (loginIp == null || loginIp.length() == 0 || "unknown".equalsIgnoreCase(loginIp)) {
			loginIp = request.getHeader("WL-Proxy-Client-IP");
		}
		if (loginIp == null || loginIp.length() == 0 || "unknown".equalsIgnoreCase(loginIp))
			loginIp = request.getHeader("WL-Proxy-Client-lastLoginIp");
		if (loginIp == null || loginIp.length() == 0 || "unknown".equalsIgnoreCase(loginIp))
			loginIp = request.getHeader("HTTP_CLIENT_lastLoginIp");
		if (loginIp == null || loginIp.length() == 0 || "unknown".equalsIgnoreCase(loginIp))
			loginIp = request.getHeader("HTTP_X_FORWARDED_FOR");
		if (loginIp == null || loginIp.length() == 0 || "unknown".equalsIgnoreCase(loginIp)) {
			loginIp = request.getRemoteAddr();
		}
		if ("127.0.0.1".equals(loginIp) || "0:0:0:0:0:0:0:1".equals(loginIp))
			try {
				loginIp = InetAddress.getLocalHost().getHostAddress();
			} catch (UnknownHostException unknownhostexception) {
				unknownhostexception.printStackTrace();
			}
		return loginIp;
		
	}

}

