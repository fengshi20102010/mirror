package com.mirror.tk.core.module.sys.service;

import com.mirror.tk.core.module.sys.domain.SysRegion;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 地区 Service
 *
 * Date: 2016-03-30 10:31:49
 *
 * @author Code Generator
 *
 */
public interface SysRegionService extends EntityService<SysRegion> {

}
