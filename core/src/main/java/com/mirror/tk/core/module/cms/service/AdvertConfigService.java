package com.mirror.tk.core.module.cms.service;

import java.util.List;

import com.mirror.tk.core.module.cms.domain.AdvertConfig;
import com.mirror.tk.framework.common.service.EntityService;


/**
 * 广告设置表 Service
 *
 * Date: 2017-02-25 10:24:51
 *
 * @author Code Generator
 *
 */
public interface AdvertConfigService extends EntityService<AdvertConfig> {

	/**
	 * 根据类型查找正在启用的广告轮播列表
	 * @param type
	 * @return
	 */
	List<AdvertConfig> findByType(Integer type);

}
