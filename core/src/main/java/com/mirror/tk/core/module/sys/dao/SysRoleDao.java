package com.mirror.tk.core.module.sys.dao;

import com.mirror.tk.core.module.sys.domain.SysRole;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 系统角色表 JPA Dao
 *
 * Date: 2015-05-12 15:40:19
 *
 * @author Code Generator
 *
 */
public interface SysRoleDao extends EntityJpaDao<SysRole, Long> {


}
