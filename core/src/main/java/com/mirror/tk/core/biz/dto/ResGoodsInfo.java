package com.mirror.tk.core.biz.dto;

import java.util.List;

public class ResGoodsInfo {

	/* 商品图片 **/
	private String imgUrl;

	/* 商品名称 **/
	private String name;

	/* 商品ID **/
	private Long numIid;

	/* 优惠券金额 **/
	private String couponPrice;

	/* 优惠券总数 **/
	private Integer counponTotal;

	/* 优惠券剩余 **/
	private Integer counponRemained;

	/* 佣金 **/
	private String commission;

	/* 价格 **/
	private String price;

	/* 销量 **/
	private Integer sales;

	/* 店铺名称 **/
	private String shopName;

	/* 创建时间 **/
	private String createTime;

	/* 来源 1：天猫，2：淘宝 **/
	private Integer source;

	/* 商品小图 **/
	private List<String> imgs;

	/* 推广计划(0:无,1:通用,2:定向,3.鹊桥) **/
	private Integer type;

	/** 商品副标题 */
	private String subTitle;

	/** 预告时间 */
	private String adTime;

	/** 推广计划链接 */
	private String planUrl;

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getNumIid() {
		return numIid;
	}

	public void setNumIid(Long numIid) {
		this.numIid = numIid;
	}

	public String getCouponPrice() {
		return couponPrice;
	}

	public void setCouponPrice(String couponPrice) {
		this.couponPrice = couponPrice;
	}

	public Integer getCounponTotal() {
		return counponTotal;
	}

	public void setCounponTotal(Integer counponTotal) {
		this.counponTotal = counponTotal;
	}

	public Integer getCounponRemained() {
		return counponRemained;
	}

	public void setCounponRemained(Integer counponRemained) {
		this.counponRemained = counponRemained;
	}

	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public Integer getSales() {
		return sales;
	}

	public void setSales(Integer sales) {
		this.sales = sales;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Integer getSource() {
		return source;
	}

	public void setSource(Integer source) {
		this.source = source;
	}

	public List<String> getImgs() {
		return imgs;
	}

	public void setImgs(List<String> imgs) {
		this.imgs = imgs;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getAdTime() {
		return adTime;
	}

	public void setAdTime(String adTime) {
		this.adTime = adTime;
	}

	public String getPlanUrl() {
		return planUrl;
	}

	public void setPlanUrl(String planUrl) {
		this.planUrl = planUrl;
	}

}
