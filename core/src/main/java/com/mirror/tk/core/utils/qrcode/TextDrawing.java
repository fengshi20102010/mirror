package com.mirror.tk.core.utils.qrcode;

import java.awt.image.BufferedImage;

public interface TextDrawing {

	void drawing(String text, BufferedImage qrcodeImg);

}
