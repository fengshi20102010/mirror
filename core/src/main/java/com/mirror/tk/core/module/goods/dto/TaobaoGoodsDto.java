package com.mirror.tk.core.module.goods.dto;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TaobaoGoodsDto implements Serializable {

	private static final long serialVersionUID = 1L;

	// "tkSpecialCampaignIdRateMap": {
	// "45569187": "25.00"
	// },
	// "eventCreatorId": 45770207,
	// "rootCatId": 0,
	// "leafCatId": 50012820,
	// "debugInfo": null,
	// "rootCatScore": 0,
	// "sellerId": 2589682954,
	// "userType": 0,
	// "shopTitle": "抱抱龙品牌直购店",
	// "pictUrl":
	// "http://img03.taobaocdn.com/bao/uploaded/i3/2589682954/TB2iKDjegJkpuFjSszcXXXfsFXa_!!2589682954.jpg",
	// "title": "前抱式婴儿背带多功能四季通用宝宝腰凳单凳儿童坐凳抱带背娃神器",
	// "auctionId": 537659370987,
	// "tkRate": 20,
	// "tkCommFee": 13.8,
	// "totalNum": 1,
	// "totalFee": 4.72,
	// "couponActivityId": null,
	// "reservePrice": 248,
	// "auctionUrl": "http://item.taobao.com/item.htm?id=537659370987",
	// "couponLink": "",
	// "couponLinkTaoToken": "",
	// "dayLeft": 29,
	// "tk3rdRate": null,
	// "biz30day": 153,
	// "rlRate": 72.17,
	// "nick": "抱抱龙品牌直购店",
	// "hasRecommended": 0,
	// "hasSame": 0,
	// "zkPrice": 69,
	// "sameItemPid": "-938955230",
	// "couponTotalCount": 0,
	// "couponLeftCount": 0,
	// "includeDxjh": 1,
	// "auctionTag": "385 587 1163 1483 2059 2115 4491 4550 4806 6603 11083
	// 11339 11531 12491 13707 13771 25282 27137 30977 34305 36161 40897 52290
	// 54337 59137 61954 79106 96002 104514 105666",
	// "couponAmount": 0,
	// "eventRate": 35,
	// "couponShortLink": null,
	// "couponInfo": "无",
	// "couponStartFee": 0,
	// "couponEffectiveStartTime": "",
	// "couponEffectiveEndTime": "",
	// "hasUmpBonus": null,
	// "isBizActivity": null,
	// "umpBonus": null,
	// "rootCategoryName": null,
	// "couponOriLink": null,
	// "userTypeName": null

	/** 淘宝对应的商品ID */
	@JsonProperty("auctionId")
	private Long numIid;

	/** 活动类型 */
	@JsonProperty("includeDxjh")
	private Integer type;

	/** 商品名称 */
	@JsonProperty("title")
	private String name;

	/** 卖家ID */
	@JsonProperty("sellerId")
	private Long userId;

	/** 店铺名称 */
	@JsonProperty("shopTitle")
	private String shopName;

	/** 卖家昵称 */
	@JsonProperty("nick")
	private String nickName;

	/** 商品链接地址 */
	@JsonProperty("auctionUrl")
	private String url;

	/** 佣金 */
	@JsonProperty("tkCommFee")
	private String commission;

	/** 比率 */
	@JsonProperty("tkRate")
	private String ratio;

	/** 商品图片 */
	@JsonProperty("pictUrl")
	private String picUrl;

	/** 商品价格 */
	@JsonProperty("reservePrice")
	private String price;

	/** 折扣价 */
	@JsonProperty("zkPrice")
	private String finalPrice;

	/** 商品销量 */
	@JsonProperty("biz30day")
	private Long sales;

	/** 商品来源 */
	@JsonProperty("userType")
	private Integer source;

	public Long getNumIid() {
		return numIid;
	}

	public void setNumIid(Long numIid) {
		this.numIid = numIid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}


	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	public String getRatio() {
		return ratio;
	}

	public void setRatio(String ratio) {
		this.ratio = ratio;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(String finalPrice) {
		this.finalPrice = finalPrice;
	}

	public Long getSales() {
		return sales;
	}

	public void setSales(Long sales) {
		this.sales = sales;
	}

	public Integer getSource() {
		return source;
	}

	public void setSource(Integer source) {
		this.source = source;
	}

	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
