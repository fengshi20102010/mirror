package com.mirror.tk.core.module.gather.service;

import java.util.List;
import java.util.Map;

import com.mirror.tk.core.biz.req.ReqMessageGather;
import com.mirror.tk.core.biz.req.ReqMessageGatherQueryInfo;
import com.mirror.tk.core.module.gather.domain.MessageGather;
import com.mirror.tk.core.module.gather.dto.MessageGatherDto;
import com.mirror.tk.framework.common.dao.support.PageInfo;

/**
 *
 * @author sufj
 * @date 2017-04-02 14:37:36
 *
 */

public interface MessageGatherService{

	/**
	 * 保存数据
	 * @param entity
	 * @return
     */
	int save(ReqMessageGather entity);
	/**
	 * 保存数据
	 * @param entity
	 * @return
     */
	int save(List<ReqMessageGather> list);

	/**
	 * 修改数据
	 * @param entity
	 * @return
     */
	int update(ReqMessageGather entity);

	/**
	 * 删除数据
	 * @param id
	 * @return
     */
	int delete(int id);

	/**
	 * 根据id查询数据
	 * @param id
	 * @return
     */
	MessageGather queryById(int id);

	/**
     * 分页查询
     * @param offset 偏移量
     * @param limit 每页大小
     * @return
     */
	PageInfo<MessageGatherDto>  queryPage(PageInfo<MessageGatherDto> pageInfo, ReqMessageGatherQueryInfo reqInfo) ;
	/**
	 * 批量修改消息
	 * @param reqList
	 * @return
	 */
	Integer update(List<ReqMessageGather> reqList);
}