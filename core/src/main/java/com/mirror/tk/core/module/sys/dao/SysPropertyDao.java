package com.mirror.tk.core.module.sys.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.mirror.tk.core.module.sys.domain.SysProperty;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 系统_属性表 JPA Dao
 *
 * Date: 2016-01-08 09:54:41
 *
 * @author Code Generator
 *
 */
public interface SysPropertyDao extends EntityJpaDao<SysProperty, Long> {

	// 查询数据字典数据 @author lzj 2016年3月1日9:45:04
	@Query(value = "select * from sys_property where pro_key=?1", nativeQuery = true)
	public List<SysProperty> findSysProperty(String pro_key);
	
	public SysProperty findByProKey(String proKey);
	
}
