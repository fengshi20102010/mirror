package com.mirror.tk.core.module.user.dao;

import com.mirror.tk.core.module.user.domain.UserGatherConfig;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 用户采集群配置表 JPA Dao
 *
 * Date: 2016-12-28 23:56:35
 *
 * @author Code Generator
 *
 */
public interface UserGatherConfigDao extends EntityJpaDao<UserGatherConfig, Long> {

	public UserGatherConfig getByUserId(Long userId);

}
