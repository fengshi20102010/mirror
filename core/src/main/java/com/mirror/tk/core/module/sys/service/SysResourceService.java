package com.mirror.tk.core.module.sys.service;

import java.util.List;
import java.util.Set;

import com.mirror.tk.core.module.sys.domain.SysResource;
import com.mirror.tk.core.module.sys.dto.ResDto;
import com.mirror.tk.core.module.sys.dto.SortInfo;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 系统资源表 Service
 *
 * Date: 2015-05-12 15:39:57
 *
 * @author Code Generator
 *
 */
public interface SysResourceService extends EntityService<SysResource> {

    public int updateStatusById(Long id, Integer status);

    /**
     * 通过递归查找包含子资源
     * @param type
     * @param status
     * @return
     */
    public List<SysResource> queryByTypeAndStatus(String type, Integer status);
    
    /**
     * 将资源组装成dto
     * @param res
     * @return
     */
    public List<ResDto> toRes(Set<SysResource> res);

    /**
     * 通过ID删除， 并删除子资源
     * @param id
     * @return
     */
	public int deleteTree(Long id);

    /**
     * 修改排序值
     * @param ids
     * @param sortNums
     * @param parentIds
     */
	public void sort(List<SortInfo> sortInfo);

}
