package com.mirror.tk.core.module.user.service.impl;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mirror.tk.core.module.user.dao.UserDao;
import com.mirror.tk.core.module.user.domain.User;
import com.mirror.tk.core.module.user.service.UserService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("userService")
public class UserServiceImpl extends EntityServiceImpl<User, UserDao> implements UserService {

	@Override
	public User getUserByPhone(String phone) {
		return this.getEntityDao().findByPhone(phone);
	}

	@Override
	@Transactional
	public Boolean updateByStatus(Long id, Integer status) {
		return this.getEntityDao().updateByStatus(id, status) > 0;
	}

}
