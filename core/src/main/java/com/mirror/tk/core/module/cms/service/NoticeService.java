package com.mirror.tk.core.module.cms.service;

import java.util.List;

import com.mirror.tk.core.module.cms.domain.Notice;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 公告/信息/教程相关数据表 Service
 *
 * Date: 2016-11-01 23:10:03
 *
 * @author Code Generator
 *
 */
public interface NoticeService extends EntityService<Notice> {

	/**
	 * 根据类型获取通知公告列表
	 * @param type
	 * @return
	 */
	List<Notice> findByType(Integer type);

}
