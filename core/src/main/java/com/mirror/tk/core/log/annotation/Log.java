package com.mirror.tk.core.log.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Log {

	/** 模块 */
	String module();

	/** 模块名称 */
	String moduleName();

	/** 操作 */
	String action();

	/** 操作名称 */
	String actionName();

	/** 平台标识 */
	String tag() default "manage";

	/** 是否需要记录参数 */
	boolean needRecordParameter() default true;

	/** 忽略的参数 */
	String[] ignorParam() default "";

	/** 参数中文对应 */
	String[] paramMapping() default "";

}