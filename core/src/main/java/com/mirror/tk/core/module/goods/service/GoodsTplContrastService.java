package com.mirror.tk.core.module.goods.service;

import com.mirror.tk.core.module.goods.domain.GoodsTplContrast;
import com.mirror.tk.framework.common.service.EntityService;

/**
 * 商品模板对照表 Service
 *
 * Date: 2016-12-26 13:24:59
 *
 * @author Code Generator
 *
 */
public interface GoodsTplContrastService extends EntityService<GoodsTplContrast> {

}
