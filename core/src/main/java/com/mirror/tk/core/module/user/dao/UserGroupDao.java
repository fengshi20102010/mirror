package com.mirror.tk.core.module.user.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mirror.tk.core.module.user.domain.UserGroup;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 用户QQ/微信群组设置表 JPA Dao
 *
 * Date: 2017-01-01 22:30:28
 *
 * @author Code Generator
 *
 */
public interface UserGroupDao extends EntityJpaDao<UserGroup, Long> {

	@Query("from UserGroup where userId = ?1 and type = ?2")
	public List<UserGroup> getByUserIdAndType(Long userId, Integer type);

	@Modifying
	@Query(value = "delete from UserGroup where id = ?1 and userId = ?2")
	public int deleteByIdAndUserId(Long id, Long userId);

}
