package com.mirror.tk.core.utils.qrcode;

import java.awt.image.BufferedImage;

public interface LogoDrawing {

	void drawing(BufferedImage logoImg, BufferedImage qrcodeImg);
}
