package com.mirror.tk.core.module.goods.dao.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Lists;
import com.mirror.tk.core.module.goods.dao.GoodsSendLogCustomDao;
import com.mirror.tk.core.module.goods.domain.GoodsSendLog;
import com.mirror.tk.framework.common.dao.jdbc.PagedJdbcTemplate;
import com.mirror.tk.framework.common.dao.support.PageInfo;

@Repository("goodsSendLogCustomDao")
public class GoodsSendLogDaoImpl implements GoodsSendLogCustomDao{

	@Resource
	private PagedJdbcTemplate pagedJdbcTemplate;
	
	@Override
	public PageInfo<GoodsSendLog> getPagedSendLog(PageInfo<GoodsSendLog> page, Long userId, Date startTime,
			Date endTime, String title) {
		List<Object> params = Lists.newArrayList();
		StringBuffer sql = new StringBuffer();
		sql.append(" select send.* from goods_send_log send ");
		sql.append(" where user_id = ?");
		params.add(userId);
		if(startTime != null) {
			sql.append(" and send_time >= ?");
			params.add(startTime);
		}
		if(endTime != null) {
			sql.append(" and send_time <= ?");
			params.add(endTime);
		}
		if(StringUtils.isNotBlank(title)) {
			sql.append(" and title like '%"+title+"%'");
		}
		return pagedJdbcTemplate.queryMySql(page, sql.toString(), params.toArray(), GoodsSendLog.class);
	}

}
