package com.mirror.tk.core.module.sys.dao;

import com.mirror.tk.core.module.sys.domain.SysAttachment;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 系统_附件表 JPA Dao
 *
 * Date: 2016-01-08 09:54:33
 *
 * @author Code Generator
 *
 */
public interface SysAttachmentDao extends EntityJpaDao<SysAttachment, Long> {

	public SysAttachment getByAttachUrl(String attachUrl);
	
}

