package com.mirror.tk.core.module.sys.domain;


import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.google.common.collect.Maps;
import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 数据字典 Entity
 *
 * Date: 2016-03-30 10:31:38
 *
 * @author Acooly Code Generator
 */
@Entity
@Table(name = "sys_dictionaries")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SysDictionaries extends AbstractEntity {
	/** id */
	private Long id;
	
	/** 父id */
	private Long parentId;
	
	/** 值 */
	private String key;
	
	/** 键 */
	private String value;
	
	/** 类型 */
	private Integer type;
	
	/** 状态 */
	private Integer status;
	
	/** 排序号 */
	private Long sortNo;
	
	/** 描述 */
	private String description;
	
	/** 创建时间 */
	private Date createTime;
	
	/** 下级字典 */
	private List<SysDictionaries> subList;
	
	/** 一级key */
	public static Map<Integer, String> allOneLevelKey = Maps.newTreeMap();
	static {
		allOneLevelKey.put( 1,"sex");
		allOneLevelKey.put( 2,"marital_status");
		allOneLevelKey.put( 3,"industy");
		allOneLevelKey.put( 4,"income");
		allOneLevelKey.put( 5,"hobby");
		allOneLevelKey.put( 6,"task_sort");
		allOneLevelKey.put( 7,"credit_level");
		allOneLevelKey.put( 8,"area_major");
		allOneLevelKey.put( 9,"car_clubs_comm");
	}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getParentId(){
		return this.parentId;
	}

	public void setParentId(Long parentId){
		this.parentId = parentId;
	}

	public String getKey(){
		return this.key;
	}

	public void setKey(String key){
		this.key = key;
	}

	public String getValue(){
		return this.value;
	}

	public void setValue(String value){
		this.value = value;
	}

	public Integer getType(){
		return this.type;
	}

	public void setType(Integer type){
		this.type = type;
	}

	public Integer getStatus(){
		return this.status;
	}

	public void setStatus(Integer status){
		this.status = status;
	}

	public Long getSortNo(){
		return this.sortNo;
	}

	public void setSortNo(Long sortNo){
		this.sortNo = sortNo;
	}

	public String getDescription(){
		return this.description;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "parentId")
    @OrderBy("sortNo ASC")
    //@Where(clause = "status=1")
	public List<SysDictionaries> getSubList() {
		return subList;
	}

	public void setSubList(List<SysDictionaries> subList) {
		this.subList = subList;
	}

	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
