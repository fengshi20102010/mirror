package com.mirror.tk.core.biz.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class ReqSendLog {

	@ApiModelProperty(value="商品淘宝ID" , required=true)
	private Long numIid;
	
	@ApiModelProperty(value="发送时间(yyyy-MM-dd)" , required=true)
	private String sendTime;
	
	//1:qq,2:微信
	@ApiModelProperty(value="发送类型,1:qq,2:微信" , required=true)
	private Integer type;
	
	//发送群组
	@ApiModelProperty(value="发送群组" , required=true)
	private String group;
	
	@ApiModelProperty(value="发送次数" , required=true)
	private Integer count;
	
	@ApiModelProperty(value="当前用户ID" , required=false)
	private Long userId;
	
	@ApiModelProperty(value="发送人QQ",required=false)
	private String qq;
	
	@ApiModelProperty(value="佣金",required=true)
	private Integer commission;

	public Long getNumIid() {
		return numIid;
	}

	public void setNumIid(Long numIid) {
		this.numIid = numIid;
	}

	public String getSendTime() {
		return sendTime;
	}

	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getQq() {
		return qq;
	}

	public Integer getCommission() {
		return commission;
	}

	public void setCommission(Integer commission) {
		this.commission = commission;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}
}
