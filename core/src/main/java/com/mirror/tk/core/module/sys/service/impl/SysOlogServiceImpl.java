package com.mirror.tk.core.module.sys.service.impl;


import org.springframework.stereotype.Service;

import com.mirror.tk.core.module.sys.dao.SysOlogDao;
import com.mirror.tk.core.module.sys.domain.SysOlog;
import com.mirror.tk.core.module.sys.service.SysOlogService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("sysOlogService")
public class SysOlogServiceImpl extends EntityServiceImpl<SysOlog, SysOlogDao> implements SysOlogService {

}
