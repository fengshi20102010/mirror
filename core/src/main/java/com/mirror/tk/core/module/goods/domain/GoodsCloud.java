package com.mirror.tk.core.module.goods.domain;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mirror.tk.framework.common.domain.AbstractEntity;

/**
 * 商品推送云库表 Entity
 *
 * Date: 2016-11-01 23:10:19
 *
 * @author Acooly Code Generator
 */
@Entity
@Table(name = "goods_cloud")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class GoodsCloud extends AbstractEntity {
	/** ID */
	private Long id;
	
	/** 用户ID */
	private Long userId;
	
	/** 商品ID */
	private Long goodsId;
	
	/** 创建时间 */
	private Date createTime;
	
	/** 状态 */
	private Integer status;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Long getGoodsId(){
		return this.goodsId;
	}

	public void setGoodsId(Long goodsId){
		this.goodsId = goodsId;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Integer getStatus(){
		return this.status;
	}

	public void setStatus(Integer status){
		this.status = status;
	}

	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
