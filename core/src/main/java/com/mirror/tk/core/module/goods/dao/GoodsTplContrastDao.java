package com.mirror.tk.core.module.goods.dao;

import com.mirror.tk.core.module.goods.domain.GoodsTplContrast;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 商品模板对照表 JPA Dao
 *
 * Date: 2016-12-26 13:24:59
 *
 * @author Code Generator
 *
 */
public interface GoodsTplContrastDao extends EntityJpaDao<GoodsTplContrast, Long> {

}
