package com.mirror.tk.core.module.user.service.impl;


import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.mirror.tk.core.module.user.dao.UserPidDao;
import com.mirror.tk.core.module.user.domain.UserPid;
import com.mirror.tk.core.module.user.service.UserPidService;
import com.mirror.tk.framework.common.service.EntityServiceImpl;

@Service("userPidService")
public class UserPidServiceImpl extends EntityServiceImpl<UserPid, UserPidDao> implements UserPidService {

	@Override
	public UserPid getByUserIdAndId(Long id, Long userId) {
		return this.getEntityDao().getByUserIdAndId(id, userId);
	}

	@Override
	public List<UserPid> getByUserId(Long userId) {
		Map<String, Object> map = Maps.newHashMap();
		Map<String, Boolean> sortMap = Maps.newHashMap();
		map.put("EQ_userId", userId);
		sortMap.put("createTime", false);
		return query(map, sortMap);
	}

}
