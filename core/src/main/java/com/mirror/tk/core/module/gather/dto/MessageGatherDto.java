package com.mirror.tk.core.module.gather.dto;

import java.io.Serializable;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;


/**
 * Table:MessageGather 云产品库采集产品
 * @author tonysu
 * @date 2017-04-02 14:37:36
 *
 */
public class MessageGatherDto implements Serializable{

    private static final long serialVersionUID = -7964309821395968168L;

   	@ApiModelProperty(value = "ID", required = true)
	private Integer id;
	@ApiModelProperty(value = "采集时间", required = true)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Timestamp gatherDate;
	@ApiModelProperty(value = "采集QQ群", required = true)
	private String gatherQq;
	@ApiModelProperty(value = "发送消息人", required = true)
	private String senderQq;
	@ApiModelProperty(value = "发送给群组详情", required = true)
	private String sendTo;
	@ApiModelProperty(value = "群组名称", required = true)
	private String groupsetName;
	@ApiModelProperty(value = "发送内容", required = true)
	private String messageContent;
	@ApiModelProperty(value = "是否采集", required = true)
	private String gatherOrNot;
	@ApiModelProperty(value = "商品类型（天猫/淘宝）", required = true)
	private Integer type;
	@ApiModelProperty(value = "转换状态", required = true)
	private String transferStatus;
	@ApiModelProperty(value = "发送状态", required = true)
	private String sendStatus;
	@ApiModelProperty(value = "发送时间", required = true)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private String sendTime;
	@ApiModelProperty(value = "发送次数", required = true)
	private Integer sendNum;
	@ApiModelProperty(value = "淘宝日志", required = true)
	private String taobaolog;
	@ApiModelProperty(value = "微信发送图片等待几秒发送文字", required = true)
	private Integer weixinWaitTime;
	@ApiModelProperty(value = "检测图片是否发送成功", required = true)
	private String picCheck;
	@ApiModelProperty(value = "插入时间", required = true)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Timestamp insertTime;
	@ApiModelProperty(value = "用戶ID", required = true)
   	private Long userId;
	
   	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Integer getId(){
		return id;
	}
	public void setId(Integer id){
		this.id = id;
	}
	public Timestamp getGatherDate(){
		return gatherDate;
	}
	public void setGatherDate(Timestamp gatherDate){
		this.gatherDate = gatherDate;
	}

	public String getGatherQq() {
		return gatherQq;
	}
	public void setGatherQq(String gatherQq) {
		this.gatherQq = gatherQq;
	}
	public String getSenderQq() {
		return senderQq;
	}
	public void setSenderQq(String senderQq) {
		this.senderQq = senderQq;
	}
	public String getSendTo(){
		return sendTo;
	}
	public void setSendTo(String sendTo){
		this.sendTo = sendTo;
	}
	public String getGroupsetName(){
		return groupsetName;
	}
	public void setGroupsetName(String groupsetName){
		this.groupsetName = groupsetName;
	}
	public String getMessageContent(){
		return messageContent;
	}
	public void setMessageContent(String messageContent){
		this.messageContent = messageContent;
	}
	public String getGatherOrNot(){
		return gatherOrNot;
	}
	public void setGatherOrNot(String gatherOrNot){
		this.gatherOrNot = gatherOrNot;
	}
	public Integer getType(){
		return type;
	}
	public void setType(Integer type){
		this.type = type;
	}
	public String getTransferStatus(){
		return transferStatus;
	}
	public void setTransferStatus(String transferStatus){
		this.transferStatus = transferStatus;
	}
	public String getSendStatus(){
		return sendStatus;
	}
	public void setSendStatus(String sendStatus){
		this.sendStatus = sendStatus;
	}
	public String getSendTime(){
		return sendTime;
	}
	public void setSendTime(String sendTime){
		this.sendTime = sendTime;
	}
	public Integer getSendNum(){
		return sendNum;
	}
	public void setSendNum(Integer sendNum){
		this.sendNum = sendNum;
	}
	public String getTaobaolog(){
		return taobaolog;
	}
	public void setTaobaolog(String taobaolog){
		this.taobaolog = taobaolog;
	}
	public Integer getWeixinWaitTime(){
		return weixinWaitTime;
	}
	public void setWeixinWaitTime(Integer weixinWaitTime){
		this.weixinWaitTime = weixinWaitTime;
	}
	public String getPicCheck(){
		return picCheck;
	}
	public void setPicCheck(String picCheck){
		this.picCheck = picCheck;
	}
	public Timestamp getInsertTime(){
		return insertTime;
	}
	public void setInsertTime(Timestamp insertTime){
		this.insertTime = insertTime;
	}
	@ApiModelProperty(value = "时间间隔", required = false)
	private String sendCycleTime ;
	public String getSendCycleTime() {
		return sendCycleTime;
	}
	public void setSendCycleTime(String sendCycleTime) {
		this.sendCycleTime = sendCycleTime;
	}
	@ApiModelProperty(value = "采集群号", required = false)
	private String gatherGroup;


	public String getGatherGroup() {
		return gatherGroup;
	}
	public void setGatherGroup(String gatherGroup) {
		this.gatherGroup = gatherGroup;
	}

	
}
