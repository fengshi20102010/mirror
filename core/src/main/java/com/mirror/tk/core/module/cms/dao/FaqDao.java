package com.mirror.tk.core.module.cms.dao;

import com.mirror.tk.core.module.cms.domain.Faq;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 常见问题 JPA Dao
 *
 * Date: 2016-11-01 23:10:31
 *
 * @author Code Generator
 *
 */
public interface FaqDao extends EntityJpaDao<Faq, Long> {

}
