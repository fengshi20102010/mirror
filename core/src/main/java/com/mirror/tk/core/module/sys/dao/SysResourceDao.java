package com.mirror.tk.core.module.sys.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mirror.tk.core.module.sys.domain.SysResource;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 系统资源表 JPA Dao
 *
 * Date: 2015-05-12 15:39:57
 *
 * @author Code Generator
 *
 */
public interface SysResourceDao extends EntityJpaDao<SysResource, Long> {

    @Modifying
    @Query(value = "update SysResource t set t.status=?2 where id=?1")
    public int updateStatusById(Long id, Integer status);

    @Modifying
    @Query(value = "update SysResource set sortNo=?2, parentId=?3 where id=?1")
    public int sort(Long id, Long sortNum, Long parentId);

    @Modifying
    @Query(value = "delete from SysResource where id=?1 or parentId=?1")
	public int deleteTree(Long id);
    
}
