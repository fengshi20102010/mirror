package com.mirror.tk.core.module.user.dao;

import com.mirror.tk.core.module.user.domain.UserSendTpl;
import com.mirror.tk.framework.common.dao.jpa.EntityJpaDao;

/**
 * 用户信息扩展表,将不常用的信息放入这张表 JPA Dao
 *
 * Date: 2016-11-01 23:08:57
 *
 * @author Code Generator
 *
 */
public interface UserSendTplDao extends EntityJpaDao<UserSendTpl, Long> {

	public UserSendTpl findByUserId(Long userId);
}
