package com.mirror.tk.core.test.module.sms;

import java.io.IOException;

import javax.annotation.Resource;

import org.junit.Test;

import com.mirror.tk.core.module.sms.service.ValidateCodeContext;
import com.mirror.tk.core.module.sms.service.ValidateCodeService;
import com.mirror.tk.core.sms.exception.SMSException;
import com.mirror.tk.core.sms.service.SendTemplateSmsService.TEMPLATE_CODE;
import com.mirror.tk.core.test.support.BaseTest;

public class ValidateCodeServiceTest extends BaseTest {

	@Resource
	private ValidateCodeService validateCodeService;
	
	@Test
	public void builderCodeTest() throws IOException, SMSException{
		String phone = "13617635410";
		int expSeconds = 60 * 5;
		ValidateCodeContext content = validateCodeService.builderCode(phone, TEMPLATE_CODE.register, expSeconds);
		content.sendSms(phone, TEMPLATE_CODE.register);
		logger.debug(content.getVaildateCode());
	}
	
	@Test
	public void vaildateCodeTest(){
		String phone = "13617635410";
		String code = "950521";
		logger.debug(validateCodeService.vaildateCode(phone, TEMPLATE_CODE.register, code, true));
	}
	
}

