package com.mirror.tk.core.test.module.sms;

import javax.annotation.Resource;

import org.junit.Test;

import com.mirror.tk.core.sms.work.SmsSendService;
import com.mirror.tk.core.test.support.BaseTest;

public class SmsTest extends BaseTest {

	@Resource(name = "ihuyiSmsSendService")
	SmsSendService smsSendService;
	
	@Test
	public void testSend() throws Exception{
		String message = "根据报名内容您已优先获得\"仁安里\"任务的领取权，请准备好网约车的手机端截图于今天15:00前发送至微信客服号iceet001，如逾期，该权有可能移至下一位车主。热线：61820001";
		String list = "13617635410";
		for (String phone : list.split(",")) {
			smsSendService.send(phone, message);
		}
		logger.debug("执行成功！！！");
	}
	
}
