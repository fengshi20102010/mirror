package com.mirror.tk.core.test.support;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.test.context.ContextConfiguration;

import com.mirror.tk.framework.common.test.SpringTransactionalTests;

//@ContextConfiguration(locations = { "classpath*:spring/applicationContext-test-main.xml" })
@ContextConfiguration(locations = { "classpath*:spring/applicationContext-main.xml" })
public class BaseTest extends SpringTransactionalTests {

	protected static final Logger logger = LogManager.getLogger();

}
