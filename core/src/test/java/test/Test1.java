package test;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.annotation.Rollback;

import com.alibaba.fastjson.JSON;
import com.mirror.tk.core.biz.common.Result;
import com.mirror.tk.core.biz.dto.GoodsTypeDto;
import com.mirror.tk.core.biz.dto.ResGoodsDetail;
import com.mirror.tk.core.biz.dto.ResGoodsInfo;
import com.mirror.tk.core.biz.dto.ResGoodsSendLog;
import com.mirror.tk.core.biz.dto.ResSendTpl;
import com.mirror.tk.core.biz.dto.ResTaobaoGoods;
import com.mirror.tk.core.biz.dto.ResUserSendContent;
import com.mirror.tk.core.biz.req.ReqAddGoods;
import com.mirror.tk.core.biz.req.ReqGenUserSend;
import com.mirror.tk.core.biz.req.ReqGetSendLog;
import com.mirror.tk.core.biz.req.ReqGoodsInfo;
import com.mirror.tk.core.biz.req.ReqSaveUserTpl;
import com.mirror.tk.core.biz.req.ReqSendLog;
import com.mirror.tk.core.biz.req.ReqTransformUrl;
import com.mirror.tk.core.biz.service.GoodsBiz;
import com.mirror.tk.core.biz.service.TaobaoBiz;
import com.mirror.tk.core.biz.service.UserBiz;
import com.mirror.tk.core.module.user.domain.UserPid;
import com.mirror.tk.core.test.support.BaseTest;
import com.mirror.tk.framework.common.dao.support.PageInfo;
import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.TbkItemInfoGetRequest;
import com.taobao.api.response.TbkItemInfoGetResponse;

@Rollback(value=false)
public class Test1 extends BaseTest{

	@Autowired
	GoodsBiz biz;
	@Autowired
	TaobaoBiz taobaoBiz; 
	@Autowired
	UserBiz userBiz;
	
	@Value("${taobao.app.key}")
	private String appkey ;
	
	@Value("${taobao.app.secret}")
	private String secret;
	
	@Value("${taobao.app.url}")
	private String url;
	
	@Test
	public void testType() {
		Result<List<GoodsTypeDto>> result = biz.getAllGoodsType();
		System.out.println();
	}
	
	@Test
	public void addSendLog(){
		ReqSendLog req = new ReqSendLog();
		req.setNumIid(530452925283l);
		req.setSendTime("2016-05-04 21:00:00");
		req.setType(1);
		req.setGroup("122520757");
		req.setCount(2);
		req.setUserId(1l);
		req.setQq("282726264");
		Result<Void> r = biz.addSendLog(req);
		System.out.println();
	}
	
	@Test
	public void getPagedSendLog(){
		ReqGetSendLog req = new ReqGetSendLog();
		req.setUserId(1l);
		req.setPageSize(1);
		req.setCurrentPage(1);
		 Result<PageInfo<ResGoodsSendLog>> r = biz.getPagedSendLog(req);
		 System.out.println();
	}
	
	@Test
	public void taobaos() throws ApiException {
//		TaobaoClient client = new DefaultTaobaoClient(url, appkey, secret);
//		TbkItemInfoGetRequest req = new TbkItemInfoGetRequest();
//		req.setFields("num_iid,title,pict_url,small_images,reserve_price,zk_final_price,user_type,provcity,item_url");
//		req.setPlatform(1L);
//		req.setNumIids("541955853438");
//		TbkItemInfoGetResponse rsp = client.execute(req);
//		System.out.println(JSON.toJSONString(rsp.getBody()));
		
//		http://gw.api.taobao.com/router/rest
//		23457601
//		29d2a33b3e79abb2cabc37b84ffba17d
		TaobaoClient client = new DefaultTaobaoClient(url, appkey, secret);
		TbkItemInfoGetRequest req = new TbkItemInfoGetRequest();
//		req.setFields("num_iid,title,pict_url,small_images,reserve_price,zk_final_price,user_type,provcity,item_url");
		req.setPlatform(1L);
		req.setNumIids("541955853438");
		TbkItemInfoGetResponse rsp = null;
		try {
			rsp = client.execute(req);
		} catch (ApiException e) {
		}
	}
	
	@Test
	public void getGoodsInfo() {
		ReqGoodsInfo req = new ReqGoodsInfo();
		req.setCat(2);
		req.setCurrentPage(1);
		req.setPageSize(10);
		req.setSort((byte) -3);//0-综合，1-最新，2-销量，3-佣金，4-价格，5-剩余时间
		Result<PageInfo<ResGoodsInfo>> r = biz.getGoodsInfo(req);
		System.out.println(JSON.toJSONString(r));
	}
	
	@Test
	public void getDetail() {
		Result<ResGoodsDetail> r = biz.getDetail(40169034917l);
		System.out.println();
	}
	
	@Test
	public void fetchGoods(){
		Result<ResTaobaoGoods> r = taobaoBiz.fetchGoods("https://detail.tmall.com/item.htm?id=5413438");
		System.out.println();
	}
	
	@Test
	public void addGoods() throws Exception {
		ReqAddGoods req = new ReqAddGoods();
		req.setGoodsUrl("https://item.taobao.com/item.htm?id=545503329821");
		req.setCouponUrl("http://shop.m.taobao.com/shop/coupon.htm?seller_id=2769581941&activity_id=9f08cf323787423495958ad47e081051");
		req.setSubTitle("这个是测试");
		req.setMemo("纯棉材质，舒适透气，保暖亲肤，耐磨耐穿，不起球不褪色，日韩潮流新品，时尚百搭，潮范儿十足，女孩必备。");
		req.setPlan(1);
		req.setAttr(3);
		req.setAdTime("2017-02-28");
		req.setTypeId(25l);
		req.setPlanUrl("http://baidu.com");
		req.setQq("272925327");
		Result<Long> result = biz.addGoods(req);
		System.out.println(result);
	}
	
	@Test
	public void transformUrl() {
		ReqTransformUrl req = new ReqTransformUrl();
		req.setCouponUrl("https://taoquan.taobao.com/coupon/unify_apply.htm?sellerId=2121604138&activityId=d90b07fe25d04117b1ad434a91bd1a9c&scene=taobao_shop");
		req.setGorup(6l);
		req.setUserId(1l);
		req.setUrl("https://detail.tmall.com/item.htm?id=40169034917");
		Result<String> r = biz.transformUrl(req);
		System.out.println();
	}
	
	@Test
	public void getListUserPid() {
		Result<List<UserPid>> r = userBiz.getListUserPid(1l);
		System.out.println();
	}
	
	@Test
	public void getListTpl(){
		Result<List<ResSendTpl>> r = biz.getListTpl();
		System.out.println(JSON.toJSONString(r, true));
	}
	
	@Test
	public void saveUserTpl() {
		ReqSaveUserTpl req = new ReqSaveUserTpl();
		req.setUserId(1l);
		req.setGroup(6l);
		req.setContent("[商品图片][换行符][原标题][换行符][短标题][换行符][介绍文案][换行符][店铺类型][换行符][原价][换行符][券后价][换行符][销量][换行符][包邮][换行符][佣金比例][换行符][领券链接][换行符][券满][换行符][券减][换行符][优惠券剩余数量][换行符][传统模式下单链接][换行符][二合一模式下单链接][换行符]");
		Result<Void> result = biz.saveUserTpl(req);
		System.out.println();
	}
	
	@Test
	public void generateUserSend() {
		ReqGenUserSend req = new ReqGenUserSend();
		req.setUserId(1l);
		req.setNumIid(40169034917l);
		Result<ResUserSendContent> r = biz.generateUserSend(req);
		System.out.println(JSON.toJSONString(r, true));
	}
	
}
