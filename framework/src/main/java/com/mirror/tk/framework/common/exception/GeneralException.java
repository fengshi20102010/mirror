package com.mirror.tk.framework.common.exception;

/**
 * 一般业务异常（非事务异常）
 */
public class GeneralException extends AbstractI18NMessageException {

	private static final long serialVersionUID = 1L;

	public GeneralException() {
		super();
	}

	public GeneralException(String errorCode, String[] errorArgs, String debugMessage, Throwable cause) {
		super(errorCode, errorArgs, debugMessage, cause);
	}

	public GeneralException(String errorCode, String[] errorArgs, Throwable cause) {
		super(errorCode, errorArgs, cause);
	}

	public GeneralException(String errorCode, String[] errorArgs) {
		super(errorCode, errorArgs);
	}

	public GeneralException(String debugMessage, Throwable cause) {
		super(debugMessage, cause);
	}

	public GeneralException(String debugMessage) {
		super(debugMessage);
	}

}
