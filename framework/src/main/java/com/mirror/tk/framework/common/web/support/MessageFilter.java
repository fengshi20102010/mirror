package com.mirror.tk.framework.common.web.support;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * 为了信息在redirect时不会消失,BaseController使用session暂存message． 而此filter负责把消息 从session搬回到 request．
 */
public class MessageFilter implements Filter {
	@SuppressWarnings("unchecked")
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		List<Object> messages = (List<Object>) request.getSession().getAttribute("messages");
		if (messages != null) {
			request.setAttribute("messages", messages);
			request.getSession().removeAttribute("messages");
		}
		chain.doFilter(req, res);
	}

	public void init(FilterConfig filterConfig) {
		
	}

	public void destroy() {
		
	}
	
}
