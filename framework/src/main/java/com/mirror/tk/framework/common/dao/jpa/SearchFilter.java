package com.mirror.tk.framework.common.dao.jpa;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import com.mirror.tk.framework.common.exception.BusinessException;
import com.google.common.collect.Lists;

public class SearchFilter {

	public enum Operator {
		EQ, NOTEQ, LIKE, LLIKE, RLIKE, NLIKE /** NOT Like **/, GT, LT, GTE, LTE, IN, NOTIN, NULL, NOTNULL;

		public static Operator getOpeartor(String name) {
			for (Operator op : Operator.values()) {
				if (op.toString().equals(name)) {
					return op;
				}
			}
			return null;
		}
	}

	public String fieldName;
	public String originalFieldName;
	public Object value;
	public Object originalValue;
	public Operator operator;

	public SearchFilter(String fieldName, Operator operator, Object value) {
		this.fieldName = fieldName;
		this.value = value;
		this.originalValue = value;
		this.operator = operator;
	}

	public SearchFilter(String fieldName, String originalFieldName, Operator operator, Object value) {
		this.fieldName = fieldName;
		this.originalFieldName = originalFieldName;
		this.value = value;
		this.originalValue = value;
		this.operator = operator;
	}

	public static List<SearchFilter> parse(Map<String, Object> searchParams) {
		List<SearchFilter> filters = Lists.newArrayList();
		for (Entry<String, Object> entry : searchParams.entrySet()) {
			String[] names = StringUtils.split(entry.getKey(), "_");
			if (names.length != 2) {
				throw new IllegalArgumentException(entry.getKey() + " is not a valid search filter name");
			}
			Object value = null;
			if (!StringUtils.contains(names[1], "NULL")) {
				value = entry.getValue();
				if (value == null || StringUtils.isBlank(String.valueOf(value))) {
					continue;
				}
			}
			SearchFilter filter = new SearchFilter(names[1], Operator.valueOf(names[0]), value);
			filters.add(filter);
		}
		return filters;
	}

	public static List<SearchFilter> parseMap(Map<String, Object> searchParams) {
		List<SearchFilter> filters = Lists.newArrayList();
		if (searchParams == null) {
			return Collections.emptyList();
		}
		for (Entry<String, Object> entry : searchParams.entrySet()) {
			String key = entry.getKey();
			if (!StringUtils.contains(key, '_')) {
				throw new BusinessException("查询条件不符合规范!");
			}
			int index = StringUtils.indexOfAny(key, "_");
			String[] names = StringUtils.split(key, "_", (index + 1));
			if (names.length < 2) {
				throw new BusinessException("查询条件截取出错!");
			}
			Operator op = Operator.getOpeartor(names[0]);
			if (op == null) {
				throw new BusinessException("查询方式未定义!");
			}
			String filedName = names[1];
			Object value = entry.getValue();
			if (value == null || StringUtils.isBlank(String.valueOf(value))) {
				continue;
			}
			filters.add(new SearchFilter(filedName, coverdColumName(filedName),	op, value));
		}
		return filters;
	}

	public static String coverdColumName(String columName) {
		StringBuffer str = new StringBuffer();
		for (int i = 0; i < columName.length(); i++) {
			char c = columName.charAt(i);
			if (!Character.isLowerCase(c) && c != 46 && c != 95) {
				str.append((char) 95).append((char) (c + 32));
			} else {
				str.append(c);
			}
		}
		return str.toString();
	}
	
}
