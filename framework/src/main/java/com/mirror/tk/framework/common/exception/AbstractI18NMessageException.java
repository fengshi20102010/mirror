package com.mirror.tk.framework.common.exception;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * 异常抽象类,将错误信息国际化
 */
public abstract class AbstractI18NMessageException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/** 国际化支援文件 */
	private static final String ERROR_BUNDLE = "i18n/errors";

	/** 错误信息的i18n ResourceBundle. */
	static protected ResourceBundle rb = ResourceBundle.getBundle(ERROR_BUNDLE, LocaleContextHolder.getLocale());

	/** 错误代码,默认为未知错误 */
	private String errorCode = "UNKNOW_ERROR";

	/** 错误信息中的参数 */
	protected String[] errorArgs;

	/** 调试信息 */
	protected String debugMessage;

	public AbstractI18NMessageException() {
		this(null, null, null);
	}

	public AbstractI18NMessageException(String debugMessage) {
		this(null, null, debugMessage, null);
	}

	public AbstractI18NMessageException(String debugMessage, Throwable cause) {
		this(null, null, debugMessage, cause);
	}

	public AbstractI18NMessageException(String errorCode, String[] errorArgs) {
		this(errorCode, errorArgs, null, null);
	}

	public AbstractI18NMessageException(String errorCode, String[] errorArgs, Throwable cause) {
		this(errorCode, errorArgs, null, cause);
	}

	public AbstractI18NMessageException(String errorCode, String[] errorArgs, String debugMessage, Throwable cause) {
		super(debugMessage, cause);
		this.errorCode = errorCode;
		this.errorArgs = errorArgs;
	}

	public String getResourceMessage() {
		if (StringUtils.isBlank(errorCode)) {
			return getMessage();
		}
		String message = null;
		try {
			message = rb.getString(errorCode);
		} catch (MissingResourceException mse) {
			message = "ErrorCode is: " + errorCode + ", but can't get the message of the Error Code";
		}
		// 将出错信息中的参数代入到出错信息。
		if (errorArgs != null) {
			message = MessageFormat.format(message, (Object[]) errorArgs);
		}
		return message;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

}

