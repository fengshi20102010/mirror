package com.mirror.tk.framework.utils;

public interface CollectionFilter<T, V> {

	public boolean filter(T entity, V o);

}
