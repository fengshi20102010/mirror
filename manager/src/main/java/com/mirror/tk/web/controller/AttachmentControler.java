package com.mirror.tk.web.controller;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.google.common.collect.Lists;
import com.mirror.tk.core.module.sys.domain.SysAttachment;
import com.mirror.tk.core.module.sys.service.SysAttachmentService;
import com.mirror.tk.framework.common.web.support.JsonListResult;
import com.mirror.tk.framework.common.web.support.JsonResult;
import com.mirror.tk.framework.utils.Collections3;
import com.mirror.tk.web.common.Constant;
import com.mirror.tk.web.common.support.SessionFace;
import com.mirror.tk.web.common.support.SessionUser;

@Controller
@RequestMapping("global")
public class AttachmentControler extends CommonController {

	@Resource
	private SysAttachmentService attachmentService;

	@ResponseBody
	@RequestMapping(value = "upload", method = RequestMethod.POST)
	public JsonListResult<SysAttachment> upload(HttpServletRequest request, HttpServletResponse response) {
		JsonListResult<SysAttachment> result = new JsonListResult<SysAttachment>();
		// 上传附件需要合法用户（本平台登录用户）
		if(!SessionFace.isUserLogin(request)){
			result.setCode(Constant.Application.FAILURE);
			result.setMessage(Constant.Application.ILLEGAL_ACCESS);
			return result;
		}
		SessionUser su = SessionFace.getSessionUser(request);
		String sysKey = "sys";
		// 转换附件对象
		CommonsMultipartResolver mutilpartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());
		List<SysAttachment> list = Lists.newArrayList();
		// request如果是Multipart类型
		if (mutilpartResolver.isMultipart(request)) {
			// 强转成 MultipartHttpServletRequest
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			// 获取MultipartFile类型文件
			Iterator<String> it = multiRequest.getFileNames();
			while (it != null && it.hasNext()) {
				MultipartFile fileDetail = multiRequest.getFile(it.next());
				if (fileDetail != null) {
					SysAttachment attach = attachmentService.upload(su.getId(), sysKey, "sys", fileDetail);
					list.add(attach);
				}
			}
		}
		if (Collections3.isEmpty(list)) {
			result.setCode(Constant.Application.FAILURE);
			result.setMessage(Constant.Application.UPLOAD_NO_DATA_FOUND);
			return result;
		}
		result.setRows(list);
		result.setSuccess(true);
		result.setCode(Constant.Application.SUCCESS);
		result.setTotal(Long.valueOf(list.size()));
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "upload", method = RequestMethod.DELETE)
	public JsonResult delete(HttpServletRequest request, 
			@RequestParam(value = "url", required = true) String url) {
		JsonResult result = new JsonResult(false);
		// 删除附件只能删除自己的附件，必须登录
		if(!SessionFace.isUserLogin(request)){
			result.setCode(Constant.Application.FAILURE);
			result.setMessage(Constant.Application.ILLEGAL_ACCESS);
			return result;
		}
		try {
			if (url.indexOf(",") > 0) {
				for (String u : url.split(",")) {
					attachmentService.delete(SessionFace.getSessionUser(request).getId(), u);
				}
			} else {
				attachmentService.delete(SessionFace.getSessionUser(request).getId(), url);
			}
			result.setSuccess(true);
			result.setCode(Constant.Application.SUCCESS);
			return result;
		} catch (Exception e) {
			result.setCode(Constant.Application.FAILURE);
			result.setMessage("删除失败！");
			return result;
		}
	}
	
}
