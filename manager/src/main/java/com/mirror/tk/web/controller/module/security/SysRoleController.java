package com.mirror.tk.web.controller.module.security;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import com.mirror.tk.core.module.sys.domain.SysResource;
import com.mirror.tk.core.module.sys.domain.SysRole;
import com.mirror.tk.core.module.sys.service.SysResourceService;
import com.mirror.tk.core.module.sys.service.SysRoleService;
import com.mirror.tk.framework.common.dao.support.PageInfo;
import com.mirror.tk.framework.common.web.support.JsonListResult;
import com.mirror.tk.web.common.Constant;
import com.mirror.tk.web.controller.CommonController;

@Controller
@RequestMapping(value = "/module/security/role")
public class SysRoleController extends CommonController {

	@Resource
	private SysRoleService sysRoleService;
	@Resource
	private SysResourceService sysResourceService;

	// 列表
	private static final String ROLE_LIST = "module/security/role/sys_role_list";
	// 详情
	private static final String ROLE_VIEW = "module/security/role/sys_role_view";
	// 添加、修改
	private static final String ROLE_DETAIL = "module/security/role/sys_role_detail";

	// 列表
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public String list(HttpServletRequest request, ModelMap map) {
		return ROLE_LIST;
	}

	@ResponseBody
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public JsonListResult<SysRole> ajaxList(HttpServletRequest request) {
		Map<String, Object> searchMap = new HashMap<String, Object>();
		Map<String, Boolean> sortMap = new HashMap<String, Boolean>();
		PageInfo<SysRole> pageInfo = sysRoleService.query(fromJsonListResult(request), searchMap, sortMap);
		return toJsonListResult(pageInfo);
	}

	// 增加-查看
	@RequestMapping(value = "add", method = RequestMethod.GET)
	public String add(HttpServletRequest request, Map<String, Object> map) {
		// 获取资源
		map.put("resc", sysResourceService.toRes(null));
		return ROLE_DETAIL;
	}

	// 增加-保存
	@RequestMapping(value = "add", method = RequestMethod.POST)
	public String postAdd(HttpServletRequest request, Map<String, Object> map, SysRole sysRole,
			@RequestParam(value = "resIds[]", required = false) Long[] resIds) {
		try {
			// 主键ID
			sysRole.setId(null);
			// 拥有的资源
			sysRole.setRescs(new HashSet<SysResource>());
			if (resIds != null) {
				for (Long resid : resIds) {
					SysResource res = sysResourceService.get(resid);
					sysRole.getRescs().add(res);
				}
			}
			sysRoleService.save(sysRole);
		} catch (Exception ex) {
			logger.error("Save Method (inster) SysRole Error : " + sysRole.toString(), ex);
			// 增加失败
			return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.SAVE_FAILURE), map);
		}
		// 操作提示
		return super.operSuccess(new RequestContext(request).getMessage(Constant.I18nMessage.SAVE_SUCCESS), ROLE_DETAIL, map);
	}

	// 修改-查看
	@RequestMapping(value = "update", method = RequestMethod.GET)
	public String update(HttpServletRequest request, Map<String, Object> map, @RequestParam(value = "id") Long id) {
		SysRole sysRole = sysRoleService.get(id);
		if (sysRole == null) {
			// 没有记录
			return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
		}
		map.put("item", sysRole);
		// 获取资源
		map.put("resc", sysResourceService.toRes(sysRole.getRescs()));
		return ROLE_DETAIL;
	}

	// 修改-保存
	@RequestMapping(value = "update", method = RequestMethod.POST)
	public String postUpdate(HttpServletRequest request, Map<String, Object> map, SysRole sysRole,
			@RequestParam(value = "resIds[]", required = false) Long[] resIds) {
		if (sysRole == null || sysRole.getId() == null) {
			// 没有记录
			return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
		}
		try {
			SysRole sourceSysRole = sysRoleService.get(sysRole.getId());
			if (sourceSysRole == null) {
				// 没有记录
				return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
			}
			// 角色名称
			sourceSysRole.setRoleName(sysRole.getRoleName());
			// 描述信息
			sourceSysRole.setDescription(sysRole.getDescription());
			// 拥有的资源
			sourceSysRole.setRescs(new HashSet<SysResource>());
			if (resIds != null) {
				for (Long resid : resIds) {
					SysResource res = sysResourceService.get(resid);
					sourceSysRole.getRescs().add(res);
				}
			}

			sysRoleService.update(sourceSysRole);
			sysRole = sourceSysRole;
		} catch (Exception ex) {
			logger.error("Save Method (Update) SysRole Error : " + sysRole.toString(), ex);
			// 修改失败
			return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.SAVE_FAILURE), map);
		}
		// 操作提示
		return super.operSuccess(new RequestContext(request).getMessage(Constant.I18nMessage.SAVE_SUCCESS), String.format(ROLE_DETAIL + "?id=%s", sysRole.getId()), map);
	}

	// 查看
	@RequestMapping(value = "view")
	public String show(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id) {
		SysRole sysRole = sysRoleService.get(id);
		if (sysRole == null) {
			// 没有记录
			return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
		}
		map.put("item", sysRole);
		return ROLE_VIEW;
	}

}
