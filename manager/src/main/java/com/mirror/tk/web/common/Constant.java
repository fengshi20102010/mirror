package com.mirror.tk.web.common;

public final class Constant {

    /**
     * 全局定义
     */
    public static final class Global {

        /** 状态 */
        public static enum State {
            Enable(1, "启用"),
            Disable(0, "禁用");

            private int value;
            private String title;

            private State(int value, String title) {
                this.value = value;
                this.title = title;
            }

            public int getValue() {
                return value;
            }

            public String getTitle() {
                return title;
            }
        }
    }

	public static class Application{

		/**
		 * 提示信息页面样式控制
		 */
		public static final String COMMON_SUCCESS_ALERTINFO = "success";
		public static final String COMMON_INFO_ALERTINFO = "info";
		public static final String COMMON_WARNING_ALERTINFO = "warning";
		public static final String COMMON_DANGER_ALERTINFO = "danger";
		
		public static final String SUCCESS = "0";
		public static final String FAILURE = "-1";
		
		public static final String UNAUTHORIZED = "401";
		public static final String UNAUTHORIZED_MSG = "TOKEN错误或TOKEN已过期，请重新登录后访问";
		public static final String ILLEGAL_ACCESS = "非法访问";
		public static final String UPLOAD_NO_DATA_FOUND = "没有找到上传对像";
		
	}

    public static class I18nMessage {

        public static final String RECORD_NOT_FOUND = "记录未找到";

        public static final String SAVE_SUCCESS = "保存成功";
        public static final String SAVE_FAILURE = "保存失败";

        public static final String DEL_SUCCESS = "删除成功";
        public static final String DEL_FAILURE = "删除失败";
        
        public static final String UPDATE_FAILURE = "修改成功";
        public static final String UPDATE_SUCCESS = "修改失败";
        
        public static final String SORT_SUCCESS = "排序成功";
        public static final String SORT_FAILURE = "排序失败";
        
        public static final String STATUS_FAILURE = "修改成功";
        public static final String STATUS_SUCCESS = "修改失败";
        
        public static final String CHECK_RECORD_EXISTS = "check.record.exists";
        public static final String CHECK_RECORD_NOT_EXISTS = "check.record.notexists";
        
        public static final String NEED_ENTER_PASSWORD = "need.enter.password";

        public static final String LOGIN_ERROR = "登陆错误";

        public static final String USER_PASS_FORMAT_ERR = "用户密码格式错误";

        public static final String USER_PASS_ORIGINAL_CHECK_ERR= "user.pass.original.check.err" ;

        public static final String USER_PASS_UPDATE_SUCCESS = "user.pass.update.success";

    }

    public static class BusinessMessage{
    	public static final String ADVERT_NOT_EXIST = "advert.not.exist";
    	public static final String DURATION_TOO_LONG = "duration.too.long";
    }
    
}
