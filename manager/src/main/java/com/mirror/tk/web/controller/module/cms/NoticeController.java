package com.mirror.tk.web.controller.module.cms;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import com.mirror.tk.core.log.annotation.Log;
import com.mirror.tk.core.module.cms.domain.Notice;
import com.mirror.tk.core.module.cms.service.NoticeService;
import com.mirror.tk.framework.common.web.support.JsonListResult;
import com.mirror.tk.framework.common.web.support.JsonResult;
import com.mirror.tk.web.common.Constant;
import com.mirror.tk.web.controller.CommonController;


@Controller
@RequestMapping(value = "/module/cms/notice")
public class NoticeController extends CommonController {

    @Resource
    private NoticeService noticeService;
    
    // 列表
    private static final String NOTICE_LIST = "module/cms/notice/notice_list";
    // 详情
    private static final String NOTICE_VIEW = "module/cms/notice/notice_view";
    // 新增/修改
    private static final String NOTICE_DETAIL = "module/cms/notice/notice_detail";

    // 列表-查看
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(HttpServletRequest request, ModelMap map) {
        return NOTICE_LIST;
    }
    
    // 列表-数据
    @ResponseBody
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public JsonListResult<Notice> ajaxList(HttpServletRequest request,
            @RequestParam(value="name", required=false) String name,
            @RequestParam(value="status", required=false) Integer status,
            @RequestParam(value="type", required=false) Integer type
            ) {
        Map<String, Object> searchMap = new HashMap<String, Object>();
        Map<String, Boolean> sortMap = new HashMap<String, Boolean>();

        //公告名称
        if(StringUtils.isNotBlank(name)){
            searchMap.put("LIKE_name", name);
        }
        //公告状态
        if(status!=null){
            searchMap.put("EQ_status", status);
        }
        //公告类型
        if(type!=null){
            searchMap.put("EQ_type", type);
        }
        return toJsonListResult(noticeService.query(fromJsonListResult(request), searchMap, sortMap));
    }

    // 新增-查看
    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add(HttpServletRequest request, ModelMap map){
        return NOTICE_DETAIL;
    }

    // 新增-保存
    @ResponseBody
    @RequestMapping(value = "add", method = RequestMethod.POST)
    @Log(module = "Notice", moduleName = "公告/信息/教程相关数据表", action = "ajaxAdd", actionName = "公告/信息/教程相关数据表新增")
    public JsonResult ajaxAdd(HttpServletRequest request, Notice notice){
        JsonResult result = new JsonResult(false);
        try{
            //ID
            notice.setId(null);

            noticeService.save(notice);
        }catch (Exception ex){
            logger.error("Save Method (inster) Notice Error : {}", ex.getMessage());
            result.setMessage(Constant.I18nMessage.SAVE_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.SAVE_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

    // 编辑-查看
    @RequestMapping(value = "update", method = RequestMethod.GET)
    public String update(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id){
        Notice notice = noticeService.get(id);
        if(notice==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
        map.put("item", notice);
        return NOTICE_DETAIL;
    }

    // 编辑-保存
    @ResponseBody
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @Log(module = "Notice", moduleName = "公告/信息/教程相关数据表", action = "ajaxUpdate", actionName = "公告/信息/教程相关数据表编辑")
    public JsonResult ajaxUpdate(HttpServletRequest request, Notice notice){
        JsonResult result = new JsonResult(false);
        if(notice==null || notice.getId()==null){
            //没有记录
            result.setMessage(Constant.I18nMessage.RECORD_NOT_FOUND);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        try{
            Notice sourceNotice = noticeService.get(notice.getId());
            if(sourceNotice==null){
                //没有记录
                result.setMessage(Constant.I18nMessage.RECORD_NOT_FOUND);
                result.setCode(Constant.Application.FAILURE);
                return result;
            }

            //公告名称
            sourceNotice.setName(notice.getName());
            //公告内容
            sourceNotice.setContent(notice.getContent());
            //公告状态
            sourceNotice.setStatus(notice.getStatus());
            //公告类型
            sourceNotice.setType(notice.getType());
            //发布者
            sourceNotice.setAuthor(notice.getAuthor());
            //阅读次数
            sourceNotice.setReadNo(notice.getReadNo());
            //创建时间
            sourceNotice.setCreateTime(notice.getCreateTime());
            noticeService.update(sourceNotice);
            notice = sourceNotice;
        }catch (Exception ex){
            logger.error("Save Method (Update) Notice Error : {}", ex.getMessage());
            result.setMessage(Constant.I18nMessage.UPDATE_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.UPDATE_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

    // 详情
    @RequestMapping(value = "view", method = RequestMethod.GET)
    public String show(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id){
        Notice notice = noticeService.get(id);
        if(notice==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
        map.put("item", notice);
        return NOTICE_VIEW;
    }

    // 删除
    @ResponseBody
    @RequestMapping(value = "del", method = RequestMethod.POST)
    @Log(module = "Notice", moduleName = "公告/信息/教程相关数据表", action = "ajaxDel", actionName = "公告/信息/教程相关数据表删除")
    public JsonResult ajaxDel(HttpServletRequest request, @RequestParam(value = "id") Long id){
        JsonResult result = new JsonResult(false);
        try{
            noticeService.removeById(id);
        }catch (Exception ex){
            logger.error("Del Method (Del) Notice Error : {}", ex.getMessage());
            //删除失败
            result.setMessage(Constant.I18nMessage.DEL_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.DEL_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

}
