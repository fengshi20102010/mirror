package com.mirror.tk.web.controller.module.goods;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import com.mirror.tk.core.log.annotation.Log;
import com.mirror.tk.core.module.goods.domain.Goods;
import com.mirror.tk.core.module.goods.service.GoodsService;
import com.mirror.tk.core.module.goods.service.GoodsTypeService;
import com.mirror.tk.framework.common.web.support.JsonListResult;
import com.mirror.tk.framework.common.web.support.JsonResult;
import com.mirror.tk.web.common.Constant;
import com.mirror.tk.web.controller.CommonController;


@Controller
@RequestMapping(value = "/module/goods/goods")
public class GoodsController extends CommonController {

    @Resource
    private GoodsService goodsService;
    
    @Resource
    private GoodsTypeService goodsTypeService;
    
    // 列表
    private static final String GOODS_LIST = "module/goods/goods/goods_list";
    // 详情
    private static final String GOODS_VIEW = "module/goods/goods/goods_view";
    // 新增/修改
    private static final String GOODS_DETAIL = "module/goods/goods/goods_detail";

    // 列表-查看
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(HttpServletRequest request, ModelMap map) {
    	map.put("types", goodsTypeService.getAll());
        return GOODS_LIST;
    }
    
    // 列表-数据
    @ResponseBody
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public JsonListResult<Goods> ajaxList(HttpServletRequest request,
            @RequestParam(value="numIid", required=false) Long numIid,
            @RequestParam(value="type", required=false) Integer type,
            @RequestParam(value="typeId", required=false) Integer typeId,
            @RequestParam(value="name", required=false) String name,
            @RequestParam(value="isExtend", required=false) Integer isExtend,
            @RequestParam(value="isCoupon", required=false) Integer isCoupon,
            @RequestParam(value="recommend", required=false) Integer recommend,
            @RequestParam(value="attribute", required=false) Integer attribute,
            @RequestParam(value="status", required=false) Integer status,
            @RequestParam(value="source", required=false) Integer source,
            @RequestParam(value="qq", required=false) String qq
            ) {

        Map<String, Object> searchMap = new HashMap<String, Object>();
        Map<String, Boolean> sortMap = new HashMap<String, Boolean>();

        //淘宝对应的商品ID
        if(numIid!=null){
            searchMap.put("EQ_numIid", numIid);
        }
        //推广计划
        if(type!=null){
            searchMap.put("EQ_type", type);
        }
        //类型
        if(typeId!=null){
            searchMap.put("EQ_typeId", typeId);
        }
        //商品名称
        if(StringUtils.isNotBlank(name)){
            searchMap.put("LIKE_name", name);
        }
        //是否直推
        if(isExtend!=null){
            searchMap.put("EQ_isExtend", isExtend);
        }
        //是否有优惠劵
        if(isCoupon!=null){
            searchMap.put("EQ_isCoupon", isCoupon);
        }
        //是否今日推荐
        if(recommend!=null){
            searchMap.put("EQ_recommend", recommend);
        }
        //商品属性
        if(attribute!=null){
            searchMap.put("EQ_attribute", attribute);
        }
        //商品状态
        if(status!=null){
            searchMap.put("EQ_status", status);
        }
        //商品来源
        if(source!=null){
            searchMap.put("EQ_source", source);
        }
        //发单人QQ
        if(StringUtils.isNotBlank(qq)){
            searchMap.put("LIKE_qq", qq);
        }
        return toJsonListResult(goodsService.query(fromJsonListResult(request), searchMap, sortMap));
    }

    // 新增-查看
    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add(HttpServletRequest request, ModelMap map){
    	map.put("types", goodsTypeService.getAll());
        return GOODS_DETAIL;
    }

    // 新增-保存
    @ResponseBody
    @RequestMapping(value = "add", method = RequestMethod.POST)
    @Log(module = "Goods", moduleName = "商品信息", action = "ajaxAdd", actionName = "商品信息新增")
    public JsonResult ajaxAdd(HttpServletRequest request, Goods goods){
        JsonResult result = new JsonResult(false);
        try{
            //ID
            goods.setId(null);

            goodsService.save(goods);
        }catch (Exception ex){
            logger.error("Save Method (inster) Goods Error : {}", ex.getMessage());
            result.setMessage(Constant.I18nMessage.SAVE_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.SAVE_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

    // 编辑-查看
    @RequestMapping(value = "update", method = RequestMethod.GET)
    public String update(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id){
        Goods goods = goodsService.get(id);
        if(goods==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
        map.put("types", goodsTypeService.getAll());
        map.put("item", goods);
        return GOODS_DETAIL;
    }

    // 编辑-保存
    @ResponseBody
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @Log(module = "Goods", moduleName = "商品信息", action = "ajaxUpdate", actionName = "商品信息编辑")
    public JsonResult ajaxUpdate(HttpServletRequest request, Goods goods){
        JsonResult result = new JsonResult(false);
        if(goods==null || goods.getId()==null){
            //没有记录
            result.setMessage(Constant.I18nMessage.RECORD_NOT_FOUND);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        try{
            Goods sourceGoods = goodsService.get(goods.getId());
            if(sourceGoods==null){
                //没有记录
                result.setMessage(Constant.I18nMessage.RECORD_NOT_FOUND);
                result.setCode(Constant.Application.FAILURE);
                return result;
            }

            //对应的店铺信息ID
            sourceGoods.setShopId(goods.getShopId());
            //淘宝对应的商品ID
            sourceGoods.setNumIid(goods.getNumIid());
            //商品类型ID
            sourceGoods.setTypeId(goods.getTypeId());
            //推广计划
            sourceGoods.setType(goods.getType());
            //商品名称
            sourceGoods.setName(goods.getName());
            //商品简介
            sourceGoods.setDetail(goods.getDetail());
            //商品链接地址
            sourceGoods.setUrl(goods.getUrl());
            //是否直推
            sourceGoods.setIsExtend(goods.getIsExtend());
            //是否有优惠劵
            sourceGoods.setIsCoupon(goods.getIsCoupon());
            //是否今日推荐
            sourceGoods.setRecommend(goods.getRecommend());
            //商品属性
            sourceGoods.setAttribute(goods.getAttribute());
            //商品状态
            sourceGoods.setStatus(goods.getStatus());
            //佣金比例,1234代表12.34%
            sourceGoods.setCommission(goods.getCommission());
            //商品图片
            sourceGoods.setPicUrl(goods.getPicUrl());
            //商品价格
            sourceGoods.setPrice(goods.getPrice());
            //折扣价格
            sourceGoods.setFinalPrice(goods.getFinalPrice());
            //商品销量
            sourceGoods.setSales(goods.getSales());
            //创建时间
            sourceGoods.setCreateTime(goods.getCreateTime());
            //修改时间
            sourceGoods.setUpdateTime(goods.getUpdateTime());
            //计划链接
            sourceGoods.setPlanUrl(goods.getPlanUrl());
            //商品来源
            sourceGoods.setSource(goods.getSource());
            //发单人QQ
            sourceGoods.setQq(goods.getQq());
            //副标题
            sourceGoods.setSubTitle(goods.getSubTitle());
            //预告时间
            sourceGoods.setAdTime(goods.getAdTime());
            goodsService.update(sourceGoods);
            goods = sourceGoods;
        }catch (Exception ex){
            logger.error("Save Method (Update) Goods Error : {}", ex.getMessage());
            result.setMessage(Constant.I18nMessage.UPDATE_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.UPDATE_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

    // 详情
    @RequestMapping(value = "view", method = RequestMethod.GET)
    public String show(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id){
        Goods goods = goodsService.get(id);
        if(goods==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
        map.put("types", goodsTypeService.getAll());
        map.put("item", goods);
        return GOODS_VIEW;
    }

    // 删除
    @ResponseBody
    @RequestMapping(value = "del", method = RequestMethod.POST)
    @Log(module = "Goods", moduleName = "商品信息", action = "ajaxDel", actionName = "商品信息删除")
    public JsonResult ajaxDel(HttpServletRequest request, @RequestParam(value = "id") Long id){
        JsonResult result = new JsonResult(false);
        try{
            goodsService.removeById(id);
        }catch (Exception ex){
            logger.error("Del Method (Del) Goods Error : {}", ex.getMessage());
            //删除失败
            result.setMessage(Constant.I18nMessage.DEL_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.DEL_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }
    
    // 修改状态
    @ResponseBody
    @RequestMapping(value = "status", method = RequestMethod.POST)
    @Log(module = "Goods", moduleName = "商品信息", action = "ajaxStatus", actionName = "商品信息修改状态")
    public JsonResult ajaxStatus(HttpServletRequest request, @RequestParam(value = "id") Long id, @RequestParam(value = "status") Integer status){
    	JsonResult result = new JsonResult(false);
    	try{
    		if(goodsService.updateByStatus(id, status)){
    			result.setMessage(Constant.I18nMessage.STATUS_SUCCESS);
    			result.setCode(Constant.Application.SUCCESS);
    			result.setSuccess(true);
    			return result;
    		}
    	}catch (Exception ex){
    		logger.error("Save Method (Update) Goods Error : {}", ex.getMessage());
    	}
    	result.setMessage(Constant.I18nMessage.STATUS_FAILURE);
    	result.setCode(Constant.Application.FAILURE);
    	return result;
    }

}
