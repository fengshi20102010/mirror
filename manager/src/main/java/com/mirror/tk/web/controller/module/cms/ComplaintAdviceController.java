package com.mirror.tk.web.controller.module.cms;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import com.mirror.tk.core.log.annotation.Log;
import com.mirror.tk.core.module.cms.domain.ComplaintAdvice;
import com.mirror.tk.core.module.cms.service.ComplaintAdviceService;
import com.mirror.tk.framework.common.web.support.JsonListResult;
import com.mirror.tk.framework.common.web.support.JsonResult;
import com.mirror.tk.web.common.Constant;
import com.mirror.tk.web.controller.CommonController;


@Controller
@RequestMapping(value = "/module/cms/advice")
public class ComplaintAdviceController extends CommonController {

    @Resource
    private ComplaintAdviceService complaintAdviceService;
    
    // 列表
    private static final String CMS_LIST = "module/cms/advice/advice_list";
    // 详情
    private static final String CMS_VIEW = "module/cms/advice/advice_view";
    // 新增/修改
    private static final String CMS_DETAIL = "module/cms/advice/advice_detail";

    // 列表-查看
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(HttpServletRequest request, ModelMap map) {
        return CMS_LIST;
    }
    
    // 列表-数据
    @ResponseBody
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public JsonListResult<ComplaintAdvice> ajaxList(HttpServletRequest request,
            @RequestParam(value="title", required=false) String title,
            @RequestParam(value="name", required=false) String name,
            @RequestParam(value="phone", required=false) String phone,
            @RequestParam(value="status", required=false) Integer status,
            @RequestParam(value="type", required=false) Integer type
            ) {

        Map<String, Object> searchMap = new HashMap<String, Object>();
        Map<String, Boolean> sortMap = new HashMap<String, Boolean>();

        //投诉/建议标题
        if(StringUtils.isNotBlank(title)){
            searchMap.put("LIKE_title", title);
        }
        //姓名
        if(StringUtils.isNotBlank(name)){
            searchMap.put("LIKE_name", name);
        }
        //电话
        if(StringUtils.isNotBlank(phone)){
            searchMap.put("LIKE_phone", phone);
        }
        //状态
        if(status!=null){
            searchMap.put("EQ_status", status);
        }
        //类型
        if(type!=null){
            searchMap.put("EQ_type", type);
        }
        return toJsonListResult(complaintAdviceService.query(fromJsonListResult(request), searchMap, sortMap));
    }

    // 新增-查看
    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add(HttpServletRequest request, ModelMap map){
        return CMS_DETAIL;
    }

    // 新增-保存
    @ResponseBody
    @RequestMapping(value = "add", method = RequestMethod.POST)
    @Log(module = "ComplaintAdvice", moduleName = "投诉建议", action = "ajaxAdd", actionName = "投诉建议新增")
    public JsonResult ajaxAdd(HttpServletRequest request, ComplaintAdvice complaintAdvice){
        JsonResult result = new JsonResult(false);
        try{
            //ID
            complaintAdvice.setId(null);
            complaintAdvice.setCreateTime(new Date());
            complaintAdvice.setUpdateTime(new Date());
            complaintAdviceService.save(complaintAdvice);
        }catch (Exception ex){
            logger.error("Save Method (inster) ComplaintAdvice Error : {}", ex.getMessage());
            result.setMessage(Constant.I18nMessage.SAVE_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.SAVE_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

    // 编辑-查看
    @RequestMapping(value = "update", method = RequestMethod.GET)
    public String update(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id){
        ComplaintAdvice complaintAdvice = complaintAdviceService.get(id);
        if(complaintAdvice==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
        map.put("item", complaintAdvice);
        return CMS_DETAIL;
    }

    // 编辑-保存
    @ResponseBody
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @Log(module = "ComplaintAdvice", moduleName = "投诉建议", action = "ajaxUpdate", actionName = "投诉建议编辑")
    public JsonResult ajaxUpdate(HttpServletRequest request, ComplaintAdvice complaintAdvice){
        JsonResult result = new JsonResult(false);
        if(complaintAdvice==null || complaintAdvice.getId()==null){
            //没有记录
            result.setMessage(Constant.I18nMessage.RECORD_NOT_FOUND);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        try{
            ComplaintAdvice sourceComplaintAdvice = complaintAdviceService.get(complaintAdvice.getId());
            if(sourceComplaintAdvice==null){
                //没有记录
                result.setMessage(Constant.I18nMessage.RECORD_NOT_FOUND);
                result.setCode(Constant.Application.FAILURE);
                return result;
            }

            //投诉/建议标题
            sourceComplaintAdvice.setTitle(complaintAdvice.getTitle());
            //投诉/建议内容
            sourceComplaintAdvice.setContent(complaintAdvice.getContent());
            //姓名
            sourceComplaintAdvice.setName(complaintAdvice.getName());
            //电话
            sourceComplaintAdvice.setPhone(complaintAdvice.getPhone());
            //邮箱
            sourceComplaintAdvice.setEmail(complaintAdvice.getEmail());
            //状态
            sourceComplaintAdvice.setStatus(complaintAdvice.getStatus());
            //类型
            sourceComplaintAdvice.setType(complaintAdvice.getType());
            //修改时间
            sourceComplaintAdvice.setUpdateTime(new Date());
            complaintAdviceService.update(sourceComplaintAdvice);
            complaintAdvice = sourceComplaintAdvice;
        }catch (Exception ex){
            logger.error("Save Method (Update) ComplaintAdvice Error : {}", ex.getMessage());
            result.setMessage(Constant.I18nMessage.UPDATE_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.UPDATE_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

    // 详情
    @RequestMapping(value = "view", method = RequestMethod.GET)
    public String show(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id){
        ComplaintAdvice complaintAdvice = complaintAdviceService.get(id);
        if(complaintAdvice==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
        map.put("item", complaintAdvice);
        return CMS_VIEW;
    }

    // 删除
    @ResponseBody
    @RequestMapping(value = "del", method = RequestMethod.POST)
    @Log(module = "ComplaintAdvice", moduleName = "投诉建议", action = "ajaxDel", actionName = "投诉建议删除")
    public JsonResult ajaxDel(HttpServletRequest request, @RequestParam(value = "id") Long id){
        JsonResult result = new JsonResult(false);
        try{
            complaintAdviceService.removeById(id);
        }catch (Exception ex){
            logger.error("Del Method (Del) ComplaintAdvice Error : {}", ex.getMessage());
            //删除失败
            result.setMessage(Constant.I18nMessage.DEL_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.DEL_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

}
