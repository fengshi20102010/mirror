package com.mirror.tk.web.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 首页
 */
@Controller
public class IndexController extends CommonController {

	private static final String INDEX = "index";
	private static final String LOGIN = "login";
	
	@RequestMapping("/index")
	public String index() {
		return INDEX;
	}

	@RequestMapping(value = "/login")
	public String login(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws IOException {
		return LOGIN;
	}

}
