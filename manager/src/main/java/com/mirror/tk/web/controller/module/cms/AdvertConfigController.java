package com.mirror.tk.web.controller.module.cms;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import com.mirror.tk.core.log.annotation.Log;
import com.mirror.tk.core.module.cms.domain.AdvertConfig;
import com.mirror.tk.core.module.cms.service.AdvertConfigService;
import com.mirror.tk.framework.common.web.support.JsonListResult;
import com.mirror.tk.framework.common.web.support.JsonResult;
import com.mirror.tk.web.common.Constant;
import com.mirror.tk.web.controller.CommonController;


@Controller
@RequestMapping(value = "/module/cms/advert")
public class AdvertConfigController extends CommonController {

    @Resource
    private AdvertConfigService advertConfigService;
    
    // 列表
    private static final String ADVERT_LIST = "module/cms/advert/advert_list";
    // 详情
    private static final String ADVERT_VIEW = "module/cms/advert/advert_view";
    // 新增/修改
    private static final String ADVERT_DETAIL = "module/cms/advert/advert_detail";

    // 列表-查看
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(HttpServletRequest request, ModelMap map) {
        return ADVERT_LIST;
    }
    
    // 列表-数据
    @ResponseBody
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public JsonListResult<AdvertConfig> ajaxList(HttpServletRequest request,
            @RequestParam(value="title", required=false) String title,
            @RequestParam(value="type", required=false) Integer type,
            @RequestParam(value="status", required=false) Integer status
            ) {

        Map<String, Object> searchMap = new HashMap<String, Object>();
        Map<String, Boolean> sortMap = new HashMap<String, Boolean>();

        //标题
        if(StringUtils.isNotBlank(title)){
            searchMap.put("LIKE_title", title);
        }
        //类型:
        if(type!=null){
            searchMap.put("EQ_type", type);
        }
        //状态:
        if(status!=null){
            searchMap.put("EQ_status", status);
        }
        return toJsonListResult(advertConfigService.query(fromJsonListResult(request), searchMap, sortMap));
    }

    // 新增-查看
    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add(HttpServletRequest request, ModelMap map){
        return ADVERT_DETAIL;
    }

    // 新增-保存
    @ResponseBody
    @RequestMapping(value = "add", method = RequestMethod.POST)
    @Log(module = "AdvertConfig", moduleName = "广告设置", action = "ajaxAdd", actionName = "广告设置新增")
    public JsonResult ajaxAdd(HttpServletRequest request, AdvertConfig advertConfig){
        JsonResult result = new JsonResult(false);
        try{
            //id
            advertConfig.setId(null);
            //创建时间
            advertConfig.setCreateTime(new Date());

            advertConfigService.save(advertConfig);
        }catch (Exception ex){
            logger.error("Save Method (inster) AdvertConfig Error : {}", ex.getMessage());
            result.setMessage(Constant.I18nMessage.SAVE_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.SAVE_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

    // 编辑-查看
    @RequestMapping(value = "update", method = RequestMethod.GET)
    public String update(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id){
        AdvertConfig advertConfig = advertConfigService.get(id);
        if(advertConfig==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
        map.put("item", advertConfig);
        return ADVERT_DETAIL;
    }

    // 编辑-保存
    @ResponseBody
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @Log(module = "AdvertConfig", moduleName = "广告设置", action = "ajaxUpdate", actionName = "广告设置编辑")
    public JsonResult ajaxUpdate(HttpServletRequest request, AdvertConfig advertConfig){
        JsonResult result = new JsonResult(false);
        if(advertConfig==null || advertConfig.getId()==null){
            //没有记录
            result.setMessage(Constant.I18nMessage.RECORD_NOT_FOUND);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        try{
            AdvertConfig sourceAdvertConfig = advertConfigService.get(advertConfig.getId());
            if(sourceAdvertConfig==null){
                //没有记录
                result.setMessage(Constant.I18nMessage.RECORD_NOT_FOUND);
                result.setCode(Constant.Application.FAILURE);
                return result;
            }

            //标题
            sourceAdvertConfig.setTitle(advertConfig.getTitle());
            //图片地址
            sourceAdvertConfig.setImageUrl(advertConfig.getImageUrl());
            //跳转连接
            sourceAdvertConfig.setUrl(advertConfig.getUrl());
            //类型:
            sourceAdvertConfig.setType(advertConfig.getType());
            //状态:
            sourceAdvertConfig.setStatus(advertConfig.getStatus());
            //排序
            sourceAdvertConfig.setSortNo(advertConfig.getSortNo());
            advertConfigService.update(sourceAdvertConfig);
            advertConfig = sourceAdvertConfig;
        }catch (Exception ex){
            logger.error("Save Method (Update) AdvertConfig Error : {}", ex.getMessage());
            result.setMessage(Constant.I18nMessage.UPDATE_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.UPDATE_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

    // 详情
    @RequestMapping(value = "view", method = RequestMethod.GET)
    public String show(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id){
        AdvertConfig advertConfig = advertConfigService.get(id);
        if(advertConfig==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
        map.put("item", advertConfig);
        return ADVERT_VIEW;
    }

    // 删除
    @ResponseBody
    @RequestMapping(value = "del", method = RequestMethod.POST)
    @Log(module = "AdvertConfig", moduleName = "广告设置", action = "ajaxDel", actionName = "广告设置删除")
    public JsonResult ajaxDel(HttpServletRequest request, @RequestParam(value = "id") Long id){
        JsonResult result = new JsonResult(false);
        try{
            advertConfigService.removeById(id);
        }catch (Exception ex){
            logger.error("Del Method (Del) AdvertConfig Error : {}", ex.getMessage());
            //删除失败
            result.setMessage(Constant.I18nMessage.DEL_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.DEL_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

}
