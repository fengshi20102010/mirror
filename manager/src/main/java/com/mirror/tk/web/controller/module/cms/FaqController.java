package com.mirror.tk.web.controller.module.cms;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import com.mirror.tk.core.log.annotation.Log;
import com.mirror.tk.core.module.cms.domain.Faq;
import com.mirror.tk.core.module.cms.service.FaqService;
import com.mirror.tk.framework.common.web.support.JsonListResult;
import com.mirror.tk.framework.common.web.support.JsonResult;
import com.mirror.tk.web.common.Constant;
import com.mirror.tk.web.controller.CommonController;


@Controller
@RequestMapping(value = "/module/cms/faq")
public class FaqController extends CommonController {

    @Resource
    private FaqService faqService;
    
    // 列表
    private static final String FAQ_LIST = "module/cms/faq/faq_list";
    // 详情
    private static final String FAQ_VIEW = "module/cms/faq/faq_view";
    // 新增/修改
    private static final String FAQ_DETAIL = "module/cms/faq/faq_detail";

    // 列表-查看
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(HttpServletRequest request, ModelMap map) {
        return FAQ_LIST;
    }
    
    // 列表-数据
    @ResponseBody
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public JsonListResult<Faq> ajaxList(HttpServletRequest request,
            @RequestParam(value="title", required=false) String title,
            @RequestParam(value="status", required=false) Integer status
            ) {
        Map<String, Object> searchMap = new HashMap<String, Object>();
        Map<String, Boolean> sortMap = new HashMap<String, Boolean>();

        //标题
        if(StringUtils.isNotBlank(title)){
            searchMap.put("LIKE_title", title);
        }
        //状态
        if(status!=null){
            searchMap.put("EQ_status", status);
        }
        return toJsonListResult(faqService.query(fromJsonListResult(request), searchMap, sortMap));
    }

    // 新增-查看
    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add(HttpServletRequest request, ModelMap map){
        return FAQ_DETAIL;
    }

    // 新增-保存
    @ResponseBody
    @RequestMapping(value = "add", method = RequestMethod.POST)
    @Log(module = "Faq", moduleName = "常见问题", action = "ajaxAdd", actionName = "常见问题新增")
    public JsonResult ajaxAdd(HttpServletRequest request, Faq faq){
        JsonResult result = new JsonResult(false);
        try{
            //ID
            faq.setId(null);

            faqService.save(faq);
        }catch (Exception ex){
            logger.error("Save Method (inster) Faq Error : {}", ex.getMessage());
            result.setMessage(Constant.I18nMessage.SAVE_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.SAVE_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

    // 编辑-查看
    @RequestMapping(value = "update", method = RequestMethod.GET)
    public String update(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id){
        Faq faq = faqService.get(id);
        if(faq==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
        map.put("item", faq);
        return FAQ_DETAIL;
    }

    // 编辑-保存
    @ResponseBody
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @Log(module = "Faq", moduleName = "常见问题", action = "ajaxUpdate", actionName = "常见问题编辑")
    public JsonResult ajaxUpdate(HttpServletRequest request, Faq faq){
        JsonResult result = new JsonResult(false);
        if(faq==null || faq.getId()==null){
            //没有记录
            result.setMessage(Constant.I18nMessage.RECORD_NOT_FOUND);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        try{
            Faq sourceFaq = faqService.get(faq.getId());
            if(sourceFaq==null){
                //没有记录
                result.setMessage(Constant.I18nMessage.RECORD_NOT_FOUND);
                result.setCode(Constant.Application.FAILURE);
                return result;
            }

            //标题
            sourceFaq.setTitle(faq.getTitle());
            //问题
            sourceFaq.setQuestion(faq.getQuestion());
            //答案
            sourceFaq.setAnswer(faq.getAnswer());
            //状态
            sourceFaq.setStatus(faq.getStatus());
            //创建时间
            sourceFaq.setCreateTime(faq.getCreateTime());
            faqService.update(sourceFaq);
            faq = sourceFaq;
        }catch (Exception ex){
            logger.error("Save Method (Update) Faq Error : {}", ex.getMessage());
            result.setMessage(Constant.I18nMessage.UPDATE_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.UPDATE_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

    // 详情
    @RequestMapping(value = "view", method = RequestMethod.GET)
    public String show(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id){
        Faq faq = faqService.get(id);
        if(faq==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
        map.put("item", faq);
        return FAQ_VIEW;
    }

    // 删除
    @ResponseBody
    @RequestMapping(value = "del", method = RequestMethod.POST)
    @Log(module = "Faq", moduleName = "常见问题", action = "ajaxDel", actionName = "常见问题删除")
    public JsonResult ajaxDel(HttpServletRequest request, @RequestParam(value = "id") Long id){
        JsonResult result = new JsonResult(false);
        try{
            faqService.removeById(id);
        }catch (Exception ex){
            logger.error("Del Method (Del) Faq Error : {}", ex.getMessage());
            //删除失败
            result.setMessage(Constant.I18nMessage.DEL_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.DEL_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

}
