package com.mirror.tk.web.controller.module.goods;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import com.mirror.tk.core.log.annotation.Log;
import com.mirror.tk.core.module.goods.domain.GoodsTplContrast;
import com.mirror.tk.core.module.goods.service.GoodsTplContrastService;
import com.mirror.tk.framework.common.web.support.JsonListResult;
import com.mirror.tk.framework.common.web.support.JsonResult;
import com.mirror.tk.web.common.Constant;
import com.mirror.tk.web.controller.CommonController;


@Controller
@RequestMapping(value = "/module/goods/tpl")
public class GoodsTplContrastController extends CommonController {

    @Resource
    private GoodsTplContrastService goodsTplContrastService;
    
    // 列表
    private static final String GOODS_LIST = "module/goods/tpl/tpl_list";
    // 详情
    private static final String GOODS_VIEW = "module/goods/tpl/tpl_view";
    // 新增/修改
    private static final String GOODS_DETAIL = "module/goods/tpl/tpl_detail";

    // 列表-查看
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(HttpServletRequest request, ModelMap map) {
        return GOODS_LIST;
    }
    
    // 列表-数据
    @ResponseBody
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public JsonListResult<GoodsTplContrast> ajaxList(HttpServletRequest request,
            @RequestParam(value="name", required=false) String name,
            @RequestParam(value="value", required=false) String value
            ) {

        Map<String, Object> searchMap = new HashMap<String, Object>();
        Map<String, Boolean> sortMap = new HashMap<String, Boolean>();

        //显示名称
        if(StringUtils.isNotBlank(name)){
            searchMap.put("LIKE_name", name);
        }
        //对应的字段
        if(StringUtils.isNotBlank(value)){
            searchMap.put("LIKE_value", value);
        }
        return toJsonListResult(goodsTplContrastService.query(fromJsonListResult(request), searchMap, sortMap));
    }

    // 新增-查看
    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add(HttpServletRequest request, ModelMap map){
        return GOODS_DETAIL;
    }

    // 新增-保存
    @ResponseBody
    @RequestMapping(value = "add", method = RequestMethod.POST)
    @Log(module = "GoodsTplContrast", moduleName = "商品模板对照", action = "ajaxAdd", actionName = "商品模板对照新增")
    public JsonResult ajaxAdd(HttpServletRequest request, GoodsTplContrast goodsTplContrast){
        JsonResult result = new JsonResult(false);
        try{
            //id
            goodsTplContrast.setId(null);
            goodsTplContrast.setCreateTime(new Date());
            goodsTplContrast.setUpdateTime(new Date());
            goodsTplContrastService.save(goodsTplContrast);
        }catch (Exception ex){
            logger.error("Save Method (inster) GoodsTplContrast Error : {}", ex.getMessage());
            result.setMessage(Constant.I18nMessage.SAVE_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.SAVE_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

    // 编辑-查看
    @RequestMapping(value = "update", method = RequestMethod.GET)
    public String update(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id){
        GoodsTplContrast goodsTplContrast = goodsTplContrastService.get(id);
        if(goodsTplContrast==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
        map.put("item", goodsTplContrast);
        return GOODS_DETAIL;
    }

    // 编辑-保存
    @ResponseBody
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @Log(module = "GoodsTplContrast", moduleName = "商品模板对照", action = "ajaxUpdate", actionName = "商品模板对照编辑")
    public JsonResult ajaxUpdate(HttpServletRequest request, GoodsTplContrast goodsTplContrast){
        JsonResult result = new JsonResult(false);
        if(goodsTplContrast==null || goodsTplContrast.getId()==null){
            //没有记录
            result.setMessage(Constant.I18nMessage.RECORD_NOT_FOUND);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        try{
            GoodsTplContrast sourceGoodsTplContrast = goodsTplContrastService.get(goodsTplContrast.getId());
            if(sourceGoodsTplContrast==null){
                //没有记录
                result.setMessage(Constant.I18nMessage.RECORD_NOT_FOUND);
                result.setCode(Constant.Application.FAILURE);
                return result;
            }

            //显示名称
            sourceGoodsTplContrast.setName(goodsTplContrast.getName());
            //对应的字段
            sourceGoodsTplContrast.setValue(goodsTplContrast.getValue());
            //修改时间
            sourceGoodsTplContrast.setUpdateTime(new Date());
            goodsTplContrastService.update(sourceGoodsTplContrast);
            goodsTplContrast = sourceGoodsTplContrast;
        }catch (Exception ex){
            logger.error("Save Method (Update) GoodsTplContrast Error : {}", ex.getMessage());
            result.setMessage(Constant.I18nMessage.UPDATE_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.UPDATE_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

    // 详情
    @RequestMapping(value = "view", method = RequestMethod.GET)
    public String show(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id){
        GoodsTplContrast goodsTplContrast = goodsTplContrastService.get(id);
        if(goodsTplContrast==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
        map.put("item", goodsTplContrast);
        return GOODS_VIEW;
    }

    // 删除
    @ResponseBody
    @RequestMapping(value = "del", method = RequestMethod.POST)
    @Log(module = "GoodsTplContrast", moduleName = "商品模板对照", action = "ajaxDel", actionName = "商品模板对照删除")
    public JsonResult ajaxDel(HttpServletRequest request, @RequestParam(value = "id") Long id){
        JsonResult result = new JsonResult(false);
        try{
            goodsTplContrastService.removeById(id);
        }catch (Exception ex){
            logger.error("Del Method (Del) GoodsTplContrast Error : {}", ex.getMessage());
            //删除失败
            result.setMessage(Constant.I18nMessage.DEL_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.DEL_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

}
