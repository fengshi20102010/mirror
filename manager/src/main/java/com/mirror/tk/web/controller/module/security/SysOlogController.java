package com.mirror.tk.web.controller.module.security;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import com.mirror.tk.core.module.sys.domain.SysOlog;
import com.mirror.tk.core.module.sys.service.SysOlogService;
import com.mirror.tk.framework.common.web.support.JsonListResult;
import com.mirror.tk.web.common.Constant;
import com.mirror.tk.web.controller.CommonController;


@Controller
@RequestMapping(value = "/module/security/olog")
public class SysOlogController extends CommonController {

    @Resource
    private SysOlogService sysOlogService;
    
    // 列表
    private static final String OLOG_LIST = "module/security/olog/sys_olog_list";
    // 详情
    private static final String OLOG_VIEW = "module/security/olog/sys_olog_view";

    // 列表-查看
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(HttpServletRequest request, ModelMap map) {

	    //操作时间 默认时间范围3个月
	    map.put("endOperateTime", Calendar.getInstance().getTime());
	    map.put("beginOperateTime", DateUtils.addMonths(Calendar.getInstance().getTime(), -3));

        return OLOG_LIST;
    }
    
    // 列表-数据
    @ResponseBody
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public JsonListResult<SysOlog> ajaxList(HttpServletRequest request,
            @RequestParam(value="module", required=false) String module,
            @RequestParam(value="moduleName", required=false) String moduleName,
            @RequestParam(value="beginOperateTime", required=false) Date beginOperateTime,
            @RequestParam(value="endOperateTime", required=false) Date endOperateTime,
            @RequestParam(value="operateUser", required=false) String operateUser,
            @RequestParam(value="operateResult", required=false) Integer operateResult,
            @RequestParam(value="tag", required=false) String tag
            ) {

        //操作时间 默认时间范围3个月
        if(endOperateTime==null){
            endOperateTime=Calendar.getInstance().getTime();
        }
        if(beginOperateTime==null){
            beginOperateTime = DateUtils.addMonths(endOperateTime, -3);
        }

        Map<String, Object> searchMap = new HashMap<String, Object>();
        Map<String, Boolean> sortMap = new HashMap<String, Boolean>();

        //模块
        if(StringUtils.isNotBlank(module)){
            searchMap.put("LIKE_module", module);
        }
        //模块名称
        if(StringUtils.isNotBlank(moduleName)){
            searchMap.put("LIKE_moduleName", moduleName);
        }
        //操作时间
        if(beginOperateTime!=null){
            searchMap.put("GTE_operateTime", DateUtils.truncate(beginOperateTime, Calendar.DATE));
        }
        if(endOperateTime!=null){
            searchMap.put("LTE_operateTime", DateUtils.truncate(DateUtils.addDays(endOperateTime, 1), Calendar.DATE));
        }
        //操作人
        if(StringUtils.isNotBlank(operateUser)){
            searchMap.put("LIKE_operateUser", operateUser);
        }
        //操作结果
        if(operateResult!=null){
            searchMap.put("EQ_operateResult", operateResult);
        }
        //平台标识
        if(StringUtils.isNotBlank(tag)){
            searchMap.put("EQ_tag", tag);
        }
        return toJsonListResult(sysOlogService.query(fromJsonListResult(request), searchMap, sortMap));
    }

    // 详情
    @RequestMapping(value = "view", method = RequestMethod.GET)
    public String show(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id){
        SysOlog sysOlog = sysOlogService.get(id);
        if(sysOlog==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
        map.put("item", sysOlog);
        return OLOG_VIEW;
    }

}
