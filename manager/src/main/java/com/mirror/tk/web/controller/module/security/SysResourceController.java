package com.mirror.tk.web.controller.module.security;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import com.mirror.tk.core.log.annotation.Log;
import com.mirror.tk.core.module.sys.domain.SysResource;
import com.mirror.tk.core.module.sys.dto.SortInfo;
import com.mirror.tk.core.module.sys.service.SysResourceService;
import com.mirror.tk.framework.common.web.support.JsonResult;
import com.mirror.tk.web.common.Constant;
import com.mirror.tk.web.controller.CommonController;

@Controller
@RequestMapping(value = "/module/security/resource")
public class SysResourceController extends CommonController {

	@Resource
	private SysResourceService sysResourceService;

	// 列表
	private static final String RESOURCE_LIST = "module/security/resource/sys_resource_list";
	// 详情
	private static final String RESOURCE_VIEW = "module/security/resource/sys_resource_view";
	// 添加、修改
	private static final String RESOURCE_DETAIL = "module/security/resource/sys_resource_detail";

    //列表
	@RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(HttpServletRequest request, ModelMap map) {
        Map<String, Object> seachMap = new HashMap<String, Object>();
        Map<String, Boolean> sortMap = new HashMap<String, Boolean>();
        
        seachMap.put("EQ_parentId", 0);
        sortMap.put("sortNo", true);
        List<SysResource> list = sysResourceService.query(seachMap, sortMap);
        map.put("resourceList", list);
        return RESOURCE_LIST;
    }
	
    // 新增-查看
    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add(HttpServletRequest request, ModelMap map, 
    		@RequestParam(value = "pid") Long pid,
    		@RequestParam(value = "sort") Long sort){
    	map.put("pid", pid);
    	map.put("sort",sort);
        return RESOURCE_DETAIL;
    }

    // 新增-保存
    @ResponseBody
    @RequestMapping(value = "add", method = RequestMethod.POST)
    @Log(module = "SysResource", moduleName = "资源", action = "ajaxAdd", actionName = "资源新增")
    public JsonResult ajaxAdd(HttpServletRequest request, SysResource sysResource){
        JsonResult result = new JsonResult(false);
        try{
            //id
            sysResource.setId(null);
            //创建时间
            sysResource.setCreateTime(new Date());
            sysResourceService.save(sysResource);
        }catch (Exception ex){
            logger.error("Save Method (inster) SysResource Error : {}", ex.getMessage());
            result.setMessage(Constant.I18nMessage.SAVE_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.SAVE_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

    // 编辑-查看
    @RequestMapping(value = "update", method = RequestMethod.GET)
    public String update(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id){
        SysResource sysResource = sysResourceService.get(id);
        if(sysResource==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
        map.put("item", sysResource);
        return RESOURCE_DETAIL;
    }

    // 编辑-保存
    @ResponseBody
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @Log(module = "SysResource", moduleName = "资源", action = "ajaxUpdate", actionName = "资源编辑")
    public JsonResult ajaxUpdate(HttpServletRequest request, SysResource sysResource){
        JsonResult result = new JsonResult(false);
        if(sysResource==null || sysResource.getId()==null){
            //没有记录
            result.setMessage(Constant.I18nMessage.RECORD_NOT_FOUND);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        try{
            SysResource sourceSysResource = sysResourceService.get(sysResource.getId());
            if(sourceSysResource==null){
                //没有记录
                result.setMessage(Constant.I18nMessage.RECORD_NOT_FOUND);
                result.setCode(Constant.Application.FAILURE);
                return result;
            }

			// 资源名称
			sourceSysResource.setTitle(sysResource.getTitle());
			// 资源类型
			sourceSysResource.setResType(sysResource.getResType());
			// 资源值
			sourceSysResource.setResKey(sysResource.getResKey());
			// 权限
			sourceSysResource.setPermissionValue(sysResource.getPermissionValue());
			// 描述
			sourceSysResource.setDescription(sysResource.getDescription());
			// 状态
			sourceSysResource.setStatus(sysResource.getStatus());
            sysResourceService.update(sourceSysResource);
            sysResource = sourceSysResource;
        }catch (Exception ex){
            logger.error("Save Method (Update) SysResource Error : {}", ex.getMessage());
            result.setMessage(Constant.I18nMessage.UPDATE_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.UPDATE_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

    // 详情
    @RequestMapping(value = "view", method = RequestMethod.GET)
    public String show(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id){
        SysResource sysResource = sysResourceService.get(id);
        if(sysResource==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
        map.put("item", sysResource);
        return RESOURCE_VIEW;
    }

    // 删除
    @ResponseBody
    @RequestMapping(value = "del", method = RequestMethod.POST)
    @Log(module = "SysResource", moduleName = "资源", action = "ajaxDel", actionName = "资源删除")
    public JsonResult ajaxDel(HttpServletRequest request, @RequestParam(value = "id") Long id){
        JsonResult result = new JsonResult(false);
        try{
            if(0==sysResourceService.deleteTree(id)){
                //没有记录
                return new JsonResult("failure", new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND));
            }
        }catch (Exception ex){
            logger.error("Del Method (Del) SysResource Error : {}", ex.getMessage());
            //删除失败
            result.setMessage(Constant.I18nMessage.DEL_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.DEL_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

    //修改排序和结构
    @ResponseBody
    @RequestMapping(value ="sort", method = RequestMethod.POST)
    @Log(module = "SysResource", moduleName = "资源", action = "ajaxSort", actionName = "资源排序")
    public JsonResult ajaxSort(HttpServletRequest request, @RequestBody List<SortInfo> sortInfo){
    	JsonResult result = new JsonResult(false);
        try{
        	sysResourceService.sort(sortInfo);
        }catch (Exception ex){
        	// 排序失败
            logger.error("Save Method (sort) SysMenu Error :{} ", ex.getMessage());
            result.setMessage(Constant.I18nMessage.SORT_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.SORT_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

}
