package com.mirror.tk.web.exception;

import com.mirror.tk.framework.common.exception.BusinessException;

/**
 * 参数错误异常
 * 
 * HttpStatus 400
 * 
 */
public class WebServiceException extends BusinessException {
	
	private static final long serialVersionUID = 1L;
	private int status;

	public WebServiceException(int status, String errorCode, String detail) {
		this(status, errorCode, detail, null);
	}

	public WebServiceException(int status, String errorCode, String deitai,
			Throwable cause) {
		super(errorCode, null, deitai, cause);
		this.status = status;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
