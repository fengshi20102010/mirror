package com.mirror.tk.web.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import com.mirror.tk.framework.common.dao.support.PageInfo;
import com.mirror.tk.framework.common.web.support.JsonListResult;
import com.mirror.tk.web.common.Constant;

public class CommonController {

	protected Logger logger = LogManager.getLogger(this.getClass());

	/**
	 * 初始化绑定数据，加入字符串转日期
	 * 
	 * @param request
	 * @param binder
	 */
	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		CustomDateEditor dateEditor = new CustomDateEditor(format, true);
		binder.registerCustomEditor(Date.class, dateEditor);
	}

	/**
	 * 分页
	 * 
	 * @param request
	 * @param pageSize
	 * @return
	 */
	protected <T> PageInfo<T> getPageInfo(HttpServletRequest request, int pageSize) {
		int currentPage = 1;
		if (StringUtils.isNotBlank(request.getParameter("pageNo"))) {
			try {
				currentPage = Integer.parseInt(request.getParameter("pageNo"));
			} catch (Exception ex) {
				
			}
		}
		PageInfo<T> pageInfo = new PageInfo<T>(pageSize, currentPage);
		return pageInfo;
	}
	
	/**
	 * 分页
	 * 
	 * @param request
	 * @return
	 */
	protected <T> PageInfo<T> getPageInfo(HttpServletRequest request) {
		return getPageInfo(request, 10);
	}
	
	/**
	 * bootstrap-table分页转换
	 * 
	 * @param request
	 * @return
	 */
	protected <T> PageInfo<T> fromJsonListResult(HttpServletRequest request){
		int currentPage = 1;
		int pageSize = 10;
		if (StringUtils.isNotBlank(request.getParameter("pageNumber"))) {
			try {
				currentPage = Integer.parseInt(request.getParameter("pageNumber"));
			} catch (Exception ex) {
				
			}
		}
		if (StringUtils.isNotBlank(request.getParameter("pageSize"))) {
			try {
				pageSize = Integer.parseInt(request.getParameter("pageSize"));
			} catch (Exception ex) {
				
			}
		}
		PageInfo<T> pageInfo = new PageInfo<T>(pageSize, currentPage);
		return pageInfo;
	}

	/**
	 * 将pageInfo转换为JsonListResult
	 * 
	 * @param pageInfo
	 * @return
	 */
	protected <T> JsonListResult<T> toJsonListResult(PageInfo<T> pageInfo) {
		JsonListResult<T> result = new JsonListResult<T>();
		if(pageInfo.getTotalCount() > 0){
			result.setCode("0");
			result.setMessage("获取数据成功");
		}
		result.setTotal(pageInfo.getTotalCount());
		result.setRows(pageInfo.getPageResults());
		return result;
	}
	
	/**
	 * 操作成功提示
	 * 
	 * @param msg
	 *            - 提示信息
	 * @param location
	 *            - 跳转页面
	 * @param map
	 * @return
	 */
	protected String operSuccess(String msg, String location, Map<String, Object> map) {
		map.put("alertinfo", Constant.Application.COMMON_SUCCESS_ALERTINFO);
		map.put("msg", msg);
		map.put("location", location);
		return "module.common.message";
	}

	/**
	 * 操作失败提示
	 * 
	 * @param msg
	 *            - 提示信息
	 * @param map
	 * @return
	 */
	protected String operFailure(String msg, Map<String, Object> map) {
		map.put("alertinfo", Constant.Application.COMMON_DANGER_ALERTINFO);
		map.put("msg", msg);
		return "module.common.message";
	}
	
}

