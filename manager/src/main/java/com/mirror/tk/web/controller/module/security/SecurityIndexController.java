package com.mirror.tk.web.controller.module.security;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mirror.tk.core.module.sys.service.SysMenuService;
import com.mirror.tk.core.module.sys.service.SysUserService;
import com.mirror.tk.security.shiro.realm.SampleRealm;
import com.mirror.tk.web.common.support.SessionFace;
import com.mirror.tk.web.controller.CommonController;

/**
 * 安全框架首页
 */
@Controller
@RequestMapping("/module/security")
public class SecurityIndexController extends CommonController {

	@Resource
	private SysUserService sysUserService;
	@Resource
	private SysMenuService sysMenuService;
	@Resource
	private SampleRealm sampleRealm;

	private static final String SECURITY_INDEX = "module/security/index";
	private static final String SECURITY_WELCOME = "module/security/welcome";
	private static final String SECURITY_PERSONAL = "module/security/personal";

	/**
	 * 首页
	 * 
	 * @param request
	 * @param map
	 * @return
	 */
	@RequestMapping("index")
	public String index(HttpServletRequest request, ModelMap map) {
		// 获取用户信息和菜单权限
		map.put("authorisedMenus",sysMenuService.queryAuthorisedMenus(SessionFace.getSessionUser(request).getUsername()));
		map.put("user", sysUserService.findByUsername(SessionFace.getSessionUser(request).getUsername()));
		return SECURITY_INDEX;
	}

	/**
	 * 欢迎首页
	 * 
	 * @param request
	 * @param map
	 * @return
	 */
	@RequestMapping("welcome")
	public String welcome(HttpServletRequest request, ModelMap map) {
		map.put("item", sysUserService.findByUsername(SessionFace.getSessionUser(request).getUsername()));
		return SECURITY_WELCOME;
	}

	/**
	 * 个人信息
	 * 
	 * @param request
	 * @param map
	 * @return
	 */
	@RequestMapping("personal")
	public String personal(HttpServletRequest request, ModelMap map) {
		map.put("item", sysUserService.findByUsername(SessionFace.getSessionUser(request).getUsername()));
		return SECURITY_PERSONAL;
	}

}
