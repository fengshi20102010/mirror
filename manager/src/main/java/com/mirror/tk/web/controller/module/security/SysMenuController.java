package com.mirror.tk.web.controller.module.security;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import com.mirror.tk.core.log.annotation.Log;
import com.mirror.tk.core.module.sys.domain.SysMenu;
import com.mirror.tk.core.module.sys.dto.SortInfo;
import com.mirror.tk.core.module.sys.service.SysMenuService;
import com.mirror.tk.core.module.sys.service.SysResourceService;
import com.mirror.tk.framework.common.web.support.JsonEntityResult;
import com.mirror.tk.framework.common.web.support.JsonResult;
import com.mirror.tk.web.common.Constant;
import com.mirror.tk.web.controller.CommonController;
import com.mirror.tk.web.utils.PinyinUtil;


@Controller
@RequestMapping(value = "/module/security/menu")
public class SysMenuController extends CommonController {

    @Resource
    private SysMenuService sysMenuService;
    @Resource
    private SysResourceService sysResourceService;
    
    //列表
    private static final String MENU_LIST = "module/security/menu/sys_menu_list";
    //详情
    private static final String MENU_VIEW = "module/security/menu/sys_menu_view";
    //添加、修改
    private static final String MENU_DETAIL = "module/security/menu/sys_menu_detail";

    //列表
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(HttpServletRequest request, ModelMap map) {
        Map<String, Object> seachMap = new HashMap<String, Object>();
        Map<String, Boolean> sortMap = new HashMap<String, Boolean>();
        
        seachMap.put("EQ_parentId", 0);
        sortMap.put("sortNo", true);
        List<SysMenu> list = sysMenuService.query(seachMap, sortMap);
        map.put("menuList", list);
        return MENU_LIST;
    }

    // 详情
    @RequestMapping(value = "view", method = RequestMethod.GET)
    public String show(HttpServletRequest request, ModelMap map, 
    		@RequestParam(value = "id") Long id){
        SysMenu sysMenu = sysMenuService.get(id);
        if(sysMenu==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
        map.put("item", sysMenu);
        map.put("res", sysResourceService.get(sysMenu.getResourceId()));
        return MENU_VIEW;
    }
    
    // 新增-查看
    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add(HttpServletRequest request, ModelMap map, 
    		@RequestParam(value = "pid") Long pid,
    		@RequestParam(value = "sort") Long sort){
    	map.put("pid", pid);
    	map.put("sort",sort);
    	map.put("resc", sysResourceService.queryByTypeAndStatus("URL", 1));
        return MENU_DETAIL;
    }

    // 新增-保存
    @ResponseBody
    @RequestMapping(value = "add", method = RequestMethod.POST)
    @Log(module = "SysMenu", moduleName = "菜单", action = "ajaxAdd", actionName = "菜单新增")
    public JsonEntityResult<SysMenu> ajaxAdd(HttpServletRequest request, SysMenu sysMenu){
    	JsonEntityResult<SysMenu> result = new JsonEntityResult<SysMenu>();
        try{
            //主键ID
            sysMenu.setId(null);
            //拼音
            sysMenu.setTitleFirstSpell(PinyinUtil.cn2FirstSpell(sysMenu.getTitle()));
            sysMenu.setCreateTime(new Date());
            sysMenuService.save(sysMenu);
        }catch (Exception ex){
            logger.error("Save Method (inster) SysMenu Error : {}", ex.getMessage());
            result.setMessage(Constant.I18nMessage.SAVE_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.SAVE_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setEntity(sysMenu);
        result.setSuccess(true);
        return result;
    }

    // 编辑-查看
    @RequestMapping(value = "update", method = RequestMethod.GET)
    public String update(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id){
        SysMenu sysMenu = sysMenuService.get(id);
        if(sysMenu==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
		map.put("resc", sysResourceService.queryByTypeAndStatus("URL", 1));
		map.put("item", sysMenu);
        return MENU_DETAIL;
    }

    // 编辑-保存
    @ResponseBody
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @Log(module = "SysMenu", moduleName = "菜单", action = "ajaxUpdate", actionName = "菜单编辑")
    public JsonResult ajaxUpdate(HttpServletRequest request, SysMenu sysMenu){
        JsonResult result = new JsonResult(false);
        if(sysMenu==null || sysMenu.getId()==null){
            //没有记录
            result.setMessage(Constant.I18nMessage.RECORD_NOT_FOUND);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        try{
            SysMenu sourceSysMenu = sysMenuService.get(sysMenu.getId());
            if(sourceSysMenu==null){
                //没有记录
                result.setMessage(Constant.I18nMessage.RECORD_NOT_FOUND);
                result.setCode(Constant.Application.FAILURE);
                return result;
            }

            //菜单名称
            sourceSysMenu.setTitle(sysMenu.getTitle());
            //菜单图标
            sourceSysMenu.setIcon(sysMenu.getIcon());
            //显示方式
            sourceSysMenu.setShowMode(sysMenu.getShowMode());
            //描述
            sourceSysMenu.setDescription(sysMenu.getDescription());
            //状态
            sourceSysMenu.setStatus(sysMenu.getStatus());
            //资源
            sourceSysMenu.setResourceId(sysMenu.getResourceId());
            //拼音
            sourceSysMenu.setTitleFirstSpell(PinyinUtil.cn2FirstSpell(sysMenu.getTitle()));

            sysMenuService.update(sourceSysMenu);
            sysMenu = sourceSysMenu;
        }catch (Exception ex){
            logger.error("Save Method (Update) SysMenu Error : {}", ex.getMessage());
            result.setMessage(Constant.I18nMessage.UPDATE_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.UPDATE_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }
    
    // 删除
    @ResponseBody
    @RequestMapping(value = "del", method = RequestMethod.POST)
    @Log(module = "SysMenu", moduleName = "菜单", action = "ajaxDel", actionName = "菜单删除")
    public JsonResult ajaxDel(HttpServletRequest request, @RequestParam(value = "id") Long id){
        JsonResult result = new JsonResult(false);
        try{
            if(0==sysMenuService.deleteTree(id)){
                //没有记录
                return new JsonResult("failure", new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND));
            }
        }catch (Exception ex){
            logger.error("Del Method (Del) SysMenu Error : {}", ex.getMessage());
            result.setMessage(Constant.I18nMessage.DEL_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.DEL_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

    //修改排序和结构
    @ResponseBody
    @RequestMapping(value ="sort", method = RequestMethod.POST)
    @Log(module = "SysMenu", moduleName = "菜单", action = "ajaxSort", actionName = "菜单排序")
    public JsonResult ajaxSort(HttpServletRequest request, @RequestBody List<SortInfo> sortInfo){
    	JsonResult result = new JsonResult(false);
        try{
            sysMenuService.sort(sortInfo);
        }catch (Exception ex){
        	// 排序失败
            logger.error("Save Method (sort) SysMenu Error :{} ", ex.getMessage());
            result.setMessage(Constant.I18nMessage.SORT_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        result.setMessage(Constant.I18nMessage.SORT_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }
    
}
