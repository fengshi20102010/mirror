package com.mirror.tk.web.exception;

import org.springframework.http.HttpStatus;
/**
 * 方法不允许访问
 * 
 * HttpStatus 405
 * 
 */
public class MethodNotAllowedException extends WebServiceException {

	private static final long serialVersionUID = 1L;

	public MethodNotAllowedException(String errorCode, String detail) {
		super(HttpStatus.METHOD_NOT_ALLOWED.value(), errorCode, detail);
	}

	public MethodNotAllowedException(String errorCode, String detail, Throwable cause) {
		super(HttpStatus.METHOD_NOT_ALLOWED.value(), errorCode, detail, cause);
	}
}
