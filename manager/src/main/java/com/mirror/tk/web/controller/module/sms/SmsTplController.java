package com.mirror.tk.web.controller.module.sms;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import com.mirror.tk.core.module.sms.domain.SmsTpl;
import com.mirror.tk.core.module.sms.service.SmsTplService;
import com.mirror.tk.framework.common.web.support.JsonListResult;
import com.mirror.tk.framework.common.web.support.JsonResult;
import com.mirror.tk.web.common.Constant;
import com.mirror.tk.web.controller.CommonController;


@Controller
@RequestMapping(value = "/module/sms")
public class SmsTplController extends CommonController {

    @Resource
    private SmsTplService smsTplService;
    
    //列表
    private static final String SMS_LIST = "/module/sms/sms_list";
    //详情
    private static final String SMS_VIEW = "/module/sms/sms_view";
    //添加、修改
    private static final String SMS_DETAIL = "/module/sms/sms_detail";

    /**
     * 列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value ="list", method = RequestMethod.GET)
    public String list(HttpServletRequest request, ModelMap map) {

	      //创建时间 默认时间范围3个月
	      map.put("endCreateTime", Calendar.getInstance().getTime());
	      map.put("beginCreateTime", DateUtils.addMonths(Calendar.getInstance().getTime(), -3));

        return SMS_LIST;
    }
    
    /**
     * 列表
     *
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value ="list", method = RequestMethod.POST)
    public JsonListResult<SmsTpl> ajaxList(HttpServletRequest request,
                                    @RequestParam(value="id", required=false) Long id,
                                    @RequestParam(value="name", required=false) String name,
                                    @RequestParam(value="tpl", required=false) String tpl,
                                    @RequestParam(value="status", required=false) Integer status,
                                    @RequestParam(value="description", required=false) String description,
                                    @RequestParam(value="beginCreateTime", required=false) Date beginCreateTime,
                                    @RequestParam(value="endCreateTime", required=false) Date endCreateTime
                                    ) {

        //创建时间 默认时间范围3个月
        if(endCreateTime==null){
            endCreateTime=Calendar.getInstance().getTime();
        }
        if(beginCreateTime==null){
            beginCreateTime = DateUtils.addMonths(endCreateTime, -3);
        }

        Map<String, Object> searchMap = new HashMap<String, Object>();
        Map<String, Boolean> sortMap = new HashMap<String, Boolean>();

        //ID
        if(id!=null){
            searchMap.put("EQ_id", id);
        }
        //模板名称
        if(StringUtils.isNotBlank(name)){
            searchMap.put("LIKE_name", name);
        }
        //模板内容
        if(StringUtils.isNotBlank(tpl)){
            searchMap.put("LIKE_tpl", tpl);
        }
        //模板状态
        if(status!=null){
            searchMap.put("EQ_status", status);
        }
        //模板描述
        if(StringUtils.isNotBlank(description)){
            searchMap.put("LIKE_description", description);
        }
        //创建时间
        if(beginCreateTime!=null){
            searchMap.put("GTE_createTime", DateUtils.truncate(beginCreateTime, Calendar.DATE));
        }
        if(endCreateTime!=null){
            searchMap.put("LTE_createTime", DateUtils.truncate(DateUtils.addDays(endCreateTime, 1), Calendar.DATE));
        }

        return toJsonListResult(smsTplService.query(getPageInfo(request), searchMap, sortMap));
    }

    /**
     * 增加
     * 
     * @param request
     * @param map
     * @return
     */
    @RequestMapping(value ="add", method = RequestMethod.GET)
    public String add(HttpServletRequest request, ModelMap map){
        return SMS_DETAIL;
    }

    /**
     * 增加
     * 
     * @param request
     * @param smsTpl
     * @return
     */
    @ResponseBody
    @RequestMapping(value ="add", method = RequestMethod.POST)
    public JsonResult ajaxAdd(HttpServletRequest request, SmsTpl smsTpl){
        JsonResult result = new JsonResult(false);
        try{
            //ID
            smsTpl.setId(null);

            smsTplService.save(smsTpl);
        }catch (Exception ex){
            logger.error("Save Method (inster) SmsTpl Error : {}", smsTpl.toString(), ex);
            //增加失败
            result.setMessage(Constant.I18nMessage.SAVE_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        //返回结果
        result.setMessage(Constant.I18nMessage.SAVE_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

    /**
     * 修改
     * 
     * @param request
     * @param map
     * @param id
     * @return
     */
    @RequestMapping(value ="update", method = RequestMethod.GET)
    public String update(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id){
        SmsTpl smsTpl = smsTplService.get(id);
        if(smsTpl==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
        map.put("item", smsTpl);
        return SMS_DETAIL;
    }

    /**
     * 修改
     * 
     * @param request
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value ="update", method = RequestMethod.POST)
    public JsonResult ajaxUpdate(HttpServletRequest request, SmsTpl smsTpl){
        JsonResult result = new JsonResult(false);
        if(smsTpl==null || smsTpl.getId()==null){
            //没有记录
            result.setMessage(Constant.I18nMessage.RECORD_NOT_FOUND);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        try{
            SmsTpl sourceSmsTpl = smsTplService.get(smsTpl.getId());
            if(sourceSmsTpl==null){
                //没有记录
                result.setMessage(Constant.I18nMessage.RECORD_NOT_FOUND);
                result.setCode(Constant.Application.FAILURE);
                return result;
            }

            //模板名称
            sourceSmsTpl.setName(smsTpl.getName());
            //模板内容
            sourceSmsTpl.setTpl(smsTpl.getTpl());
            //模板状态
            sourceSmsTpl.setStatus(smsTpl.getStatus());
            //模板描述
            sourceSmsTpl.setDescription(smsTpl.getDescription());
            //创建时间
            sourceSmsTpl.setCreateTime(smsTpl.getCreateTime());
            smsTplService.update(sourceSmsTpl);
            smsTpl = sourceSmsTpl;
        }catch (Exception ex){
            logger.error("Save Method (Update) SmsTpl Error : " + smsTpl.toString(), ex);
            //修改失败
            result.setMessage(Constant.I18nMessage.SAVE_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        //操作提示
        result.setMessage(Constant.I18nMessage.SAVE_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

    /**
     * 查看
     * 
     * @param request
     * @param map
     * @param id
     * @return
     */
    @RequestMapping(value ="view")
    public String show(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id){
        SmsTpl smsTpl = smsTplService.get(id);
        if(smsTpl==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
        map.put("item", smsTpl);
        return SMS_VIEW;
    }

    /**
     * 删除
     * 
     * @param request
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value ="del")
    public JsonResult delete(HttpServletRequest request, @RequestParam(value = "id") Long id){
        JsonResult result = new JsonResult(false);
        try{
            smsTplService.removeById(id);
        }catch (Exception ex){
            logger.error("Del Method (Del) SmsTpl Error : {}", id, ex);
            //删除失败
            result.setMessage(Constant.I18nMessage.DEL_FAILURE);
            result.setCode(Constant.Application.FAILURE);
            return result;
        }
        //返回结果
        result.setMessage(Constant.I18nMessage.DEL_SUCCESS);
        result.setCode(Constant.Application.SUCCESS);
        result.setSuccess(true);
        return result;
    }

}
