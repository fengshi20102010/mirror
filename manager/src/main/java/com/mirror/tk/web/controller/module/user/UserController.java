package com.mirror.tk.web.controller.module.user;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import com.mirror.tk.core.log.annotation.Log;
import com.mirror.tk.core.module.user.domain.User;
import com.mirror.tk.core.module.user.service.UserService;
import com.mirror.tk.framework.common.web.support.JsonListResult;
import com.mirror.tk.framework.common.web.support.JsonResult;
import com.mirror.tk.web.common.Constant;
import com.mirror.tk.web.controller.CommonController;


@Controller
@RequestMapping(value = "/module/user")
public class UserController extends CommonController {

    @Resource
    private UserService userService;
    
    // 列表
    private static final String USER_LIST = "module/user/user_list";
    // 详情
    private static final String USER_VIEW = "module/user/user_view";

    // 列表-查看
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(HttpServletRequest request, ModelMap map) {
        return USER_LIST;
    }
    
    // 列表-数据
    @ResponseBody
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public JsonListResult<User> ajaxList(HttpServletRequest request,
            @RequestParam(value="username", required=false) String username,
            @RequestParam(value="phone", required=false) String phone,
            @RequestParam(value="sex", required=false) Integer sex,
            @RequestParam(value="status", required=false) Integer status,
            @RequestParam(value="teamName", required=false) String teamName,
            @RequestParam(value="type", required=false) Integer type,
            @RequestParam(value="income", required=false) Integer income
            ) {

        Map<String, Object> searchMap = new HashMap<String, Object>();
        Map<String, Boolean> sortMap = new HashMap<String, Boolean>();

        //用户名
        if(StringUtils.isNotBlank(username)){
            searchMap.put("LIKE_username", username);
        }
        //电话
        if(StringUtils.isNotBlank(phone)){
            searchMap.put("LIKE_phone", phone);
        }
        //性别
        if(sex!=null){
            searchMap.put("EQ_sex", sex);
        }
        //用户状态
        if(status!=null){
            searchMap.put("EQ_status", status);
        }
        //团队或个人名称
        if(StringUtils.isNotBlank(teamName)){
            searchMap.put("LIKE_teamName", teamName);
        }
        //团队类型
        if(type!=null){
            searchMap.put("EQ_type", type);
        }
        //收入
        if(income!=null){
            searchMap.put("EQ_income", income);
        }
        return toJsonListResult(userService.query(fromJsonListResult(request), searchMap, sortMap));
    }

    // 详情
    @RequestMapping(value = "view", method = RequestMethod.GET)
    public String show(HttpServletRequest request, ModelMap map, @RequestParam(value = "id") Long id){
        User user = userService.get(id);
        if(user==null){
            //没有记录
            return super.operFailure(new RequestContext(request).getMessage(Constant.I18nMessage.RECORD_NOT_FOUND), map);
        }
        map.put("item", user);
        return USER_VIEW;
    }

    // 修改状态
    @ResponseBody
    @RequestMapping(value = "status", method = RequestMethod.POST)
    @Log(module = "User", moduleName = "用户信息", action = "ajaxStatus", actionName = "用户信息修改状态")
    public JsonResult ajaxStatus(HttpServletRequest request, @RequestParam(value = "id") Long id, @RequestParam(value = "status") Integer status){
    	JsonResult result = new JsonResult(false);
    	try{
    		if(userService.updateByStatus(id, status)){
    			result.setMessage(Constant.I18nMessage.STATUS_SUCCESS);
    			result.setCode(Constant.Application.SUCCESS);
    			result.setSuccess(true);
    			return result;
    		}
    	}catch (Exception ex){
    		logger.error("Save Method (Update) Goods Error : {}", ex.getMessage());
    	}
    	result.setMessage(Constant.I18nMessage.STATUS_FAILURE);
    	result.setCode(Constant.Application.FAILURE);
    	return result;
    }

}
