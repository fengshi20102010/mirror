package com.mirror.tk.web.common.support;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import com.mirror.tk.core.module.sys.domain.SysRole;
import com.mirror.tk.core.module.sys.domain.SysUser;

/**
 * sessionUser
 */
public class SessionUser implements HttpSessionBindingListener{

    public static final String SESSION_USER_OBJECT_KEY="session_user_obj";

    private SessionUser(){}

    public static SessionUser bulider(SysUser sysUser){
        return new SessionUser().update(sysUser);
    }

    public SessionUser update(SysUser sysUser){
        this.setId(sysUser.getId());
        this.setUsername(sysUser.getUsername());
        this.setNickname(sysUser.getNickname());
        this.setType(sysUser.getType());
        this.setRole(sysUser.getRole());
        return this;
    }

	/** 用户ID*/
	private Long id;

	/** 登录用户名 */
	private String username;
	
	/** 操作员姓名 */
    private String nickname;
	
	/** 用户类型 */
	private Integer type;

    /** 角色 */
    private SysRole role;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

    public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public SysRole getRole() {
        return role;
    }

    public void setRole(SysRole role) {
        this.role = role;
    }

    @Override
    public void valueBound(HttpSessionBindingEvent event) {
        if(SESSION_USER_OBJECT_KEY.equals(event.getName()) && event.getValue() instanceof SessionUser){
            SessionUser sessionUser = (SessionUser) event.getValue();
            SessionFace.userMap.put(sessionUser.getUsername(), sessionUser);
        }
    }

    @Override
    public void valueUnbound(HttpSessionBindingEvent event) {
        if(SESSION_USER_OBJECT_KEY.equals(event.getName()) && event.getValue() instanceof SessionUser){
            SessionUser sessionUser = (SessionUser) event.getValue();
            SessionFace.userMap.remove(sessionUser.getUsername());
        }
    }
    
}
