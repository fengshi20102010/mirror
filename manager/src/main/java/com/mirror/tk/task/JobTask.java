package com.mirror.tk.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 定时任务父类 记录定时任务执行情况 尚未记录到数据库，暂存到日志列表
 */
public abstract class JobTask {

	private final Logger logger = LogManager.getLogger(getClass());

	/**
	 * 定时任务描述
	 */
	protected String taskDiscription;

	public void setTaskDiscription(String taskDiscription) {
		this.taskDiscription = taskDiscription;
	}

	public String getTaskDiscription() {
		setTaskDiscription(getClass().getSimpleName());
		return taskDiscription;
	}

	/**
	 * spring调用方法
	 */
	public void run() {
		try {
			logger.info("======开始执行" + getTaskDiscription() + "定时任务======");
			execute();
		} catch (Throwable e) {
			logger.error("任务" + getTaskDiscription() + "执行失败", e);
		}
		logger.info("======结束执行" + getTaskDiscription() + "定时任务======");
	}

	/**
	 * 定时任务应重写此方法
	 * @throws Throwable
	 */
	protected abstract void execute() throws Throwable;

}

