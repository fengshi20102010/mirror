package com.mirror.tk.task.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mirror.tk.core.biz.service.GoodsBiz;
import com.mirror.tk.task.JobTask;

@Component
public class RefreshGoodsInfoTask extends JobTask {

	private static final String TASK_NAME = "刷新商品信息";
	
	@Resource
	private GoodsBiz goodsBiz;

	@Override
	public String getTaskDiscription() {
		return TASK_NAME;
	}

	@Override
	@Transactional
	protected void execute() throws Throwable {
		goodsBiz.updateGoodsTask();
	}
	
}
