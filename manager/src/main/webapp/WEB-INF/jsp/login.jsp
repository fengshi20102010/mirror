<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	    <title>魔镜助手 - 登录</title>
	    <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	    <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
	    <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
	    <link href="${pageContext.request.contextPath }/resource/css/login.min.css" rel="stylesheet">
	    <!--[if lt IE 9]>
	    	<meta http-equiv="refresh" content="0;ie.html" />
	    <![endif]-->
	    <script>
	        if(window.top!==window.self){window.top.location=window.location};
	    </script>
	</head>
	<body class="signin">
	    <div class="signinpanel">
	        <div class="row">
	            <div class="col-sm-7">
	                <div class="signin-info">
	                    <div class="logopanel m-b">
	                        <h1>魔镜助手</h1>
	                    </div>
	                    <div class="m-b"></div>
	                    <h4>欢迎使用 <strong>魔镜助手</strong></h4>
	                    <ul class="m-b">
	                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 优势一</li>
	                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 优势二</li>
	                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 优势三</li>
	                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 优势四</li>
	                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 优势五</li>
	                    </ul>
	                    <strong>还没有账号？ <a href="#">立即注册&raquo;</a></strong>
	                </div>
	            </div>
	            <div class="col-sm-5">
		            <c:if test="${not empty loginError}">
	                    <div class="alert alert-danger" role="alert"><strong>登陆失败：</strong>${loginError}</div>
	                </c:if>
	                <form action="/login.html" method="post">
	                    <h4 class="no-margins">登录：</h4>
	                    <p class="m-t-md">登录到魔镜助手</p>
	                    <input id="username" name="username" type="text" class="form-control uname" placeholder="账户" />
	                    <input id="password" name="password" type="password" class="form-control pword m-b" placeholder="密码" />
	                    <a href="#">忘记密码了？</a>
	                    <button type="submit" class="btn btn-success btn-block">登录</button>
	                </form>
	            </div>
	        </div>
	        <div class="signup-footer">
	            <div class="pull-left">
	                &copy; 2016 All Rights Reserved. 魔镜助手
	            </div>
	        </div>
	    </div>
	</body>
</html>