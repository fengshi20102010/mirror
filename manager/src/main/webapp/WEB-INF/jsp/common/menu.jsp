<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="nav-close">
    	<i class="fa fa-times-circle"></i>
    </div>
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span><img alt="image" class="img-circle" src="${pageContext.request.contextPath }/resource/images/profile_small.jpg" /></span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                        <span class="block m-t-xs"><strong class="font-bold">${user.username}</strong></span>
                        <span class="text-muted text-xs block">${user.role.roleName}<b class="caret"></b></span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a class="J_menuItem" href="form_avatar.html">修改头像</a>
                        </li>
                        <li><a class="J_menuItem" href="personal.html" id="demo">个人资料</a>
                        </li>
                        <li><a class="J_menuItem" href="contacts.html">联系我们</a>
                        </li>
                        <li><a class="J_menuItem" href="mailbox.html">信箱</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="javascript:void(0)" class="logout">安全退出</a>
                        </li>
                    </ul>
                </div>
                <div class="logo-element">FS
                </div>
            </li>
            
            <c:forEach items="${authorisedMenus}" var="menu">
				<li>
	                <a href="#">
	                    <i class="${menu.iconCls}"></i>
	                    <span class="nav-label">${menu.text}</span>
	                    <span class="fa arrow"></span>
	                </a>
	                <ul class="nav nav-second-level">
	                	<c:forEach items="${menu.children}" var="resc">
							<li>
		                        <a class="J_menuItem" href="${pageContext.request.contextPath }${resc.attributes.url}" data-index="0">${resc.text}</a>
		                    </li>
						</c:forEach>
	                </ul>
	            </li>
			</c:forEach>
        </ul>
    </div>
</nav>