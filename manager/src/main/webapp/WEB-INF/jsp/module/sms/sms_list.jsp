<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="renderer" content="webkit|ie-comp|ie-stand">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
		<meta http-equiv="Cache-Control" content="no-siteapp" />
		<LINK rel="Bookmark" href="/favicon.ico">
		<LINK rel="Shortcut Icon" href="/favicon.ico" />
		<!--[if lt IE 9]>
			<script type="text/javascript" src="${pageContext.request.contextPath }/resource/components/html5.js"></script>
			<script type="text/javascript" src="${pageContext.request.contextPath }/resource/components/respond.min.js"></script>
			<script type="text/javascript" src="${pageContext.request.contextPath }/resource/components/PIE_IE678.js"></script>
		<![endif]-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/H-ui.min.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/H-ui.admin.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/components/Hui-iconfont/1.0.7/iconfont.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/components/icheck/icheck.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/skin/default/skin.css" id="skin" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/style.css" />
		<!--[if IE 6]>
			<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
			<script>DD_belatedPNG.fix('*');</script>
		<![endif]-->
		<title>短信模板记录表列表</title>
	</head>
	<body>
		<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 短信模板记录表管理 <span class="c-gray en">&gt;</span> 短信模板记录表列表 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
		<div class="page-container">
			<div class="text-c"> 日期范围：
				<input type="text" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'datemax\')||\'%y-%M-%d\'}'})" id="datemin" class="input-text Wdate" style="width:120px;">
				-
				<input type="text" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'datemin\')}',maxDate:'%y-%M-%d'})" id="datemax" class="input-text Wdate" style="width:120px;">
				<input type="text" class="input-text" style="width:250px" placeholder="输入管理员名称" id="username" name="username">
				<button type="submit" class="btn btn-success" id="" name=""><i class="Hui-iconfont">&#xe665;</i> 搜用户</button>
			</div>
			
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-2 col-md-2 col-lg-1 control-label">ID</label>

                        <div class="col-sm-8">
                            <input class="form-control" name="id" id="id" type="text" value="${param.id}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-2 col-md-2 col-lg-1 control-label">模板名称</label>

                        <div class="col-sm-8">
                            <input class="form-control" name="name" id="name" type="text" value="${param.name}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-2 col-md-2 col-lg-1 control-label">模板内容</label>

                        <div class="col-sm-8">
                            <input class="form-control" name="tpl" id="tpl" type="text" value="${param.tpl}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-2 col-md-2 col-lg-1 control-label">模板状态</label>

                        <div class="col-sm-8">
                            <input class="form-control" name="status" id="status" type="text" value="${param.status}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-2 col-md-2 col-lg-1 control-label">模板描述</label>

                        <div class="col-sm-8">
                            <input class="form-control" name="description" id="description" type="text" value="${param.description}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-2 col-md-2 col-lg-1 control-label">创建时间</label>

                        <div class="col-sm-8">
                            <script type="text/javascript">
                                require(["daterangepicker"], function($){
                                    $(function(){
                                        var elm = $("#createTime");
                                        $(elm).daterangepicker({
                                            startDate: $(elm).prev().prev().val(),
                                            endDate: $(elm).prev().val(),
                                            format: "YYYY-MM-DD",
                                            showDropdowns: true
                                        }, function(start, end){
                                            $(elm).find(".date-title").html(start.toDateStr() + " 至 " + end.toDateStr());
                                            $(elm).prev().prev().val(start.toDateStr());
                                            $(elm).prev().val(end.toDateStr());
                                        });
                                    });
                                });
                            </script>
                            <input name="beginCreateTime" type="hidden" value="<fmt:formatDate value="${beginCreateTime}" pattern="yyyy-MM-dd" />">
                            <input name="endCreateTime" type="hidden" value="<fmt:formatDate value="${endCreateTime}" pattern="yyyy-MM-dd" />">
                            <button class="btn btn-default daterange" id="createTime" type="button" data-original-title="" title=""><span class="date-title"><fmt:formatDate value="${beginCreateTime}" pattern="yyyy-MM-dd" /> 至 <fmt:formatDate value="${endCreateTime}" pattern="yyyy-MM-dd" /></span> <i class="fa fa-calendar"></i></button>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-lg-1">
                            <button class="btn btn-default"><i class="fa fa-search"></i> 搜索</button>
                        </div>
                    </div>
                    
			<div class="cl pd-5 bg-1 bk-gray mt-20"> <span class="l"><a href="javascript:;" onclick="datadel()" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe6e2;</i> 批量删除</a> <a href="javascript:;" onclick="admin_add('添加管理员','admin-add.html','800','500')" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe600;</i> 添加管理员</a></span> <!-- <span class="r">共有数据：<strong>54</strong> 条</span> --> </div>
			<%--列表--%>
			<table class="table table-border table-bordered table-bg table-sort">
				<thead>
					<tr>
						<th scope="col" colspan="10">短信模板记录表列表</th>
					</tr>
                    <tr class="text-c">
                    	<th width="25"><input type="checkbox" name="" value=""></th>
                        <th>ID</th>
                        <th>模板名称</th>
                        <th>模板内容</th>
                        <th>模板状态</th>
                        <th>模板描述</th>
                        <th>创建时间</th>
                        <th>操作</th>
                    </tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/app/config.js"></script>
		<script type="text/javascript">
			require(['jquery', 'jquery.dataTables', 'layer', 'datePicker', 'hui.admin'],function($, dataTable, layer){
				//初始化layer
				layer.config({
					path: '${pageContext.request.contextPath }/resource/components/layer/2.2/'
				});
				//初始化dataTables
				$('.table-sort').dataTable({
					autoWidth: false,
					serverSide: true,
					processing: false,
					pageLength: 10,
					ordering: false,
					language: {
						url: '${pageContext.request.contextPath }/resource/js/lib/de_DE.txt'
					},
					ajax: {
						type: "POST",
						url: location.href,
						dataSrc: "data",
					},
					columns: [
								{
									data: 'id',
									render: function(data, type, full, meta){
										return '<input type="checkbox" value="'+data+'" name="">';
									}
								},
			                        {
			                        	data: 'id'
			                        },
			                        {
			                        	data: 'name'
			                        },
			                        {
			                        	data: 'tpl'
			                        },
			                        {
			                        	data: 'status'
			                        },
			                        {
			                        	data: 'description'
			                        },
			                        {
			                        	data: 'createTime'
			                        },
			                    {
									render: function(data, type, full, meta){
									        content = '';
									        <security:hasPermission name="sms:view">
			                                	content += '<a title="详情" href="javascript:;" class="ml-5 view" style="text-decoration:none"><i class="Hui-iconfont"></i></a>';
			                                </security:hasPermission>
			                                <security:hasPermission name="sms:update">
			                                	content += '<a title="编辑" href="javascript:;" class="update" style="text-decoration:none" ><i class="Hui-iconfont"></i></a>';
			                                </security:hasPermission>
			                                <security:hasPermission name="sms:del">
			                                	content += '<a title="删除" href="javascript:;" class="ml-5 del" style="text-decoration:none"><i class="Hui-iconfont"></i></a>';
											</security:hasPermission>
											return content;
									}
								}
							],
					createdRow: function(row, data, index){
						$(row).addClass('text-c');
						$(row).attr('id', data.id);
					}
					
				});
				
				// 添加
				$('.add').on('click', function(){
					layer_show('增加短信模板记录表','add.html',"800","500");
				});
				
				// 批量删除
				$('.del_all').on('click', function(){
					
				});
				
				// 查看
				$('.table-sort').delegate('.view', 'click', function(){
					layer_show('短信模板记录表详情','view.html?id=' + $(this).parents('tr').attr('id'));
				});
				
				// 修改
				$('.table-sort').delegate('.update', 'click', function(){
					layer_show('短信模板记录表修改','update.html?id=' + $(this).parents('tr').attr('id'));
				});
				
				// 删除
				$('.table-sort').delegate('.del', 'click', function(){
					$this = $(this);
					var id = $(this).parents('tr').attr('id');
					layer.confirm('确认要删除吗？',function(index){
						$.post('del.html', {"id": id}, function(d){
							if(d.success){
								$this.parents("tr").remove();
								layer.msg('已删除!',{icon:1,time:1000});
								return;
							}
							layer.msg(d.message,{icon:2,time:1000});
						},"json");
					});
				});
				
			})
		</script>
	</body>
</html>