<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="renderer" content="webkit|ie-comp|ie-stand">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
		<meta http-equiv="Cache-Control" content="no-siteapp" />
		<LINK rel="Bookmark" href="/favicon.ico">
		<LINK rel="Shortcut Icon" href="/favicon.ico" />
		<!--[if lt IE 9]>
			<script type="text/javascript" src="${pageContext.request.contextPath }/resource/components/html5.js"></script>
			<script type="text/javascript" src="${pageContext.request.contextPath }/resource/components/respond.min.js"></script>
			<script type="text/javascript" src="${pageContext.request.contextPath }/resource/components/PIE_IE678.js"></script>
		<![endif]-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/H-ui.min.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/H-ui.admin.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/components/Hui-iconfont/1.0.7/iconfont.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/components/icheck/icheck.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/skin/default/skin.css" id="skin" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/style.css" />
		<!--[if IE 6]>
			<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
			<script>DD_belatedPNG.fix('*');</script>
		<![endif]-->
		<title>短信模板记录表</title>
	</head>
	<body>
		<article class="page-container">
			<form class="form form-horizontal" method="post" id="form1">
            <c:if test="${not empty item}">
            	<input type="hidden" name="id" value="${item.id}" />
            </c:if>
              	<div class="row cl">
									<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>模板名称：</label>
									<div class="formControls col-xs-8 col-sm-9">
                    	<input type="text" class="input-text" name="name" id="name" value="${item.name}" placeholder="请输入模板名称">
						</div>
					</div>
              	<div class="row cl">
									<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>模板内容：</label>
									<div class="formControls col-xs-8 col-sm-9">
                    	<input type="text" class="input-text" name="tpl" id="tpl" value="${item.tpl}" placeholder="请输入模板内容">
						</div>
					</div>
              	<div class="row cl">
									<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>模板状态：</label>
									<div class="formControls col-xs-8 col-sm-9 skin-minimal">
	                    <input type="text" class="input-text" name="status" id="status" value="${item.status}">
						</div>
					</div>
              	<div class="row cl">
									<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>模板描述：</label>
									<div class="formControls col-xs-8 col-sm-9">
                    	<input type="text" class="input-text" name="description" id="description" value="${item.description}" placeholder="请输入模板描述">
						</div>
					</div>
              	<div class="row cl">
									<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>创建时间：</label>
									<div class="formControls col-xs-8 col-sm-9">
                      <script type="text/javascript">
                          require(["daterangepicker"], function($){
                              $(function(){
                                  var elm = $("#createTime");
                                  $(elm).daterangepicker({
                                      format: "YYYY-MM-DD",
                                      showDropdowns: true,
                                      singleDatePicker:true
                                  });
                              });
                          });
                      </script>
                      <div class="input-prepend input-group">
                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                          <input class="form-control" type="text" name="createTime" id="createTime" readonly="readonly" value="<fmt:formatDate value="${item.createTime}" pattern="yyyy-MM-dd" />">
                      </div>
						</div>
					</div>
				<div class="row cl">
					<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
						<input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
					</div>
				</div>
			</form>
		</article>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/app/config.js"></script>
		<script type="text/javascript">
			require(['jquery', 'layer', 'jquery.icheck', 'jquery.validate', 'datePicker', 'hui.admin'],function($, layer){
				//初始化layer
				layer.config({
					path: '${pageContext.request.contextPath }/resource/components/layer/2.2/'
				});
				$('.skin-minimal input').iCheck({
					checkboxClass: 'icheckbox-blue',
					radioClass: 'iradio-blue',
					increaseArea: '20%'
				});
				$("#form1").validate({
					rules: {
                  name : {
                  required: true
              },
                  tpl : {
                  required: true
              },
                  status : {
                  required: true,
                  number:true
              },
                  description : {
                  required: true
              },
                  createTime : {
                  required: true,
                  date:true
              }
          },
          messages: {
              name : {
                  required: ' '
              },
              tpl : {
                  required: ' '
              },
              status : {
                  required: ' ',
                  number:' '
              },
              description : {
                  required: ' '
              },
              createTime : {
                  required: ' ',
                  date:' '
              }
          },
					onkeyup:false,
					focusCleanup:true,
					success:"valid",
					submitHandler:function(form){
						/* $(form).ajaxSubmit();
						var index = parent.layer.getFrameIndex(window.name);
						parent.$('.btn-refresh').click();
						parent.layer.close(index); */
					}
				});
			})
		</script>
	</body>
</html>