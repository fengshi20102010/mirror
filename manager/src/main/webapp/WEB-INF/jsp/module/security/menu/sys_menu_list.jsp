<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Cache-Control" content="no-siteapp" />
		<title>魔镜助手 - 菜单编辑器</title>
		<!--[if lt IE 9]>
	    	<meta http-equiv="refresh" content="0;ie.html" />
	    <![endif]-->
		<link rel="shortcut icon" href="favicon.ico">
		<link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	    <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
	    <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
	    <style>
			.edit-menus span {
				color: #fff;
				padding: 5px 8px;
				border-radius: 4px;
				font-weight: normal;
				top: 0;
			}
			
			.edit-menus span.glyphicon-search {
				background: #12D6B2;
			}
			
			.edit-menus span.glyphicon-edit {
				background: #0F8FDA;
			}
			
			.edit-menus span.glyphicon-trash {
				background: #F30446;
			}
			
			.edit-menus span.glyphicon-plus {
				background: #12D6B2;
			}
			
			.dd-nodrag.end {
				padding: 10px;
				display: block;
				margin: 5px 0;
				color: #333;
				text-decoration: none;
				border: 1px solid #e7eaec;
				background: #D2D2D2;
				border-radius: 3px;
				box-sizing: border-box;
				-moz-box-sizing: border-box;
				cursor: pointer;
				-webkit-user-select: none;
				-moz-user-select: none;
				user-select: none;
			}
		</style>
	</head>
	<body class="gray-bg">
		<div class="wrapper wrapper-content  animated fadeInRight">
			<div class="row">
				<div class="col-sm-4">
					<div id="nestable-menu">
						<button type="button" data-action="expand-all" class="btn btn-white btn-sm">展开所有</button>
						<button type="button" data-action="collapse-all" class="btn btn-white btn-sm">收起所有</button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="ibox ">
						<div class="ibox-title">
							<h5>菜单设计器</h5>
						</div>
						<div class="ibox-content">
							<p class="m-b-lg">
								菜单设计器将根据用户设计来展示后台菜单展示情况
								删除的时候请小心，他可能会让你的菜单不可用
								删除时如有子菜单，将会一并删除
								排序性能较低，请谨慎操作
								请根据实际需求来调整菜单
							</p>
							<div class="dd" id="nestable">
								<ol class="dd-list">
									<c:forEach items="${menuList }" var="item" varStatus="i">
										<li class="dd-item parents" data-id="${item.id }" data-sort="${item.sortNo }" data-parent-id="${item.parentId }">
											<div class="dd-handle">
												<span class="label label-info"><i class="${item.icon }"></i></span> ${item.title }
												<div class="dd-nodrag pull-right edit-menus">
													<security:hasPermission name="sys:menu:view">
														<span class="glyphicon glyphicon-search">查看</span>
													</security:hasPermission>
													<security:hasPermission name="sys:menu:update">
														<span class="glyphicon glyphicon-edit">编辑</span>
													</security:hasPermission>
													<security:hasPermission name="sys:menu:del">
														<span class="glyphicon glyphicon-trash">删除</span>
													</security:hasPermission>
												</div>
											</div>
											<ol class="dd-list">
												<c:forEach items="${item.subMenus }" var="subItem" varStatus="j">
													<li class="dd-item" data-id="${subItem.id }" data-sort="${subItem.sortNo }" data-parent-id="${subItem.parentId }">
														<div class="dd-handle">
															<span class="pull-right"> ${subItem.description }</span>
															<span class="label label-info"><i class="${subItem.icon }"></i></span> ${subItem.title }
															<div class="dd-nodrag pull-right edit-menus">
																<security:hasPermission name="sys:menu:view">
																	<span class="glyphicon glyphicon-search">查看</span>
																</security:hasPermission>
																<security:hasPermission name="sys:menu:update">
																	<span class="glyphicon glyphicon-edit">编辑</span>
																</security:hasPermission>
																<security:hasPermission name="sys:menu:del">
																	<span class="glyphicon glyphicon-trash">删除</span>
																</security:hasPermission>
															</div>
														</div>
													</li>
													<c:set var="sortL2" value="${subItem.sortNo }" />
												</c:forEach>
												<security:hasPermission name="sys:menu:add">
													<li class="dd-item add" data-id="0" data-sort="${sortL2+1 }" data-parent-id="${item.id }">
														<div class="dd-nodrag end">
															<span class="glyphicon glyphicon-plus">添加子菜单</span> 
														</div>
													</li>
												</security:hasPermission>
											</ol>
										</li>
										<c:set var="sortL1" value="${item.sortNo }" />
									</c:forEach>
									<security:hasPermission name="sys:menu:add">
										<li class="dd-item add" data-id="0" data-sort="${sortL1+1 }" data-parent-id="0">
											<div class="dd-nodrag end">
												<span class="glyphicon glyphicon-plus">添加菜单</span> 
											</div>
										</li>
									</security:hasPermission>
								</ol>
							</div>
							<div class="m-t-md">
								<h5>数据：</h5>
							</div>
							<textarea id="nestable-output" class="form-control"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
		<script type="application/javascript">
			require(['jquery', 'layer', 'jquery.nestable', 'contabs.min', 'content.min', 'main.min'], function($, layer) {
				//初始化layer
				layer.config({
					path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
				});
				
				var updateOutput = function(e) {
					var list = e.length ? e : $(e.target), output = list.data("output");
					if(window.JSON) {
						output.val(window.JSON.stringify(list.nestable("serialize")))
					} else {
						output.val("浏览器不支持")
					}
				};
				
				// 初始化js
				$("#nestable").nestable({group: 1});
				
				// 全部展开/关闭
				$("#nestable-menu").on("click", function(e) {
					var target = $(e.target), action = target.data("action");
					if(action === "expand-all") {
						$(".dd").nestable("expandAll")
					}
					if(action === "collapse-all") {
						$(".dd").nestable("collapseAll")
					}
				})
				
				// 排序处理
				$('#nestable').on('change', function(e) {
					updateOutput($("#nestable").data("output", $("#nestable-output")));
					var list = e.length ? e : $(e.target), output = list.data("output");
					if(window.JSON) {
						output.val(window.JSON.stringify(list.nestable("serialize")));
						var sortInfo = window.JSON.stringify(list.nestable("serialize"));
						$.ajax({
				            type: "POST",
				            url: "sort.html",
				            data: sortInfo,
				            contentType: "application/json; charset=utf-8",
				            dataType: "json",
				            success: function (d) {
				                if(d.success){
									layer.msg('成功!',{icon:1,time:1000});
									return;
								}
								layer.msg(d.message,{icon:5,time:1000});
				            }
				        });
					} else {
						output.val('浏览器不支持');
						alert('浏览器不支持');
					}
				});
				
				// 增加菜单
				$('#nestable').delegate('.add', 'click', function(){
					$this = $(this);
					var pid = $(this).data('parentId');
					var sort = $(this).data('sort');
					var title = '添加菜单';
					var url = 'add.html?pid=' + pid + '&sort=' + sort;
					layerShow(title,url,"800","500");
				});
				
				// 按钮事件
				$('#nestable').delegate('.edit-menus span', 'click', function(){
					var id = title = url = null;
					id = $(this).parents('li').data('id');
					if($(this).hasClass('glyphicon-search')){
						title = '菜单详情';
						url = 'view.html?id=' + id;
						layerShow(title, url);
					} else if($(this).hasClass('glyphicon-edit')){
						title = '编辑菜单';
						url = 'update.html?id=' + id;
						layerShow(title, url);
					} else if($(this).hasClass('glyphicon-trash')){
						var $this = $(this);
						url = 'del.html';
						layer.confirm('确认删除吗?\n 如有子菜单将一并删除',function(index){
							$.post(url, {"id": id}, function(d){
								if(d.success){
									$this.parent().parent().parent().remove();
									layer.msg('已删除!',{icon:1,time:1000});
									return;
								}
								layer.msg('删除失败!',{icon:5,time:1000});
							},"json");
						});
					}
				});
			});
		</script>
	</body>
</html>