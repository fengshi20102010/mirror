<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title>魔镜助手 - 系统_角色表详情</title>
        <!--[if lt IE 9]>
            <meta http-equiv="refresh" content="0;ie.html" />
        <![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="col-sm-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-info-circle"></i> 详情
                        </div>
                        <div id="tree_a" class="panel-body">
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">角色名称</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.roleName}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">角色描述</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.description}
                                    </p>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
        <script>
            require(['jquery', 'layer', 'jsTree', 'contabs.min', 'content.min', 'main.min'], function($, layer) {
                //初始化layer
                layer.config({
                    path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
                });
                
                var data = [
						{
							"text" : "系统管理员",
							"state" : { "opened" : true },
							"children" : [
								{"text" : "小明", "state" : { "selected" : false },"icon" : "jstree-file"},
								{"text" : "小红", "state" : { "selected" : false },"icon" : "jstree-file"}
							]
						},
						{
							"text" : "综合报表",
							"state" : { "opened" : true },
							"children" : [
								{"text" : "增加", "state" : { "selected" : false },"icon" : "jstree-file"},
								{"text" : "删除", "state" : { "selected" : false },"icon" : "jstree-file"},
								{"text" : "编辑", "state" : { "selected" : false },"icon" : "jstree-file"},
								{"text" : "查看", "state" : { "selected" : false },"icon" : "jstree-file"},
								{"text" : "导入", "state" : { "selected" : true },"icon" : "jstree-file"},
								{"text" : "导出", "state" : { "selected" : true },"icon" : "jstree-file"}
							]
						}
					];

                $(tree_a).jstree({
					'core': {
	                    'data': data,
	                    "check_callback": true,
	                    'multiple': false,
	                },
	                "force_text": true,
	                plugins: ["sort", "types", "checkbox", "themes", "html_data"],
	                "checkbox": {
	                    "keep_selected_style": false,//是否默认选中
	                    "three_state": true,//父子级别级联选择
	                    "tie_selection": false
	                }                
				}).bind('click.jstree',function(event){
      				var tagger = $(event.currentTarget);
      				var ary = [];
      				$(tagger[0].childNodes[0]).find('li').each(function(){
      					
      					if($(this).attr('aria-level') == 1){
      						
      						var obj = {type:'',	name:''	};     						
      						obj.type = $(this).find('>a').text();
      						
      						$(this).find('li').each(function(){
      							if($(this).attr('aria-selected') == 'true'){
      								obj.name += $(this).find('a').text()+',';
      							}
      						})
      						ary.push(obj)
      					}	
      				})
      				console.log(ary)
   				}).on('loaded.jstree', function(e, data){  
				    
				    $(e.currentTarget).find('li').each(function(){
				    	if($(this).attr('aria-selected') == 'true'){
				    		$(this).find('>a').addClass('jstree-checked')
				    	}
				    })  
				});
            });
        </script>
    </body>
</html>