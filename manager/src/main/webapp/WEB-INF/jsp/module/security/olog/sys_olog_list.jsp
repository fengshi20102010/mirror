<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title>魔镜助手 - 操作日志表列表</title>
        <!--[if lt IE 9]>
            <meta http-equiv="refresh" content="0;ie.html" />
        <![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
    </head>
    
    <body class="gray-bg">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>操作日志列表</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <a href="javascript:void(0);">选项1</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">选项2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row row-lg">
                    <!-- 查询条件 -->
                    <div class="well">
                        <h3>搜索</h3>
                        <form id="searchForm" class="form-inline">
                            <div class="form-group">
                                <div class="input-group">
                                	<span class="input-group-addon">模块</span>
                                    <input class="form-control" name="module" id="module" type="text" maxlength="50" value=" ${ param.module}">
                                </div>
                                <div class="input-group">
                                	<span class="input-group-addon">模块名称</span>
                                    <input class="form-control" name="moduleName" id="moduleName" type="text" maxlength="50" value=" ${ param.moduleName}">
                                </div>
                                <div class="input-group">
                                	<span class="input-group-addon">操作人</span>
                                    <input class="form-control" name="operateUser" id="operateUser" type="text" maxlength="50" value=" ${ param.operateUser}">
                                </div>
                                <div class="input-group">
                                	<span class="input-group-addon">操作结果</span>
                                    <select name="operateResult" id="operateResult" class="form-control">
                                        <option value="">所有</option>
                                        <option value="1" <c:if test=" ${ param.operateResult eq '1'}"> selected="selected"</c:if>>成功</option>
                                        <option value="2" <c:if test=" ${ param.operateResult eq '2'}"> selected="selected"</c:if>>失败</option>
                                    </select>
                                </div>
                                <div class="input-group">
                                	<span class="input-group-addon">平台</span>
                                    <select name="tag" id="tag" class="form-control">
                                        <option value="">所有</option>
                                        <option value="api" <c:if test=" ${ param.tag eq 'api'}"> selected="selected"</c:if>>api</option>
                                        <option value="manage" <c:if test=" ${ param.tag eq 'manage'}"> selected="selected"</c:if>>manage</option>
                                    </select>
                                </div>
                                <div class="input-group">
                                	<span class="input-group-addon">操作时间</span>
                                    <script type="text/javascript">
                                        require(["jquery", "daterangepicker"], function($) {
                                            var elm = $("#operateTime");
                                            $(elm).daterangepicker({
                                                startDate: $(elm).prev().prev().val(),
                                                endDate: $(elm).prev().val(),
                                                format: "YYYY-MM-DD",
                                                showDropdowns: true
                                            }, function(start, end) {
                                                $(elm).find(".date-title").html(start.format("YYYY-MM-DD") + " 至 " + end.format("YYYY-MM-DD"));
                                                $(elm).prev().prev().val(start.format("YYYY-MM-DD"));
                                                $(elm).prev().val(end.format("YYYY-MM-DD"));
                                            });
                                        });
                                    </script>
                                    <input name="beginOperateTime" type="hidden" value="<fmt:formatDate value="${beginOperateTime}" pattern="yyyy-MM-dd" />">
                                    <input name="endOperateTime" type="hidden" value="<fmt:formatDate value="${endOperateTime}" pattern="yyyy-MM-dd" />">
                                    <button class="btn btn-default daterange" id="operateTime" type="button" data-original-title="" title="" style="margin-bottom:0"><span class="date-title"><fmt:formatDate value="${beginOperateTime}" pattern="yyyy-MM-dd" /> 至 <fmt:formatDate value="${endOperateTime}" pattern="yyyy-MM-dd" /></span> <i class="fa fa-calendar"></i></button>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" id="pageSize" name="pageSize">
                                    <input type="hidden" id="pageNumber" name="pageNumber">
                                    <input type="button" class="btn btn-primary" id="search" value="搜索" style="margin-bottom:0">
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>
                    <div class="col-sm-12">
                        <table id="bootstrap-table" data-mobile-responsive="true">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>id</th>
                                    <th>模块</th>
                                    <th>模块名称</th>
                                    <th>操作</th>
                                    <th>操作名称</th>
                                    <th>执行时间</th>
                                    <th>操作时间</th>
                                    <th>操作人</th>
                                    <th>操作结果</th>
                                    <th>平台标识</th>
                                    <th>备注</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script>
            require(['bootstrap-table.zh-CN', 'layer', 'moment', 'contabs.min', 'content.min', 'main.min'], function($, layer, moment) {
                layer.config({
                    path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
                });
                
                $('#bootstrap-table').bootstrapTable({
                    contentType: "application/x-www-form-urlencoded",
                    method: 'post',
                    toolbar: '#toolbar',
                    iconSize: "outline",
                    striped: true,
                    cache: false,
                    pagination: true,
                    sortable: false,
                    pageNumber: 1,
                    pageSize: 10,
                    pageList: [10, 25, 50, 100],
                    url: window.location.href,
                    queryParamsType: '',
                    queryParams: function(params){
                        $('#pageNumber').val(params.pageNumber);
                        $('#pageSize').val(params.pageSize);
                        return $('#searchForm').serialize();
                    },
                    sidePagination: "server", 
                    strictSearch: true,
                    showColumns: true, 
                    showRefresh: true, 
                    minimumCountColumns: 2, 
                    searchOnEnterKey: true,
                    pagination: true,
                    columns: [
                        {
                            checkbox: true,
                            align: 'center'
                        }, 
                        {
                            field: 'id',
                            align: 'center'
                        }, 
                        {
                            field: 'module',
                            align: 'center'
                        }, 
                        {
                            field: 'moduleName',
                            align: 'center'
                        }, 
                        {
                            field: 'action',
                            align: 'center'
                        }, 
                        {
                            field: 'actionName',
                            align: 'center'
                        }, 
                        {
                            field: 'executeMilliseconds',
                            align: 'center'
                        }, 
                        {
                            field: 'operateTime',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	return moment(value).format('YYYY-MM-DD HH:mm:ss');
                            }
                        }, 
                        {
                            field: 'operateUser',
                            align: 'center'
                        }, 
                        {
                            field: 'operateResult',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	var content = '<span class="badge badge-primary">成功</span>';
                            	if(value == 2) content = '<span class="badge badge-danger">失败</span>';
                            	return content;
                            }
                        }, 
                        {
                            field: 'tag',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	var content = '<span class="badge badge-success">' + value + '</span>';
                            	return content;
                            }
                        }, 
                        {
                            field: 'description',
                            align: 'center'
                        }, 
                        {
                            field: 'id',
                            align: 'center',
                            formatter: function(value, row, index){
                                var content = '';
                                <security:hasPermission name="sys:olog:view">
                                    content += '<a title="详情" href="javascript:;" class="ml-5 view" style="text-decoration:none"><i class="fa fa-eye"></i></a>';
                                </security:hasPermission>
                                if('' == content) content = '无权限';
                                return content;
                            }
                        }
                    ]
                });

                // 搜索
                $('#search').on('click', function() {
                    $('#bootstrap-table').bootstrapTable('refresh');
                });
                
                // 查看
                $('#bootstrap-table').delegate('.view', 'click', function() {
                    var index = $(this).parents('tr').data('index');
                    var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
                    var title = '查看操作日志';
                    var url = 'view.html?id=' + id;
                    layerShow(title, url);
                });
            });
        </script>
    </body>
</html>