<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title>魔镜助手 - 系统角色列表</title>
        <!--[if lt IE 9]>
            <meta http-equiv="refresh" content="0;ie.html" />
        <![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
    </head>
    
    <body class="gray-bg">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>系统角色列表</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <a href="javascript:void(0);">选项1</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">选项2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row row-lg">
                    <!-- 查询条件 -->
                    <div class="well">
                        <h3>搜索</h3>
                        <form id="searchForm" class="form-inline">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">角色名称</span>
                                    <input class="form-control" name="roleName" id="roleName" type="text" maxlength="50" value="${param.roleName}">
                                </div>
                                <div class="form-group">
                                    <input type="hidden" id="pageSize" name="pageSize">
                                    <input type="hidden" id="pageNumber" name="pageNumber">
                                    <input type="button" class="btn btn-primary" id="search" value="搜索" style="margin-bottom:0">
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>
                    <div class="col-sm-12">
                        <div class="btn-group hidden-xs" id="toolbar" role="group">
                            <button type="button" class="btn btn-outline btn-default" id="add">
                                <i class="glyphicon glyphicon-plus" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="btn btn-outline btn-default">
                                <i class="glyphicon glyphicon-heart" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="btn btn-outline btn-default">
                                <i class="glyphicon glyphicon-trash" aria-hidden="true"></i>
                            </button>
                        </div>
                        <table id="bootstrap-table" data-mobile-responsive="true">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>id</th>
                                    <th>角色名称</th>
                                    <th>角色描述</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script>
            require(['bootstrap-table.zh-CN', 'layer', 'moment', 'contabs.min', 'content.min', 'main.min'], function($, layer, moment) {
                layer.config({
                    path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
                });
                
                $('#bootstrap-table').bootstrapTable({
                    contentType: "application/x-www-form-urlencoded",
                    method: 'post',
                    toolbar: '#toolbar',
                    iconSize: "outline",
                    icons: {
                        refresh: "glyphicon-repeat",
                        toggle: "glyphicon-list-alt",
                        columns: "glyphicon-list"
                    },
                    striped: true,
                    cache: false,
                    pagination: true,
                    sortable: false,
                    pageNumber: 1,
                    pageSize: 10,
                    pageList: [10, 25, 50, 100],
                    url: window.location.href,
                    queryParamsType: '',
                    queryParams: function(params){
                        $('#pageNumber').val(params.pageNumber);
                        $('#pageSize').val(params.pageSize);
                        return $('#searchForm').serialize();
                    },
                    sidePagination: "server", 
                    strictSearch: true,
                    showColumns: true, 
                    showRefresh: true, 
                    minimumCountColumns: 2, 
                    searchOnEnterKey: true,
                    pagination: true,
                    columns: [
                        {
                            checkbox: true,
                            align: 'center'
                        }, 
                        {
                            field: 'id',
                            align: 'center'
                        }, 
                        {
                            field: 'roleName',
                            align: 'center'
                        }, 
                        {
                            field: 'description',
                            align: 'center'
                        }, 
                        {
                            field: 'id',
                            align: 'center',
                            formatter: function(value, row, index){
                                var content = '';
                                <security:hasPermission name="sys:role:view">
                                    content += '<a title="详情" href="javascript:;" class="ml-5 view" style="text-decoration:none"><i class="fa fa-eye"></i></a>';
                                </security:hasPermission>
                                <security:hasPermission name="sys:role:update">
                                    content += '<a title="编辑" href="javascript:;" class="ml-5 update" style="text-decoration:none" ><i class="fa fa-edit"></i></a>';
                                </security:hasPermission>
                                <security:hasPermission name="sys:role:del">
                                    content += '<a title="删除" href="javascript:;" class="ml-5 del" style="text-decoration:none"><i class="fa fa-close"></i></a>';
                                </security:hasPermission>
                                if('' == content) content = '无权限';
                                return content;
                            }
                        }
                    ]
                });

                // 搜索
                $('#search').on('click', function() {
                    $('#bootstrap-table').bootstrapTable('refresh');
                });
                
                // 新增
                $('#add').on('click', function() {
                    var title = '新增系统_角色表';
                    var url = 'add.html';
                    layerShow(title, url, "800", "500");
                });

                // 编辑
                $('#bootstrap-table').delegate('.update', 'click', function() {
                    var index = $(this).parents('tr').data('index');
                    var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
                    var title = '编辑系统_角色表';
                    var url = 'update.html?id=' + id;
                    layerShow(title, url, 800, 500);
                });

                // 查看
                $('#bootstrap-table').delegate('.view', 'click', function() {
                    var index = $(this).parents('tr').data('index');
                    var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
                    var title = '查看系统_角色表';
                    var url = 'view.html?id=' + id;
                    layerShow(title, url);
                });

                // 删除
                $('#bootstrap-table').delegate('.del', 'click', function() {
                    $this = $(this);
                    var index = $(this).parents('tr').data('index');
                    var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
                    layer.confirm('确认删除吗?\n 删除后的数据将不可恢复！', function(i) {
                        $.post('del.html', {"id": id}, function(d) {
                            if(d.success) {
                                layer.msg('删除成功！', {icon: 1, time: 1000});
                                setTimeout($('#bootstrap-table').bootstrapTable('refresh'), 1000);
                                return;
                            }
                            layer.msg(d.message, {icon: 5, time: 1000});
                        }, "json");
                    });
                });
            });
        </script>
    </body>
</html>