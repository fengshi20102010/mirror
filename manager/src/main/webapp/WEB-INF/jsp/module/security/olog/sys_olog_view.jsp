<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title>魔镜助手 - 操作日志详情</title>
        <!--[if lt IE 9]>
            <meta http-equiv="refresh" content="0;ie.html" />
        <![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="col-sm-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-info-circle"></i> 详情
                        </div>
                        <div class="panel-body">
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">模块</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.module}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">模块名称</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.moduleName}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">操作</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.action}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">操作名称</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.actionName}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">执行时间</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                            ${item.executeMilliseconds}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">操作时间</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <fmt:formatDate value="${item.operateTime}" pattern="yyyy-MM-dd HH:mm:ss" />
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">操作人</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.operateUser}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">操作人id</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                            ${item.operateUserId}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">操作结果</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <c:choose>
                                            <c:when test="${item.operateResult == 1}">
                                                <span class="badge badge-primary">成功</span>
                                            </c:when>
                                            <c:otherwise>
                                             	<span class="badge badge-danger">失败</span>
                                            </c:otherwise>
                                        </c:choose>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">消息</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.operateMessage}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">请求参数</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.requestParameters}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">客户端信息</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.clientInfo}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">平台标识</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <span class="badge badge-success">${item.tag}</span>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">备注</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.description}
                                    </p>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
        <script>
            require(['jquery', 'layer', 'contabs.min', 'content.min', 'main.min'], function($, layer) {
                //初始化layer
                layer.config({
                    path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
                });
            });
        </script>
    </body>
</html>