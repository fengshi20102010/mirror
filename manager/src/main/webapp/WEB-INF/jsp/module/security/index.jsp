<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Cache-Control" content="no-siteapp" />
		<title>魔镜助手 - 主页</title>
		<!--[if lt IE 9]>
	    	<meta http-equiv="refresh" content="0;ie.html" />
	    <![endif]-->
		<link rel="shortcut icon" href="favicon.ico">
		<link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	    <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
	    <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
	</head>
	<body class="fixed-sidebar full-height-layout gray-bg" style="overflow:hidden">
		<div id="wrapper">
			<!-- 菜单  -->
			<%@ include file="/WEB-INF/jsp/common/menu.jsp"%>
			<div id="page-wrapper" class="gray-bg dashbard-1">
				<div class="row border-bottom">
					<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
						<div class="navbar-header"><a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
							<form role="search" class="navbar-form-custom" method="post" action="http://www.zi-han.net/theme/hplus/search_results.html">
								<div class="form-group">
									<input type="text" placeholder="请输入您需要查找的内容 …" class="form-control" name="top-search" id="top-search">
								</div>
							</form>
						</div>
						<ul class="nav navbar-top-links navbar-right">
							<li class="dropdown">
								<a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
									<i class="fa fa-envelope"></i> <span class="label label-warning">16</span>
								</a>
								<ul class="dropdown-menu dropdown-messages">
									<li class="m-t-xs">
										<div class="dropdown-messages-box">
											<a href="profile.html" class="pull-left">
												<img alt="image" class="img-circle" src="${pageContext.request.contextPath }/resource/images/a7.jpg">
											</a>
											<div class="media-body">
												<small class="pull-right">46小时前</small>
												<strong>小四</strong> 这个在日本投降书上签字的军官，建国后一定是个不小的干部吧？
												<br>
												<small class="text-muted">3天前 2014.11.8</small>
											</div>
										</div>
									</li>
									<li class="divider"></li>
									<li>
										<div class="dropdown-messages-box">
											<a href="profile.html" class="pull-left">
												<img alt="image" class="img-circle" src="${pageContext.request.contextPath }/resource/images/a4.jpg">
											</a>
											<div class="media-body ">
												<small class="pull-right text-navy">25小时前</small>
												<strong>国民岳父</strong> 如何看待“男子不满自己爱犬被称为狗，刺伤路人”？——这人比犬还凶
												<br>
												<small class="text-muted">昨天</small>
											</div>
										</div>
									</li>
									<li class="divider"></li>
									<li>
										<div class="text-center link-block">
											<a class="J_menuItem" href="mailbox.html">
												<i class="fa fa-envelope"></i> <strong> 查看所有消息</strong>
											</a>
										</div>
									</li>
								</ul>
							</li>
							<li class="dropdown">
								<a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
									<i class="fa fa-bell"></i> <span class="label label-primary">8</span>
								</a>
								<ul class="dropdown-menu dropdown-alerts">
									<li>
										<a href="mailbox.html">
											<div>
												<i class="fa fa-envelope fa-fw"></i> 您有16条未读消息
												<span class="pull-right text-muted small">4分钟前</span>
											</div>
										</a>
									</li>
									<li class="divider"></li>
									<li>
										<a href="profile.html">
											<div>
												<i class="fa fa-qq fa-fw"></i> 3条新回复
												<span class="pull-right text-muted small">12分钟钱</span>
											</div>
										</a>
									</li>
									<li class="divider"></li>
									<li>
										<div class="text-center link-block">
											<a class="J_menuItem" href="notifications.html">
												<strong>查看所有 </strong>
												<i class="fa fa-angle-right"></i>
											</a>
										</div>
									</li>
								</ul>
							</li>
							<li class="dropdown hidden-xs">
								<a class="right-sidebar-toggle" aria-expanded="false">
									<i class="fa fa-tasks"></i> 主题
								</a>
							</li>
						</ul>
					</nav>
				</div>
				<!-- 内容显示区域 -->
				<div class="row content-tabs">
					<button class="roll-nav roll-left J_tabLeft">
                	<i class="fa fa-backward"></i>
                </button>
					<nav class="page-tabs J_menuTabs">
						<div class="page-tabs-content">
							<a href="javascript:;" class="active J_menuTab" data-id="index_v1.html">首页</a>
						</div>
					</nav>
					<button class="roll-nav roll-right J_tabRight">
                	<i class="fa fa-forward"></i>
                </button>
					<div class="btn-group roll-nav roll-right">
						<button class="dropdown J_tabClose" data-toggle="dropdown">关闭操作<span class="caret"></span>
                    </button>
						<ul role="menu" class="dropdown-menu dropdown-menu-right">
							<li class="J_tabShowActive">
								<a>定位当前选项卡</a>
							</li>
							<li class="divider"></li>
							<li class="J_tabCloseAll">
								<a>关闭全部选项卡</a>
							</li>
							<li class="J_tabCloseOther">
								<a>关闭其他选项卡</a>
							</li>
						</ul>
					</div>
					<a href="javascript:void(0)" class="roll-nav roll-right J_tabExit logout"><i class="fa fa fa-sign-out"></i> 退出</a>
				</div>
				<div class="row J_mainContent" id="content-main">
					<iframe class="J_iframe" name="iframe0" width="100%" height="100%" src="welcome.html" frameborder="0" data-id="index_v1.html" seamless></iframe>
				</div>
				<!-- 页脚  -->
				<div class="footer">
				    <div class="pull-right">
				    	本后台系统由<a href="#" target="_blank" title="魔镜助手">魔镜助手</a>提供技术支持</p>
				    </div>
				    <div>
					    <strong>Copyright</strong> fs Company © 2016-2018
					</div>
				</div>
				<!-- 页脚  -->
			</div>

			<!--右侧边栏-->
			<%@ include file="/WEB-INF/jsp/common/notify.jsp"%>

		</div>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
		<script type="application/javascript">
			require(['bootstrap', 'contabs.min', 'content.min', 'main.min'], function($) {
				$('#demo').on('click', function(){
					var title = '我的详情';
					var url = 'personal.html';
					layerShow(title,url,"300","500");
				});
				
				// 退出登陆
				$('.logout').on('click', function(){
					layer.confirm('确认退出登陆吗？', {icon: 3, title:'退出登陆'}, function(index){
						layer.close(index);
						window.location.href = '${pageContext.request.contextPath }/logout.html';
					});
				});
			});
		</script>
	</body>

</html>