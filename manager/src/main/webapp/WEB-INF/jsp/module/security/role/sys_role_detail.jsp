<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title>魔镜助手 - 系统角色新增/修改</title>
        <!--[if lt IE 9]>
            <meta http-equiv="refresh" content="0;ie.html" />
        <![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
    </head>
    <body>
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="form-horizontal m-t" id="form1">
                    <c:if test="${not empty item}">
                        <input type="hidden" name="id" value="${item.id}" />
                    </c:if>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">角色名称：</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="roleName" name="roleName" placeholder="请输入角色名称"  value="${item.roleName}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">角色描述：</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="description" name="description" placeholder="请输入角色描述"  value="${item.description}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                            </div>
                    </div>
	                <div class="row cl">
						<label class="form-label col-xs-4 col-sm-3">网站角色：</label>
						<div class="formControls col-xs-8 col-sm-9">
							<c:forEach items="${resc }" varStatus="i" var="ress">
								<dl class="permission-list">
									<dt>
										<label>
											<input type="checkbox" value="" name="user-Character-${i.index }" id="user-Character-${i.index }">${ress.title }
										</label>
									</dt>
									<dd>
										<dl class="cl permission-list2">
											<dt>
												<label class="">
													<input type="checkbox" value="" name="user-Character-1-0" id="user-Character-1-0">${ress.title }
												</label>
											</dt>
											<dd>
												<c:forEach items="${ress.subRes }" varStatus="j" var="subres">
													<label class="">
														<input type="checkbox" value="" ${subres.selected ? 'checked':''} name="user-Character-1-0-0" id="user-Character-1-0-0">${subres.title }
													</label>
												</c:forEach>
											</dd>
										</dl>
									</dd>
								</dl>
							</c:forEach>
						</div>
					</div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-3">
                            <button class="btn btn-primary" type="submit">提交</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        <script>
            require(['jquery', 'layer', 'jquery.validate.methods', 'jquery.icheck', 'contabs.min', 'content.min', 'main.min'], function($, layer) {
                //初始化layer
                layer.config({
                    path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
                });
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                    increaseArea: '20%'
                });
                $(".permission-list dt input:checkbox").click(function(){
					$(this).closest("dl").find("dd input:checkbox").prop("checked",$(this).prop("checked"));
				});
				$(".permission-list2 dd input:checkbox").click(function(){
					var l =$(this).parent().parent().find("input:checked").length;
					var l2=$(this).parents(".permission-list").find(".permission-list2 dd").find("input:checked").length;
					if($(this).prop("checked")){
						$(this).closest("dl").find("dt input:checkbox").prop("checked",true);
						$(this).parents(".permission-list").find("dt").first().find("input:checkbox").prop("checked",true);
					}
					else{
						if(l==0){
							$(this).closest("dl").find("dt input:checkbox").prop("checked",false);
						}
						if(l2==0){
							$(this).parents(".permission-list").find("dt").first().find("input:checkbox").prop("checked",false);
						}
					}
				});
                var err = "<i class='fa fa-times-circle'></i> ";
                $("#form1").validate({
                    rules: {
                            roleName : {
                                required: true
                            }
                    },
                    messages: {
                        roleName : {
                            required: err + ' '
                        }
                    },
                    highlight: function(element) {
                        $(element).closest(".form-group").removeClass("has-success").addClass("has-error")
                    },
                    success: function(element) {
                        element.closest(".form-group").removeClass("has-error").addClass("has-success")
                    },
                    errorElement: "span",
                    errorPlacement: function(element, r) {
                        element.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
                    },
                    errorClass: "help-block m-b-none",
                    validClass: "help-block m-b-none",
                    onkeyup:false,
                    submitHandler:function(form){
                        $.ajax({
                            url:window.location.href,
                            dataType:'json',
                            type:'post',
                            data: $('#form1').serialize(),
                            success:function(data){
                                if(data.success){
                                    var index = parent.layer.getFrameIndex(window.name);
                                    parent.$('#search').click();
                                    parent.layer.close(index);
                                    return;
                                }else{
                                    layer.msg(data.message,{icon:2,time:1000});
                                }
                            }
                        })
                    }
                });
            });
        </script>
    </body>
</html>