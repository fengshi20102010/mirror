<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="renderer" content="webkit|ie-comp|ie-stand">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
		<meta http-equiv="Cache-Control" content="no-siteapp" />
		<LINK rel="Bookmark" href="/favicon.ico">
		<LINK rel="Shortcut Icon" href="/favicon.ico" />
		<!--[if lt IE 9]>
			<script type="text/javascript" src="${pageContext.request.contextPath }/resource/components/html5.js"></script>
			<script type="text/javascript" src="${pageContext.request.contextPath }/resource/components/respond.min.js"></script>
			<script type="text/javascript" src="${pageContext.request.contextPath }/resource/components/PIE_IE678.js"></script>
		<![endif]-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/H-ui.min.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/H-ui.admin.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/components/Hui-iconfont/1.0.7/iconfont.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/style.css" />
		<!--[if IE 6]>
			<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
			<script>DD_belatedPNG.fix('*');</script>
		<![endif]-->
		<title>个人信息</title>
	</head>
	<body>
		<div class="cl pd-20" style=" background-color:#5bacb6">
			<img class="avatar size-XL l" src="${pageContext.request.contextPath }/resource/images/user.png">
				<dl style="margin-left:80px; color:#fff">
		    		<dt><span class="f-18">${empty item.nickname ? item.username:item.nickname }</span> <span class="pl-10 f-12">${item.role.roleName }</span></dt>
		    		<dd class="pt-10 f-12" style="margin-left:0">${empty item.description ? '这家伙很懒，什么也没有留下':item.description }</dd>
		  		</dl>
		</div>
		<div class="pd-20">
			<table class="table">
		    	<tbody>
		      		<tr>
		        		<th class="text-r">用户名：</th>
		        		<td>${item.username }</td>
		      		</tr>
		      		<tr>
				        <th class="text-r">昵称：</th>
				        <td>${item.nickname }</td>
		      		</tr>
		      		<tr>
				        <th class="text-r">邮箱：</th>
				        <td>${item.email }</td>
		      		</tr>
		      		<tr>
				        <th class="text-r">状态：</th>
				        <td>${item.status eq 0 ? '停用':item.status eq 1 ? '启用':'正常' }</td>
		      		</tr>
		      		<tr>
				        <th class="text-r">登陆ip：</th>
				        <td>${item.lastLoginIp }</td>
		      		</tr>
		      		<tr>
				        <th class="text-r">登陆时间：</th>
				        <td><fmt:formatDate value="${item.lastLoginTime}" pattern="yyyy-MM-dd hh:mm:ss" /></td>
		      		</tr>
		      		<tr>
				        <th class="text-r">创建时间：</th>
				        <td><fmt:formatDate value="${item.createTime}" pattern="yyyy-MM-dd hh:mm:ss" /></td>
		      		</tr>
		      		<tr>
				        <th class="text-r">解锁时间：</th>
				        <td><fmt:formatDate value="${item.unlockTime}" pattern="yyyy-MM-dd hh:mm:ss" /></td>
		      		</tr>
		      		<tr>
				        <th class="text-r">描述：</th>
				        <td>${item.description }</td>
		      		</tr>
		    	</tbody>
		  	</table>
		</div>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/app/config.js"></script>
		<script type="text/javascript">
			require(['jquery', 'hui.admin'],function($){
			
			})
		</script>
	</body>
</html>