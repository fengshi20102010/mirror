<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title>魔镜助手 - 商品信息列表</title>
        <!--[if lt IE 9]>
            <meta http-equiv="refresh" content="0;ie.html" />
        <![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
    </head>
    
    <body class="gray-bg">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>商品信息列表</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <a href="javascript:void(0);">选项1</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">选项2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row row-lg">
                    <!-- 查询条件 -->
                    <div class="well">
                        <h3>搜索</h3>
                        <form id="searchForm" class="form-inline">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">商品ID</span>
                                    <input class="form-control" name="numIid" id="numIid" type="text" value="${param.numIid}">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">推广计划</span>
                                    <select name="type" id="type" class="form-control">
                                        <option value="">所有</option>
                                        <option value="0" <c:if test="${param.type eq '0'}"> selected="selected"</c:if>>无</option>
                                        <option value="1" <c:if test="${param.type eq '1'}"> selected="selected"</c:if>>通用</option>
                                        <option value="2" <c:if test="${param.type eq '2'}"> selected="selected"</c:if>>定向</option>
                                        <option value="3" <c:if test="${param.type eq '3'}"> selected="selected"</c:if>>鹊桥</option>
                                    </select>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">名称</span>
                                    <input class="form-control" name="name" id="name" type="text" maxlength="50" value="${param.name}">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">类型</span>
                                    <select name="typeId" id="typeId" class="form-control">
                                        <option value="">所有</option>
                                        <c:forEach items="${types }" var="type" varStatus="i">
	                                        <option value="${type.id }" <c:if test="${typeId eq type.id}"> selected="selected"</c:if>>${type.name }</option>
                                        </c:forEach>
                                    </select>
                                </div>                                <div class="input-group">
                                    <span class="input-group-addon">直推</span>
                                    <select name="isExtend" id="isExtend" class="form-control">
                                        <option value="">所有</option>
                                        <option value="0" <c:if test="${param.isExtend eq '0'}"> selected="selected"</c:if>>否</option>
                                        <option value="1" <c:if test="${param.isExtend eq '1'}"> selected="selected"</c:if>>是</option>
                                    </select>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">优惠劵</span>
                                    <select name="isCoupon" id="isCoupon" class="form-control">
                                        <option value="">所有</option>
                                        <option value="0" <c:if test="${param.isCoupon eq '0'}"> selected="selected"</c:if>>没有</option>
                                        <option value="1" <c:if test="${param.isCoupon eq '1'}"> selected="selected"</c:if>>有</option>
                                    </select>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">今日推荐</span>
                                    <select name="recommend" id="recommend" class="form-control">
                                        <option value="">所有</option>
                                        <option value="0" <c:if test="${param.recommend eq '0'}"> selected="selected"</c:if>>否</option>
                                        <option value="1" <c:if test="${param.recommend eq '1'}"> selected="selected"</c:if>>是</option>
                                    </select>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">属性</span>
                                    <select name="attribute" id="attribute" class="form-control">
                                        <option value="">所有</option>
                                        <option value="0" <c:if test="${param.attribute eq '0'}"> selected="selected"</c:if>>普通</option>
                                        <option value="1" <c:if test="${param.attribute eq '1'}"> selected="selected"</c:if>>优质</option>
                                        <option value="2" <c:if test="${param.attribute eq '2'}"> selected="selected"</c:if>>秒杀</option>
                                        <option value="3" <c:if test="${param.attribute eq '3'}"> selected="selected"</c:if>>精选直播</option>
                                        <option value="4" <c:if test="${param.attribute eq '4'}"> selected="selected"</c:if>>客户端首页推广</option>
                                        <option value="5" <c:if test="${param.attribute eq '5'}"> selected="selected"</c:if>>预告</option>
                                    </select>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">状态</span>
                                    <select name="status" id="status" class="form-control">
                                        <option value="">所有</option>
                                        <option value="0" <c:if test="${param.status eq '0'}"> selected="selected"</c:if>>冻结</option>
                                        <option value="1" <c:if test="${param.status eq '1'}"> selected="selected"</c:if>>待完善</option>
                                        <option value="2" <c:if test="${param.status eq '2'}"> selected="selected"</c:if>>待审核</option>
                                        <option value="3" <c:if test="${param.status eq '3'}"> selected="selected"</c:if>>发布中</option>
                                        <option value="4" <c:if test="${param.status eq '4'}"> selected="selected"</c:if>>待结算</option>
                                        <option value="5" <c:if test="${param.status eq '5'}"> selected="selected"</c:if>>已下架</option>
                                    </select>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">来源</span>
                                    <select name="source" id="source" class="form-control">
                                        <option value="">所有</option>
                                        <option value="1" <c:if test="${param.source eq '1'}"> selected="selected"</c:if>>天猫</option>
                                        <option value="2" <c:if test="${param.source eq '2'}"> selected="selected"</c:if>>淘宝</option>
                                    </select>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">QQ</span>
                                    <input class="form-control" name="qq" id="qq" type="text" maxlength="50" value="${param.qq}">
                                </div>
                                <div class="form-group">
                                    <input type="hidden" id="pageSize" name="pageSize">
                                    <input type="hidden" id="pageNumber" name="pageNumber">
                                    <input type="button" class="btn btn-primary" id="search" value="搜索" style="margin-bottom:0">
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>
                    <div class="col-sm-12">
                        <div class="btn-group hidden-xs" id="toolbar" role="group">
                            <button type="button" class="btn btn-outline btn-default" id="add">
                                <i class="glyphicon glyphicon-plus" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="btn btn-outline btn-default">
                                <i class="glyphicon glyphicon-heart" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="btn btn-outline btn-default">
                                <i class="glyphicon glyphicon-trash" aria-hidden="true"></i>
                            </button>
                        </div>
                        <table id="bootstrap-table" data-mobile-responsive="true">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>ID</th>
                                    <th>淘宝ID</th>
                                    <th>类型</th>
                                    <th>推广</th>
                                    <th>名称</th>
                                    <th>链接</th>
                                    <th>直推</th>
                                    <th>优惠劵</th>
                                    <th>推荐</th>
                                    <th>属性</th>
                                    <th>状态</th>
                                    <th>佣金</th>
                                    <th>主图</th>
                                    <th>原价</th>
                                    <th>折扣价</th>
                                    <th>销量</th>
                                    <th>计划链接</th>
                                    <th>来源</th>
                                    <th>QQ</th>
                                    <th>预告时间</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script>
            require(['bootstrap-table.zh-CN', 'layer', 'moment', 'contabs.min', 'content.min', 'main.min'], function($, layer, moment) {
                layer.config({
                    path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
                });
                
                $('#bootstrap-table').bootstrapTable({
                    contentType: "application/x-www-form-urlencoded",
                    method: 'post',
                    toolbar: '#toolbar',
                    iconSize: "outline",
                    icons: {
                        refresh: "glyphicon-repeat",
                        toggle: "glyphicon-list-alt",
                        columns: "glyphicon-list"
                    },
                    striped: true,
                    cache: false,
                    pagination: true,
                    sortable: false,
                    pageNumber: 1,
                    pageSize: 10,
                    pageList: [10, 25, 50, 100],
                    url: window.location.href,
                    queryParamsType: '',
                    queryParams: function(params){
                        $('#pageNumber').val(params.pageNumber);
                        $('#pageSize').val(params.pageSize);
                        return $('#searchForm').serialize();
                    },
                    sidePagination: "server", 
                    strictSearch: true,
                    showColumns: true, 
                    showRefresh: true, 
                    minimumCountColumns: 2, 
                    searchOnEnterKey: true,
                    pagination: true,
                    columns: [
                        {
                            checkbox: true,
                            align: 'center'
                        }, 
                        {
                            field: 'id',
                            align: 'center'
                        }, 
                        {
                            field: 'numIid',
                            align: 'center'
                        }, 
                        {
                            field: 'typeId',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	var content = '<span class="label label-danger">未分类</span>';
                    	        <c:forEach items="${types }" var="type" varStatus="i">
	                            	if(value == ${type.id }){
	                                    content = '<span class="${type.status eq 1 ? "label label-info":"label label-warning" }">${type.name }</span>';
	                            	}
                                </c:forEach>
                            	return content;
                            }
                        }, 
                        {
                            field: 'type',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	var content = '-';
                            	if(value == 0){
                                    content = '<span class="label label-info">无</span>';
                            	}
                            	if(value == 1){
                                    content = '<span class="label label-info">通用</span>';
                            	}
                            	if(value == 2){
                                    content = '<span class="label label-info">定向</span>';
                            	}
                            	if(value == 3){
                                    content = '<span class="label label-info">鹊桥</span>';
                            	}
                            	return content;
                            }
                        }, 
                        {
                            field: 'name',
                            align: 'center'
                        }, 
                        {
                            field: 'url',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	return '<a href="' + value + '" target="_blank">点击查看</a>';
                            }
                        }, 
                        {
                            field: 'isExtend',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	var content = '-';
                            	if(value == 0){
                                    content = '<span class="label label-info">否</span>';
                            	}
                            	if(value == 1){
                                    content = '<span class="label label-info">是</span>';
                            	}
                            	return content;
                            }
                        }, 
                        {
                            field: 'isCoupon',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	var content = '-';
                            	if(value == 0){
                                    content = '<span class="label label-info">没有</span>';
                            	}
                            	if(value == 1){
                                    content = '<span class="label label-info">有</span>';
                            	}
                            	return content;
                            }
                        }, 
                        {
                            field: 'recommend',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	var content = '-';
                            	if(value == 0){
                                    content = '<span class="label label-info">否</span>';
                            	}
                            	if(value == 1){
                                    content = '<span class="label label-info">是</span>';
                            	}
                            	return content;
                            }
                        }, 
                        {
                            field: 'attribute',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	var content = '-';
                            	if(value == 0){
                                    content = '<span class="label label-info">普通</span>';
                            	}
                            	if(value == 1){
                                    content = '<span class="label label-info">优质</span>';
                            	}
                            	if(value == 2){
                                    content = '<span class="label label-info">秒杀</span>';
                            	}
                            	if(value == 3){
                                    content = '<span class="label label-info">精选直播</span>';
                            	}
                            	if(value == 4){
                                    content = '<span class="label label-info">客户端首页推广</span>';
                            	}
                            	if(value == 5){
                                    content = '<span class="label label-info">预告</span>';
                            	}
                            	return content;
                            }
                        }, 
                        {
                            field: 'status',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	var content = '-';
                            	if(value == 0){
                                    content = '<span class="label label-info">冻结</span>';
                            	}
                            	if(value == 1){
                                    content = '<span class="label label-info">待完善</span>';
                            	}
                            	if(value == 2){
                                    content = '<span class="label label-info">待审核</span>';
                            	}
                            	if(value == 3){
                                    content = '<span class="label label-info">发布中</span>';
                            	}
                            	if(value == 4){
                                    content = '<span class="label label-info">待结算</span>';
                            	}
                            	if(value == 5){
                                    content = '<span class="label label-info">已下架</span>';
                            	}
                            	return content;
                            }
                        }, 
                        {
                            field: 'commission',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	return value / 100;
                            }
                        }, 
                        {
                            field: 'picUrl',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	return '<a href="' + value + '" target="_blank">点击查看</a>';
                            }
                        }, 
                        {
                            field: 'price',
                            align: 'center'
                        }, 
                        {
                            field: 'finalPrice',
                            align: 'center'
                        }, 
                        {
                            field: 'sales',
                            align: 'center'
                        }, 
                        {
                            field: 'planUrl',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	return '<a href="' + value + '" target="_blank">点击查看</a>';
                            }
                        }, 
                        {
                            field: 'source',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	var content = '-';
                            	if(value == 1){
                                    content = '<span class="label label-info">天猫</span>';
                            	}
                            	if(value == 2){
                                    content = '<span class="label label-info">淘宝</span>';
                            	}
                            	return content;
                            }
                        }, 
                        {
                            field: 'qq',
                            align: 'center'
                        }, 
                        {
                            field: 'adTime',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	if(null == value) return '不适用';
                            	return moment(value).format('YYYY-MM-DD');
                            }
                        }, 
                        {
                            field: 'id',
                            align: 'center',
                            formatter: function(value, row, index){
                                var content = '';
                                <security:hasPermission name="goods:goods:view">
                                    content += '<a title="详情" href="javascript:;" class="ml-5 view" style="text-decoration:none"><i class="fa fa-eye"></i></a>';
                                </security:hasPermission>
                                <security:hasPermission name="goods:goods:update">
                                    content += '<a title="编辑" href="javascript:;" class="ml-5 update" style="text-decoration:none" ><i class="fa fa-edit"></i></a>';
                                </security:hasPermission>
                                <security:hasPermission name="goods:goods:del">
                                    content += '<a title="删除" href="javascript:;" class="ml-5 del" style="text-decoration:none"><i class="fa fa-close"></i></a>';
                                </security:hasPermission>
                                <security:hasPermission name="goods:goods:status">
                                    if(row.status == 2) content += '<a title="通过" href="javascript:;" class="ml-5 status" style="text-decoration:none"><i class="fa fa-check"></i></a>';
                                </security:hasPermission>
                                if('' == content) content = '无权限';
                                return content;
                            }
                        }
                    ]
                });

                // 搜索
                $('#search').on('click', function() {
                    $('#bootstrap-table').bootstrapTable('refresh');
                });
                
                // 新增
                $('#add').on('click', function() {
                    var title = '新增商品信息';
                    var url = 'add.html';
                    layerShow(title, url, "800", "500");
                });

                // 编辑
                $('#bootstrap-table').delegate('.update', 'click', function() {
                    var index = $(this).parents('tr').data('index');
                    var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
                    var title = '编辑商品信息';
                    var url = 'update.html?id=' + id;
                    layerShow(title, url, 800, 500);
                });

                // 查看
                $('#bootstrap-table').delegate('.view', 'click', function() {
                    var index = $(this).parents('tr').data('index');
                    var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
                    var title = '查看商品信息';
                    var url = 'view.html?id=' + id;
                    layerShow(title, url);
                });

                // 删除
                $('#bootstrap-table').delegate('.del', 'click', function() {
                    $this = $(this);
                    var index = $(this).parents('tr').data('index');
                    var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
                    layer.confirm('确认删除吗?\n 删除后的数据将不可恢复！', function(i) {
                        $.post('del.html', {"id": id}, function(d) {
                            if(d.success) {
                                layer.msg('删除成功！', {icon: 1, time: 1000});
                                setTimeout($('#bootstrap-table').bootstrapTable('refresh'), 1000);
                                return;
                            }
                            layer.msg(d.message, {icon: 5, time: 1000});
                        }, "json");
                    });
                });
                
                // 审核
                $('#bootstrap-table').delegate('.status', 'click', function() {
                    $this = $(this);
                    var index = $(this).parents('tr').data('index');
                    var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
                    layer.confirm('确认通过审核？', {btn:['通过', '驳回', '取消'], title: '审核'},
                    function(i){
                    	console.log('通过');
                    	$.post('status.html', {"id": id, "status":3}, function(d) {
                            if(d.success) {
                                layer.msg('审核成功！', {icon: 1, time: 1000});
                                setTimeout($('#bootstrap-table').bootstrapTable('refresh'), 1000);
                                return;
                            }
                            layer.msg(d.message, {icon: 5, time: 1000});
                        }, "json");
                    }, function(i){
                    	console.log('驳回');
                    	$.post('status.html', {"id": id, "status":1}, function(d) {
                            if(d.success) {
                                layer.msg('驳回成功！', {icon: 1, time: 1000});
                                setTimeout($('#bootstrap-table').bootstrapTable('refresh'), 1000);
                                return;
                            }
                            layer.msg(d.message, {icon: 5, time: 1000});
                        }, "json");
                    })
                });
            });
        </script>
    </body>
</html>