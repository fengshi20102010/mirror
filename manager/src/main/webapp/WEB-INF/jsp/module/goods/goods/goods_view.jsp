<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title>魔镜助手 - 商品信息主表详情</title>
        <!--[if lt IE 9]>
            <meta http-equiv="refresh" content="0;ie.html" />
        <![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="col-sm-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-info-circle"></i> 详情
                        </div>
                        <div class="panel-body">
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">对应的店铺信息ID</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                            ${item.shopId}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">淘宝对应的商品ID</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                            ${item.numIid}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">商品类型</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                   		<c:if test="${not empty types }">
	                                    	<c:forEach items="${types }" var="type" varStatus="i">
	                                            <c:if test="${item.typeId == type.id}">
	                                                <span class="${type.status eq 1 ? "label label-info":"label label-warning" }">${type.name }</span>
	                                            </c:if>
	                                        </c:forEach>
                                   		</c:if>
                                    	<c:if test="${empty item.typeId }">
                                    		<span class="label label-danger">未分类</span>
                                    	</c:if>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">推广计划</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <c:choose>
                                            <c:when test="${item.type == 0}">
                                                <span class="label label-info">无</span>
                                            </c:when>
                                            <c:when test="${item.type == 1}">
                                                <span class="label label-info">通用</span>
                                            </c:when>
                                            <c:when test="${item.type == 2}">
                                                <span class="label label-info">定向</span>
                                            </c:when>
                                            <c:when test="${item.type == 3}">
                                                <span class="label label-info">鹊桥</span>
                                            </c:when>
                                        </c:choose>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">商品名称</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.name}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">商品简介</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.detail}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">商品链接地址</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.url}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">是否直推</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <c:choose>
                                            <c:when test="${item.isExtend == 0}">
                                                <span class="label label-info">否</span>
                                            </c:when>
                                            <c:when test="${item.isExtend == 1}">
                                                <span class="label label-info">是</span>
                                            </c:when>
                                        </c:choose>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">是否有优惠劵</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <c:choose>
                                            <c:when test="${item.isCoupon == 0}">
                                                <span class="label label-info">没有</span>
                                            </c:when>
                                            <c:when test="${item.isCoupon == 1}">
                                                <span class="label label-info">有</span>
                                            </c:when>
                                        </c:choose>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">是否今日推荐</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <c:choose>
                                            <c:when test="${item.recommend == 0}">
                                                <span class="label label-info">否</span>
                                            </c:when>
                                            <c:when test="${item.recommend == 1}">
                                                <span class="label label-info">是</span>
                                            </c:when>
                                        </c:choose>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">商品属性</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <c:choose>
                                            <c:when test="${item.attribute == 0}">
                                                <span class="label label-info">普通</span>
                                            </c:when>
                                            <c:when test="${item.attribute == 1}">
                                                <span class="label label-info">优质</span>
                                            </c:when>
                                            <c:when test="${item.attribute == 2}">
                                                <span class="label label-info">秒杀</span>
                                            </c:when>
                                            <c:when test="${item.attribute == 3}">
                                                <span class="label label-info">精选直播</span>
                                            </c:when>
                                            <c:when test="${item.attribute == 4}">
                                                <span class="label label-info">客户端首页推广</span>
                                            </c:when>
                                            <c:when test="${item.attribute == 5}">
                                                <span class="label label-info">预告</span>
                                            </c:when>
                                        </c:choose>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">商品状态</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <c:choose>
                                            <c:when test="${item.status == 0}">
                                                <span class="label label-info">冻结</span>
                                            </c:when>
                                            <c:when test="${item.status == 1}">
                                                <span class="label label-info">待完善</span>
                                            </c:when>
                                            <c:when test="${item.status == 2}">
                                                <span class="label label-info">待审核</span>
                                            </c:when>
                                            <c:when test="${item.status == 3}">
                                                <span class="label label-info">发布中</span>
                                            </c:when>
                                            <c:when test="${item.status == 4}">
                                                <span class="label label-info">待结算</span>
                                            </c:when>
                                            <c:when test="${item.status == 5}">
                                                <span class="label label-info">已下架</span>
                                            </c:when>
                                        </c:choose>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">佣金比例</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                            ${item.commission}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">商品图片</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.picUrl}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">商品价格</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.price}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">折扣价格</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.finalPrice}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">商品销量</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                            ${item.sales}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">创建时间</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <fmt:formatDate value="${item.createTime}" pattern="yyyy-MM-dd" />
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">修改时间</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <fmt:formatDate value="${item.updateTime}" pattern="yyyy-MM-dd" />
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">计划链接</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.planUrl}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">商品来源</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <c:choose>
                                            <c:when test="${item.source == 1}">
                                                <span class="label label-info">天猫</span>
                                            </c:when>
                                            <c:when test="${item.source == 2}">
                                                <span class="label label-info">淘宝</span>
                                            </c:when>
                                        </c:choose>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">发单人QQ</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.qq}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">副标题</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.subTitle}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">预告时间</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <fmt:formatDate value="${item.adTime}" pattern="yyyy-MM-dd" />
                                    </p>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
        <script>
            require(['jquery', 'layer', 'contabs.min', 'content.min', 'main.min'], function($, layer) {
                //初始化layer
                layer.config({
                    path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
                });
            });
        </script>
    </body>
</html>