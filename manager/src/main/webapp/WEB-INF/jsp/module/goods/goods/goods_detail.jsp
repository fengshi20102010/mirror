<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title>魔镜助手 - 商品信息新增/修改</title>
        <!--[if lt IE 9]>
            <meta http-equiv="refresh" content="0;ie.html" />
        <![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
    </head>
    <body>
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="form-horizontal m-t" id="form1">
                    <c:if test="${not empty item}">
                        <input type="hidden" name="id" value="${item.id}" />
                    </c:if>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">店铺ID：</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="shopId" id="shopId" type="text" value="${item.shopId}">
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">商品ID：</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="numIid" id="numIid" type="text" value="${item.numIid}">
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">类型：</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="typeId" id="typeId" type="text" value="${item.typeId}">
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">类型：</label>
                        <div class="col-sm-4">
                            <select class="form-control m-b" name="typeId" value="${item.typeId}" size="1">
                                <c:forEach items="${types }" var="type">
		                            <option value="${type.id }" ${type.id eq item.typeId ? 'selected':''}>${type.name }</option>
		                        </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">计划：</label>
                            <div class="radio i-checks">
                                <label>
                                    <input type="radio" name="type" id="type0" value="0"> <i></i> 无
                                </label>
                                <label>
                                    <input type="radio" name="type" id="type1" value="1"> <i></i> 通用
                                </label>
                                <label>
                                    <input type="radio" name="type" id="type2" value="2"> <i></i> 定向
                                </label>
                                <label>
                                    <input type="radio" name="type" id="type3" value="3"> <i></i> 鹊桥
                                </label>
                                <script type="text/javascript">
                                    document.getElementById("type${empty item ? 0 : item.type}").checked=true;
                                </script>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">名称：</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="name" name="name" placeholder="请输入商品名称"  value="${item.name}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">商品简介：</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="detail" name="detail" placeholder="请输入商品简介"  value="${item.detail}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">链接地址：</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="url" name="url" placeholder="请输入商品链接地址"  value="${item.url}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">直推：</label>
                            <div class="radio i-checks">
                                <label>
                                    <input type="radio" name="isExtend" id="isExtend0" value="0"> <i></i> 否
                                </label>
                                <label>
                                    <input type="radio" name="isExtend" id="isExtend1" value="1"> <i></i> 是
                                </label>
                                <script type="text/javascript">
                                    document.getElementById("isExtend${empty item ? 0 : item.isExtend}").checked=true;
                                </script>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">优惠劵：</label>
                            <div class="radio i-checks">
                                <label>
                                    <input type="radio" name="isCoupon" id="isCoupon0" value="0"> <i></i> 没有
                                </label>
                                <label>
                                    <input type="radio" name="isCoupon" id="isCoupon1" value="1"> <i></i> 有
                                </label>
                                <script type="text/javascript">
                                    document.getElementById("isCoupon${empty item ? 0 : item.isCoupon}").checked=true;
                                </script>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">今日推荐：</label>
                            <div class="radio i-checks">
                                <label>
                                    <input type="radio" name="recommend" id="recommend0" value="0"> <i></i> 否
                                </label>
                                <label>
                                    <input type="radio" name="recommend" id="recommend1" value="1"> <i></i> 是
                                </label>
                                <script type="text/javascript">
                                    document.getElementById("recommend${empty item ? 0 : item.recommend}").checked=true;
                                </script>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">属性：</label>
                            <div class="radio i-checks">
                                <label>
                                    <input type="radio" name="attribute" id="attribute0" value="0"> <i></i> 普通
                                </label>
                                <label>
                                    <input type="radio" name="attribute" id="attribute1" value="1"> <i></i> 优质
                                </label>
                                <label>
                                    <input type="radio" name="attribute" id="attribute2" value="2"> <i></i> 秒杀
                                </label>
                                <label>
                                    <input type="radio" name="attribute" id="attribute3" value="3"> <i></i> 精选直播
                                </label>
                                <label>
                                    <input type="radio" name="attribute" id="attribute4" value="4"> <i></i> 客户端首页推广
                                </label>
                                <label>
                                    <input type="radio" name="attribute" id="attribute5" value="5"> <i></i> 预告
                                </label>
                                <script type="text/javascript">
                                    document.getElementById("attribute${empty item ? 0 : item.attribute}").checked=true;
                                </script>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">状态：</label>
                            <div class="radio i-checks">
                                <label>
                                    <input type="radio" name="status" id="status0" value="0"> <i></i> 冻结
                                </label>
                                <label>
                                    <input type="radio" name="status" id="status1" value="1"> <i></i> 待完善
                                </label>
                                <label>
                                    <input type="radio" name="status" id="status2" value="2"> <i></i> 待审核
                                </label>
                                <label>
                                    <input type="radio" name="status" id="status3" value="3"> <i></i> 发布中
                                </label>
                                <label>
                                    <input type="radio" name="status" id="status4" value="4"> <i></i> 待结算
                                </label>
                                <label>
                                    <input type="radio" name="status" id="status5" value="5"> <i></i> 已下架
                                </label>
                                <script type="text/javascript">
                                    document.getElementById("status${empty item ? 0 : item.status}").checked=true;
                                </script>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">佣金比例：</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="commission" id="commission" type="text" value="${item.commission}">
                                <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 佣金比例,1234代表12.34%</span>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">图片：</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="picUrl" name="picUrl" placeholder="请输入商品图片"  value="${item.picUrl}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">价格：</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="price" name="price" placeholder="请输入商品价格"  value="${item.price}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">折扣价：</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="finalPrice" name="finalPrice" placeholder="请输入折扣价格"  value="${item.finalPrice}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">销量：</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="sales" id="sales" type="text" value="${item.sales}">
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">计划链接：</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="planUrl" name="planUrl" placeholder="请输入计划链接"  value="${item.planUrl}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">商品来源：</label>
                            <div class="radio i-checks">
                                <label>
                                    <input type="radio" name="source" id="source1" value="1"> <i></i> 天猫
                                </label>
                                <label>
                                    <input type="radio" name="source" id="source2" value="2"> <i></i> 淘宝
                                </label>
                                <script type="text/javascript">
                                    document.getElementById("source${empty item ? 1 : item.source}").checked=true;
                                </script>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">发单人QQ：</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="qq" name="qq" placeholder="请输入发单人QQ"  value="${item.qq}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">副标题：</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="subTitle" name="subTitle" placeholder="请输入副标题"  value="${item.subTitle}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">预告时间：</label>
                            <div class="col-sm-4">
                                <input class="form-control" type="text" name="adTime" id="adTime" readonly="readonly" value="<fmt:formatDate value="${item.adTime}" pattern="yyyy-MM-dd" />">
                            </div>
                            <script type="text/javascript">
                                require(['datetimepicker.zh-CN'], function($){
                                    var elm = $("#adTime");
                                    $(elm).datetimepicker({
                                        language: 'zh-CN',
                                        format: "yyyy-mm-dd",
                                        minView: "month",
                                        autoclose: true,
                                        showDropdowns: true,
                                        singleDatePicker:true
                                    });
                                });
                            </script>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-3">
                            <button class="btn btn-primary" type="submit">提交</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        <script>
            require(['jquery', 'layer', 'jquery.validate.methods', 'jquery.icheck', 'contabs.min', 'content.min', 'main.min'], function($, layer) {
                //初始化layer
                layer.config({
                    path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
                });
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                    increaseArea: '20%'
                });
                var err = "<i class='fa fa-times-circle'></i> ";
                $("#form1").validate({
                    rules: {
                            shopId : {
                                required: true,
                                number:true
                            },
                            numIid : {
                                required: true,
                                number:true
                            },
                            type : {
                                required: true,
                                number:true
                            },
                            name : {
                                required: true
                            },
                            detail : {
                                required: true
                            },
                            url : {
                                required: true
                            },
                            isExtend : {
                                required: true,
                                number:true
                            },
                            isCoupon : {
                                required: true,
                                number:true
                            },
                            recommend : {
                                required: true,
                                number:true
                            },
                            attribute : {
                                required: true,
                                number:true
                            },
                            status : {
                                required: true,
                                number:true
                            },
                            commission : {
                                required: true,
                                number:true
                            },
                            picUrl : {
                                required: true
                            },
                            price : {
                                required: true
                            },
                            finalPrice : {
                                required: true
                            },
                            sales : {
                                required: true,
                                number:true
                            },
                            createTime : {
                                required: true,
                                date:true
                            },
                            updateTime : {
                                required: true,
                                date:true
                            },
                            isIndex : {
                                required: true,
                                number:true
                            },
                            planUrl : {
                                required: true
                            },
                            source : {
                                required: true,
                                number:true
                            },
                            qq : {
                                required: true
                            },
                            subTitle : {
                                required: true
                            },
                            adTime : {
                                required: true,
                                date:true
                            }
                    },
                    messages: {
                        shopId : {
                            required: err + ' ',
                            number:err + ' '
                        },
                        numIid : {
                            required: err + ' ',
                            number:err + ' '
                        },
                        typeId : {
                            required: err + ' ',
                            number:err + ' '
                        },
                        type : {
                            required: err + ' ',
                            number:err + ' '
                        },
                        name : {
                            required: err + ' '
                        },
                        detail : {
                            required: err + ' '
                        },
                        url : {
                            required: err + ' '
                        },
                        isExtend : {
                            required: err + ' ',
                            number:err + ' '
                        },
                        isCoupon : {
                            required: err + ' ',
                            number:err + ' '
                        },
                        recommend : {
                            required: err + ' ',
                            number:err + ' '
                        },
                        attribute : {
                            required: err + ' ',
                            number:err + ' '
                        },
                        status : {
                            required: err + ' ',
                            number:err + ' '
                        },
                        commission : {
                            required: err + ' ',
                            number:err + ' '
                        },
                        picUrl : {
                            required: err + ' '
                        },
                        price : {
                            required: err + ' '
                        },
                        finalPrice : {
                            required: err + ' '
                        },
                        sales : {
                            required: err + ' ',
                            number:err + ' '
                        },
                        createTime : {
                            required: err + ' ',
                            date:err + ' '
                        },
                        updateTime : {
                            required: err + ' ',
                            date:err + ' '
                        },
                        isIndex : {
                            required: err + ' ',
                            number:err + ' '
                        },
                        planUrl : {
                            required: err + ' '
                        },
                        source : {
                            required: err + ' ',
                            number:err + ' '
                        },
                        qq : {
                            required: err + ' '
                        },
                        subTitle : {
                            required: err + ' '
                        },
                        adTime : {
                            required: err + ' ',
                            date:err + ' '
                        }
                    },
                    highlight: function(element) {
                        $(element).closest(".form-group").removeClass("has-success").addClass("has-error")
                    },
                    success: function(element) {
                        element.closest(".form-group").removeClass("has-error").addClass("has-success")
                    },
                    errorElement: "span",
                    errorPlacement: function(element, r) {
                        element.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
                    },
                    errorClass: "help-block m-b-none",
                    validClass: "help-block m-b-none",
                    onkeyup:false,
                    submitHandler:function(form){
                        $.ajax({
                            url:window.location.href,
                            dataType:'json',
                            type:'post',
                            data: $('#form1').serialize(),
                            success:function(data){
                                if(data.success){
                                    var index = parent.layer.getFrameIndex(window.name);
                                    parent.$('#search').click();
                                    parent.layer.close(index);
                                    return;
                                }else{
                                    layer.msg(data.message,{icon:2,time:1000});
                                }
                            }
                        })
                    }
                });
            });
        </script>
    </body>
</html>