<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title>魔镜助手 - 公告/信息/教程相关数据表详情</title>
        <!--[if lt IE 9]>
            <meta http-equiv="refresh" content="0;ie.html" />
        <![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="col-sm-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-info-circle"></i> 详情
                        </div>
                        <div class="panel-body">
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">公告名称</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.name}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">公告内容</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.content}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">公告状态</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <c:choose>
                                            <c:when test="${item.status == 0}">
                                                <span class="label label-info">禁用</span>
                                            </c:when>
                                            <c:when test="${item.status == 1}">
                                                <span class="label label-info">启用</span>
                                            </c:when>
                                        </c:choose>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">公告类型</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <c:choose>
                                            <c:when test="${item.type == 1}">
                                                <span class="label label-info">普通公告</span>
                                            </c:when>
                                            <c:when test="${item.type == 2}">
                                                <span class="label label-info">推荐公告</span>
                                            </c:when>
                                            <c:when test="${item.type == 3}">
                                                <span class="label label-info">教程</span>
                                            </c:when>
                                        </c:choose>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">发布者</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.author}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">阅读次数</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                            ${item.readNo}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">创建时间</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <fmt:formatDate value="${item.createTime}" pattern="yyyy-MM-dd" />
                                    </p>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
        <script>
            require(['jquery', 'layer', 'contabs.min', 'content.min', 'main.min'], function($, layer) {
                //初始化layer
                layer.config({
                    path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
                });
            });
        </script>
    </body>
</html>