<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title>魔镜助手 - 广告设置新增/修改</title>
        <!--[if lt IE 9]>
            <meta http-equiv="refresh" content="0;ie.html" />
        <![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
    </head>
    <body>
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="form-horizontal m-t" id="form1">
                    <c:if test="${not empty item}">
                        <input type="hidden" name="id" value="${item.id}" />
                    </c:if>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">标题：</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="title" name="title" placeholder="请输入标题"  value="${item.title}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">图片地址：</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="imageUrl" name="imageUrl" placeholder="请输入图片地址"  value="${item.imageUrl}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                                <script type="text/javascript">
	                                require(['webuploader'], function($){
	                                    var elm = $("#createTime");
	                                    $(elm).datetimepicker({
	                                        language: 'zh-CN',
	                                        format: "yyyy-mm-dd",
	                                        minView: "month",
	                                        autoclose: true,
	                                        showDropdowns: true,
	                                        singleDatePicker:true
	                                    });
	                                });
                                </script>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">跳转连接：</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="url" name="url" placeholder="请输入跳转连接"  value="${item.url}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">类型：</label>
                            <div class="radio i-checks">
                                <label>
                                    <input type="radio" name="type" id="type1" value="1"> <i></i> 首页
                                </label>
                                <label>
                                    <input type="radio" name="type" id="type2" value="2"> <i></i> 热销
                                </label>
                                <label>
                                    <input type="radio" name="type" id="type3" value="3"> <i></i> 预告
                                </label>
                                <script type="text/javascript">
                                    document.getElementById("type${empty item ? 1 : item.type}").checked=true;
                                </script>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">状态：</label>
                            <div class="radio i-checks">
                                <label>
                                    <input type="radio" name="status" id="status0" value="0"> <i></i> 停用
                                </label>
                                <label>
                                    <input type="radio" name="status" id="status1" value="1"> <i></i> 启用
                                </label>
                                <script type="text/javascript">
                                    document.getElementById("status${empty item ? 1 : item.status}").checked=true;
                                </script>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">排序：</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="sortNo" id="sortNo" type="text" value="${item.sortNo}">
                            </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-3">
                            <button class="btn btn-primary" type="submit">提交</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        <script>
            require(['jquery', 'layer', 'jquery.validate.methods', 'jquery.icheck', 'contabs.min', 'content.min', 'main.min'], function($, layer) {
                //初始化layer
                layer.config({
                    path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
                });
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                    increaseArea: '20%'
                });
                var err = "<i class='fa fa-times-circle'></i> ";
                $("#form1").validate({
                    rules: {
                            title : {
                                required: true
                            },
                            imageUrl : {
                                required: true
                            },
                            url : {
                                required: true
                            },
                            type : {
                                required: true,
                                number:true
                            },
                            status : {
                                required: true,
                                number:true
                            },
                            sortNo : {
                                required: true,
                                number:true
                            },
                    },
                    messages: {
                        title : {
                            required: err + ' '
                        },
                        imageUrl : {
                            required: err + ' '
                        },
                        url : {
                            required: err + ' '
                        },
                        type : {
                            required: err + ' ',
                            number:err + ' '
                        },
                        status : {
                            required: err + ' ',
                            number:err + ' '
                        },
                        sortNo : {
                            required: err + ' ',
                            number:err + ' '
                        },
                    },
                    highlight: function(element) {
                        $(element).closest(".form-group").removeClass("has-success").addClass("has-error")
                    },
                    success: function(element) {
                        element.closest(".form-group").removeClass("has-error").addClass("has-success")
                    },
                    errorElement: "span",
                    errorPlacement: function(element, r) {
                        element.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
                    },
                    errorClass: "help-block m-b-none",
                    validClass: "help-block m-b-none",
                    onkeyup:false,
                    submitHandler:function(form){
                        $.ajax({
                            url:window.location.href,
                            dataType:'json',
                            type:'post',
                            data: $('#form1').serialize(),
                            success:function(data){
                                if(data.success){
                                    var index = parent.layer.getFrameIndex(window.name);
                                    parent.$('#search').click();
                                    parent.layer.close(index);
                                    return;
                                }else{
                                    layer.msg(data.message,{icon:2,time:1000});
                                }
                            }
                        })
                    }
                });
            });
        </script>
    </body>
</html>