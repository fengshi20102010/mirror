<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title>魔镜助手 - 常见问题新增/修改</title>
        <!--[if lt IE 9]>
            <meta http-equiv="refresh" content="0;ie.html" />
        <![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
    </head>
    <body>
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="form-horizontal m-t" id="form1">
                    <c:if test="${not empty item}">
                        <input type="hidden" name="id" value="${item.id}" />
                    </c:if>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">标题：</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="title" name="title" placeholder="请输入标题"  value="${item.title}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">问题：</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="question" name="question" placeholder="请输入问题"  value="${item.question}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">答案：</label>
                            <div class="col-sm-8">
<!--                                 <input type="text" class="form-control" id="answer" name="answer" placeholder="请输入答案"  value="${item.answer}"> -->
                                <div id="demo"></div>
                            </div>
                            <script type="text/javascript">
                                require(['summernote.zh-CN'], function($){
                                	$("#demo").summernote({
	                                	lang:"zh-CN",  
								        callbacks: {
								            onImageUpload: function(files, editor, $editable) {
								                sendFile(files);
								            }
								        }
							        });
                            
                            		function sendFile(files, editor, $editable) {
                            			var data = new FormData();
                            			data.append("ajaxTaskFile", files[0]);
                            			$.ajax({
                            				data : data,
                            				type : "POST",
                            				url : "${pageContext.request.contextPath }/global/upload.html", //图片上传出来的url，返回的是图片上传后的路径，http格式  
                            				cache : false,
                            				contentType : false,
                            				processData : false,
                            				dataType : "json",
                            				success : function(data) { //data是返回的hash,key之类的值，key是定义的文件名  
                            					$('#summernote').summernote('insertImage', data.data);
                            				},
                            				error : function() {
                            					alert("上传失败");
                            				}
                            			});
                            		}
                            
                            	});
                            </script>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">状态：</label>
                            <div class="radio i-checks">
                                <label>
                                    <input type="radio" name="status" id="status0" value="0"> <i></i> 禁用
                                </label>
                                <label>
                                    <input type="radio" name="status" id="status1" value="1"> <i></i> 启用
                                </label>
                                <script type="text/javascript">
                                    document.getElementById("status${empty item ? 0 : item.status}").checked=true;
                                </script>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">创建时间：</label>
                            <div class="col-sm-4">
                                <input class="form-control" type="text" name="createTime" id="createTime" readonly="readonly" value="<fmt:formatDate value="${item.createTime}" pattern="yyyy-MM-dd" />">
                            </div>
                            <script type="text/javascript">
                                require(['datetimepicker.zh-CN'], function($){
                                    var elm = $("#createTime");
                                    $(elm).datetimepicker({
                                        language: 'zh-CN',
                                        format: "yyyy-mm-dd",
                                        minView: "month",
                                        autoclose: true,
                                        showDropdowns: true,
                                        singleDatePicker:true
                                    });
                                });
                            </script>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-3">
                            <button class="btn btn-primary" type="submit">提交</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        <script>
            require(['jquery', 'layer', 'jquery.validate.methods', 'jquery.icheck', 'contabs.min', 'content.min', 'main.min'], function($, layer) {
                //初始化layer
                layer.config({
                    path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
                });
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                    increaseArea: '20%'
                });
                var err = "<i class='fa fa-times-circle'></i> ";
                $("#form1").validate({
                    rules: {
                            title : {
                                required: true
                            },
                            question : {
                                required: true
                            },
                            answer : {
                                required: true
                            },
                            status : {
                                required: true,
                                number:true
                            },
                            createTime : {
                                required: true,
                                date:true
                            }
                    },
                    messages: {
                        title : {
                            required: err + ' '
                        },
                        question : {
                            required: err + ' '
                        },
                        answer : {
                            required: err + ' '
                        },
                        status : {
                            required: err + ' ',
                            number:err + ' '
                        },
                        createTime : {
                            required: err + ' ',
                            date:err + ' '
                        }
                    },
                    highlight: function(element) {
                        $(element).closest(".form-group").removeClass("has-success").addClass("has-error")
                    },
                    success: function(element) {
                        element.closest(".form-group").removeClass("has-error").addClass("has-success")
                    },
                    errorElement: "span",
                    errorPlacement: function(element, r) {
                        element.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
                    },
                    errorClass: "help-block m-b-none",
                    validClass: "help-block m-b-none",
                    onkeyup:false,
                    submitHandler:function(form){
                        $.ajax({
                            url:window.location.href,
                            dataType:'json',
                            type:'post',
                            data: $('#form1').serialize(),
                            success:function(data){
                                if(data.success){
                                    var index = parent.layer.getFrameIndex(window.name);
                                    parent.$('#search').click();
                                    parent.layer.close(index);
                                    return;
                                }else{
                                    layer.msg(data.message,{icon:2,time:1000});
                                }
                            }
                        })
                    }
                });
            });
        </script>
    </body>
</html>