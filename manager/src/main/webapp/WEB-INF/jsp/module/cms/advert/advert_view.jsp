<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title>魔镜助手 - 广告设置详情</title>
        <!--[if lt IE 9]>
            <meta http-equiv="refresh" content="0;ie.html" />
        <![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="col-sm-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-info-circle"></i> 详情
                        </div>
                        <div class="panel-body">
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">标题</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.title}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">图片地址</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.imageUrl}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">跳转连接</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.url}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">类型</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <c:choose>
                                            <c:when test="${item.type == 1}">
                                                <span class="label label-info">首页</span>
                                            </c:when>
                                            <c:when test="${item.type == 2}">
                                                <span class="label label-info">热销</span>
                                            </c:when>
                                            <c:when test="${item.type == 3}">
                                                <span class="label label-info">预告</span>
                                            </c:when>
                                        </c:choose>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">状态</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <c:choose>
                                            <c:when test="${item.status == 0}">
                                                <span class="label label-info">停用</span>
                                            </c:when>
                                            <c:when test="${item.status == 1}">
                                                <span class="label label-info">启用</span>
                                            </c:when>
                                        </c:choose>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">排序</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                            ${item.sortNo}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">创建时间</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <fmt:formatDate value="${item.createTime}" pattern="yyyy-MM-dd" />
                                    </p>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
        <script>
            require(['jquery', 'layer', 'contabs.min', 'content.min', 'main.min'], function($, layer) {
                //初始化layer
                layer.config({
                    path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
                });
            });
        </script>
    </body>
</html>