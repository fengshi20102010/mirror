<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title>魔镜助手 - 用户信息详情</title>
        <!--[if lt IE 9]>
            <meta http-equiv="refresh" content="0;ie.html" />
        <![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="col-sm-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-info-circle"></i> 详情
                        </div>
                        <div class="panel-body">
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">用户名</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.username}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">电话</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.phone}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">邮箱</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.email}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">性别</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <c:choose>
                                            <c:when test="${item.sex == 0}">
                                                <span class="label label-info">未知</span>
                                            </c:when>
                                            <c:when test="${item.sex == 1}">
                                                <span class="label label-info">男</span>
                                            </c:when>
                                            <c:when test="${item.sex == 2}">
                                                <span class="label label-info">女</span>
                                            </c:when>
                                            <c:otherwise>
                                            	<span class="label label-info">无数据</span>
                                            </c:otherwise>
                                        </c:choose>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">年龄</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                            ${item.age}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">余额</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                            ${item.score}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">QQ</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.qq}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">微信</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.wechat}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">用户状态</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <c:choose>
                                            <c:when test="${item.status == 0}">
                                                <span class="label label-info">冻结</span>
                                            </c:when>
                                            <c:when test="${item.status == 1}">
                                                <span class="label label-info">待完善</span>
                                            </c:when>
                                            <c:when test="${item.status == 2}">
                                                <span class="label label-info">待审核</span>
                                            </c:when>
                                            <c:when test="${item.status == 3}">
                                                <span class="label label-info">已审核</span>
                                            </c:when>
                                            <c:when test="${item.status == 4}">
                                                <span class="label label-info">已驳回</span>
                                            </c:when>
                                            <c:otherwise>
                                            	<span class="label label-info">无数据</span>
                                            </c:otherwise>
                                        </c:choose>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">团队或个人名称</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.teamName}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">团队类型</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <c:choose>
                                            <c:when test="${item.type == 1}">
                                                <span class="label label-info">个人</span>
                                            </c:when>
                                            <c:when test="${item.type == 2}">
                                                <span class="label label-info">团队</span>
                                            </c:when>
                                            <c:otherwise>
                                            	<span class="label label-info">无数据</span>
                                            </c:otherwise>
                                        </c:choose>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">收入</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <c:choose>
                                            <c:when test="${item.income == 1}">
                                                <span class="label label-info">1000以下</span>
                                            </c:when>
                                            <c:when test="${item.income == 2}">
                                                <span class="label label-info">1000-5000</span>
                                            </c:when>
                                            <c:when test="${item.income == 3}">
                                                <span class="label label-info">5000-1W</span>
                                            </c:when>
                                            <c:when test="${item.income == 4}">
                                                <span class="label label-info">1W-5W</span>
                                            </c:when>
                                            <c:when test="${item.income == 5}">
                                                <span class="label label-info">5W-10W</span>
                                            </c:when>
                                            <c:when test="${item.income == 6}">
                                                <span class="label label-info">5W以上</span>
                                            </c:when>
                                            <c:otherwise>
                                            	<span class="label label-info">无数据</span>
                                            </c:otherwise>
                                        </c:choose>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">渠道</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.channel}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">申请理由</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.reason}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">附件</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <c:if test="${!empty item.attachment}"><a href="${item.attachment}" target="_blank">点击下载附件</a></c:if>
                                        <c:if test="${empty item.attachment}"><span class="label label-info">无数据</span></c:if>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">用户备注</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        ${item.remark}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">用户创建时间</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <fmt:formatDate value="${item.createTime}" pattern="yyyy-MM-dd" />
                                    </p>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-xs-12 col-sm-3 col-md-2 control-label form-control-static">用户修改时间</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">
                                        <fmt:formatDate value="${item.updateTime}" pattern="yyyy-MM-dd" />
                                    </p>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
        <script>
            require(['jquery', 'layer', 'contabs.min', 'content.min', 'main.min'], function($, layer) {
                //初始化layer
                layer.config({
                    path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
                });
            });
        </script>
    </body>
</html>