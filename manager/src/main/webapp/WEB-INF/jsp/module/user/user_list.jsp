<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title>魔镜助手 - 用户信息列表</title>
        <!--[if lt IE 9]>
            <meta http-equiv="refresh" content="0;ie.html" />
        <![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
    </head>
    
    <body class="gray-bg">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>用户信息列表</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <a href="javascript:void(0);">选项1</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">选项2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row row-lg">
                    <!-- 查询条件 -->
                    <div class="well">
                        <h3>搜索</h3>
                        <form id="searchForm" class="form-inline">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">用户名</span>
                                    <input class="form-control" name="username" id="username" type="text" maxlength="50" value="${param.username}">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">电话</span>
                                    <input class="form-control" name="phone" id="phone" type="text" maxlength="50" value="${param.phone}">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">性别</span>
                                    <select name="sex" id="sex" class="form-control">
                                        <option value="">所有</option>
                                        <option value="0" <c:if test="${param.sex eq '0'}"> selected="selected"</c:if>>未知</option>
                                        <option value="1" <c:if test="${param.sex eq '1'}"> selected="selected"</c:if>>男</option>
                                        <option value="2" <c:if test="${param.sex eq '2'}"> selected="selected"</c:if>>女</option>
                                    </select>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">用户状态</span>
                                    <select name="status" id="status" class="form-control">
                                        <option value="">所有</option>
                                        <option value="0" <c:if test="${param.status eq '0'}"> selected="selected"</c:if>>冻结</option>
                                        <option value="1" <c:if test="${param.status eq '1'}"> selected="selected"</c:if>>待完善</option>
                                        <option value="2" <c:if test="${param.status eq '2'}"> selected="selected"</c:if>>待审核</option>
                                        <option value="3" <c:if test="${param.status eq '3'}"> selected="selected"</c:if>>已审核</option>
                                        <option value="4" <c:if test="${param.status eq '4'}"> selected="selected"</c:if>>驳回</option>
                                    </select>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">团队或个人名称</span>
                                    <input class="form-control" name="teamName" id="teamName" type="text" maxlength="50" value="${param.teamName}">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">团队类型</span>
                                    <select name="type" id="type" class="form-control">
                                        <option value="">所有</option>
                                        <option value="1" <c:if test="${param.type eq '1'}"> selected="selected"</c:if>>个人</option>
                                        <option value="2" <c:if test="${param.type eq '2'}"> selected="selected"</c:if>>团队</option>
                                    </select>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">收入</span>
                                    <select name="income" id="income" class="form-control">
                                        <option value="">所有</option>
                                        <option value="1" <c:if test="${param.income eq '1'}"> selected="selected"</c:if>>1000以下</option>
                                        <option value="2" <c:if test="${param.income eq '2'}"> selected="selected"</c:if>>1000-5000</option>
                                        <option value="3" <c:if test="${param.income eq '3'}"> selected="selected"</c:if>>5000-1W</option>
                                        <option value="4" <c:if test="${param.income eq '4'}"> selected="selected"</c:if>>1W-5W</option>
                                        <option value="5" <c:if test="${param.income eq '5'}"> selected="selected"</c:if>>5W-10W</option>
                                        <option value="6" <c:if test="${param.income eq '6'}"> selected="selected"</c:if>>5W以上</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" id="pageSize" name="pageSize">
                                    <input type="hidden" id="pageNumber" name="pageNumber">
                                    <input type="button" class="btn btn-primary" id="search" value="搜索" style="margin-bottom:0">
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>
                    <div class="col-sm-12">
                        <div class="btn-group hidden-xs" id="toolbar" role="group">
                            <button type="button" class="btn btn-outline btn-default" id="add">
                                <i class="glyphicon glyphicon-plus" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="btn btn-outline btn-default">
                                <i class="glyphicon glyphicon-heart" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="btn btn-outline btn-default">
                                <i class="glyphicon glyphicon-trash" aria-hidden="true"></i>
                            </button>
                        </div>
                        <table id="bootstrap-table" data-mobile-responsive="true">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>id</th>
                                    <th>用户名</th>
                                    <th>电话</th>
                                    <th>邮箱</th>
                                    <th>性别</th>
                                    <th>年龄</th>
                                    <th>余额</th>
                                    <th>QQ</th>
                                    <th>微信</th>
                                    <th>用户状态</th>
                                    <th>团队或个人名称</th>
                                    <th>团队类型</th>
                                    <th>收入</th>
                                    <th>附件</th>
                                    <th>用户创建时间</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script>
            require(['bootstrap-table.zh-CN', 'layer', 'moment', 'contabs.min', 'content.min', 'main.min'], function($, layer, moment) {
                layer.config({
                    path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
                });
                
                $('#bootstrap-table').bootstrapTable({
                    contentType: "application/x-www-form-urlencoded",
                    method: 'post',
                    toolbar: '#toolbar',
                    iconSize: "outline",
                    icons: {
                        refresh: "glyphicon-repeat",
                        toggle: "glyphicon-list-alt",
                        columns: "glyphicon-list"
                    },
                    striped: true,
                    cache: false,
                    pagination: true,
                    sortable: false,
                    pageNumber: 1,
                    pageSize: 10,
                    pageList: [10, 25, 50, 100],
                    url: window.location.href,
                    queryParamsType: '',
                    queryParams: function(params){
                        $('#pageNumber').val(params.pageNumber);
                        $('#pageSize').val(params.pageSize);
                        return $('#searchForm').serialize();
                    },
                    sidePagination: "server", 
                    strictSearch: true,
                    showColumns: true, 
                    showRefresh: true, 
                    minimumCountColumns: 2, 
                    searchOnEnterKey: true,
                    pagination: true,
                    columns: [
                        {
                            checkbox: true,
                            align: 'center'
                        }, 
                        {
                            field: 'id',
                            align: 'center'
                        }, 
                        {
                            field: 'username',
                            align: 'center'
                        }, 
                        {
                            field: 'phone',
                            align: 'center'
                        }, 
                        {
                            field: 'email',
                            align: 'center'
                        }, 
                        {
                            field: 'sex',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	var content = '-';
                            	if(value == 0){
                                    content = '<span class="label label-info">未知</span>';
                            	}
                            	if(value == 1){
                                    content = '<span class="label label-info">男</span>';
                            	}
                            	if(value == 2){
                                    content = '<span class="label label-info">女</span>';
                            	}
                            	return content;
                            }
                        }, 
                        {
                            field: 'age',
                            align: 'center'
                        }, 
                        {
                            field: 'score',
                            align: 'center'
                        }, 
                        {
                            field: 'qq',
                            align: 'center'
                        }, 
                        {
                            field: 'wechat',
                            align: 'center'
                        }, 
                        {
                            field: 'status',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	var content = '-';
                            	if(value == 0){
                                    content = '<span class="label label-info">冻结</span>';
                            	}
                            	if(value == 1){
                                    content = '<span class="label label-info">待完善</span>';
                            	}
                            	if(value == 2){
                                    content = '<span class="label label-info">待审核</span>';
                            	}
                            	if(value == 3){
                                    content = '<span class="label label-info">已审核</span>';
                            	}
                            	if(value == 4){
                                    content = '<span class="label label-info">驳回</span>';
                            	}
                            	return content;
                            }
                        }, 
                        {
                            field: 'teamName',
                            align: 'center'
                        }, 
                        {
                            field: 'type',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	var content = '-';
                            	if(value == 1){
                                    content = '<span class="label label-info">个人</span>';
                            	}
                            	if(value == 2){
                                    content = '<span class="label label-info">团队</span>';
                            	}
                            	return content;
                            }
                        }, 
                        {
                            field: 'income',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	var content = '-';
                            	if(value == 1){
                                    content = '<span class="label label-info">1000以下</span>';
                            	}
                            	if(value == 2){
                                    content = '<span class="label label-info">1000-5000</span>';
                            	}
                            	if(value == 3){
                                    content = '<span class="label label-info">5000-1W</span>';
                            	}
                            	if(value == 4){
                                    content = '<span class="label label-info">1W-5W</span>';
                            	}
                            	if(value == 5){
                                    content = '<span class="label label-info">5W-10W</span>';
                            	}
                            	if(value == 6){
                                    content = '<span class="label label-info">5W以上</span>';
                            	}
                            	return content;
                            }
                        }, 
                        {
                            field: 'attachment',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	var content = '-';
                            	if(value){
	                            	content = '<a href="'+ value +'" target="_blank">下载附件</a>';
                            	}
                            	return content;
                            }
                        }, 
                        {
                            field: 'createTime',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	return moment(value).format('YYYY-MM-DD');
                            }
                        }, 
                        {
                            field: 'id',
                            align: 'center',
                            formatter: function(value, row, index){
                                var content = '';
                                <security:hasPermission name="user:view">
                                    content += '<a title="详情" href="javascript:;" class="ml-5 view" style="text-decoration:none"><i class="fa fa-eye"></i></a>';
                                </security:hasPermission>
                                <security:hasPermission name="user:status">
                                	if(row.status == 2) {
	                                    content += '<a title="审核" href="javascript:;" class="ml-5 status" style="text-decoration:none"><i class="fa fa-check"></i></a>';
                                	}
                                </security:hasPermission>
                                if('' == content) content = '无权限';
                                return content;
                            }
                        }
                    ]
                });

                // 搜索
                $('#search').on('click', function() {
                    $('#bootstrap-table').bootstrapTable('refresh');
                });

                // 查看
                $('#bootstrap-table').delegate('.view', 'click', function() {
                    var index = $(this).parents('tr').data('index');
                    var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
                    var title = '查看用户信息主表';
                    var url = 'view.html?id=' + id;
                    layerShow(title, url);
                });

                // 审核
                $('#bootstrap-table').delegate('.status', 'click', function() {
                    $this = $(this);
                    var index = $(this).parents('tr').data('index');
                    var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
                    layer.confirm('确认通过审核？', {btn:['通过', '驳回', '取消'], title: '审核'},
                    function(i){
                    	$.post('status.html', {"id": id, "status":3}, function(d) {
                            if(d.success) {
                                layer.msg('审核成功！', {icon: 1, time: 1000});
                                setTimeout($('#bootstrap-table').bootstrapTable('refresh'), 1000);
                                return;
                            }
                            layer.msg(d.message, {icon: 5, time: 1000});
                        }, "json");
                    }, function(i){
                    	$.post('status.html', {"id": id, "status":4}, function(d) {
                            if(d.success) {
                                layer.msg('驳回成功！', {icon: 1, time: 1000});
                                setTimeout($('#bootstrap-table').bootstrapTable('refresh'), 1000);
                                return;
                            }
                            layer.msg(d.message, {icon: 5, time: 1000});
                        }, "json");
                    })
                });
            });
        </script>
    </body>
</html>