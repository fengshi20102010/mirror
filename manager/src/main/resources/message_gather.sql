/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50713
Source Host           : localhost:3306
Source Database       : tk

Target Server Type    : MYSQL
Target Server Version : 50713
File Encoding         : 65001

Date: 2017-04-05 22:57:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for message_gather
-- ----------------------------
DROP TABLE IF EXISTS `message_gather`;
CREATE TABLE `message_gather` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) DEFAULT NULL,
  `gather_date` datetime DEFAULT NULL COMMENT '采集时间',
  `gather_group` varchar(20) DEFAULT NULL,
  `gather_qq` varchar(30) DEFAULT NULL COMMENT '采集QQ群',
  `sender_qq` varchar(30) DEFAULT NULL COMMENT '发送消息人',
  `send_to` varchar(255) DEFAULT NULL COMMENT '发送给群组详情',
  `groupset_name` varchar(50) DEFAULT NULL COMMENT '群组名称',
  `message_content` varchar(500) DEFAULT NULL COMMENT '发送内容',
  `gather_or_not` varchar(5) DEFAULT NULL COMMENT '是否采集',
  `type` varchar(10) DEFAULT NULL COMMENT '商品类型（天猫/淘宝）',
  `transfer_status` varchar(2) DEFAULT NULL COMMENT '转换状态（0 待转换， 1 转换成功，待发送 2转换失败',
  `send_status` varchar(2) DEFAULT NULL COMMENT '发送状态(0 添加带转换 1 转换待发送  2发送成功  3 发送失败）',
  `send_time` datetime DEFAULT NULL COMMENT '发送时间',
  `send_cycle_time` int(11) DEFAULT NULL,
  `send_num` int(11) DEFAULT NULL COMMENT '发送次数',
  `taobaolog` varchar(1000) DEFAULT NULL COMMENT '淘宝日志（待发送 需要写日志）',
  `weixin_wait_time` tinyint(4) DEFAULT NULL COMMENT '微信发送图片等待几秒发送文字',
  `pic_check` char(1) DEFAULT NULL COMMENT '检测图片是否发送成功',
  `insert_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '插入时间',
  `valiable_tag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '可用标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='采集消息表';
